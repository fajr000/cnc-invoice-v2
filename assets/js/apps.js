
function showAlert(description = "", title = "", type = "info", position = "top-right") {
    // Toast position options
    // toast-top-right
    // toast-bottom-right
    // toast-bottom-left
    // toast-top-left
    // toast-top-full-width
    // toast-bottom-full-width
    // toast-top-center
    // toast-bottom-center

    var toastr_position = "toast-"+position;
    
    toastr.options = {
      "closeButton": false,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": toastr_position,
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "2000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }

    switch(type) {
        case "info" :
            if(title != "") {
                toastr.info(description, title);
            } else {
                toastr.info(description);
            }

            break;
        case "success" :
            if(title != "") {
                toastr.success(description, title);
            } else {
                toastr.success(description);
            }

            break;
        case "warning" :
            if(title != "") {
                toastr.warning(description, title);
            } else {
                toastr.warning(description);
            }

            break;
        case "error" :
            if(title != "") {
                toastr.error(description, title);
            } else {
                toastr.error(description);
            }

            break;
        
    }

}

function ValidateEmail(inputText)
{
    var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if($(inputText).val().match(mailformat)){
        return true;
    }
    else{
        return false;
    }
}

function openWindow(url,winName,winWidth,winHeight,features) {
// This function opens a popup window centered by x and y
    if (winWidth == 'max') {
        winWidth = screen.availWidth;
    }
    if (winHeight == 'max') {
        winHeight = screen.availHeight;
    }
    x = (screen.availWidth-winWidth)/2;
    y = (screen.availHeight-winHeight)/2;
    features += ",width="+winWidth+",height="+winHeight;
    features += ",left="+x+",top="+y+",screenX="+x+",screenY="+y;
    var featresize = features.search(/resizable/i);
    if (featresize==-1)
        {
        features += ",resizable=yes";
        }
    var featscrol = features.search(/scrollbars/i);
    if (featscrol==-1)
        {
        features += ",scrollbars=yes";
        }
    newin = window.open(url,winName,features);
    newin.focus();
    return newin;
}

function numberToCurrency(x) {
    x = x.toString().replace(/\B(?<!\,\d*)(?=(\d{3})+(?!\d))/g, ".");
    
    return x;
}

function isDecimal(evt, obj) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    var value = $(obj).val();
    var dotcontains = value.indexOf(".") != -1;
    if (dotcontains)
        if (charCode == 46) return false;
    if (charCode == 46) return true;
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

$(function () {
    /*
    $('.sample1').alphanumeric();
    $('.sample2').alphanumeric({
        allow: "., "
    });
    $('.sample3').alpha({
        nocaps: true
    });
    $('.sample4').numeric();
    $('.sample5').numeric({
        allow: "."
    });
    $('.sample6').alphanumeric({
        ichars: '.1a'
    });
    */
    $.fn.alphanumeric = function (p) {
        var input = $(this),
            az = "abcdefghijklmnopqrstuvwxyz",
            options = $.extend({
                ichars: '!@#$%^&*()+=[]\\\';,/{}|":<>?~`.- _',
                nchars: '',
                allow: ''
            }, p),
            s = options.allow.split(''),
            i = 0,
            ch,
            regex;

        for (i; i < s.length; i++) {
            if (options.ichars.indexOf(s[i]) != -1) {
                s[i] = '\\' + s[i];
            }
        }

        if (options.nocaps) {
            options.nchars += az.toUpperCase();
        }
        if (options.allcaps) {
            options.nchars += az;
        }

        options.allow = s.join('|');

        regex = new RegExp(options.allow, 'gi');
        ch = (options.ichars + options.nchars).replace(regex, '');

        input.keypress(function (e) {
            var key = String.fromCharCode(!e.charCode ? e.which : e.charCode);

            if (ch.indexOf(key) != -1 && !e.ctrlKey) {
                e.preventDefault();
            }
        });

        input.blur(function () {
            var value = input.val(),
                j = 0;

            for (j; j < value.length; j++) {
                if (ch.indexOf(value[j]) != -1) {
                    input.val('');
                    return false;
                }
            }
            return false;
        });

        return input;
    };

    $.fn.numeric = function (p) {
        var az = 'abcdefghijklmnopqrstuvwxyz',
            aZ = az.toUpperCase();

        return this.each(function () {
            $(this).alphanumeric($.extend({
                nchars: az + aZ
            }, p));
        });
    };

    $.fn.alpha = function (p) {
        var nm = '1234567890';
        return this.each(function () {
            $(this).alphanumeric($.extend({
                nchars: nm
            }, p));
        });
    };

    var Toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      timer: 3000
    });
    
    $.ajax({
        url: "settings/check_stock", 
        type: "post",
        dataType: "json",
        cache: false,
        success: function(result){
            if(result.min_stock > 0){
                var items = result.items;
                var msg = new Array();
                for(var i=0;i<items.length;i++){
                    msg.push("Plat "+items[i].name_plat+" "+items[i].size+" mm hampir habis.");
                }

                $(document).Toasts('create', {
                    class: 'bg-warning',
                    body: msg.join("<br>"),
                    title: 'Peringatan',
                    icon: 'fas fa-exclamation-triangle fa-lg',
                });   
            }

        }
    });    
});
