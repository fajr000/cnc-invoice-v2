/*
SQLyog Community v12.4.0 (64 bit)
MySQL - 5.6.50-log : Database - db_cnc_live
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `accounting_cancels` */

DROP TABLE IF EXISTS `accounting_cancels`;

CREATE TABLE `accounting_cancels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaction` int(10) unsigned NOT NULL,
  `id_invoice` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `accounting_invoice_item_attachments` */

DROP TABLE IF EXISTS `accounting_invoice_item_attachments`;

CREATE TABLE `accounting_invoice_item_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_invoice_item` int(10) unsigned NOT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `encrypted_name` varchar(255) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_invoice_item` (`id_invoice_item`),
  CONSTRAINT `accounting_invoice_item_attachments_ibfk_1` FOREIGN KEY (`id_invoice_item`) REFERENCES `accounting_invoice_items` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `accounting_invoice_items` */

DROP TABLE IF EXISTS `accounting_invoice_items`;

CREATE TABLE `accounting_invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_invoice` int(10) unsigned NOT NULL,
  `id_plat` int(10) unsigned DEFAULT NULL,
  `id_service` int(10) unsigned DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL COMMENT 'item price',
  `qty` decimal(10,2) unsigned NOT NULL COMMENT 'service_price qty using decimal',
  `minutes` int(11) NOT NULL DEFAULT '0' COMMENT 'for price using minutes',
  `amount` decimal(10,2) NOT NULL COMMENT 'subtotal item',
  `discount` decimal(10,2) DEFAULT NULL COMMENT 'discount per item',
  `discount_amount` decimal(10,2) DEFAULT NULL,
  `notes` text,
  `fully_refunded` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: confirmation, 1: request token, 2: waiting list, 3: processing, 4: done, 5 : Cancelled',
  PRIMARY KEY (`id`),
  KEY `id_invoice` (`id_invoice`),
  KEY `id_plat` (`id_plat`),
  KEY `id_service` (`id_service`),
  CONSTRAINT `accounting_invoice_items_ibfk_1` FOREIGN KEY (`id_invoice`) REFERENCES `accounting_invoices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

/*Table structure for table `accounting_invoices` */

DROP TABLE IF EXISTS `accounting_invoices`;

CREATE TABLE `accounting_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaction` int(10) unsigned NOT NULL,
  `invoice_number` varchar(30) DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `discount_amount` decimal(10,2) DEFAULT NULL COMMENT 'discount on total invoice',
  `processed_time` datetime DEFAULT NULL,
  `done_time` datetime DEFAULT NULL,
  `status` tinyint(2) DEFAULT '0' COMMENT '0: confirmation, 1: request token, 2: waiting list, 3: processing, 4: done, 5 : Cancelled',
  PRIMARY KEY (`id`),
  KEY `id_transaction` (`id_transaction`),
  CONSTRAINT `accounting_invoices_ibfk_1` FOREIGN KEY (`id_transaction`) REFERENCES `transactions` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Table structure for table `accounting_payment_attachments` */

DROP TABLE IF EXISTS `accounting_payment_attachments`;

CREATE TABLE `accounting_payment_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment` int(10) unsigned NOT NULL,
  `original_name` varchar(255) DEFAULT NULL,
  `encrypted_name` varchar(255) DEFAULT NULL,
  `ext` varchar(20) DEFAULT NULL,
  `size` double DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `accounting_payment_invoices` */

DROP TABLE IF EXISTS `accounting_payment_invoices`;

CREATE TABLE `accounting_payment_invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_payment` int(10) unsigned NOT NULL,
  `id_invoice` int(10) unsigned NOT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `is_down_payment` tinyint(1) NOT NULL DEFAULT '0',
  `cancelled_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `fully_refunded` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `id_payment` (`id_payment`),
  KEY `id_invoice` (`id_invoice`),
  CONSTRAINT `accounting_payment_invoices_ibfk_1` FOREIGN KEY (`id_payment`) REFERENCES `accounting_payments` (`id`),
  CONSTRAINT `accounting_payment_invoices_ibfk_2` FOREIGN KEY (`id_invoice`) REFERENCES `accounting_invoices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Table structure for table `accounting_payments` */

DROP TABLE IF EXISTS `accounting_payments`;

CREATE TABLE `accounting_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaction` int(10) unsigned NOT NULL,
  `payment_method_id` int(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_transaction` (`id_transaction`),
  KEY `payment_method_id` (`payment_method_id`),
  CONSTRAINT `accounting_payments_ibfk_1` FOREIGN KEY (`id_transaction`) REFERENCES `transactions` (`id`),
  CONSTRAINT `accounting_payments_ibfk_2` FOREIGN KEY (`payment_method_id`) REFERENCES `payment_methods` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_estonian_ci;

/*Table structure for table `ci_sessions` */

DROP TABLE IF EXISTS `ci_sessions`;

CREATE TABLE `ci_sessions` (
  `id` varchar(128) CHARACTER SET latin1 NOT NULL,
  `ip_address` varchar(45) CHARACTER SET latin1 NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned NOT NULL DEFAULT '0',
  `id_sales` int(11) unsigned NOT NULL DEFAULT '0',
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `name_contact` varchar(100) DEFAULT NULL,
  `address` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `cutting_types` */

DROP TABLE IF EXISTS `cutting_types`;

CREATE TABLE `cutting_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `groups` */

DROP TABLE IF EXISTS `groups`;

CREATE TABLE `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `description` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `groups_access` */

DROP TABLE IF EXISTS `groups_access`;

CREATE TABLE `groups_access` (
  `group_id` int(10) unsigned NOT NULL,
  `menu_id` int(10) unsigned NOT NULL,
  `create` tinyint(4) NOT NULL DEFAULT '0',
  `read` tinyint(4) NOT NULL DEFAULT '0',
  `update` tinyint(4) NOT NULL DEFAULT '0',
  `delete` tinyint(4) NOT NULL DEFAULT '0',
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`group_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `login_attempts` */

DROP TABLE IF EXISTS `login_attempts`;

CREATE TABLE `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `machine_logs` */

DROP TABLE IF EXISTS `machine_logs`;

CREATE TABLE `machine_logs` (
  `invoice` varchar(30) NOT NULL,
  `id_office` int(10) NOT NULL,
  `no_mesin` varchar(8) NOT NULL,
  `token` varchar(255) NOT NULL,
  `penggunaan_start` datetime NOT NULL,
  `penggunaan_end` datetime NOT NULL,
  `token_time` int(11) NOT NULL,
  `sisa_time` time NOT NULL,
  `is_sync` char(2) NOT NULL DEFAULT '0',
  `is_finish` char(1) NOT NULL DEFAULT '0',
  `is_active` char(2) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Table structure for table `menu` */

DROP TABLE IF EXISTS `menu`;

CREATE TABLE `menu` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `parent_id` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `sort` int(11) NOT NULL DEFAULT '1',
  `url` varchar(100) NOT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  `can_read` tinyint(1) NOT NULL DEFAULT '0',
  `can_add` tinyint(1) NOT NULL DEFAULT '0',
  `can_update` tinyint(1) NOT NULL DEFAULT '0',
  `can_delete` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

/*Table structure for table `office_accounts` */

DROP TABLE IF EXISTS `office_accounts`;

CREATE TABLE `office_accounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `number_account` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `owner_name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `office_format_files` */

DROP TABLE IF EXISTS `office_format_files`;

CREATE TABLE `office_format_files` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned NOT NULL,
  `prefix` varchar(10) DEFAULT NULL,
  `no_file` int(3) DEFAULT NULL,
  `month_file` int(2) DEFAULT NULL,
  `year_file` int(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `office_machines` */

DROP TABLE IF EXISTS `office_machines`;

CREATE TABLE `office_machines` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned DEFAULT NULL,
  `id_cutting_type` int(10) unsigned DEFAULT NULL,
  `no_machine` varchar(4) DEFAULT NULL,
  `is_used` tinyint(1) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `office_messages` */

DROP TABLE IF EXISTS `office_messages`;

CREATE TABLE `office_messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `office_plat_order` */

DROP TABLE IF EXISTS `office_plat_order`;

CREATE TABLE `office_plat_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(11) DEFAULT NULL,
  `transacted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `id_plat` int(11) DEFAULT NULL,
  `size` varchar(20) DEFAULT NULL,
  `price_purchase` decimal(10,0) DEFAULT NULL,
  `price_sale` decimal(10,0) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `handle_by_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `office_settings` */

DROP TABLE IF EXISTS `office_settings`;

CREATE TABLE `office_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned NOT NULL,
  `code` varchar(50) NOT NULL,
  `value` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`,`id_office`,`code`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

/*Table structure for table `offices` */

DROP TABLE IF EXISTS `offices`;

CREATE TABLE `offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `phone` varchar(30) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `payment_methods` */

DROP TABLE IF EXISTS `payment_methods`;

CREATE TABLE `payment_methods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `description` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Table structure for table `plat_prices` */

DROP TABLE IF EXISTS `plat_prices`;

CREATE TABLE `plat_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned NOT NULL,
  `id_plat` int(10) unsigned NOT NULL,
  `size` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `price` double NOT NULL,
  `discount` int(11) NOT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Table structure for table `plat_sizes` */

DROP TABLE IF EXISTS `plat_sizes`;

CREATE TABLE `plat_sizes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `size` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

/*Table structure for table `plat_types` */

DROP TABLE IF EXISTS `plat_types`;

CREATE TABLE `plat_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `reports` */

DROP TABLE IF EXISTS `reports`;

CREATE TABLE `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `name_func` varchar(200) DEFAULT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Table structure for table `sales` */

DROP TABLE IF EXISTS `sales`;

CREATE TABLE `sales` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nik` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `name` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `address` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `phone` varchar(50) CHARACTER SET latin1 DEFAULT NULL,
  `email` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `id_office` int(10) unsigned DEFAULT NULL,
  `sort` int(11) NOT NULL DEFAULT '1',
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_office` (`id_office`),
  CONSTRAINT `sales_ibfk_1` FOREIGN KEY (`id_office`) REFERENCES `offices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `service_discounts` */

DROP TABLE IF EXISTS `service_discounts`;

CREATE TABLE `service_discounts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned DEFAULT NULL,
  `id_service_price` int(10) unsigned DEFAULT NULL,
  `discount_type` tinyint(1) DEFAULT NULL,
  `discount_amount` int(11) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `service_prices` */

DROP TABLE IF EXISTS `service_prices`;

CREATE TABLE `service_prices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_plat` int(10) unsigned NOT NULL,
  `id_cutting_type` int(10) unsigned DEFAULT NULL,
  `size` varchar(30) CHARACTER SET latin1 DEFAULT NULL,
  `easy` double DEFAULT NULL,
  `medium` double DEFAULT NULL,
  `difficult` double DEFAULT NULL,
  `per_minute` double DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_on` datetime DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

/*Table structure for table `token_logs` */

DROP TABLE IF EXISTS `token_logs`;

CREATE TABLE `token_logs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_token` int(10) unsigned DEFAULT NULL,
  `id_machine` int(10) unsigned DEFAULT NULL,
  `no_machine` varchar(4) DEFAULT NULL,
  `token_order` tinyint(2) DEFAULT NULL,
  `minutes` int(3) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `tokens` */

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned DEFAULT NULL,
  `id_invoice` int(10) unsigned DEFAULT NULL,
  `id_machine` int(10) unsigned DEFAULT NULL,
  `invoice_number` varchar(20) DEFAULT NULL,
  `no_machine` varchar(4) DEFAULT NULL,
  `token_order` tinyint(2) DEFAULT NULL,
  `minutes` int(3) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(10) unsigned DEFAULT NULL,
  `modified_on` datetime DEFAULT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `generated_on` datetime DEFAULT NULL,
  `generated_by` int(10) unsigned DEFAULT NULL,
  `plaintext` varchar(21) DEFAULT NULL,
  `key_token` varchar(21) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Table structure for table `transactions` */

DROP TABLE IF EXISTS `transactions`;

CREATE TABLE `transactions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_office` int(10) unsigned NOT NULL,
  `id_customer` int(10) unsigned DEFAULT NULL,
  `transaction_type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1:purchasing, 2:payment',
  `transacted_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `handle_by_id` int(10) unsigned DEFAULT NULL,
  `cancelled_on` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `posted` tinyint(1) NOT NULL DEFAULT '1',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `id_office` (`id_office`),
  KEY `id_customer` (`id_customer`),
  CONSTRAINT `transactions_ibfk_1` FOREIGN KEY (`id_office`) REFERENCES `offices` (`id`),
  CONSTRAINT `transactions_ibfk_2` FOREIGN KEY (`id_customer`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `id_office` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  UNIQUE KEY `uc_remember_selector` (`remember_selector`),
  KEY `id_office` (`id_office`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`id_office`) REFERENCES `offices` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `users_groups` */

DROP TABLE IF EXISTS `users_groups`;

CREATE TABLE `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/* Function  structure for function  `formatInvoice` */

/*!50003 DROP FUNCTION IF EXISTS `formatInvoice` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `formatInvoice`(invoice VARCHAR(50)) RETURNS varchar(50) CHARSET latin1
BEGIN
	DECLARE formated_invoice varchar(50) DEFAULT '';
	DECLARE prefix VARCHAR(10) DEFAULT '';
	DECLARE infix VARCHAR(2) DEFAULT '';
	DECLARE postfix VARCHAR(4) DEFAULT '';
	
	SET prefix = (SELECT SUBSTRING_INDEX(invoice, '/', 3));
	SET postfix = (SELECT SUBSTRING_INDEX(invoice, '/', -1));
	SET infix = (SELECT SUBSTRING(SUBSTRING_INDEX(invoice, '/', -2),1,POSITION("/" IN SUBSTRING_INDEX(invoice, '/', -2))-1));
	set formated_invoice = CONCAT(prefix,'/',toRoman(infix),'/',postfix);
	
	return formated_invoice;
    END */$$
DELIMITER ;

/* Function  structure for function  `toIndonesianMonth` */

/*!50003 DROP FUNCTION IF EXISTS `toIndonesianMonth` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `toIndonesianMonth`(month_number TINYINT(2)) RETURNS varchar(30) CHARSET latin1
BEGIN
    DECLARE month_string varchar(30) DEFAULT "OK";
    
IF month_number = 1 THEN
	SET month_string = 'Januari';
ELSEIF month_number = 2 THEN
	SET month_string = 'Februari';
ELSEIF month_number = 3 THEN
	SET month_string = 'Maret';
ELSEIF month_number = 4 THEN
	SET month_string = 'April';
ELSEIF month_number = 5 THEN
	SET month_string = 'Mei';
ELSEIF month_number = 6 THEN
	SET month_string = 'Juni';
ELSEIF month_number = 7 THEN
	SET month_string = 'Juli';
ELSEIF month_number = 8 THEN
	SET month_string = 'Agustus';
ELSEIF month_number = 9 THEN
	SET month_string = 'September';
ELSEIF month_number = 10 THEN
	SET month_string = 'Oktober';
ELSEIF month_number = 11 THEN
	SET month_string = 'November';
ELSEIF month_number = 12 THEN
	SET month_string = 'Desember';
ELSE 
        SET  month_string = 'Undefined';
END IF;
    
    RETURN month_string;

    END */$$
DELIMITER ;

/* Function  structure for function  `toRoman` */

/*!50003 DROP FUNCTION IF EXISTS `toRoman` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `toRoman`(number INT) RETURNS text CHARSET latin1
BEGIN
DECLARE basic_roman TEXT DEFAULT 'M,CM,D,CD,C,XC,L,XL,X,IX,V,IV,I';
	DECLARE basic_value TEXT DEFAULT '1000,900,500,400,100,90,50,40,10,9,5,4,1';
	DECLARE roman_string TEXT DEFAULT '';
	DECLARE i INT DEFAULT 1;
	DECLARE roman_symbol TEXT;
	DECLARE roman_value INT;
 
	SET roman_string = (SELECT IF(number<=0 OR number>=4000,NULL,roman_string));
 
	WHILE number > 0 AND number < 4000 DO
 
		SET roman_symbol = SUBSTRING_INDEX(SUBSTRING_INDEX(basic_roman,',',i),',',-1);
		SET roman_value  = SUBSTRING_INDEX(SUBSTRING_INDEX(basic_value,',',i),',',-1);
 
		IF number >= roman_value THEN
			SET roman_string = CONCAT(roman_string,roman_symbol);
			SET number = number - roman_value;
		ELSE
			SET i = i + 1;
		END IF;
 
	END WHILE;
 
	RETURN 	roman_string;
    END */$$
DELIMITER ;

/*Table structure for table `v_customer_amount_per_invoice` */

DROP TABLE IF EXISTS `v_customer_amount_per_invoice`;

/*!50001 DROP VIEW IF EXISTS `v_customer_amount_per_invoice` */;
/*!50001 DROP TABLE IF EXISTS `v_customer_amount_per_invoice` */;

/*!50001 CREATE TABLE  `v_customer_amount_per_invoice`(
 `id_transaction` int(11) unsigned ,
 `period` varchar(17) ,
 `id_office` int(11) unsigned ,
 `id_customer` int(11) unsigned ,
 `id_invoice` varchar(341) ,
 `invoice_number` varchar(341) ,
 `cancelled_on` datetime ,
 `invoice_amount` decimal(18,2) ,
 `invoice_discount_amount` decimal(18,2) ,
 `due` decimal(19,2) 
)*/;

/*Table structure for table `v_customer_amount_per_period` */

DROP TABLE IF EXISTS `v_customer_amount_per_period`;

/*!50001 DROP VIEW IF EXISTS `v_customer_amount_per_period` */;
/*!50001 DROP TABLE IF EXISTS `v_customer_amount_per_period` */;

/*!50001 CREATE TABLE  `v_customer_amount_per_period`(
 `id_office` int(11) unsigned ,
 `id_customer` int(11) unsigned ,
 `period` varchar(17) ,
 `amount` decimal(41,2) 
)*/;

/*Table structure for table `v_customer_payment` */

DROP TABLE IF EXISTS `v_customer_payment`;

/*!50001 DROP VIEW IF EXISTS `v_customer_payment` */;
/*!50001 DROP TABLE IF EXISTS `v_customer_payment` */;

/*!50001 CREATE TABLE  `v_customer_payment`(
 `id_transaction` int(10) unsigned ,
 `transacted_on` varchar(8) ,
 `transacted_date` varchar(10) ,
 `id_payment` int(10) unsigned ,
 `id_invoice` int(10) unsigned ,
 `id_office` int(10) unsigned ,
 `id_customer` int(10) unsigned ,
 `name_customer` varchar(100) ,
 `invoice_number` varchar(30) ,
 `amount` decimal(10,2) ,
 `code` varchar(20) 
)*/;

/*Table structure for table `v_customer_period_rank_all` */

DROP TABLE IF EXISTS `v_customer_period_rank_all`;

/*!50001 DROP VIEW IF EXISTS `v_customer_period_rank_all` */;
/*!50001 DROP TABLE IF EXISTS `v_customer_period_rank_all` */;

/*!50001 CREATE TABLE  `v_customer_period_rank_all`(
 `id_office` int(11) unsigned ,
 `id_customer` int(11) unsigned ,
 `period` varchar(17) ,
 `amount` decimal(41,2) ,
 `rank` int(3) 
)*/;

/*Table structure for table `v_customer_period_rank_prev_month` */

DROP TABLE IF EXISTS `v_customer_period_rank_prev_month`;

/*!50001 DROP VIEW IF EXISTS `v_customer_period_rank_prev_month` */;
/*!50001 DROP TABLE IF EXISTS `v_customer_period_rank_prev_month` */;

/*!50001 CREATE TABLE  `v_customer_period_rank_prev_month`(
 `id_office` int(11) unsigned ,
 `id_customer` int(11) unsigned ,
 `period` varchar(17) ,
 `period_month` varchar(69) ,
 `amount` decimal(41,2) ,
 `rank` int(3) 
)*/;

/*Table structure for table `v_invoice_due` */

DROP TABLE IF EXISTS `v_invoice_due`;

/*!50001 DROP VIEW IF EXISTS `v_invoice_due` */;
/*!50001 DROP TABLE IF EXISTS `v_invoice_due` */;

/*!50001 CREATE TABLE  `v_invoice_due`(
 `id_office` int(10) unsigned ,
 `id_customer` int(10) unsigned ,
 `id_invoice` int(10) unsigned ,
 `invoice_number` varchar(30) ,
 `amount` decimal(10,2) ,
 `discount` decimal(10,2) ,
 `discount_amount` decimal(10,2) ,
 `due` decimal(11,2) ,
 `paid` decimal(32,2) ,
 `current_due` decimal(33,2) 
)*/;

/*Table structure for table `v_invoices` */

DROP TABLE IF EXISTS `v_invoices`;

/*!50001 DROP VIEW IF EXISTS `v_invoices` */;
/*!50001 DROP TABLE IF EXISTS `v_invoices` */;

/*!50001 CREATE TABLE  `v_invoices`(
 `id_transaction` int(11) unsigned ,
 `transaction_type` tinyint(4) ,
 `id_invoice` varchar(341) ,
 `cancelled_invoice` varchar(341) ,
 `id_payment` bigint(20) ,
 `invoice_number` varchar(341) ,
 `id_office` int(11) unsigned ,
 `id_customer` int(11) unsigned ,
 `transacted_on` timestamp ,
 `cancelled_on` datetime ,
 `posted` tinyint(4) ,
 `name_office` varchar(100) ,
 `name_customer` varchar(100) ,
 `id_invoice_item` bigint(20) unsigned ,
 `id_item` bigint(20) unsigned ,
 `product` varchar(12) ,
 `name_plat` varchar(30) ,
 `machine` varchar(30) ,
 `size` varchar(30) ,
 `price` decimal(18,2) ,
 `qty` decimal(18,2) ,
 `minute` bigint(20) ,
 `amount` decimal(32,2) ,
 `discount` decimal(18,2) ,
 `discount_amount` decimal(18,2) ,
 `due` decimal(18,2) ,
 `payment_method` varchar(20) ,
 `is_down_payment` bigint(20) ,
 `invoice_amount` decimal(18,2) ,
 `invoice_discount` decimal(18,2) ,
 `invoice_discount_amount` decimal(18,2) ,
 `id_user` int(11) unsigned 
)*/;

/*Table structure for table `v_payment_details` */

DROP TABLE IF EXISTS `v_payment_details`;

/*!50001 DROP VIEW IF EXISTS `v_payment_details` */;
/*!50001 DROP TABLE IF EXISTS `v_payment_details` */;

/*!50001 CREATE TABLE  `v_payment_details`(
 `transacted_on` timestamp ,
 `cancelled_on` datetime ,
 `id_office` int(10) unsigned ,
 `id_customer` int(10) unsigned ,
 `id_invoice` int(10) unsigned ,
 `amount` decimal(10,2) ,
 `is_down_payment` tinyint(1) ,
 `code` varchar(20) 
)*/;

/*Table structure for table `v_payment_invoices` */

DROP TABLE IF EXISTS `v_payment_invoices`;

/*!50001 DROP VIEW IF EXISTS `v_payment_invoices` */;
/*!50001 DROP TABLE IF EXISTS `v_payment_invoices` */;

/*!50001 CREATE TABLE  `v_payment_invoices`(
 `id_office` int(10) unsigned ,
 `id_customer` int(10) unsigned ,
 `id_invoice` int(10) unsigned ,
 `amount` decimal(32,2) 
)*/;

/*View structure for view v_customer_amount_per_invoice */

/*!50001 DROP TABLE IF EXISTS `v_customer_amount_per_invoice` */;
/*!50001 DROP VIEW IF EXISTS `v_customer_amount_per_invoice` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_customer_amount_per_invoice` AS (select `vi`.`id_transaction` AS `id_transaction`,concat(date_format((last_day((date_format(`vi`.`transacted_on`,'%Y%m%d') - interval 1 month)) + interval 1 day),'%Y%m%d'),'-',date_format(last_day(date_format(`vi`.`transacted_on`,'%Y%m%d')),'%Y%m%d')) AS `period`,`vi`.`id_office` AS `id_office`,`vi`.`id_customer` AS `id_customer`,`vi`.`id_invoice` AS `id_invoice`,`vi`.`invoice_number` AS `invoice_number`,`vi`.`cancelled_on` AS `cancelled_on`,`vi`.`invoice_amount` AS `invoice_amount`,`vi`.`invoice_discount_amount` AS `invoice_discount_amount`,(`vi`.`invoice_amount` - `vi`.`invoice_discount_amount`) AS `due` from `v_invoices` `vi` where ((`vi`.`transaction_type` = 1) and (`vi`.`cancelled_on` = '0000-00-00 00:00:00')) group by `vi`.`id_transaction`,`vi`.`id_office`,`vi`.`id_customer`,`vi`.`id_invoice`,`vi`.`invoice_number`) */;

/*View structure for view v_customer_amount_per_period */

/*!50001 DROP TABLE IF EXISTS `v_customer_amount_per_period` */;
/*!50001 DROP VIEW IF EXISTS `v_customer_amount_per_period` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_customer_amount_per_period` AS (select `inv`.`id_office` AS `id_office`,`inv`.`id_customer` AS `id_customer`,`inv`.`period` AS `period`,sum(`inv`.`due`) AS `amount` from `v_customer_amount_per_invoice` `inv` group by `inv`.`id_office`,`inv`.`id_customer`,`inv`.`period`) */;

/*View structure for view v_customer_payment */

/*!50001 DROP TABLE IF EXISTS `v_customer_payment` */;
/*!50001 DROP VIEW IF EXISTS `v_customer_payment` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_customer_payment` AS (select `ap`.`id_transaction` AS `id_transaction`,date_format(`tr`.`transacted_on`,'%Y%m%d') AS `transacted_on`,date_format(`tr`.`transacted_on`,'%d/%m/%Y') AS `transacted_date`,`api`.`id_payment` AS `id_payment`,`api`.`id_invoice` AS `id_invoice`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`cr`.`name` AS `name_customer`,`ai`.`invoice_number` AS `invoice_number`,`api`.`amount` AS `amount`,`pm`.`code` AS `code` from (((((`accounting_payments` `ap` left join `accounting_payment_invoices` `api` on((`api`.`id_payment` = `ap`.`id`))) left join `payment_methods` `pm` on((`pm`.`id` = `ap`.`payment_method_id`))) left join `accounting_invoices` `ai` on((`ai`.`id` = `api`.`id_invoice`))) left join `transactions` `tr` on((`tr`.`id` = `ai`.`id_transaction`))) left join `customers` `cr` on((`cr`.`id` = `tr`.`id_customer`))) where (`api`.`cancelled_on` = '0000-00-00 00:00:00') order by `pm`.`code`,`tr`.`transacted_on`,`api`.`id_invoice`) */;

/*View structure for view v_customer_period_rank_all */

/*!50001 DROP TABLE IF EXISTS `v_customer_period_rank_all` */;
/*!50001 DROP VIEW IF EXISTS `v_customer_period_rank_all` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_customer_period_rank_all` AS (select `vi`.`id_office` AS `id_office`,`vi`.`id_customer` AS `id_customer`,`vi`.`period` AS `period`,`vi`.`amount` AS `amount`,find_in_set(`vi`.`amount`,(select group_concat(`vi2`.`amount` order by `vi2`.`id_office` ASC,`vi2`.`period` ASC,`vi2`.`amount` DESC separator ',') from `v_customer_amount_per_period` `vi2` where ((`vi2`.`period` = `vi`.`period`) and (`vi2`.`id_office` = `vi`.`id_office`)))) AS `rank` from `v_customer_amount_per_period` `vi` order by `vi`.`id_office`,`vi`.`period`,`vi`.`amount` desc,`vi`.`id_customer`) */;

/*View structure for view v_customer_period_rank_prev_month */

/*!50001 DROP TABLE IF EXISTS `v_customer_period_rank_prev_month` */;
/*!50001 DROP VIEW IF EXISTS `v_customer_period_rank_prev_month` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_customer_period_rank_prev_month` AS (select `v_customer_period_rank_all`.`id_office` AS `id_office`,`v_customer_period_rank_all`.`id_customer` AS `id_customer`,`v_customer_period_rank_all`.`period` AS `period`,date_format(last_day((date_format(curdate(),'%Y%m%d') - interval 1 month)),'%M %Y') AS `period_month`,`v_customer_period_rank_all`.`amount` AS `amount`,`v_customer_period_rank_all`.`rank` AS `rank` from `v_customer_period_rank_all` where (`v_customer_period_rank_all`.`period` = concat(date_format((last_day((date_format(curdate(),'%Y%m%d') - interval 2 month)) + interval 1 day),'%Y%m%d'),'-',date_format(last_day((date_format(curdate(),'%Y%m%d') - interval 1 month)),'%Y%m%d')))) */;

/*View structure for view v_invoice_due */

/*!50001 DROP TABLE IF EXISTS `v_invoice_due` */;
/*!50001 DROP VIEW IF EXISTS `v_invoice_due` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoice_due` AS (select `tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`ai`.`id` AS `id_invoice`,`ai`.`invoice_number` AS `invoice_number`,`ai`.`amount` AS `amount`,`ai`.`discount` AS `discount`,`ai`.`discount_amount` AS `discount_amount`,(`ai`.`amount` - `ai`.`discount_amount`) AS `due`,ifnull(`vpi`.`amount`,0) AS `paid`,((`ai`.`amount` - `ai`.`discount_amount`) - ifnull(`vpi`.`amount`,0)) AS `current_due` from ((`accounting_invoices` `ai` left join `transactions` `tr` on((`tr`.`id` = `ai`.`id_transaction`))) left join `v_payment_invoices` `vpi` on(((`vpi`.`id_office` = `tr`.`id_office`) and (`vpi`.`id_customer` = `tr`.`id_customer`) and (`vpi`.`id_invoice` = `ai`.`id`)))) where ((`tr`.`cancelled_on` = '0000-00-00 00:00:00') and (`tr`.`posted` = 1))) */;

/*View structure for view v_invoices */

/*!50001 DROP TABLE IF EXISTS `v_invoices` */;
/*!50001 DROP VIEW IF EXISTS `v_invoices` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_invoices` AS select `tr`.`id` AS `id_transaction`,`tr`.`transaction_type` AS `transaction_type`,`ai`.`id` AS `id_invoice`,'' AS `cancelled_invoice`,0 AS `id_payment`,`ai`.`invoice_number` AS `invoice_number`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`tr`.`transacted_on` AS `transacted_on`,`tr`.`cancelled_on` AS `cancelled_on`,`tr`.`posted` AS `posted`,`of`.`name` AS `name_office`,`cr`.`name` AS `name_customer`,`aii`.`id` AS `id_invoice_item`,`aii`.`id_plat` AS `id_item`,'Plat' AS `product`,`pt`.`name` AS `name_plat`,'' AS `machine`,`pp`.`size` AS `size`,`aii`.`price` AS `price`,`aii`.`qty` AS `qty`,0 AS `minute`,`aii`.`amount` AS `amount`,`aii`.`discount` AS `discount`,`aii`.`discount_amount` AS `discount_amount`,(`aii`.`amount` - `aii`.`discount_amount`) AS `due`,'' AS `payment_method`,0 AS `is_down_payment`,`ai`.`amount` AS `invoice_amount`,`ai`.`discount` AS `invoice_discount`,`ai`.`discount_amount` AS `invoice_discount_amount`,`tr`.`handle_by_id` AS `id_user` from ((((((`accounting_invoice_items` `aii` left join `accounting_invoices` `ai` on((`ai`.`id` = `aii`.`id_invoice`))) left join `plat_prices` `pp` on((`pp`.`id` = `aii`.`id_plat`))) left join `plat_types` `pt` on((`pt`.`id` = `pp`.`id_plat`))) left join `transactions` `tr` on(((`tr`.`id` = `ai`.`id_transaction`) and (`tr`.`id_office` = `pp`.`id_office`)))) left join `offices` `of` on((`of`.`id` = `tr`.`id_office`))) left join `customers` `cr` on((`cr`.`id` = `tr`.`id_customer`))) where ((`tr`.`transaction_type` = 1) and (`aii`.`id_plat` > 0)) union select `tr`.`id` AS `id_transaction`,`tr`.`transaction_type` AS `transaction_type`,`ai`.`id` AS `id_invoice`,'' AS `cancelled_invoice`,0 AS `id_payment`,`ai`.`invoice_number` AS `invoice_number`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`tr`.`transacted_on` AS `transacted_on`,`tr`.`cancelled_on` AS `cancelled_on`,`tr`.`posted` AS `posted`,`of`.`name` AS `name_office`,`cr`.`name` AS `name_customer`,`aii`.`id` AS `id_invoice_item`,`aii`.`id_service` AS `id_item`,'Jasa Cutting' AS `product`,`pt`.`name` AS `name_plat`,`ct`.`name` AS `machine`,`sp`.`size` AS `size`,`aii`.`price` AS `price`,`aii`.`qty` AS `qty`,`aii`.`minutes` AS `minutes`,`aii`.`amount` AS `amount`,`aii`.`discount` AS `discount`,`aii`.`discount_amount` AS `discount_amount`,(`aii`.`amount` - `aii`.`discount_amount`) AS `due`,'' AS `payment_method`,0 AS `is_down_payment`,`ai`.`amount` AS `invoice_amount`,`ai`.`discount` AS `invoice_discount`,`ai`.`discount_amount` AS `invoice_discount_amount`,`tr`.`handle_by_id` AS `id_user` from (((((((`accounting_invoice_items` `aii` left join `accounting_invoices` `ai` on((`ai`.`id` = `aii`.`id_invoice`))) left join `service_prices` `sp` on((`sp`.`id` = `aii`.`id_service`))) left join `cutting_types` `ct` on((`ct`.`id` = `sp`.`id_cutting_type`))) left join `plat_types` `pt` on((`pt`.`id` = `sp`.`id_plat`))) left join `transactions` `tr` on((`tr`.`id` = `ai`.`id_transaction`))) left join `offices` `of` on((`of`.`id` = `tr`.`id_office`))) left join `customers` `cr` on((`cr`.`id` = `tr`.`id_customer`))) where ((`tr`.`transaction_type` = 1) and (`aii`.`id_service` > 0)) union (select `ap`.`id_transaction` AS `id_transaction`,`tr`.`transaction_type` AS `transaction_type`,group_concat(`api`.`id_invoice` separator ',') AS `id_invoice`,group_concat(`api`.`fully_refunded` separator ',') AS `cancelled_invoice`,`api`.`id_payment` AS `id_payment`,group_concat(`ai`.`invoice_number` separator ',') AS `invoice_number`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`tr`.`transacted_on` AS `transacted_on`,`tr`.`cancelled_on` AS `cancelled_on`,`tr`.`posted` AS `posted`,`of`.`name` AS `name_office`,`cr`.`name` AS `name_customer`,0 AS `id_invoice_item`,0 AS `id_item`,'' AS `product`,'' AS `name_plat`,'' AS `machine`,'' AS `size`,0 AS `price`,0 AS `qty`,0 AS `minute`,sum(`api`.`amount`) AS `amount`,0 AS `discount`,0 AS `discount_amount`,0 AS `due`,`pm`.`code` AS `payment_method`,`api`.`is_down_payment` AS `is_down_payment`,0 AS `invoice_amount`,0 AS `invoice_discount`,0 AS `invoice_discount_amount`,`tr`.`handle_by_id` AS `id_user` from ((((((`transactions` `tr` left join `accounting_payments` `ap` on((`ap`.`id_transaction` = `tr`.`id`))) left join `payment_methods` `pm` on((`pm`.`id` = `ap`.`payment_method_id`))) left join `accounting_payment_invoices` `api` on((`api`.`id_payment` = `ap`.`id`))) left join `accounting_invoices` `ai` on((`ai`.`id` = `api`.`id_invoice`))) left join `offices` `of` on((`of`.`id` = `tr`.`id_office`))) left join `customers` `cr` on((`cr`.`id` = `tr`.`id_customer`))) where (`tr`.`transaction_type` = 2) group by `ap`.`id_transaction`,`tr`.`transaction_type`,`tr`.`id_office`,`tr`.`id_customer`) union (select `tr`.`id` AS `id_transaction`,`tr`.`transaction_type` AS `transaction_type`,group_concat(`ac`.`id_invoice` separator ',') AS `id_invoice`,'' AS `cancelled_invoice`,0 AS `id_payment`,group_concat(`ai`.`invoice_number` separator ',') AS `invoice_number`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`tr`.`transacted_on` AS `transacted_on`,`tr`.`cancelled_on` AS `cancelled_on`,`tr`.`posted` AS `posted`,`of`.`name` AS `name_office`,`cr`.`name` AS `name_customer`,0 AS `id_invoice_item`,0 AS `id_item`,'' AS `product`,'' AS `name_plat`,'' AS `machine`,'' AS `size`,0 AS `price`,0 AS `qty`,0 AS `minute`,0 AS `amount`,0 AS `discount`,0 AS `discount_amount`,0 AS `due`,'' AS `payment_method`,0 AS `is_down_payment`,0 AS `invoice_amount`,0 AS `invoice_discount`,0 AS `invoice_discount_amount`,`tr`.`handle_by_id` AS `id_user` from ((((`transactions` `tr` left join `accounting_cancels` `ac` on((`ac`.`id_transaction` = `tr`.`id`))) left join `accounting_invoices` `ai` on((`ai`.`id` = `ac`.`id_invoice`))) left join `offices` `of` on((`of`.`id` = `tr`.`id_office`))) left join `customers` `cr` on((`cr`.`id` = `tr`.`id_customer`))) where (`tr`.`transaction_type` = 3) group by `tr`.`id`,`tr`.`transaction_type`,`tr`.`id_office`,`tr`.`id_customer`) */;

/*View structure for view v_payment_details */

/*!50001 DROP TABLE IF EXISTS `v_payment_details` */;
/*!50001 DROP VIEW IF EXISTS `v_payment_details` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_payment_details` AS (select `tr`.`transacted_on` AS `transacted_on`,`tr`.`cancelled_on` AS `cancelled_on`,`tr`.`id_office` AS `id_office`,`tr`.`id_customer` AS `id_customer`,`api`.`id_invoice` AS `id_invoice`,`api`.`amount` AS `amount`,`api`.`is_down_payment` AS `is_down_payment`,`pm`.`code` AS `code` from (((`accounting_payment_invoices` `api` left join `accounting_payments` `ap` on((`ap`.`id` = `api`.`id_payment`))) left join `payment_methods` `pm` on((`pm`.`id` = `ap`.`payment_method_id`))) left join `transactions` `tr` on((`tr`.`id` = `ap`.`id_transaction`)))) */;

/*View structure for view v_payment_invoices */

/*!50001 DROP TABLE IF EXISTS `v_payment_invoices` */;
/*!50001 DROP VIEW IF EXISTS `v_payment_invoices` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_payment_invoices` AS (select `vp`.`id_office` AS `id_office`,`vp`.`id_customer` AS `id_customer`,`vp`.`id_invoice` AS `id_invoice`,sum(`vp`.`amount`) AS `amount` from `v_payment_details` `vp` where (`vp`.`cancelled_on` = '0000-00-00 00:00:00') group by `vp`.`id_office`,`vp`.`id_customer`,`vp`.`id_invoice`) */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
