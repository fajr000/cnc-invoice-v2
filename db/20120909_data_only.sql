/*
SQLyog Community v12.4.0 (64 bit)
MySQL - 5.6.50-log : Database - db_cnc_new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Data for the table `accounting_cancels` */

/*Data for the table `accounting_invoice_item_attachments` */

/*Data for the table `accounting_invoice_items` */

/*Data for the table `accounting_invoices` */

/*Data for the table `accounting_payment_attachments` */

/*Data for the table `accounting_payment_invoices` */

/*Data for the table `accounting_payments` */

/*Data for the table `ci_sessions` */

/*Data for the table `customers` */

/*Data for the table `cutting_types` */

insert  into `cutting_types`(`id`,`name`,`deleted`) values 
(1,'Plasma',0),
(2,'Laser',0);

/*Data for the table `groups` */

insert  into `groups`(`id`,`name`,`description`) values 
(1,'administrator','Administrator'),
(2,'admin pusat','Admin Pusat'),
(3,'admin cabang','Admin Cabang'),
(4,'customer service (cs)','Customer Service (CS)'),
(5,'operator alat','Operator Alat');

/*Data for the table `groups_access` */

insert  into `groups_access`(`group_id`,`menu_id`,`create`,`read`,`update`,`delete`,`active`) values 
(1,1,0,1,0,0,1),
(1,2,1,1,1,1,1),
(1,3,1,1,1,1,1),
(1,4,1,1,1,1,1),
(1,5,1,1,1,1,1),
(1,6,1,1,1,1,1),
(1,7,1,1,1,1,1),
(1,8,1,1,1,1,1),
(1,10,1,1,1,1,1),
(1,11,1,1,1,1,1),
(1,12,0,1,0,0,1),
(1,13,0,1,0,0,1),
(1,14,1,1,0,0,1),
(1,15,1,1,0,0,1),
(1,16,0,1,0,0,1),
(1,17,0,1,0,0,1),
(1,18,0,1,0,0,1),
(1,19,0,1,0,0,1),
(1,20,0,1,0,0,1),
(1,21,0,1,0,0,1),
(1,22,0,1,0,0,1),
(1,23,0,1,0,0,1),
(1,24,0,1,0,0,1),
(1,25,0,1,0,0,1);

/*Data for the table `login_attempts` */

/*Data for the table `machine_logs` */

/*Data for the table `menu` */

insert  into `menu`(`id`,`name`,`parent_id`,`active`,`sort`,`url`,`icon`,`publish`,`can_read`,`can_add`,`can_update`,`can_delete`) values 
(1,'Beranda',0,1,1,'dashboard','fa-home',1,1,0,0,0),
(2,'User',12,1,2,'user','fa-user-lock',1,1,1,1,1),
(3,'Hak Akses',0,1,7,'privileges','fa-cogs',1,1,1,1,1),
(4,'Kantor Cabang',12,1,1,'offices','fa-store',1,1,1,1,1),
(5,'Sales',12,1,3,'sales','fa-user-tie',1,1,1,1,1),
(6,'Customer',12,1,4,'customers','fa-users',1,1,1,1,1),
(7,'Plat',12,1,6,'price_plat','fa-shield-alt',1,1,1,1,1),
(8,'Jasa Potong',12,1,7,'price_service','fa-cut',1,1,1,1,1),
(10,'Transactions',0,1,9,'transactions','fa-folder-open',0,1,0,0,0),
(11,'Mesin',12,1,5,'machines','fa-wrench',1,1,1,1,1),
(12,'Master',0,1,2,'','fa fa-folder',1,1,0,0,0),
(13,'Payments',0,1,4,'','fa fa-folder',1,1,0,0,0),
(14,'Add New',16,1,1,'orders','fa-plus-circle',1,1,1,0,0),
(15,'Add New',13,1,1,'payments','fa-plus-circle',1,1,1,0,0),
(16,'Invoices',0,1,3,'','fa fa-folder',1,1,0,0,0),
(17,'All',16,1,2,'invoices','fa-clone',1,1,0,0,0),
(18,'Konfirmasi',16,1,3,'invoices_confirm','fa-clone',1,1,0,0,0),
(19,'Request Token',16,1,4,'invoices_requesttoken','fa-clone',1,1,0,0,0),
(20,'Waiting List',16,1,5,'invoices_waitinglist','fa-clone',1,1,0,0,0),
(21,'Diproses',16,1,6,'invoices_processing','fa-clone',1,1,0,0,0),
(22,'Selesai',16,1,7,'invoices_done','fa-clone',1,1,0,0,0),
(23,'Dibatalkan',16,1,8,'invoices_cancelled','fa-clone',1,1,0,0,0),
(24,'All',13,1,2,'payments_all','fa-clone',1,1,0,0,0),
(25,'Reports',0,1,5,'reports','fa fa-folder',1,1,0,0,0);

/*Data for the table `office_accounts` */

/*Data for the table `office_format_files` */

/*Data for the table `office_machines` */

/*Data for the table `office_messages` */

/*Data for the table `office_plat_order` */

/*Data for the table `office_settings` */

/*Data for the table `offices` */

/*Data for the table `payment_methods` */

insert  into `payment_methods`(`id`,`code`,`description`) values 
(1,'CASH','pay cash on the office'),
(2,'TRANSFER','transfer by bank');

/*Data for the table `plat_prices` */

/*Data for the table `plat_sizes` */

insert  into `plat_sizes`(`id`,`size`) values 
(1,1),
(2,1.2),
(3,1.4),
(4,1.5),
(5,1.6),
(6,1.8),
(7,2),
(8,2.2),
(9,2.3),
(10,2.5),
(11,2.7),
(12,3),
(13,3.2),
(14,3.5),
(15,3.8),
(16,4),
(17,4.4),
(18,4.7),
(19,5),
(20,5.5),
(21,6);

/*Data for the table `plat_types` */

insert  into `plat_types`(`id`,`name`,`deleted`) values 
(1,'Alumunium',0),
(2,'Stainless',0),
(3,'Besi',0),
(4,'Kumingan',0),
(5,'Galvanis',0);

/*Data for the table `reports` */

insert  into `reports`(`id`,`name`,`name_func`,`publish`) values 
(1,'Ranking Transaksi Customer','customer_transaction_rank',1),
(2,'Rekap Sales Per Invoice','sales_invoice_recapitulation',1),
(3,'Rekap Komisi Sales Bulanan','monthly_sales_compensation',1),
(4,'Rekap Pembelian Bahan','material_purchase_recapitulation',1),
(5,'Rekap Penggunaan Alat','machine_usage_recapitulation',1),
(6,'Rekap Penjualan Bahan','material_sales_recapitulation',1),
(7,'Rekap Invoice','invoice_recapitulation',1),
(8,'Rekap Pembayaran Customer','payment_recapitulation',1);

/*Data for the table `sales` */

/*Data for the table `service_discounts` */

/*Data for the table `service_prices` */

/*Data for the table `token_logs` */

/*Data for the table `tokens` */

/*Data for the table `transactions` */

/*Data for the table `users` */

insert  into `users`(`id`,`ip_address`,`username`,`password`,`salt`,`email`,`activation_selector`,`activation_code`,`forgotten_password_selector`,`forgotten_password_code`,`forgotten_password_time`,`remember_selector`,`remember_code`,`created_on`,`last_login`,`active`,`first_name`,`last_name`,`company`,`phone`,`id_office`) values 
(1,'127.0.0.1','administrator','$2y$08$200Z6ZZbp3RAEXoaWcMA6uJOFicwNZaqk4oDhqTUiFXFe63MG.Daa',NULL,'admin@admin.com',NULL,'',NULL,NULL,NULL,NULL,NULL,1268889823,1631165154,1,'Administrator ','','ADMIN','0',NULL);

/*Data for the table `users_groups` */

insert  into `users_groups`(`id`,`user_id`,`group_id`) values 
(1,1,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
