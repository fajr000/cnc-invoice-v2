#clear transactions
DELETE FROM accounting_cancels;
DELETE FROM accounting_invoice_item_attachments;
DELETE FROM accounting_invoice_items;
DELETE FROM accounting_payment_attachments;
DELETE FROM accounting_payment_invoices;
DELETE FROM accounting_invoices;
DELETE FROM accounting_payments;
DELETE FROM transactions;
DELETE FROM machine_logs;
DELETE FROM token_logs;
DELETE FROM tokens;
DELETE FROM office_format_files;
DELETE FROM office_plat_order;

ALTER TABLE accounting_cancels  AUTO_INCREMENT = 1;
ALTER TABLE accounting_invoice_item_attachments  AUTO_INCREMENT = 1;
ALTER TABLE accounting_invoice_items  AUTO_INCREMENT = 1;
ALTER TABLE accounting_payment_attachments  AUTO_INCREMENT = 1;
ALTER TABLE accounting_payment_invoices  AUTO_INCREMENT = 1;
ALTER TABLE accounting_invoices  AUTO_INCREMENT = 1;
ALTER TABLE accounting_payments  AUTO_INCREMENT = 1;
ALTER TABLE transactions  AUTO_INCREMENT = 1;
ALTER TABLE machine_logs  AUTO_INCREMENT = 1;
ALTER TABLE token_logs  AUTO_INCREMENT = 1;
ALTER TABLE tokens  AUTO_INCREMENT = 1;
ALTER TABLE office_format_files  AUTO_INCREMENT = 1;
ALTER TABLE office_plat_order  AUTO_INCREMENT = 1;