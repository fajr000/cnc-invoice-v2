<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Offices extends REST_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index_get() {
        $id = $this->get('id');
        if ($id == '') {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
            $this->db->where('id', $id);
            $data = $this->db->get('offices')->result();

            $this->response($data, REST_Controller::HTTP_OK);
        }
    }

   
    public function index_post() {
        $name = $this->post('name') ? $this->post('name') : '';
        $address = $this->post('address') ? $this->post('address') : '';
        $city = $this->post('city') ? $this->post('city') : '';
        $phone = $this->post('phone') ? $this->post('phone') : '';
        $website = $this->post('website') ? $this->post('website') : '';
        $data = array(
            'name' => $name,
            'address' => $address,
            'city' => $city,
            'phone' => $phone,
            'website' => $website,
        );
        $insert = $this->db->insert('offices', $data);
        if ($insert) {
            $id = $this->db->insert_id();
            $data['id'] = $id;

            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        }
    }

    function index_put() {
        $id = $this->put('id');
        $data = array(
            'name' => $this->put('name'),
            'address' => $this->put('address'),
            'city' => $this->put('city'),
            'phone' => $this->put('phone'),
            'website' => $this->put('website'),
        );
        $this->db->where('id', $id);
        $update = $this->db->update('offices', $data);
        if ($update) {
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        }
    }


}
