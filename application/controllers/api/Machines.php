<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Machines extends REST_Controller {

    function __construct(){
        parent::__construct();
    }
	
	public function index_get(){
		$id_office = ($this->get('id_office')) ? $this->get('id_office') : 0;
		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}
		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;
		
		$data = $this->db->query("
                SELECT *
                FROM office_machines
                $filter_str")->result();

        $this->response($data, REST_Controller::HTTP_OK);
	}

    public function log_get() {
        $invoice = ($this->get('invoice')) ? $this->get('invoice') : "";
        $id_office = ($this->get('id_office')) ? $this->get('id_office') : 0;
        $no_mesin = ($this->get('no_mesin')) ? $this->get('no_mesin') : "";
        $token = ($this->get('token')) ? $this->get('token') : "";
        if ($invoice == '' || $id_office == 0 || $no_mesin == '' || $token == '') {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
            $params = array();
            if(!empty($invoice)){
                $params[] = "UPPER(invoice) = '".strtoupper($invoice)."'";
            }
            if(!empty($id_office)){
                $params[] = "id_office = $id_office";
            }
            if(!empty($no_mesin)){
                $params[] = "no_mesin = '".$no_mesin."'";
            }
            if(!empty($token)){
                $params[] = "UPPER(token) = '".strtoupper($token)."'";
            }

            $filter_arr = array();
            $filter_str = "";

            if(count($params) > 0)
                $filter_str = implode(" AND ", $params);

            $filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

            $data = $this->db->query("
                SELECT *
                FROM machine_logs
                $filter_str")->result();

            $this->response($data, REST_Controller::HTTP_OK);
        }
        
    }

    public function log_post() {
        $invoice = ($this->post('invoice')) ? $this->post('invoice') : "";
        $id_office = ($this->post('id_office')) ? $this->post('id_office') : 0;
        $no_mesin = ($this->post('no_mesin')) ? $this->post('no_mesin') : "";
        $token = ($this->post('token')) ? $this->post('token') : "";

        $penggunaan_start = ($this->post('penggunaan_start')) ? $this->post('penggunaan_start') : "0000-00-00 00:00:00";
        $penggunaan_end = ($this->post('penggunaan_end')) ? $this->post('penggunaan_end') : "0000-00-00 00:00:00";
        $token_time = ($this->post('token_time')) ? $this->post('token_time') : 0;
        $sisa_time = ($this->post('sisa_time')) ? $this->post('sisa_time') : "00:00:00";
        $is_sync = ($this->post('is_sync')) ? $this->post('is_sync') : 0;
        $is_finish = ($this->post('is_finish')) ? $this->post('is_finish') : 0;
		$is_active = ($this->post('is_active')) ? $this->post('is_active') : 1;

        $data = array(
            "invoice" => unformat_invoice($invoice),
            "id_office" => $id_office,
            "no_mesin" => $no_mesin,
            "token" => $token,
            "penggunaan_start" => $penggunaan_start,
            "penggunaan_end" => $penggunaan_end,
            "token_time" => $token_time,
            "sisa_time" => $sisa_time,
            "is_sync" => $is_sync,
            "is_finish" => $is_finish,
            "is_active" => $is_active,
        );

        $insert = $this->db->insert('machine_logs', $data);
        if ($insert) {
            $id = $this->db->insert_id();
            
            $this->response($data, REST_Controller::HTTP_OK);
        } else {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        }
    }

    public function log_put() {
        $invoice = ($this->put('invoice')) ? $this->put('invoice') : "";
        $id_office = ($this->put('id_office')) ? $this->put('id_office') : 0;
        $no_mesin = ($this->put('no_mesin')) ? $this->put('no_mesin') : "";
        $token = ($this->put('token')) ? $this->put('token') : "";

        $penggunaan_start = ($this->put('penggunaan_start')) ? $this->put('penggunaan_start') : "0000-00-00 00:00:00";
        $penggunaan_end = ($this->put('penggunaan_end')) ? $this->put('penggunaan_end') : "0000-00-00 00:00:00";
        $token_time = ($this->put('token_time')) ? $this->put('token_time') : 0;
        $sisa_time = ($this->put('sisa_time')) ? $this->put('sisa_time') : "00:00:00";
        $is_sync = ($this->put('is_sync')) ? $this->put('is_sync') : 0;
        $is_finish = ($this->put('is_finish')) ? $this->put('is_finish') : 0;
        $is_active = ($this->put('is_active')) ? $this->put('is_active') : 1;

        $data = array(
            "penggunaan_start" => $penggunaan_start,
            "penggunaan_end" => $penggunaan_end,
            "token_time" => $token_time,
            "sisa_time" => $sisa_time,
            "is_sync" => $is_sync,
            "is_finish" => $is_finish,
            "is_active" => $is_active,
        );


        if ($invoice == '' || $id_office == 0 || $no_mesin == '' || $token == '') {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
			$invoice= unformat_invoice($invoice);
			
            $params = array();
            if(!empty($invoice)){
                $this->db->where("UPPER(invoice)", strtoupper($invoice));
            }
            if(!empty($id_office)){
                $this->db->where("id_office", $id_office);
            }
            if(!empty($no_mesin)){
                $this->db->where("no_mesin", $no_mesin);
            }
            if(!empty($token)){
                $this->db->where("UPPER(token)", strtoupper($token));
            }

            $update = $this->db->update("machine_logs", $data);

            if ($update) {
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
            }
        }
        
    }
	
	public function log_delete() {
        $invoice = ($this->delete('invoice')) ? $this->delete('invoice') : "";
        $id_office = ($this->delete('id_office')) ? $this->delete('id_office') : 0;
        $no_mesin = ($this->delete('no_mesin')) ? $this->delete('no_mesin') : "";

        if ($invoice == '' || $id_office == 0 || $no_mesin == '' ) {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
			$invoice= unformat_invoice($invoice);
			
            $params = array();
            if(!empty($invoice)){
                $this->db->where("UPPER(invoice)", strtoupper($invoice));
            }
            if(!empty($id_office)){
                $this->db->where("id_office", $id_office);
            }
            if(!empty($no_mesin)){
                $this->db->where("no_mesin", $no_mesin);
            }

            $update = $this->db->delete("machine_logs");

            if ($update) {
                $this->response($data, REST_Controller::HTTP_OK);
            } else {
                $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
            }
        }
        
    }
}
