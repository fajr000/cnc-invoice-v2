<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Tokens extends REST_Controller {

    function __construct(){
        parent::__construct();
    }

    public function index_get() {

        $invoice_number = $this->get('no_inv');
        $machine_number = $this->get('no_machine');
        if (empty($invoice_number) || empty($machine_number)) {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
            $temp = explode("/", $invoice_number);
            $invoice_number = $temp[0]."/".$temp[1]."/".$temp[2]."/".to_digit($temp[3])."/".$temp[4];
            $data = $this->db->query("
                SELECT token
                FROM tokens 
                WHERE invoice_number = '".$invoice_number."' AND no_machine = '".$machine_number."' ")->row();

            $this->response($data, REST_Controller::HTTP_OK);
        }
        
    }
}
