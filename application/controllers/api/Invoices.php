<?php

defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . '/libraries/REST_Controller.php';

use Restserver\Libraries\REST_Controller;

class Invoices extends REST_Controller {

    function __construct(){
        parent::__construct();
    }

    public function start_put() {
        $invoice_number = $this->put('no_inv');
        if (empty($invoice_number)) {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
			$invoice_number = unformat_invoice($invoice_number);
            
            $this->db->query("
                UPDATE accounting_invoices
                SET processed_time = '".get_current_datetime()."', status = 3
                WHERE invoice_number = '".$invoice_number."' ");

            if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
                $this->response(array("status" => "success"), REST_Controller::HTTP_OK);
            }
            else {
                $this->response(array("status" => "fail"), REST_Controller::HTTP_OK);
            }
        }
    }

    public function finish_put() {
        $invoice_number = $this->put('no_inv');
        if (empty($invoice_number)) {
            $this->response(array('status' => 'fail', REST_Controller::HTTP_BAD_GATEWAY));
        } else {
            $invoice_number = unformat_invoice($invoice_number);
            
            $this->db->query("
                UPDATE accounting_invoices
                SET done_time = '".get_current_datetime()."', status = 4
                WHERE invoice_number = '".$invoice_number."' ");

            if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
                $this->response(array("status" => "success"), REST_Controller::HTTP_OK);
            }
            else {
                $this->response(array("status" => "fail"), REST_Controller::HTTP_OK);
            }
        }
    }
}
