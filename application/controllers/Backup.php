<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends MY_Controller {

    public function index()
    {
        $this->load->dbutil();
		$this->load->helper('file');
		
		$config = array(
			'format'	=> 'zip',
			'filename'	=> date("Ymd").'.sql'
		);
		
		$backup = $this->dbutil->backup($config);
		
		$filename = date("Ymd").'.zip';
		$location = FCPATH.'backupdb/'.$filename;
		
		write_file($location, $backup);
    }

    public function download($date = ""){
    	if(empty($date)){
    		$this->load->dbutil();
			$this->load->helper('file');
			
			$config = array(
				'format'	=> 'zip',
				'filename'	=> date("Ymd").'.sql'
			);
			
			$backup = $this->dbutil->backup($config);
			
			$filename = date("Ymd").'.zip';
			$location = FCPATH.'backupdb/'.$filename;
			
			write_file($location, $backup);

			$this->load->helper('download');
			force_download($filename, $backup);
    	}
    	else{
    		$this->load->helper('file');
    		if(file_exists(FCPATH."backupdb/$date.zip")){
    			$this->load->helper('download');
				force_download(FCPATH."backupdb/$date.zip", NULL);
    		}
    		else{
    			echo "The file is not exist.";
    		}
    	}
    }
}
