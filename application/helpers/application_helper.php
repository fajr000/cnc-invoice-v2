<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_menu'))
{
	function set_parent_url($url){
		$CI = &get_instance();
		
		$sql = "
		SELECT id AS top_menu_id, `name` AS top_menu_name 
		FROM `menu` WHERE id IN (
			SELECT `parent_id`
			FROM `menu` 
			WHERE url = '".$url."'
		)
		";

		$sql2 = "
		SELECT `id` AS active_menu_id, `parent_id` AS active_menu_parent_id
		FROM `menu` 
		WHERE url = '".$url."'
		";

		$data  = $CI->db->query($sql)->row_array();
		$data2  = $CI->db->query($sql2)->row_array();
		$CI->session->set_userdata($data);
		$CI->session->set_userdata($data2);
	}

	function get_childs_id($id){
		$CI = &get_instance();
		
		$sql = "
		SELECT *
		FROM `menu` 
		WHERE parent_id = '".$id."'
		";

		$data  = $CI->db->query($sql)->result_array();

		$array = array();
		foreach ($data as $row) {
			$array[] = $row['id'];
		}

		return $array;
	}

    function build_menu()
    {
        $CI = &get_instance();
        $html = "";

        $group_id = $CI->session->userdata('group_id');

        $sql = "
        SELECT mm.*, 
			ga.`create`,ga.`read`,ga.`update`,ga.`delete` 
		FROM  `groups_access` ga 
		LEFT JOIN `menu` mm ON mm.`id` = ga.`menu_id` 
		WHERE ga.`group_id` = $group_id AND ga.`active` = 1 AND ga.`read` = 1 AND mm.`publish` = 1
		ORDER BY mm.`sort`; 
        ";

        $raw  = $CI->db->query($sql)->result_array();

        $html = '
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <li class="nav-header">MAIN MENU</li>
        ';

        $html .= html_ordered_menu($raw);

        $html .= '</ul>';

        return $html;
    }

    function ordered_menu($array,$parent_id = 0)
	{
	  $temp_array = array();
	  foreach($array as $element)
	  {
	    if($element['parent_id']==$parent_id)
	    {
	      $element['subs'] = ordered_menu($array,$element['id']);
	      $temp_array[] = $element;
	    }
	  }
	  return $temp_array;
	}

	function html_ordered_menu($array,$parent_id = 0,$already_opened = 0)
	{
		$CI = &get_instance();

		$menu_html = '';

		if($parent_id == 0) {
		}
		else {
			if(has_child_menu($array, $parent_id) > 0){
				$menu_html = '<ul class="nav nav-treeview">';
			}
		}
	  	
		foreach($array as $element)
		{
			if($element['parent_id']==$parent_id)
			{
				$url = (empty($element['url'])) ? '#' : site_url($element['url']);

				// if($parent_id == 0 && has_child_menu($array, $element['id']) > 0){
				if(has_child_menu($array, $element['id']) > 0){
					// $treeview_active = ($already_opened == 0 && $element['id'] == $CI->session->userdata['top_menu_id'] ) ? 'active treeview menu-open' : 'treeview';
					
					$childs_of_top = get_childs_id($element['id']);
					$is_top = (isset($CI->session->userdata['top_menu_id']) && in_array($CI->session->userdata['top_menu_id'], $childs_of_top)) ? true : false;

					if(($already_opened == 0 && isset($CI->session->userdata['top_menu_id']) && ($element['id'] == $CI->session->userdata['top_menu_id'] || $is_top) && $CI->session->userdata['active_menu_parent_id'] != 0)){
						$treeview_active = 'nav-item menu-open';

						$already_opened = (!$is_top) ? 1 : 0;
					}
					else{
						$treeview_active = 'nav-item';	
					}

					$treeview_active_selected = ($treeview_active == "nav-item menu-open") ? "active" : "";

					$menu_html .= '
						<li class="'.$treeview_active.'">
						<a href="#" class="nav-link '.$treeview_active_selected.'">
							<i class="nav-icon fas '.$element['icon'].'"></i>
							<p>'.$element['name'].'
							<i class="fas fa-angle-left right"></i>
							</p>
						</a>						
					';

					
				} 
				else {
					$active = ($element['id'] == $CI->session->userdata('active_menu_id')) ? "active" : "";
					$menu_html .= "<li class='nav-item'>";
					$menu_html .= '<a href="'.$url.'" class="nav-link '.$active.'" target="_self"><i class="fas '.$element['icon'].' nav-icon"></i><p>'.$element['name'].'</p></a>';
				}
				
				$menu_html .= html_ordered_menu($array,$element['id'],$already_opened);
				$menu_html .= '</li>';
			}
		}

		if($parent_id == 0) {
		}
		else {
			if(has_child_menu($array, $parent_id) > 0)
				$menu_html .= '</ul>';
		}

		return $menu_html;
	}   

	function has_child_menu($array, $id){
		$child = 0;
		foreach ($array as $element) {
			if($element['parent_id'] == $id) {
				$child++;
			}
		}

		return $child;
	}

	function build_access($menu_name) {
		$CI = &get_instance();
        $html = "";

        $group_id = ($CI->session->userdata('group_id')) ? $CI->session->userdata('group_id') : 0;

        $sql = "
        SELECT mm.`url`, 
			ga.`create`,ga.`read`,ga.`update`,ga.`delete` 
		FROM  `groups_access` ga 
		LEFT JOIN `menu` mm ON mm.`id` = ga.`menu_id` 
		WHERE ga.`group_id` = $group_id AND ga.`active` = 1 AND mm.`url` != ''; 
        ";

        $data  = $CI->db->query($sql)->result_array();

        $access = array();
        foreach ($data as $key => $value) {
        	$temp = explode("/", $value['url']);

        	$menu = $temp[(count($temp)-1)];
        	$access[$menu] = array(
        		"create"=>$value['create'],
        		"read"=>$value['read'],
        		"update"=>$value['update'],
        		"delete"=>$value['delete']
        	);
        }

        if(count($access) > 0 && isset($access[$menu_name]))
			return $access[$menu_name];
		else{
			$access[$menu_name] = array(
        		"create"=>0,
        		"read"=>0,
        		"update"=>0,
        		"delete"=>0
        	);
			
			return $access[$menu_name];
		}

	}
	
	function logged_in(){
		$CI = &get_instance();

		if($CI->session->userdata('user_id')){
			return true;
		}


		return false;
	}

	function has_right_access($permission, $destination = "all"){
		if($destination == "crud" && $permission["read"] == 1 && $permission["create"] == 1 && $permission["update"] == 1 && $permission["delete"] == 1 ){
			return true;
		}
		else if($destination == "cud" && $permission["create"] == 1 && $permission["update"] == 1 && $permission["delete"] == 1 ){
			return true;
		}
		else if($destination == "r" && $permission["read"] == 1){
			return true;
		}
		else if($destination == "c" && $permission["create"] == 1){
			return true;
		}
		else if($destination == "u" && $permission["update"] == 1){
			return true;
		}
		else if($destination == "d" && $permission["delete"] == 1){
			return true;
		}
		else {
			return false;
		}
	}

	function redirect_not_allowed(){
		redirect("dashboard/e_405");
	}

	function get_current_datetime() {
		date_default_timezone_set('Asia/Jakarta');
		$date = date('Y-m-d h:i:s', time());

		return $date;
	}

	function get_long_current_date() {
		date_default_timezone_set('Asia/Jakarta');
		$date = date('l d F Y', time());

		$temp = explode(" ", $date);

		return ucwords(to_indonesian_day($temp[0])) .", ".$temp[1]." ".ucwords(to_indonesian_month($temp[2]))." ".$temp[3];
	}

	function get_short_current_date() {
		date_default_timezone_set('Asia/Jakarta');
		$date = date('d F Y', time());

		$temp = explode(" ", $date);

		return $temp[0]." ".ucwords(to_indonesian_month($temp[1]))." ".$temp[2];
	}

	function to_indonesian_day($day = "") {
		switch(strtolower($day)){
			case "sunday":
				return "minggu";
				break;
			case "monday":
				return "senin";
				break;
			case "tuesday":
				return "selasa";
				break;
			case "wednesday":
				return "rabu";
				break;
			case "thursday":
				return "kamis";
				break;
			case "friday":
				return "jumat";
				break;
			case "saturday":
				return "sabtu";
				break;
			default:
				return "";
				break;
		}
	}

	function to_indonesian_month($month = "") {
		switch(strtolower($month)){
			case "january":
				return "januari";
				break;
			case "february":
				return "februari";
				break;
			case "march":
				return "maret";
				break;
			case "april":
				return "april";
				break;
			case "may":
				return "mei";
				break;
			case "june":
				return "juni";
				break;
			case "july":
				return "juli";
				break;
			case "august":
				return "agustus";
				break;
			case "september":
				return "september";
				break;
			case "october":
				return "oktober";
				break;
			case "november":
				return "november";
				break;
			case "december":
				return "desember";
				break;
			default:
				return "";
				break;
		}
	}

	function to_romawi($month = 0) {
		switch(strtolower($month)){
			case 1:
				return "I";
				break;
			case 2:
				return "II";
				break;
			case 3:
				return "III";
				break;
			case 4:
				return "IV";
				break;
			case 5:
				return "V";
				break;
			case 6:
				return "VI";
				break;
			case 7:
				return "VII";
				break;
			case 8:
				return "VIII";
				break;
			case 9:
				return "IX";
				break;
			case 10:
				return "X";
				break;
			case 11:
				return "XI";
				break;
			case 12:
				return "XII";
				break;
			default:
				return "";
				break;
		}
	}

	function to_digit($romawi = "I") {
		switch($romawi){
			case "I":
				return 1;
				break;
			case "II":
				return 2;
				break;
			case "III":
				return 3;
				break;
			case "IV":
				return 4;
				break;
			case "V":
				return 5;
				break;
			case "VI":
				return 6;
				break;
			case "VII":
				return 7;
				break;
			case "VIII":
				return 8;
				break;
			case "IX":
				return 9;
				break;
			case "X":
				return 10;
				break;
			case "XI":
				return 11;
				break;
			case "XII":
				return 12;
				break;
			default:
				return "";
				break;
		}
	}

	function format_invoice($invoice_number){
		// INV/01/001/8/2021
		$temp = explode("/", $invoice_number);
		if(count($temp) == 5){
			$month = to_romawi($temp[3]);

			return $temp[0]."/".$temp[1]."/".$temp[2]."/".$month."/".$temp[4];
		}

		return $invoice_number;		
	}
	
	function unformat_invoice($invoice_number){
		// INV/01/001/VIII/2021
		$temp = explode("/", $invoice_number);
		if(count($temp) == 5){
			$month = to_digit($temp[3]);
			$month = str_pad($month, 2, "0", STR_PAD_LEFT);

			return $temp[0]."/".$temp[1]."/".$temp[2]."/".$month."/".$temp[4];
		}

		return $invoice_number;		
	}

	function format_money($number) {
		if(!empty($number))
			return number_format($number, 2, ',', '.');
		else
			return 0;
	}

	function unformat_money($money) {
		$money = str_replace(".", "", $money);
		return str_replace(",", ".", $money);
	}

	function number_to_text($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = number_to_text($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = number_to_text($nilai/10)." puluh". number_to_text($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . number_to_text($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = number_to_text($nilai/100) . " ratus" . number_to_text($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . number_to_text($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = number_to_text($nilai/1000) . " ribu" . number_to_text($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = number_to_text($nilai/1000000) . " juta" . number_to_text($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = number_to_text($nilai/1000000000) . " milyar" . number_to_text(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = number_to_text($nilai/1000000000000) . " trilyun" . number_to_text(fmod($nilai,1000000000000));
		}     
		return $temp;
	}

	function is_admin(){
		$CI = &get_instance();

		if(strtolower($CI->session->userdata('group_name')) == 'administrator'){
			return true;
		}


		return false;
	}

	function is_center_admin(){
		$CI = &get_instance();

		if(strtolower($CI->session->userdata('group_name')) == 'admin pusat'){
			return true;
		}


		return false;
	}

	function is_branch_admin(){
		$CI = &get_instance();

		if(strtolower($CI->session->userdata('group_name')) == 'admin cabang'){
			return true;
		}


		return false;
	}

	function is_customer_services(){
		$CI = &get_instance();

		if(strtolower($CI->session->userdata('group_name')) == 'customer service (cs)'){
			return true;
		}


		return false;
	}

	function is_operator(){
		$CI = &get_instance();

		if(strtolower($CI->session->userdata('group_name')) == 'operator alat'){
			return true;
		}


		return false;
	}

	function get_office_id(){
		$CI = &get_instance();

		$sql = "
		SELECT o.`id`, o.`name` AS name
		FROM users u 
		INNER JOIN offices o ON o.`id` = u.`id_office` 
		WHERE u.`id` = " . $CI->session->userdata('user_id') . "
		";

		$data  = $CI->db->query($sql)->row();

		return $data;
	}

	function get_plat_types(){
		$CI = &get_instance();

		$sql = "
		SELECT *
		FROM plat_types 
		WHERE deleted = 0 
		";

		$data  = $CI->db->query($sql)->result();

		return $data;
	}

	function get_plat_sizes(){
		$CI = &get_instance();

		$sql = "
		SELECT *
		FROM plat_sizes 
		ORDER BY size
		";

		$data  = $CI->db->query($sql)->result();

		return $data;
	}

	function get_cutting_types(){
		$CI = &get_instance();

		$sql = "
		SELECT *
		FROM cutting_types 
		WHERE deleted = 0 
		";

		$data  = $CI->db->query($sql)->result();

		return $data;
	}

	function parse_size($size) {
	  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
	  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
	  if ($unit) {
	    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
	    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
	  }
	  else {
	    return round($size);
	  }
	}

	function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
	}

	function is_valid_image($image_url){
		$filename = $image_url;
		$error = '';   
		$valid = true;
		$type = exif_imagetype( $filename );
		switch( $type ) {
			case 1:
			$isimage = @imagecreatefromgif( $filename );
			$error .= ( !$isimage ) ? "extn - gif, but not a valid gif" : '  valid gif';
			$valid  = ( !$isimage ) ? false : true;
			break;
			case 2: 
			$isimage = @imagecreatefromjpeg( $filename );
			$error .= ( !$isimage ) ? "extn - jpg, but not a valid jpg" : '  valid jpg';
			$valid  = ( !$isimage ) ? false : true;
			break;
			case 3:
			echo "png : ";
			$isimage = @imagecreatefrompng( $filename );
			$error .= ( !$isimage ) ? "extn - png, but not a valid png" : ' valid png';
			$valid  = ( !$isimage ) ? false : true;
			break;
			default: //if there is no exif data
			$error .= "Not an image" ;
			$valid  = false;
		}

		return $valid;
	}

	function generate_token($invoice_number, $no_machine, $no_order, $minutes){
		$CI = &get_instance();

		$result = "";

		if(!empty($invoice_number) && !empty($no_machine) && !empty($no_order) && !empty($minutes)){

			/*
			format plaintext: INV_KODE CABANG_TANGGAL_BULAN_TAHUN_#_NO.TOKEN_MENIT
			format key: J_KODE MESIN_KODE CABANG_12345ABCDEFGHI

			contoh (1):
			$invoice_number = "INV/01/001/VIII/2021";
			$no_order = "1";
			$minutes = 999;
			$no_machine = "B01";

			format plaintext: NV01001VIII2021#1999 --> I pada INV dihilangkan
			format key: JB010112345ABCDEFGHI

			contoh (2):
			$invoice_number = "INV/01/001/V/2021";
			$no_order = "1";
			$minutes = 999;
			$no_machine = "B01";

			format plaintext: NV01001V0002021#1999 --> sisa digit dari bulan isi sebelah kanan dengan 0 (nol) --> I pada INV dihilangkan
			format key: JB010112345ABCDEFGHI
			*/

			$temp = explode("/", $invoice_number);
			$month = $temp[3];
			$month = str_pad($month, 4, "0", STR_PAD_RIGHT);
			$invoice_number = "V".$temp[1].$temp[2].$month.$temp[4];
			$plaintext = str_replace("/", "", $invoice_number)."#".$no_order.str_pad($minutes, 4, "0", STR_PAD_LEFT);
			$key = "J".$no_machine.$temp[1]."12345ABCDEFGHI";

			$result = array(
				"plaintext" => $plaintext,
				"key_token" => $key,
				"token" => strtoupper(bin2hex(venkrip($key, $plaintext)))
			);
		}


		return $result;
		
	}

	function venkrip($key,$plaintext){
		$j = $i = $shift = $offset = 0;
		$hasil = "";

		$shift_arr = array();
		for($i=0;$i<strlen($plaintext);$i++){
			$shift = ord($key[$j]) - 47;

			$offset = (ord($plaintext[$i]) + $shift) % 256;
			$hasil .= chr($offset);

			$j++;
		}
		
		return $hasil;
	}

	function vcdecrypt($key,$encryptedtext){
		$j = $i = $shift = $offset = 0;
		$hasil = "";

		$shift_arr = array();
		for($i=0;$i<strlen($encryptedtext);$i++){
			$shift = ord($key[$j]) - 47;

			$offset = (ord($encryptedtext[$i]) - $shift) % 256;
			if($offset < 0){
				$offset+=256;
			}
			$hasil .= chr($offset);

			$j++;
		}
		
		return $hasil;
	}

	function object_to_array($objects){
		$result = array();
		
		foreach ($objects as $obj) {

			foreach($obj as $key => $val){
				$temp[$key] = $val;
			}
			$result[] = $temp;
		}
		
		return $result;

	}
}