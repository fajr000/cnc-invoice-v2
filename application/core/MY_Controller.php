<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* load the MX_Loader class */
require APPPATH."third_party/MX/Controller.php";

class MY_Controller extends MX_Controller {
	protected $data = array();

	//config pagination
	protected $offset = 20;
	
	function __construct(){
		parent::__construct();
		$this->data['page_title'] = '';
		$this->data['before_head'] = '';
		$this->data['before_body'] ='';

		unset($_SESSION['notif']);
	}
	
	protected function render($the_view = NULL, $template = 'master', $html = FALSE){
		if($template == 'json' || $this->input->is_ajax_request()){
			header('Content-Type: application/json');
			echo json_encode($this->data);
		}
		else{
			$this->data['the_view_content'] = (is_null($the_view)) ? '' : $this->load->view($the_view,$this->data, TRUE);
			$this->load->view('templates/'.$template.'_view', $this->data, $html);
		}
	}
}

class Admin_Controller extends MY_Controller {
	function __construct(){
		parent::__construct();

		$this->load->library('ion_auth');
	    if (!logged_in())
	    {
	      //redirect them to the login page
	      redirect('user/login', 'refresh');
	    }

		$this->data['page_title'] = '';

		$class_name  = $this->router->fetch_class();
		$method_name = $this->router->fetch_method();
		$permission  = build_access($class_name);

		if(!has_right_access($permission, "r")){
	      redirect_not_allowed();
	    }

	    if(!has_right_access($permission, "c") && $method_name == "add"){
	      redirect_not_allowed();
	    }

	    if(!has_right_access($permission, "d") && $method_name == "delete"){
	      redirect_not_allowed();
	    }
	}
	
	protected function render($the_view = NULL, $template = 'admin_master', $html = FALSE){
		parent::render($the_view, $template, $html);
	}
}

class Public_Controller extends MY_Controller {
	function __construct(){
		parent::__construct();
	}
}