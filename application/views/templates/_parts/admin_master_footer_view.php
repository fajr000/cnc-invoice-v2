<?php
if(logged_in()) {
?>
	<footer class="main-footer">
		<div class="float-right d-none d-sm-block">
			<b>Version</b> 1.0
		</div>
		<strong>&copy; 2021.</strong> All rights reserved.
	</footer>
</div>
<!-- ./wrapper -->
<?php
}
?>
 
<!-- jQuery -->
<script src="assets/libs/adminlte-3.1.0/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="assets/libs/adminlte-3.1.0/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="assets/libs/adminlte-3.1.0/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- Select2 -->
<script src="assets/libs/adminlte-3.1.0/plugins/select2/js/select2.full.min.js"></script>
<!-- Date-Range-Picker -->
<script src="assets/libs/adminlte-3.1.0/plugins/moment/moment.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="assets/libs/adminlte-3.1.0/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- AdminLTE App -->
<script src="assets/libs/adminlte-3.1.0/dist/js/adminlte.min.js"></script>
<!-- Ekko Lightbox -->
<script src="assets/libs/adminlte-3.1.0/plugins/ekko-lightbox/ekko-lightbox.min.js"></script>
<!-- Summernote -->
<script src="assets/libs/adminlte-3.1.0/plugins/summernote/summernote-bs4.min.js"></script>
<!-- JQuery UI -->
<script src="assets/libs/adminlte-3.1.0/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- HoldOn JS -->
<script src="assets/libs/holdon-js/js/HoldOn.js"></script>
<!-- Mask JS -->
<script src="assets/libs/jquery-mask/jquery.mask.min.js"></script>
<!-- Toastr JS --> 
<script src="assets/libs/adminlte-3.1.0/plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/toastr/toastr.min.js"></script>
<!-- DataTables  & Plugins -->
<script src="assets/libs/adminlte-3.1.0/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<!-- <script src="assets/libs/adminlte-3.1.0/plugins/jszip/jszip.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/pdfmake/pdfmake.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/pdfmake/vfs_fonts.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/js/buttons.colVis.min.js"></script> -->

<!-- Custom apps JS -->
<script type="text/javascript" src="assets/js/apps.js"></script>

<?php echo $before_body;?> 
</body>
</html>