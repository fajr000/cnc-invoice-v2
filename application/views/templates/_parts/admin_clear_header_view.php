<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<?php echo base_url();?>" target="_blank">
<title><?php echo $page_title;?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> 
<!-- Font Awesome -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/fontawesome-free/css/all.min.css">
<!-- Ekko Lightbox -->
  <link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/ekko-lightbox/ekko-lightbox.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
 <!-- iCheck -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
  <link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/summernote/summernote-bs4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/select2-bootstrap4-theme/select2-bootstrap4.css">
<!-- JQuery UI -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/jquery-ui/jquery-ui.css">
<!-- HoldOn JS -->
<link rel="stylesheet" href="assets/libs/holdon-js/css/HoldOn.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Toastr JS -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/toastr/toastr.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<?php echo $before_head;?>
</head>
<body class="layout-top-nav" style="height: auto;">
  <div class="wrapper"> <!-- wrapper-->
    <div class="content-wrapper"> <!-- content wrapper-->
