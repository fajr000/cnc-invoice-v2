<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<base href="<?php echo base_url();?>" target="_blank">
<title><?php echo $page_title;?></title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

<!-- Google Font: Source Sans Pro --> 
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/fontawesome-free/css/all.min.css">
<!-- Ekko Lightbox -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/ekko-lightbox/ekko-lightbox.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- Tempusdominus Bootstrap 4 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
 <!-- iCheck -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/dist/css/adminlte.min.css">
<!-- overlayScrollbars -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
<!-- Daterange picker -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/daterangepicker/daterangepicker.css">
<!-- summernote -->
  <link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/summernote/summernote-bs4.min.css">
<!-- Select2 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/select2/css/select2.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/select2-bootstrap4-theme/select2-bootstrap4.css">
<!-- JQuery UI -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/jquery-ui/jquery-ui.css">
<!-- HoldOn JS -->
<link rel="stylesheet" href="assets/libs/holdon-js/css/HoldOn.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- Toastr JS -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/toastr/toastr.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
<link rel="stylesheet" href="assets/libs/adminlte-3.1.0/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
<?php echo $before_head;?>

<style type="text/css">
[class*=sidebar-dark-] .nav-sidebar>.nav-item>.nav-treeview {
     /*background-color: transparent; */
    /*background-color: #adcbf0;
    border-radius: .25rem;*/
    
}
[class*=sidebar-dark-] .nav-treeview>.nav-item>.nav-link {
  margin-left: 10px;
   /* color:  #fff;
    background-color: #6c757d;*/
}
[class*=sidebar-dark-] .nav-treeview>.nav-item>.nav-link:hover {
    /*color:  #000;
    /*background-color: #fafafa;*/

}
.toast {
    -webkit-flex-basis: 350px;
    -ms-flex-preferred-size: 350px;
    flex-basis: 350px;
    max-width: 350px;
    font-size: .875rem;
    background-color: rgba(255,255,255,.85) !important;
    background-clip: padding-box;
    border: 1px solid rgba(0,0,0,.1);
    box-shadow: 0 0.25rem 0.75rem rgb(0 0 0 / 10%);
    opacity: 0;
    border-radius: .25rem;
}
.toast.bg-warning {
    background-color: rgb(255 255 255 / 90%)!important;
}
.bg-warning, .bg-warning>a {
    color: #7f7a7a!important;
}
.toast.bg-info {
    background-color: rgb(255 255 255 / 90%)!important;
}
.bg-info, .bg-info>a {
    color: #7f7a7a!important;
}
.toast.bg-danger {
    background-color: rgb(255 255 255 / 90%)!important;
    color: #333;
}
.bg-danger, .bg-danger>a {
    color: #7f7a7a!important;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
  }
</style>
</head>
<body class="<?php if(logged_in()){?>control-sidebar-slide-open layout-fixed layout-navbar-fixed<?php }else{?>login-page<?php }?>">
<?php
if(logged_in()) {
?>
<!-- Site wrapper -->
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown user-menu">
        <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
          <img src="assets/libs/adminlte-3.1.0/dist/img/user2-160x160.jpg" class="user-image img-circle elevation-2" alt="User Image">
          <span class="d-none d-md-inline"><?php echo $this->session->userdata('name');?></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <!-- User image -->
          <li class="user-header bg-primary">
            <img src="assets/libs/adminlte-3.1.0/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">

            <p>
              <?php echo $this->session->userdata('name') . ' - ' . $this->session->userdata('group');?>
              <small>Last login <?php echo $this->session->userdata('old_last_login');?></small>
            </p>
          </li>
          <!-- Menu Footer-->
          <li class="user-footer">
            <a href="user/logout" target="_self" class="btn btn-default btn-flat float-right">Log out</a>
          </li>
        </ul>
      </li>

      <li class="nav-item">
        <a class="nav-link" data-widget="fullscreen" href="#" role="button">
          <i class="fas fa-expand-arrows-alt"></i>
        </a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link" target="_self">
      <img src="assets/libs/adminlte-3.1.0/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">PioneerCNC ID</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="assets/libs/adminlte-3.1.0/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"><?php echo $this->session->userdata('name');?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <?php
          echo build_menu();
        ?>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->

  </aside>
<?php
}
?>