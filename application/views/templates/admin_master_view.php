<?php defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('templates/_parts/admin_master_header_view'); ?>
 
<!-- Content Wrapper. Contains page content -->
	<?php
	if(logged_in()) {
	?>
	<div class="content-wrapper" style="min-height: 946.3px;">
	<?php }?>

	<?php echo $the_view_content; ?>
	
	<?php
	if(logged_in()) {
	?>
	</div>
	<?php }?>
<!-- /.content-wrapper -->

<?php $this->load->view('templates/_parts/admin_master_footer_view');?>