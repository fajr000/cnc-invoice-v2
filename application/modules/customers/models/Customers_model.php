<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers_model extends CI_Model
{
	private $_table = 'customers';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND c.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				c.`id` as id,
				c.`name`, 
				c.`name_contact`, 
				c.`address`, 
				c.`phone`, 
				c.`email`,
				c.`id_office`,
				c.`id_sales`,
				IFNULL(s.`name`, '') as name_sales,
				o.`name` as name_office
			FROM `customers` c 
			LEFT JOIN `sales` s ON s.`id` = c.`id_sales`
			LEFT JOIN `offices` o ON o.`id` = c.`id_office`
			WHERE c.`deleted` = 0 $filter_office
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND c.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				c.`id` as id,
				c.`name`, 
				c.`name_contact`, 
				c.`address`, 
				c.`phone`, 
				c.`email`,
				c.`id_office`,
				c.`id_sales`,
				IFNULL(s.`name`, '') as name_sales,
				o.`name` as name_office
			FROM `customers` c 
			LEFT JOIN `sales` s ON s.`id` = c.`id_sales`
			LEFT JOIN `offices` o ON o.`id` = c.`id_office`
			WHERE c.`deleted` = 0 $filter_office
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				c.`id` as id,
				c.`name`, 
				c.`name_contact`, 
				c.`address`, 
				c.`phone`, 
				c.`email`,
				c.`id_office`,
				c.`id_sales`,
				IFNULL(s.`name`, '') as name_sales,
				o.`name` as name_office
			FROM `customers` c 
			LEFT JOIN `sales` s ON s.`id` = c.`id_sales`
			LEFT JOIN `offices` o ON o.`id` = c.`id_office`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$data['created_by'] = $this->session->userdata("user_id");
		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, 
			array(
				"deleted" => 1, 
				"deleted_on" => get_current_datetime(),
				"deleted_on" => $this->session->userdata("user_id")
			)
		);


		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function get_invoice_due($id_office, $id_customer) {
		$sql = "
		SELECT vid.*
		FROM v_invoice_due vid
		WHERE vid.`id_office` = $id_office
		AND vid.`id_customer` = $id_customer 
		AND vid.`current_due` > 0 
		";

		return $this->db->query($sql)->result();
	}

	public function get_amount_due($id_office, $id_customer) {

		$sql = "
		SELECT cr.*, sl.`id_office`, SUM(vid.`current_due`) due
		FROM customers cr
		LEFT JOIN sales sl ON sl.`id` = cr.`id_sales`
		LEFT JOIN v_invoice_due vid ON vid.`id_office` = cr.`id_office` AND vid.`id_customer` = cr.`id`
		WHERE cr.`id_office` = $id_office
			AND cr.`id` = $id_customer 
		GROUP BY cr.`id_office`, cr.`id`;
		";

		return $this->db->query($sql)->row();
	}

	public function get_invoices($id_office, $id_customer, $id_invoice = "", $posted = 1){
		$filter_posted  = ($posted > 0) ? " AND vi.posted = $posted " : "";
		$filter_invoice = (!empty($id_invoice)) ? " AND vi.id_invoice = $id_invoice " : "";
		$sql = "
		SELECT vi.*, u.`first_name` AS name_operator
		FROM v_invoices vi 
		LEFT JOIN users u ON u.`id` = vi.`id_user`
		WHERE vi.`id_office` = $id_office 
			AND vi.`id_customer` = $id_customer 
			$filter_posted 
			AND vi.`transaction_type` = 1 
			$filter_invoice 
		ORDER BY vi.`id_transaction` ASC, vi.`id_invoice` ASC;
		";

		return $this->db->query($sql)->result();
	}

	public function get_transaction_history($id_office, $id_customer){
		$sql = "
		SELECT * 
		FROM v_invoices vi
		WHERE vi.`id_office` = $id_office 
			AND vi.`id_customer` = $id_customer 
		ORDER BY vi.`id_transaction` ASC, vi.`id_invoice` ASC;
		";

		return $this->db->query($sql)->result();
	}

	public function get_rank($id_office, $id_customer){
		$sql = "
		SELECT * 
		FROM v_customer_period_rank_prev_month 
		WHERE id_office = $id_office
		AND id_customer = $id_customer
		";

		return $this->db->query($sql)->row();
	}

}
