<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Customers extends Admin_Controller
{ 
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Customers_model', 'customer');
    $this->load->model('offices/Offices_model', 'office'); 
    $this->load->model('sales/Sales_model', 'sales');    

    $this->data['page_title'] = 'Customer | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Customer';
    $this->data['page_subheader'] = 'Pelanggan';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_customer_view', 'admin_master');
  }

  public function get_customers_list()
  {
    // populate data
    $params = array();

    $all_items = $this->customer->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" . $row->id . ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->customer->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();
    $this->data['sales'] = $this->sales->get_all_items();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_customer_view', 'admin_master'); 
  
}
  public function edit($id_item, $param = "")
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->customer->get_detail_item($id_item);
    $due = $this->customer->get_amount_due($item->id_office, $id_item);
    

    if(!has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();
    $this->data['sales'] = $this->sales->get_all_items();
    $this->data['param'] = $param;
    $this->data['due'] = isset($due) ? $due : (object)array("amount" => 0);

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_customer_view', 'admin_master');
  }

  public function delete($id_item)
  {
    $result = $this->customer->delete_item($id_item);   

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('customers');
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id       = $this->input->post('id');
    $name     = $this->input->post('name');
    $cp       = $this->input->post('name_contact');
    $address  = $this->input->post('address');
    $phone    = $this->input->post('phone');
    $email    = $this->input->post('email');
    $id_office = ($this->input->post('id_office') && !empty($this->input->post('id_office'))) ? $this->input->post('id_office') : 0;
    $id_sales = ($this->input->post('id_sales') && !empty($this->input->post('id_sales'))) ? $this->input->post('id_sales') : 0;

    $temp = array(
      'id' => $id,
      'name' => $name,
      'name_contact' => $cp,
      'address' => $address,
      'phone' => $phone,
      'email' => $email,
      'id_office' => $id_office,
      'id_sales' => $id_sales,
    );

    // validate form input
    $this->form_validation->set_rules('name', 'Nama', 'trim|required');

    $error = "";
    if ($this->form_validation->run($this) === TRUE){

      $data = array(
        'name' => $name,
        'name_contact' => $cp,
        'address' => $address,
        'phone' => $phone,
        'email' => $email,
        'id_office' => $id_office,  
        'id_sales' => $id_sales,
      );

      //register
      if(empty($id)) {
          $result = $this->customer->insert_item($data);

          if($result > 0){
            $id = $result;
          }
      }
      //edit
      else {
          $result = $this->customer->update_item($id, $data);
          if($result > 0) {
            
          }
      }
    }
    else{
      $result = 0;

      $error =  str_replace("<p>", "", validation_errors());
      $error =  str_replace("</p>", "<br/>", $error);
    }

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil disimpan.';

      $this->session->set_flashdata('notif', $notif);
      redirect('customers');
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = '<strong>Data gagal disimpan!</strong>' . '<br/>' . $error;

      $this->session->set_flashdata('notif', $notif);

      // populate data
      $item = $temp;

      $this->data['new'] = true;
      $this->data['item'] = (object)$item;
      $this->data['sales'] = $this->sales->get_all_items();
    
      $this->data['before_body'] = $this->load->view('script', '', true);

      $this->render('edit_customer_view', 'admin_master'); 
    }
  }

  public function get_customer_invoice_list($id_office, $id_customer ) {
    $inv = $this->customer->get_invoices($id_office, $id_customer);

    $inv_list = array();
    foreach ($inv as $row) {
      $inv_list[$row->id_invoice]["id_invoice"] = $row->id_invoice;
      $inv_list[$row->id_invoice]["id_office"] = $row->id_office;
      $inv_list[$row->id_invoice]["id_customer"] = $row->id_customer;
      $inv_list[$row->id_invoice]["invoice_number"] = format_invoice($row->invoice_number);
      $inv_list[$row->id_invoice]["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
      $inv_list[$row->id_invoice]["cancelled_on"] = $row->cancelled_on;
      $inv_list[$row->id_invoice]["posted"] = $row->posted;

      $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
      $cancelled = ($row->cancelled_on != "0000-00-00 00:00:00") ? "cancelled" : "";
      $cancelled_status = ($row->cancelled_on != "0000-00-00 00:00:00") ? '<small class="badge badge-danger"></i> Dibatalkan</small>' : '';
      if(isset($inv_list[$row->id_invoice]["item"]))
        $inv_list[$row->id_invoice]["item"] .= "<span class='".$cancelled."'><span class='normal-text'>".$row->product." ".$row->machine." ".$row->name_plat." ".$row->size."mm (".$row->qty.") $minutes</span></span>".$cancelled_status."<br>";
      else
        $inv_list[$row->id_invoice]["item"] = "<span class='btn btn-warning btn-sm'>".format_invoice($row->invoice_number)."</span><br/><span class='".$cancelled."'><span class='normal-text'>".$row->product." ".$row->machine." ".$row->name_plat." ".$row->size."mm (".$row->qty.") $minutes</span></span>".$cancelled_status."<br>";

      if(isset($inv_list[$row->id_invoice]["due"]))
        $inv_list[$row->id_invoice]["due"] += $row->due;
      else
        $inv_list[$row->id_invoice]["due"] = $row->due;

      $inv_list[$row->id_invoice]["invoice_amount"] = $row->invoice_amount;
      $inv_list[$row->id_invoice]["invoice_discount"] = $row->invoice_discount;
      $inv_list[$row->id_invoice]["invoice_discount_amount"] = $row->invoice_discount_amount;
      $inv_list[$row->id_invoice]["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
    }

    $temp = array();
    $i=1;
    foreach($inv_list as $key => $row){
      $row["no"] = $i;
      $tools = "";
      if($row["cancelled_on"] == "0000-00-00 00:00:00"){
        $tools = '
        <select name="tools[]" class="form-control tool-act" onchange="invoiceTodo(this,'.$key.')">
          <option>--Pilih--</option>
          <option value="detail">Detail</option>
          <option value="cancel">Batal</option>
          <option value="delete">Hapus</option>
        </select>
        ';
      }
      else{
        if($row["posted"]){
          $tools = '
          <select name="tools[]" class="form-control tool-act" onchange="invoiceTodo(this,'.$key.')">
            <option>--Pilih--</option>
            <option value="delete">Hapus</option>
          </select>
          ';
        }
      }

      $row["select"] = $tools;

      $temp[] = $row;
      $i++;
    }

    echo json_encode(array("data"=>$temp));
  }

  public function get_customer_transaction_history($id_office, $id_customer ) {
    $inv = $this->customer->get_transaction_history($id_office, $id_customer);

    $inv_list = array();
    foreach ($inv as $row) {
      if($row->transaction_type == 1){
        $inv_list[$row->id_transaction."_".$row->id_invoice]["transaction_type"] = $row->transaction_type;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["id_invoice"] = $row->id_invoice;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["id_office"] = $row->id_office;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["id_customer"] = $row->id_customer;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["invoice_number"] = format_invoice($row->invoice_number);
        $inv_list[$row->id_transaction."_".$row->id_invoice]["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
        $inv_list[$row->id_transaction."_".$row->id_invoice]["cancelled_on"] = $row->cancelled_on;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["posted"] = $row->posted;

        $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
        $cancelled = ($row->cancelled_on != "0000-00-00 00:00:00") ? "cancelled" : "";
        $cancelled_status = ($row->cancelled_on != "0000-00-00 00:00:00") ? ' <small class="badge badge-danger"></i> Dibatalkan</small>' : "";
        if(isset($inv_list[$row->id_transaction."_".$row->id_invoice]["item"]))
          $inv_list[$row->id_transaction."_".$row->id_invoice]["item"] .= "<span class='".$cancelled."'><span class='normal-text'>".$row->product." ".$row->machine." ".$row->name_plat." ".$row->size."mm (".$row->qty.") $minutes</span></span>".$cancelled_status."<br>";
        else
          $inv_list[$row->id_transaction."_".$row->id_invoice]["item"] = "<span class='btn btn-warning btn-sm'>".format_invoice($row->invoice_number)."</span><br><span class='".$cancelled."'><span class='normal-text'>".$row->product." ".$row->machine." ".$row->name_plat." ".$row->size."mm (".$row->qty.") $minutes</span></span>".$cancelled_status."<br>";

        if(isset($inv_list[$row->id_transaction."_".$row->id_invoice]["due"]))
          $inv_list[$row->id_transaction."_".$row->id_invoice]["due"] += $row->due;
        else
          $inv_list[$row->id_transaction."_".$row->id_invoice]["due"] = $row->due;

        $inv_list[$row->id_transaction."_".$row->id_invoice]["invoice_amount"] = $row->invoice_amount;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["invoice_discount"] = $row->invoice_discount;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["invoice_discount_amount"] = $row->invoice_discount_amount;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
        $inv_list[$row->id_transaction."_".$row->id_invoice]["payment"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice]["payment_method"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice]["button"] = '
        <a class="view-invoice" onclick="openWindow(\'transactions/get_invoice/'.$row->id_office.'/'.$row->id_customer.'/'.$row->id_invoice.'\', \'Lihat Invoice\', 1000, 600 )" ><i class="fa fa-eye"></i></a>
        ';
      }
      else if($row->transaction_type == 2){

        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["transaction_type"] = $row->transaction_type;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["id_invoice"] = $row->id_invoice;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["id_office"] = $row->id_office;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["id_customer"] = $row->id_customer;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["invoice_number"] = $row->invoice_number;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["cancelled_on"] = $row->cancelled_on;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["posted"] = $row->posted;

        $temp = explode(",", $row->invoice_number);
        $temp_cancelled = explode(",", $row->cancelled_invoice);

        for($i=0;$i<count($temp_cancelled);$i++){
          if($temp_cancelled[$i] == 1){
            $temp[$i] = '<span class="'.$cancelled.'"><span class="text-success">'.format_invoice($temp[$i]).'</span></span>';
          }
          else{
            $temp[$i] = '<span class=""><span class="text-success">'.format_invoice($temp[$i]).'</span></span>'; 
          }
        }


        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["item"] = "<span class='text-success'>Pembayaran invoice <strong>".implode("</strong> &amp; <strong>", $temp)."</strong></span>";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["due"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["payment"] = $row->amount;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["payment_method"] = $row->payment_method;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["button"] = '
        <a class="view-invoice" onclick="openWindow(\'transactions/get_payment/'.$row->id_office.'/'.$row->id_customer.'/'.$row->id_payment.'\', \'Lihat Pembayaran\', 1000, 600 )" ><i class="fa fa-eye"></i></a>
        ';
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["invoice_amount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["invoice_discount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["invoice_discount_amount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_2"]["invoice_due"] = "";
      }
      else if($row->transaction_type == 3){
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["transaction_type"] = $row->transaction_type;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["id_invoice"] = $row->id_invoice;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["id_office"] = $row->id_office;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["id_customer"] = $row->id_customer;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["invoice_number"] = $row->invoice_number;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["cancelled_on"] = $row->cancelled_on;
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["posted"] = $row->posted;
        $temp = explode(",", $row->invoice_number);

         for($i=0;$i<count($temp);$i++){
          $temp[$i] = format_invoice($temp[$i]);
         }

        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["item"] = "<span class='text-primary'>Pembatalan invoice <strong>".implode("</strong> &amp; <strong>", $temp)."</strong></span>";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["due"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["payment"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["payment_method"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["button"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["invoice_amount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["invoice_discount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["invoice_discount_amount"] = "";
        $inv_list[$row->id_transaction."_".$row->id_invoice."_3"]["invoice_due"] = "";
      }
    }

    $temp = array();
    $i=1;
    foreach($inv_list as $key => $row){
      $row["no"] = $i;
      $temp[] = $row;

      $i++;
    }

    echo json_encode(array("data"=>$temp));
  }

  public function get_sales(){
    $id_office = $this->input->post("id_office");
    $sales = $this->db->query("SELECT * FROM sales WHERE id_office = $id_office")->result();

    $option_arr = array();
    $option_str = "";

    $option_arr[] = '<option value="0"> &nbsp;</option>';
    foreach ($sales as $key => $row) {
      $option_arr[] = '<option value="'.$row->id.'">'.$row->name.'</option>';
    }

    if(count($option_arr) > 0){
      $option_str = implode("", $option_arr);
    }

    echo $option_str;
  }

} 