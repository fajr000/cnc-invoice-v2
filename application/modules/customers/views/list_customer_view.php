<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
  }
  
  .no{
    text-align: center;
    width: 2%;
  }
  .nama{
    vertical-align: middle;
    text-align: left;
    width: 20%; 
  }
  td.highlight a{
    color:  blue;
  }
  td.highlight a:hover{
    color:  blue;
    text-decoration: underline;
  }
  td.highlight a:visited{
    color:  blue;
    text-decoration: underline;
  }
  .alamat{
    vertical-align: middle;
    text-align: left;
    width: 40%; 
  } 
  .telp{
    vertical-align: middle;
    text-align: left;
    width: 15%; 
  } 
  .sales{
    vertical-align: middle;
    text-align: left;
    width: 15%; 
  } 
  .tools{
    text-align: center;
    width: 8%;
  }   
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
              <div class="col-md-12 pull-left">
                <?php
                if($allowed['create']){
                ?>
                <a href="customers/add/" class="btn-primary btn-sm" target="_self"><span>TAMBAH BARU</span></a>
                <?php
                }
                ?>
              </div>
            </div>
              <br/>
              <div class="row">
                <div class="col-sm-12">
                  <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                    <thead>
                      <tr>
                        <th class="no">&nbsp;</th>
                        <th class="nama">Nama</th>
                        <th class="alamat">Alamat</th>
                        <th class="telp">Telp</th>
                        <th class="sales">Nama Sales</th>
                        <th class="tools">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->