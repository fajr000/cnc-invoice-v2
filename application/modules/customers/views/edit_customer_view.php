<!-- Content Header (Page header) -->
<style type="text/css">
  .btn-file {
      position: relative;
      overflow: hidden;
  }
  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      font-size: 100px;
      text-align: right;
      filter: alpha(opacity=0);
      opacity: 0;
      outline: none;
      background: white;
      cursor: inherit;
      display: block;
  }
  .bold-text {
    font-weight: bold;
  }
  .suggestion {
    font-size: 12px;
  }
  .radio-label {
    display: inline-block;
    vertical-align: bottom;
    margin-right: 3%;
    font-weight: normal;
    line-height: 15px;
  }
  .radio-input {
      display: inline-block;
      vertical-align: bottom;
      margin-left: 0px;
  }
  .label-form {
      vertical-align: middle !important; 
  }
  .price{
    text-align: right;
  }
  .date{
    width: 10%;
    text-align: center;
    vertical-align: top;
  }
  .no{
    text-align: center;
    width: 2%;
    vertical-align: top;
  }
  .itemName{
    width: 60%;
    text-align: left;
    vertical-align: top;
  }
  .itemName2{
    width: 45%;
    text-align: left;
    vertical-align: top;
  }
  .due{
    width: 15%;
    text-align: right;
    vertical-align: top;
  }
  .payment{
    width: 15%;
    text-align: right;
    vertical-align: top;
  }
  .tools{
    width: 13%;
    text-align: center;
    vertical-align: top;
  }
  .cancelled{
    text-decoration: line-through;
    font-style: italic;
    color: #dc3545;
  }
  .cancel-string{
    color: #dc3545;
    font-style: italic;
  }
  .label-cancellation{
    color: blue;
    font-style: italic;
  }
  .normal-text{
    color:  #000;
  }

</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title">Data Customer <?php echo isset($item) ? "<span class='text-primary'>: <strong>".$item->name."</strong></span>" : '';?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
              <div class="col-md-12" id="form_user">
                <input type="hidden" name="paramTab" id="paramTab" value="<?php echo (isset($param)) ? $param : "";?>">
                <div class="card card-primary card-outline card-outline-tabs">
                  <div class="card-header p-0 pt-1 border-bottom-0">
                    <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                      <?php if(isset($item)){?>
                      <li class="nav-item">
                        <a class="nav-link" id="purchase-tab" data-toggle="pill" href="#purchase" role="tab" aria-controls="purchase" aria-selected="true">Riwayat Pembelian</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" id="payment-tab" data-toggle="pill" href="#payment" role="tab" aria-controls="payment" aria-selected="false">Riwayat Pembayaran</a>
                      </li>
                      <?php }?>
                      <li class="nav-item">
                        <a class="nav-link active" id="profile-tab" data-toggle="pill" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profil</a>
                      </li>
                    </ul>
                  </div>
                  <div class="card-body">
                    <div class="tab-content" id="custom-tabs-three-tabContent">
                      <div class="tab-pane fade" id="purchase" role="tabpanel" aria-labelledby="purchase-tab">
                        <div class="row">
                          <div class="col-md-6 pull-left">
                            <a href="javascript:void(0)" class="btn-primary btn-sm" target="_self" onclick="openWindow('transactions/add/<?php echo isset($item) ? $item->id : '';?>', 'Tambah Pembelian', 'max', 'max')"><span>TAMBAH PEMBELIAN</span></a>
                          </div>
                          <div class="col-md-6 pull-right">
                            <table style="width: 100%;">
                              <tr><td style="text-align: right; width: 70%;"><strong>Jumlah Tagihan : </strong></td><td style="text-align: right; width: 30%;">Rp. <?php echo format_money($due->due);?></td></tr>
                            </table>
                          </div>
                        </div>

                        <br/>
                        <div class="row">
                          <div class="col-sm-12">
                            <table id="customer_invoice_list" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                              <thead>
                                <tr>
                                  <th class="no"></th>
                                  <th class="date">Tanggal</th>
                                  <th class="itemName">Items</th>
                                  <th class="due">Jumlah</th>
                                  <th class="tools">&nbsp;</th>
                                </tr>
                              </thead>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade" id="payment" role="tabpanel" aria-labelledby="payment-tab">
                         <div class="row">
                          <div class="col-md-6 pull-left">
                            <a href="javascript:void(0)" class="btn-primary btn-sm" target="_self" onclick="openWindow('transactions/add_payment/<?php echo isset($item) ? $item->id : '';?>', 'Tambah Pembayaran', 'max', 'max')"><span>TAMBAH PEMBAYARAN</span></a>
                          </div>
                          <div class="col-md-6 pull-right">
                            <table style="width: 100%;">
                              <tr><td style="text-align: right; width: 70%;"><strong>Jumlah Tagihan : </strong></td><td style="text-align: right; width: 30%;">Rp. <?php echo format_money($due->due);?></td></tr>
                            </table>
                          </div>
                        </div>

                        <br/>
                        <div class="row">
                          <div class="col-sm-12">
                            <table id="customer_history_list" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                              <thead>
                                <tr>
                                  <th class="no"></th>
                                  <th class="date">Tanggal</th>
                                  <th class="itemName2">Deskripsi</th>
                                  <th class="due">Tagihan</th>
                                  <th class="payment">Pembayaran</th>
                                  <th class="tools">&nbsp;</th>
                                </tr>
                              </thead>
                            </table>
                          </div>
                        </div>
                      </div>
                      <div class="tab-pane fade active show" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <p>
                          <form autocomplete="off" name="form" id="form" method="post" action="customers/save" target="_self" enctype="multipart/form-data">
                          <table class="table table-bordered">
                            <tbody>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Kantor Cabang</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <?php
                                  if(isset($office->id) && !is_center_admin()){
                                    echo '
                                    <input type="hidden" class="form-control" id="id_office" name="id_office" value="'.$office->id.'">';
                                    echo $office->name;
                                  } 
                                  else {
                                    $selected = "";
                                    echo '<select class="form-control" id="id_office" name="id_office" onchange="changeOffice(this)">';
                                    foreach ($offices as $key => $row) {
                                      $selected = ((isset($office->id) && $office->id == $row->id) || (isset($item) && $item->id_office == $row->id)) ? "selected" : "";
                                      echo "<option value='" . $row->id . "' " . $selected . ">" . $row->name . "</option>";
                                  }
                                  echo '</select>';
                                  }
                                  ?>                            
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Nama Sales</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                    <input type="hidden" id="temp_id_sales" value="<?php echo isset($item) ? $item->id_sales : '0';?>">
                                    <select class="form-control" id="id_sales" name="id_sales" onchange="">
                                      <option value="0"> &nbsp;</option>
                                      <?php
                                      foreach ($sales as $key => $row) {
                                          $selected = (isset($item) && $item->id_sales == $row->id) ? "selected" : "";
                                          echo "<option value='" . $row->id . "' " . $selected . ">" . $row->name . "</option>";
                                      }
                                      ?>
                                    </select>                            
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Nama *</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <input type="hidden" class="form-control" name="id" id="id" value="<?php echo isset($item) ? $item->id : '';?>">
                                  <input type="text" class="form-control" name="name" id="name" value="<?php echo isset($item) ? $item->name : '';?>">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Nama Contact</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <input type="text" class="form-control" name="name_contact" id="name_contact" value="<?php echo isset($item) ? $item->name_contact : '';?>">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Alamat</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <input type="text" class="form-control" name="address" id="address" value="<?php echo isset($item) ? $item->address : '';?>">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Telp</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <input type="text" class="form-control" name="phone" id="phone" value="<?php echo isset($item) ? $item->phone : '';?>">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td class="col-sm-2 label-form"><span>Email</span></td>
                                <td class="col-sm-10">
                                  <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                                  <input type="text" class="form-control" name="email" id="email" value="<?php echo isset($item) ? $item->email : '';?>">
                                  </div>
                                </td>
                              </tr>
                              <tr>
                                <td></td>
                                <td>
                            <button id="btn_back" class="btn btn-default btn btn-sm" name="btn_back"><span>Batal</span></button>&nbsp;
                            <button id="btn_submit" class="btn btn-primary btn btn-sm" name="btn_submit"><span>Simpan</span></button>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          </form>
                        </p>                        

                      </div>
                    </div>
                  </div>
                  <!-- /.card -->
               </div>


              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
</div>
  <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="modal-default"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" name="cancelledId" id="cancelledId" value="">
      <div class="modal-body">
        <p>Anda yakin akan membatalkan transaksi ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitCancel" class="btn btn-primary" style="display: none;">Ya</button>
        <button type="button" id="submitDelete" class="btn btn-primary" style="display: none;">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->