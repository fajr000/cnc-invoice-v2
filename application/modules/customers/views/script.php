<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			if($("#name").val().length == 0){
				msg.push("Kolom <strong>Nama</strong> tidak boleh kosong!");
			}

			if(($("#email").val()).length > 0 && !ValidateEmail($("#email"))){
				msg.push("Kolom <strong>Email</strong> tidak valid!");
			}

			if(msg.length > 0) { 

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "customers/";
		});

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "customers/delete/"+deletedId;
        });

        $("#submitCancel").on("click", function(){
        	var id_office = $("#id_office").val();
        	var id_customer = $("#id").val();
        	var id_invoice = $("#cancelledId").val();

        	window.location.href = "transactions/cancel/"+id_office+"/"+id_customer+"/"+id_invoice;
        });

        $("#submitDelete").on("click", function(){
        	var id_office = $("#id_office").val();
        	var id_customer = $("#id").val();
        	var id_invoice = $("#cancelledId").val();

        	window.location.href = "transactions/delete/"+id_office+"/"+id_customer+"/"+id_invoice;
        });

        var table = $('#list_data').DataTable( {
	        "ajax": "customers/get_customers_list",
	        "columns": [
	            { data: "id", className: "no" },
	            { data: "name", className: "name highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='customers/edit/"+oData.id+"'>"+oData.name+"</a>");
			        }
	            },
	            { data: "address", className: "alamat" },
	            { data: "phone", className: "telp" },
	            { data: "name_sales", className: "sales highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='sales/edit/"+oData.id_sales+"'>"+oData.name_sales+"</a>");
			        } 
	        	},
	        	{ data: "id", className: "tools",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(oData.delete_access);
			        } 
	        	}

	        ],
	        'order': [[4, 'asc'], [1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

	    var uid_office = $("#id_office").val();
	    var uid_customer = $("#id").val();
	    if(uid_office > 0 && uid_customer > 0){
		    var table_inv = $('#customer_invoice_list').DataTable( {
		        "ajax": "customers/get_customer_invoice_list/"+uid_office+"/"+uid_customer,
		        "columns": [
		        	{ data: "no", className: "no" },
		            { data: "transacted_on", className: "date" },
		            { data: "item", className: "itemName" },
		            { data: 'invoice_due',  className: "due", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
		        	{ data: "id_invoice", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.select);
				        } 
		        	}
		        ],
		        'order': [[0, 'asc']]
		    } );

		    table_inv.on( 'order.dt search.dt', function () {
		    	table_inv.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}

		var uid_office = $("#id_office").val();
	    var uid_customer = $("#id").val();
	    if(uid_office > 0 && uid_customer > 0){
		    var table_payment = $('#customer_history_list').DataTable( {
		        "ajax": "customers/get_customer_transaction_history/"+uid_office+"/"+uid_customer,
		        "columns": [
		        	{ data: "no", className: "no" },
		            { data: "transacted_on", className: "date" },
		            { data: "item", className: "itemName2" },
		            { data: 'invoice_due',  className: "due", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
		            { data: 'payment',  className: "payment", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
		        	{ data: "id_invoice", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.button);
				        } 
		        	}
		        ],
		        'order': [[0, 'asc']]
		    } );

		    table_payment.on( 'order.dt search.dt', function () {
		    	table_payment.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}

	    var paramTab = $("#paramTab").val();
	    if(paramTab == 1){
	    	$('#purchase-tab').trigger('click');
	    }
	    else if(paramTab == 2){
	    	$('#payment-tab').trigger('click');
	    }

	    var select_office = $("select#id_office");
	    if(select_office.length > 0){
	    	var id_office = $("select#id_office").find("option:selected").val();
	    	if(id_office > 0){
	    		$("select#id_office").val(id_office).trigger("change");
	    	}
	    	else{
	    		$("select#id_office").prop('selectedIndex', 0).trigger("change");
	    	}
	    }
	});

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    }

    function selectSales(elm){
    	var id_office = $(elm).find("option:selected").data("id_office");
    }

    function goto_registrationhistory(){
    	var id = $("#id").val();
    	window.location = "customers/edit/"+id+"/1";
    } 

    function goto_paymenthistory(){
    	var id = $("#id").val();
    	window.location = "customers/edit/"+id+"/2";
    }  

    function invoiceTodo(elm, id_invoice){
    	var option = $(elm).find("option:selected").val()
    	
    	if(option == "detail"){
    		var id_office = $("#id_office").val();
    		var id_customer = $("#id").val();

    		openWindow('transactions/invoice/'+id_office+'/'+id_customer+'/'+id_invoice, 'Detil Invoice', 'max', 'max');
    	}
    	else if(option == "cancel"){
    		$("#cancelledId").val(id_invoice);
    		$('.modal-body').html("Anda yakin akan membatalkan transaksi ini?");
    		$("#submitCancel").show();
    		$("#submitDelete").hide();
			$('#modal-default').modal('show');
    	}
    	else if(option == "delete"){
    		$("#cancelledId").val(id_invoice);
    		$('.modal-body').html("Anda yakin akan menghapus transaksi ini?");
    		$("#submitCancel").hide();
    		$("#submitDelete").show();
			$('#modal-default').modal('show');
    	}
    	else{
    		$("#cancelledId").val("");
    	}
    }   

    function changeOffice(elm){
    	var id_office = $(elm).find("option:selected").val();
    	var selected_sales = $("#temp_id_sales").val();

    	if(id_office > 0){
    		$.ajax({
				url: "customers/get_sales", 
				type: "post",
				dataType: "html",
				data: {
					id_office : id_office
				},
				success: function(result){
					$("#id_sales").html(result);
					$("#id_sales").val(selected_sales);
				}
			});
    	}
    	else{
    		$("#id_sales").html("<option value='0'> &nbsp;</option>");
    	}
    }
	
</script> 