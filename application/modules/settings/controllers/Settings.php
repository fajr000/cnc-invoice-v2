<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends My_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    	$this->load->helper(array('url', 'language'));
		$this->load->model('offices/Offices_model', 'office');
		$this->load->model('price_plat/Plat_model', 'plat');
	}

	public function check_stock(){
		$id_office = $this->session->userdata("id_office");
		$result = array(
			"min_stock" => 0,
			"items" => array()
		);
		
		if(isset($id_office) && !empty($id_office) && (is_center_admin() || is_branch_admin())){
			$settings = $this->office->get_office_settings($id_office);
			$min_stock= $settings["MIN_STOCK"];

			$params = array();
			$params[] = "id_office = " . $id_office; 
			$params[] = "qty <= " . $min_stock; 

			$items = $this->plat->get_all_items($params);

			if(count($items) > 0){
				$result = array(
					"min_stock" => count($items),
					"items" => $items
				);
			}
		}

		echo json_encode($result);
	}

}
