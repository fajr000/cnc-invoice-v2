<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();


			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		var table = $('#list_data').DataTable( {
	        "ajax": "payments_all/get_payment_list",
	        "columns": [
	            { data: "id_office", className: "center-text" },
	            { data: "transacted_on", className: "center-text" },
	            { data: "invoice_number", className: "" },
	            { data: "name_customer", className: "" },
	            { data: "amount", className: "right-text" },
	            { data: "code", className: "center-text" },
	        	{ data: "id_payment", className: "",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(oData.tools);
			        } 
	        	}
	        ],
	        'order': [[1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

		if ($('#head_id_office').length > 0) {
			$.ajax({
				url: "invoices/get_offices_list", 
				type: "post",
				dataType: "json",
				data: {
				},
				success: function(result){

		  			$("#head_id_office").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Kantor Cabang--",
		  			});

		  			var id_office = $("#default_id_office").val();

		  			if(id_office.length > 0){
		  				$('#head_id_office').val(id_office).trigger('change');
		  			}
				}
			});
		}

		$('#head_id_office').on('change', function (e) {
		  	var id_office = $("#head_id_office").find("option:selected").val();

		  	if(id_office == "0"){
		  		$("#id_office").val("");
		  	}else{
		  		$("#id_office").val(id_office);
		  	}

		  	if(id_office == "0"){
		  		$('#list_data').DataTable().column(0).search('').draw();
		  		if ($('#head_id_customer').hasClass("select2-hidden-accessible")) {
				    // Select2 has been initialized
				    $("#head_id_customer").select2('destroy');
				}
		  		
		  	}else{		  		
		  		if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}

				$.ajax({
					url: "invoices/get_customers_list", 
					type: "post",
					dataType: "json",
					data: {
						id_office : id_office
					},
					success: function(result){
						if ($('#head_id_customer').hasClass("select2-hidden-accessible")) {
						    // Select2 has been initialized
						    $("#head_id_customer").select2('destroy');
						}
						$("#head_id_customer").html("");
			  			$("#head_id_customer").select2({
			  				data : result.data,
			  				theme: 'bootstrap4',
			  				placeholder: "--Pilih Customer--",
			  			});
					}
				});	
		  	}	  	
		});

		$('#head_id_customer').select2({
			theme: 'bootstrap4',
		  	placeholder: "--Pilih Customer--",
		});

		$('#head_id_customer').on('select2:select', function (e) {
		  	var data = e.params.data;

		  	$("#id_customer").val(data.id);
		  	
		  	if(data.id == "0"){
		  		$('#list_data').DataTable().column(3).search('').draw();
		  	}else{		  		
		  		if ( table.column(3).search() !== data.text ) {
					table
					.column(3)
					.search( data.text )
					.draw();
				}
		  	}
		});

		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: true
			});
		});

		$('.money').mask('000.000.000.000.000', {reverse: true});		
	});

	function paymentTodo(elm, id_payment){
    	var option = $(elm).find("option:selected").val()
    	var id_office = $(elm).data("id_office");
    	var id_customer = $(elm).data("id_customer");
    	
    	if(option == "detail"){
    		openWindow('payments_all/get_payment?id_office='+id_office+'&id_customer='+id_customer+'&id_payment='+id_payment, 'Lihat Pembayaran', 'max', 'max');
    	}
    }   

	
</script> 