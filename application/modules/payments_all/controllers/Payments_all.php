<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments_all extends My_Controller {
	
	function __construct()
	{ 
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
	    $this->load->helper(array('url', 'language'));

	    $this->load->model('payments/Payment_model', 'payment');
	    $this->load->model('offices/Offices_model', 'office');
	    $this->load->model('customers/Customers_model', 'customer');
	    $this->load->model('sales/Sales_model', 'sales');    
	    $this->load->model('transactions/Transaction_model', 'transaction');    

	    $this->data['page_title'] = 'Payment | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Payments';
	    $this->data['page_subheader'] = 'Daftar Pembayaran';

	    // Set pemission/ access
	    $class_name = $this->router->fetch_class();
	    set_parent_url($class_name);
	    $this->data['allowed'] = build_access($class_name);
	}

	public function index(){
		if (!logged_in())
		{
			//redirect them to the login page
			redirect('user/login', 'refresh');
		}

		$this->get_lists();        
	}

	public function get_lists(){
		// page script js
		$this->data['before_body'] = $this->load->view('script', '', true);
		// echo "<pre>";print_r($items);die();
		$this->render('list_view', 'admin_master');
	}

	public function get_payment_list() {
	    $params = array();

	    $id_office = ($this->input->get("id_office")) ? decrypt_url($this->input->get("id_office")) : "";
	    $id_customer = ($this->input->get("id_customer")) ? decrypt_url($this->input->get("id_customer")) : "";

	    if(isset($id_office) && !empty($id_office)){
	      $id_office = decrypt_url($id_office);
	      $params[] = "id_office = $id_office";
	    }

	    if(isset($id_customer) && !empty($id_customer)){
	      $id_customer = decrypt_url($id_customer);
	      $params[] = "id_customer = $id_customer";
	    }

      $pay = $this->payment->get_all_items($params);

      $no = 0;
      foreach ($pay as $key => $row) {
        $pay[$key]->no = $no;
        $pay[$key]->id_transaction = encrypt_url($row->id_transaction);
        $pay[$key]->id_office = encrypt_url($row->id_office);
        $pay[$key]->id_customer = encrypt_url($row->id_customer);
        $pay[$key]->id_payment = encrypt_url($row->id_payment);
        $pay[$key]->amount = format_money($row->amount);
        $pay[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

        $temp = explode(",", $row->invoice_number);
        $temp2 = explode(",", $row->id_invoice);
        $id_arr = array();
        $payoice_arr = array();
        for($i=0;$i<count($temp);$i++){
          $payoice_arr[] = format_invoice($temp[$i]);          
          $id_arr[] = encrypt_url($temp2[$i]);          
        }

        $pay[$key]->invoice_number = implode("&amp; ", $payoice_arr);
        $pay[$key]->id_invoice = implode(",", $id_arr);

        $tools = '
        <select name="tools[]" class="form-control tool-act" data-id_office="'.$row->id_office.'" data-id_customer="'.$row->id_customer.'" onchange="paymentTodo(this,\''.$row->id_payment.'\')">
        <option>--Pilih--</option>
        <option value="detail">Detail</option>
        </select>
        ';
      

        $pay[$key]->tools = $tools;
        $no++;
      }

      echo json_encode(array("data"=>object_to_array($pay)));
  }

  public function get_payment() {
  	$id_office = decrypt_url($this->input->get("id_office"));
  	$id_customer =  decrypt_url($this->input->get("id_customer"));
  	$id_payment =  decrypt_url($this->input->get("id_payment"));

    $post_max_size = parse_size(ini_get('post_max_size'));
    $upload_max = parse_size(ini_get('upload_max_filesize'));

    $off = $this->office->get_detail_item($id_office);
    $cus = $this->customer->get_detail_item($id_customer);

    $payments = $this->transaction->get_detail_payment($id_office, $id_customer, $id_payment);

    $pay_list = array();
    $total_payment = 0;
    foreach ($payments as $row) {
      $pay_list["id_transaction"] = $row->id_invoice;
      $pay_list["id_office"] = $row->id_office;
      $pay_list["id_customer"] = $row->id_customer;
      $pay_list["id_payment"] = $row->id_payment;
      $pay_list["payment_method_id"] = $row->payment_method_id;
      $pay_list["code"] = $row->code;
      $pay_list["is_down_payment"] = $row->is_down_payment;
      $pay_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
      $pay_list["posted"] = $row->posted;
      $pay_list["name_operator"] = $row->name_operator;
      $total_payment += $row->paid_now;

      $pay_list["items"][$row->id_invoice]["id_invoice"] = $row->id_invoice;
      $pay_list["items"][$row->id_invoice]["invoice_number"] = format_invoice($row->invoice_number);
      $pay_list["items"][$row->id_invoice]["due"] = $row->due_now;
      $pay_list["items"][$row->id_invoice]["pay_amount"] = $row->paid_now;
      
      $sql = "SELECT * FROM accounting_payment_attachments WHERE id_payment = $id_payment";
      $files = $this->db->query($sql)->result();
      $pay_list["items"][$row->id_invoice]["files"] = $files;
    }
    $pay_list["total_pay_amount"] = $total_payment;

    $this->data["payments"] = $pay_list;
    $this->data["customer"] = $cus;
    $this->data["post_max_size"] = formatSizeUnits($post_max_size);
    $this->data["upload_max"] = formatSizeUnits($upload_max);
    $this->data['before_body'] = $this->load->view('script_payment_detail', '', true);


    $this->render('payment_detail', 'admin_clear'); 
    

  }

	public function detail(){
		$id_office = decrypt_url($this->input->get("id_office")); 
		$id_customer = decrypt_url($this->input->get("id_customer")); 
		$id_invoice = decrypt_url($this->input->get("id_invoice"));
		$category = ($this->input->get("cat")) ? $this->input->get("cat") : "";

		$post_max_size = parse_size(ini_get('post_max_size'));
		$upload_max = parse_size(ini_get('upload_max_filesize'));

		$cus = $this->customer->get_detail_item($id_customer);
		$inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);
		$det = $this->invoice->get_detail_item($id_invoice);

		$token = $this->db->query("SELECT * FROM tokens WHERE id_office = $id_office AND id_invoice = $id_invoice")->row();
		if(count($token) > 0){
			$token_logs = $this->db->query("SELECT GROUP_CONCAT(token_order) no_order, GROUP_CONCAT(minutes) minutes FROM token_logs WHERE id_token = ".$token->id." GROUP BY id_token")->row();
			$this->data["token_logs"] = $token_logs;
		}else{
			$this->data["token_logs"] = array();
		}


		$machines = $this->db->query("SELECT of.`id`, of.`no_machine`, ct.`name` name_cutting_type FROM office_machines of LEFT JOIN cutting_types ct ON ct.`id` = of.`id_cutting_type` WHERE of.`id_office` = $id_office  AND of.`deleted` = 0  AND ct.`deleted` = 0")->result();
		$machines_arr = array();
		$machines_str = "";
		foreach($machines as $row){
			$selected = (count($token) > 0 && $token->id_machine == $row->id) ? "selected" : "";

			$machines_arr[] = '
			<option '.$selected.' data-id_office="'.$id_office.'" 
					data-id_customer="'.$id_customer.'" 
					data-id_invoice="'.$id_invoice.'" 
					data-no_invoice="'.$det->invoice_number.'" 
					value="'.$row->id.'">'.
					$row->name_cutting_type.'-'.$row->no_machine.
			'</option>';
		}
		$machines_str = '<select style="width:100%;" id="id_machine" name="id_machine" class="form-control form-control-sm">'.implode("", $machines_arr).'</select>';

		$inv_list = array();
		foreach ($inv as $row) {
			$inv_list["id_invoice"] = $row->id_invoice;
			$inv_list["id_office"] = $row->id_office;
			$inv_list["id_customer"] = $row->id_customer;
			$inv_list["invoice_number"] = format_invoice($row->invoice_number);
			$inv_list["invoice_amount"] = $row->invoice_amount;
			$inv_list["invoice_discount"] = $row->invoice_discount;
			$inv_list["invoice_discount_amount"] = $row->invoice_discount_amount;
			$inv_list["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
			$inv_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
			$inv_list["cancelled_on"] = $row->cancelled_on;
			$inv_list["posted"] = $row->posted;
			$inv_list["name_operator"] = $row->name_operator;
			$inv_list["status"] = $det->status;

			$inv_list["token"] = $token;
			$inv_list["machines"] = $machines_str;

			$minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";

			$machine = !(empty($row->machine)) ? $row->machine." " : "";
			$inv_list["items"][$row->id_invoice_item]["description"] = $row->product." ".$machine.$row->name_plat." ".$row->size."$minutes";
			$inv_list["items"][$row->id_invoice_item]["product"] = $row->product;
			$inv_list["items"][$row->id_invoice_item]["name_plat"] = $row->name_plat;
			$inv_list["items"][$row->id_invoice_item]["machine"] = $row->machine;
			$inv_list["items"][$row->id_invoice_item]["size"] = $row->size;
			$inv_list["items"][$row->id_invoice_item]["price"] = $row->price;
			$inv_list["items"][$row->id_invoice_item]["qty"] = $row->qty;
			$inv_list["items"][$row->id_invoice_item]["minute"] = $row->minute;
			$inv_list["items"][$row->id_invoice_item]["amount"] = $row->amount;
			$inv_list["items"][$row->id_invoice_item]["discount"] = $row->discount;
			$inv_list["items"][$row->id_invoice_item]["discount_amount"] = $row->discount_amount;
			$inv_list["items"][$row->id_invoice_item]["due"] = $row->due;

			$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id_invoice_item = $row->id_invoice_item";
			$files = $this->db->query($sql)->result();
			$inv_list["items"][$row->id_invoice_item]["files"] = $files;
		}

		$this->data["invoices"] = $inv_list;
		$this->data["customer"] = $cus;
		$this->data["category"] = $category;
		$this->data["post_max_size"] = formatSizeUnits($post_max_size);
		$this->data["upload_max"] = formatSizeUnits($upload_max);
		$this->data['before_body'] = $this->load->view('script', '', true);


		$this->render('detail', 'admin_clear'); 
	}

	public function remove_file_payment(){
	    $id = $this->input->post("id_file");

	    if(!empty($id)){
	      $sql = "SELECT * FROM accounting_payment_attachments WHERE id = $id";
	      $del = "DELETE FROM accounting_payment_attachments WHERE id = $id";
	      $result = $this->db->query($sql)->row();

	      if(count($result) > 0){
	        $file_url = $result->location.'/'.$result->encrypted_name;

	        if(file_exists($file_url)){
	          unlink($file_url); 
	          $this->db->query($del);

	          echo json_encode(array("result" => 1));
	        }else{
	          echo json_encode(array("result" => 0));
	        }
	      }else{
	        echo json_encode(array("result" => 2));
	      }
	    }else{
	      echo json_encode(array("result" => 3));
	    }
	  }

}


