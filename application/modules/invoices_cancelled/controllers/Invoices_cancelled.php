<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices_cancelled extends My_Controller {
	
	function __construct()
	{ 
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
	    $this->load->helper(array('url', 'language'));

	    $this->load->model('Invoice_model', 'invoice');
	    $this->load->model('offices/Offices_model', 'office');
	    $this->load->model('customers/Customers_model', 'customer');
	    $this->load->model('sales/Sales_model', 'sales');    
	    $this->load->model('transactions/Transaction_model', 'transaction');    

	    $this->data['page_title'] = 'Invoice | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Cancelled Invoices';
	    $this->data['page_subheader'] = 'Daftar Invoice Dibatalkan';

	    // Set pemission/ access
	    $class_name = $this->router->fetch_class();
	    set_parent_url($class_name);
	    $this->data['allowed'] = build_access($class_name);
	}

	public function index()
	{
		if (!logged_in())
		{
			//redirect them to the login page
			redirect('user/login', 'refresh');
		}

		$this->get_lists();        
	}

	public function get_lists()
	{
		// page script js
		$this->data['before_body'] = $this->load->view('script', '', true);
		// echo "<pre>";print_r($items);die();
		$this->render('list_invoice_view', 'admin_master');
	}

	public function get_invoice_list($id_office = null) {
		$params = array();

		if(isset($id_office) && !empty($id_office)){
			$id_office = decrypt_url($id_office);
			$params[] = "id_office = $id_office";
		}

	    $inv = $this->invoice->get_all_items($params);

	    foreach ($inv as $key => $row) {
	    	$inv[$key]->id_transaction = encrypt_url($row->id_transaction);
	    	$inv[$key]->id_office = encrypt_url($row->id_office);
	    	$inv[$key]->id_customer = encrypt_url($row->id_customer);
	    	$inv[$key]->id_invoice = encrypt_url($row->id_invoice);
	    	$inv[$key]->invoice_number = format_invoice($row->invoice_number);
	    	$inv[$key]->total = format_money($row->total);
	    	$inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

	    	switch ($row->status) {
	    		case 1:
	    			$inv[$key]->status_label = '<small class="badge badge-info" style="font-weight: normal;"> Request Token </small>';
	    			break;
	    		case 2:
	    			$inv[$key]->status_label = '<small class="badge badge-warning" style="font-weight: normal;"> Waiting List </small>';
	    			break;
	    		case 3:
	    			$inv[$key]->status_label = '<small class="badge badge-primary" style="font-weight: normal;"> Diproses </small>';
	    			break;
	    		case 4:
	    			$inv[$key]->status_label = '<small class="badge badge-success" style="font-weight: normal;"> Selesai </small>';
	    			break;
	    		case 5:
	    			$inv[$key]->status_label = '<small class="badge badge-danger" style="font-weight: normal;"> Dibatalkan </small>';
	    			break;
	    		
	    		default:
	    			$inv[$key]->status_label = '<small class="badge badge-secondary" style="font-weight: normal;"> Konfirmasi </small>';
	    			break;
	    	}

			$tools = "";
			if($row->cancelled_on == "0000-00-00 00:00:00"){
				$tools = '
				<select name="tools[]" class="form-control tool-act" data-id_office="'.$row->id_office.'" data-id_customer="'.$row->id_customer.'" onchange="invoiceTodo(this,\''.$row->id_invoice.'\')">
				<option>--Pilih--</option>
				<option value="detail">Detail</option>
				<option value="delete">Batal</option>
				</select>
				';
			}

			$inv[$key]->tools = $tools;
	    }

	    echo json_encode(array("data"=>object_to_array($inv)));
	}	

	public function detail(){
		$id_office = decrypt_url($this->input->get("id_office")); 
		$id_customer = decrypt_url($this->input->get("id_customer")); 
		$id_invoice = decrypt_url($this->input->get("id_invoice"));

		$post_max_size = parse_size(ini_get('post_max_size'));
		$upload_max = parse_size(ini_get('upload_max_filesize'));

		$cus = $this->customer->get_detail_item($id_customer);
		$inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);

		$inv_list = array();
		foreach ($inv as $row) {
			$inv_list["id_invoice"] = $row->id_invoice;
			$inv_list["id_office"] = $row->id_office;
			$inv_list["id_customer"] = $row->id_customer;
			$inv_list["invoice_number"] = format_invoice($row->invoice_number);
			$inv_list["invoice_amount"] = $row->invoice_amount;
			$inv_list["invoice_discount"] = $row->invoice_discount;
			$inv_list["invoice_discount_amount"] = $row->invoice_discount_amount;
			$inv_list["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
			$inv_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
			$inv_list["cancelled_on"] = $row->cancelled_on;
			$inv_list["posted"] = $row->posted;
			$inv_list["name_operator"] = $row->name_operator;

			$minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";

			$machine = !(empty($row->machine)) ? $row->machine." " : "";
			$inv_list["items"][$row->id_invoice_item]["description"] = $row->product." ".$machine.$row->name_plat." ".$row->size."$minutes";
			$inv_list["items"][$row->id_invoice_item]["product"] = $row->product;
			$inv_list["items"][$row->id_invoice_item]["name_plat"] = $row->name_plat;
			$inv_list["items"][$row->id_invoice_item]["machine"] = $row->machine;
			$inv_list["items"][$row->id_invoice_item]["size"] = $row->size;
			$inv_list["items"][$row->id_invoice_item]["price"] = $row->price;
			$inv_list["items"][$row->id_invoice_item]["qty"] = $row->qty;
			$inv_list["items"][$row->id_invoice_item]["minute"] = $row->minute;
			$inv_list["items"][$row->id_invoice_item]["amount"] = $row->amount;
			$inv_list["items"][$row->id_invoice_item]["discount"] = $row->discount;
			$inv_list["items"][$row->id_invoice_item]["discount_amount"] = $row->discount_amount;
			$inv_list["items"][$row->id_invoice_item]["due"] = $row->due;

			$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id_invoice_item = $row->id_invoice_item";
			$files = $this->db->query($sql)->result();
			$inv_list["items"][$row->id_invoice_item]["files"] = $files;
		}

		$this->data["invoices"] = $inv_list;
		$this->data["customer"] = $cus;
		$this->data["post_max_size"] = formatSizeUnits($post_max_size);
		$this->data["upload_max"] = formatSizeUnits($upload_max);
		$this->data['before_body'] = $this->load->view('script', '', true);


		$this->render('detail', 'admin_clear'); 
	}

}
