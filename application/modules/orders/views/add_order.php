<!-- Content Header (Page header) -->
<style type="text/css">
.size, .qty, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
}
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <?php if($this->session->userdata('notification')){ $notif = $this->session->userdata('notification');?>
    <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
      <h4><?php echo $notif["title"];?></h4>
      <?php echo $notif["msg"];?>
    </div>
    <?php 
      $this->session->unset_userdata("notification");
    }?>
    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <!-- .box-header -->
          <div class="card-header">          
            <div class="row">
              <div class="col-md-6">
                <h3 class="card-title"><?php echo $today; ?> </h3>
              </div>
              <div class="col-md-6" style="text-align: right;">
                <input type="hidden" id="total_cart" value="<?php echo $this->cart->total();?>">
                <span><i class="fa fa-shopping-cart"></i> Rp. 
                  <?php
                  if(!empty($this->cart->contents())) {
                    echo "<strong>".number_format($this->cart->total(), 0, ',', '.')."</strong>";
                  }else
                  {
                    echo "0";
                  }
                  ?>
                </span>
              </div>
            </div>
            
          </div>
          <!-- /.box-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12 pull-left">
                    <?php
                    $id_office = ($this->session->userdata("order_id_office")) ? $this->session->userdata("order_id_office") : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");
                    ?>
                    <input type="hidden" id="default_id_office" value="<?php echo $id_office;?>">
                    <div class="row">
                      <div class="col-md-8">
                        <div style="margin-bottom:5px;">
                          <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                          </select>
                        </div>    
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-8">
                        <div style="margin-bottom:5px;">  
                          <select placeholder="Nama Customer" class="form-control" name="head_id_customer" id="head_id_customer" style="width: 50%;">
                          </select>
                        </div>
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div> 
              <br/>
              <div class="col-md-12"  id="form_user" style="margin:10px 0px;">
                <form class="form-horizontal">
                    <div class="form-group row">
                      <div class="col-sm-12" style="border-bottom: 1px solid #adb5bd; margin-bottom: 10px;"><strong>Katalog Produk </strong></div>
                      <div class="col-sm-3">
                        <select name="product_type" id="product_type" class="form-control">
                          <option value="">Jenis Produk</option>
                          <option value="1">Plat</option>
                          <option value="2">Jasa Cutting</option>
                        </select>
                      </div>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="name_plat" placeholder="Jenis Plat">
                      </div>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="size" placeholder="Ukuran">
                      </div>
                      <div class="col-sm-3">
                        <input type="text" class="form-control" id="machine" placeholder="Mesin">
                      </div>
                    </div>
                </form>
                <center>
                  <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                      <div class="row">
                        <div class="col-md-6">
                          <button type="button" class="btn btn-block btn-primary btn-sm" onclick="addToCart(this);">ADD</button>
                        </div>
                        <div class="col-md-6">
                          <button type="button" class="btn btn-block btn-warning btn-sm" onclick="checkout(this);">NEXT</button>
                        </div>
                      </div>
                    </div>               
                  </div>
                </center>
                <form autocomplete="off" name="form" id="form" method="post" action="orders/add_items" target="_self" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo (isset($customer)) ? $customer->id_office : ""; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo (isset($customer)) ? $customer->id : ""; ?>">
                  <table id="list_product" class="table" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 5%;"></th>
                            <th style="width: 15%;">Plat</th>
                            <th style="width: 15%;">Ukuran</th>
                            <th style="width: 5%;">Qty</th>
                            <th style="width: 10%;">Mesin</th>
                            <th style="width: 20%;">Harga</th>
                            <th style="width: 20%;">Disc.</th>
                            <th style="width: 10%; text-align: center;"></th>
                        </tr>
                    </thead>
                  </table>
                </form>
              </div>
            </div>
            <!-- /.row -->
          </div>

        </div><!-- /.card -->
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container -->
</section>
<!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="notification" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p id="notif_msg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->