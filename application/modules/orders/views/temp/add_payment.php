<style type="text/css">
.center-text{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
}
.middle-text{
  vertical-align: middle !important;
}

</style> 
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-12">
        <table style="width: 100%;">
          <tr>
            <td style="width: 50%;"><?php echo $today; ?>
            <input type="hidden" id="default_id_office" value="<?php echo $this->session->userdata("id_office");?>">
              <br>
              <?php
              if(isset($customer) && !empty($customer->id_office)){
                echo '<strong>'.$customer->name_office.'</strong><br>
                <strong>'.$customer->name.'</strong>
                ';
              }else{
              ?>
                <div style="margin-bottom:5px;">
                  <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                  </select>
                </div>
                <div style="margin-bottom:5px;">  
                  <select placeholder="Nama Customer" class="form-control" name="head_id_customer" id="head_id_customer" style="width: 50%;">
                  </select>
                </div>
              <?php 
              }
              ?>
            </td>
            <td style="width: 50%; text-align: right; vertical-align: top;">
              <div class="row" >
                <div class="col-md-6"></div>
                <div class="col-md-6"></div>
              </div>
            </td>
          </tr>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user">
                <form autocomplete="off" class="" name="form" id="form_payment" method="post" action="transactions/payment" target="_self" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo (isset($customer)) ? $customer->id_office : ""; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo (isset($customer)) ? $customer->id : ""; ?>">
                  <table id="list_invoice" class="table text-nowrap" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 5%; text-align: center;"></th>
                            <th style="width: 60%; text-align: left;">No. Invoice</th>
                            <th style="width: 15%; text-align: right;">Tagihan (Rp)</th>
                            <th style="width: 15%; text-align: center;">Bayar</th>
                            <th style="width: 5%; text-align: center;"></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i = 1;
                      foreach ($invoices as $row) {
                        echo '
                        <tr>
                          <td class="middle-text center-text">'.$i.'</td>
                          <td class="middle-text">'.$row->invoice_number.'</td>
                          <td class="middle-text price">'.format_money($row->current_due).'</td>
                          <td class="middle-text"><input type="form-text" class="form-control money price amount_paid" data-max_amount="'.intval($row->current_due).'" value="" ></td>
                          <td class="center-text middle-text" style="">
                              <input type="checkbox" class="paid_invoices" onclick="chooseInvoice(this)" name="paid_invoices['.$row->id_invoice.']" value="">
                          </td>
                        </tr>
                        ';

                        $i++;
                      }
                      ?>
                      <tr>
                        <td colspan="3" class="price middle-text">
                          <div class="form-group clearfix" style="margin-bottom: 0px;">
                              <div class="icheck-primary d-inline">
                                <label>
                                  Total Pembayaran
                                </label>
                              </div>
                              <div class="icheck-primary d-inline">(
                                <input type="checkbox" id="is_dp" name="is_dp" value="1">
                                <label for="is_dp" style="font-weight: normal !important;">
                                  DP
                                </label>
                                )
                              </div>
                            </div>
                        </td>
                        <td class="price middle-text"><input type="hidden" name="total_paid" id="total_paid">
                          <input type="text" class="form-control price" id="total_paid_str" readonly value="">
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                          <td colspan="3" class="price middle-text"><strong>Metode :</strong></td></td>
                          <td>
                            <select name="payment_method" class="form-control"  id="payment_method">
                                <option value="0">&nbsp;</option>
                                <option value="2">TRANSFER</option>
                                <option value="1">CASH</option>
                              </select>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="3" class="price middle-text">File Lampiran Pembayaran:</td></td>
                          <td class="middle-text">
                              <button type="button" class="btn btn-block btn-success btn-sm" onclick="addFile()"><i class="fas fa-plus"></i> Add </button>
                          </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          <div class='file_container'>
                          </div>
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          
                        </td>
                        <td>
                          <div>
                            <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-ban"></i> Batalkan Transaksi</button>
                          </div>
                        </td>
                        <td>
                          <div class="">
                            <button type="button" value="make_payment" class="btn btn-block btn-primary btn-sm" id="submitpayment" disabled>Proses <i class="fa fa-step-forward"></i></button>
                          </div>                  
                        </td>
                        <td></td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

</script>