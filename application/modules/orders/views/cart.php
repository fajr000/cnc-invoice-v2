<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
    <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
      <h4><?php echo $notif["title"];?></h4>
      <?php echo $notif["msg"];?>
    </div>
    <?php }?>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <!-- .box-header -->
          <div class="card-header">          
            <div class="row">
              <div class="col-md-6">
                <h3 class="card-title"><?php echo $today; ?> </h3>
              </div>
              <div class="col-md-6" style="text-align: right;">
                <input type="hidden" id="total_cart" value="<?php echo $this->cart->total();?>">
                <span><i class="fa fa-shopping-cart"></i> Rp. 
                  <?php
                  if(!empty($this->cart->contents())) {
                    echo "<strong>".number_format($this->cart->total(), 0, ',', '.')."</strong>";
                  }else
                  {
                    echo "0";
                  }
                  ?>
                </span>
              </div>
            </div>
          </div>

          <!-- /.box-header -->
          <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-12 pull-left">
                    <?php
                    $id_office = ($this->session->userdata("order_id_office")) ? $this->session->userdata("order_id_office") : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");
                    ?>
                    <input type="hidden" id="default_id_office" value="<?php echo $id_office;?>">
                    <div class="row">
                      <div class="col-md-8">
                        <div style="margin-bottom:5px;">
                          <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                          </select>
                        </div>    
                      </div>
                      <div class="col-md-4"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-8">
                        <div style="margin-bottom:5px;">  
                          <form class="form-horizontal">
                            <div class="form-group row">
                              <div class="col-sm-6" style="padding-right: 0px;">
                                <select placeholder="Nama Customer" class="form-control" name="head_id_customer" id="head_id_customer" style="width: 100%;">
                                </select>
                              </div>
                              <div class="col-sm-6">
                                <button style="width: 50px;" type="button" data-text="<strong><?php echo $customer->name?></strong><br>Ranking <strong><?php echo $rank->rank;?></strong> pembelian bulan <?php echo $rank->period_month;?> dengan total invoice Rp.<?php echo format_money($rank->amount);?>" class="btn btn-block btn-outline-secondary" onclick="showDetailRank(this)"><i class="fa fa-trophy"></i></button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                      <div class="col-md-4">
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12" id="form_user">
                        <form autocomplete="off" name="cart" id="cart" method="post" action="transactions/payments" target="_self" enctype="multipart/form-data">
                          <input type="hidden" id="default_id_office" value="<?php echo $products["id_office"]; ?>">
                          <input type="hidden" name="id_office" id="id_office" value="<?php echo $products["id_office"]; ?>">
                          <input type="hidden" name="id_customer" id="id_customer" value="<?php echo $products["id_customer"]; ?>">
                          <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="center-text" style="width: 2%;"></th>
                                    <th class="center-text" style="width: 35%;">Nama Produk</th>
                                    <th class="center-text" style="width: 25%;">Harga (Rp.)</th>
                                    <th class="center-text" style="width: 10%;">Jml</th>
                                    <th class="center-text" style="width: 10%;">Diskon(%)</th>
                                    <th class="center-text" style="width: 13%;">Total</th>
                                    <th class="center-text" style="width: 5%;"><i class='fa fa-trash'></i></th>
                                </tr>
                            </thead>
                            <tbody>
                              <?php 
                              $i = 1;
                              foreach($products['cart'] as $row) {

                                $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;

                                $price = "";
                                if($row["options"]["type"] == 2){
                                  $selected_easy = ($row["price"] == $row["price_option"]["easy"]) ? "selected" : "";
                                  $selected_med = ($row["price"] == $row["price_option"]["medium"]) ? "selected" : "";
                                  $selected_diff = ($row["price"] == $row["price_option"]["difficult"]) ? "selected" : "";
                                  $selected_min = ($row["price"] == $row["price_option"]["per_minute"]) ? "selected" : "";
                                  $readonly = ($selected_min == "selected") ? "" : "readonly";
                                  $price = '
                                  <div class="form-group row" style="margin-bottom: 0px;">
                                    <select name="price[]" class="form-control input-sm price col-sm-8">
                                      <option value="'.$row["price_option"]["easy"].'" '.$selected_easy.'>'.number_format($row["price_option"]["easy"], 0, '', '.').' (Easy)</option>
                                      <option value="'.$row["price_option"]["medium"].'" '.$selected_med.'>'.number_format($row["price_option"]["medium"], 0, '', '.').' (Medium)</option>
                                      <option value="'.$row["price_option"]["difficult"].'" '.$selected_diff.'>'.number_format($row["price_option"]["difficult"], 0, '', '.').' (Difficult)</option>
                                      <option value="'.$row["price_option"]["per_minute"].'" '.$selected_min.'>'.number_format($row["price_option"]["per_minute"], 0, '', '.').' (Per menit)</option>
                                    </select>
                                    <div class="col-sm-4">
                                      <input type="text" class="form-control minute" name="minute['.$row["guid"].']" placeholder="Menit" '.$readonly.' value="'.$row["minute"].'">
                                    </div>
                                  </div>
                                  ';

                                  if($selected_min == "selected"){
                                    $amount= ($row["price"] * $row["minute"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["minute"] * $row["qty"])) ;
                                  }
                                }else{
                                  $price = '<input type="hidden" name="price[]" class="form-control input-sm price" value="'.$row["price"].'">
                                      '.number_format($row["price"], 0, '', '.');

                                  $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;
                                }

                                if(isset($row["qty_left"]) && $row["qty_left"] > 0){
                                  $html_qty = '
                                  <select class="custom-select qty" name="qty[]" style="text-align:right;">
                                  ';
                                  for($i=1;$i<=$row["qty_left"];$i++){
                                    $html_qty .= '<option value="'.$i.'">'.$i.'</option>';
                                  }
                                  $html_qty .= '</select>';
                                }else{
                                  $html_qty = '
                                  <input type="text" name="qty[]" class="form-control input-sm qty qty_input" value="'.number_format($row["qty"], 2, '.', '').'" style="text-align: right;" onkeypress="return isDecimal(event, this)" onchange="verifyQty(this)">';
                                }

                                $hidden_discount = "hidden";
                                $show_discount = $row["discount"];
                                if($row["options"]["type"] == 2){
                                  $hidden_discount = "text";
                                  $show_discount = "";
                                }

                                echo '
                                <tr class="item">
                                    <td class="middle-text">'.$i.'
                                    <input type="hidden" name="guid[]" class="form-control input-sm guid" value="'.$row["guid"].'">
                                    <input type="hidden" name="id[]" class="form-control input-sm id" value="'.$row["id"].'">                            
                                    </td>
                                    <td class="middle-text">'.$row["name"].'</td>
                                    <td class="middle-text right-text">'.$price.'
                                    </td>
                                    <td class="middle-text">
                                      '.$html_qty.'
                                    </td>
                                    <td class="middle-text center-text">
                                      <input type="'.$hidden_discount.'" name="discount[]" class="form-control input-sm discount" onkeypress="return isDecimal(event, this)" value="'.$row["discount"].'" style="text-align: center;">
                                      '.$show_discount.'
                                    </td>
                                    <td class="middle-text right-text">
                                      <input type="hidden" name="amount[]" class="form-control input-sm amount" value="'.$amount.'">
                                      <span class="amount_text">'.number_format($amount, 0, '', '.').'</span>
                                    </td>
                                    <td class="middle-text center-text">
                                      <input type="checkbox" class="checkbox guid_delete" name="guid_delete[]" value="'.$row["guid"].'">
                                    </td>
                                </tr>
                                ';

                                $i++;
                              }
                              ?>
                              <tr>
                                  <td colspan="5" class="right-text">Sub Total</td>
                                  <td class="right-text">
                                    <div id="subtotal"><?php echo number_format($products["summary"]["subtotal"], 0, '','.');?></div>
                                  </td>
                                  <td></td>
                              </tr>
                              <tr>
                                  <td colspan="5" class="right-text">
                                    <div class="form-group row">
                                      <span class="col-sm-11 col-form-label right-text">Diskon(%)</span>
                                      <div  class="col-sm-1">
                                        <input type="text" class="form-control payment_discount center-text" name="payment_discount" onkeypress="return isDecimal(event, this)" value="<?php echo $products["summary"]["payment_discount"];?>" placeholder="%">
                                      </div>
                                    </div>
                                  </td>
                                  <td class="right-text">
                                    <div id="total_discount"><?php echo number_format($products["summary"]["payment_discount_amount"], 0, '','.');?></div>
                                  </td>
                                  <td></td>
                              </tr>
                              <tr>
                                  <td colspan="5" class="right-text"><strong>Total Tagihan</strong></td>
                                  <td class="right-text">
                                    <strong><div id="total_due"><?php echo number_format($products["summary"]["total"], 0, '','.');?></div></strong>
                                  </td>
                                  <td></td>
                              </tr>
                            </tbody>
                          </table>
                        </form>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-12">
                        <br>
                        <center>
                          <div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-2">
                              <button type="button" class="btn btn-block btn-default btn-sm" id="gotocatalog"><i class="fas fa-step-backward"></i> Kembali ke Katalog</button>
                            </div>
                            <div class="col-md-2">
                              <button type="button" class="btn btn-block btn-danger btn-sm" id="resetchange"><i class="fas fa-history"></i> Batalkan Perubahan</button>
                            </div>
                            <div class="col-md-2">
                              <button type="button" class="btn btn-block btn-success btn-sm" id="savechange"><i class="fas fa-upload"></i> Simpan Perubahan</button>
                            </div>
                            <div class="col-md-2">
                              <button type="button" class="btn btn-block btn-primary btn-sm" id="gotopayment">Pembayaran <i class="fa fa-step-forward"></i></button>
                            </div>                  
                          </div>
                        </center>
                      </div>
                    </div>
                  </div><!-- /.row -->
                </div>
              </div> 
              <br/>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</section>

<div id="notification" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p id="notif_msg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

</script>