<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Orders extends Admin_Controller
{ 
  function __construct(){
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination', 'cart'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Order_model', 'transaction');
    $this->load->model('offices/Offices_model', 'office');
    $this->load->model('customers/Customers_model', 'customer');
    $this->load->model('price_plat/Plat_model', 'plat');
    $this->load->model('price_service/Service_model', 'service');
 
    $this->data['page_title'] = 'Invoice | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Add New'; 
    $this->data['page_subheader'] = 'Tambah Invoice';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index(){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists(){
    $this->add(); 
  }

  public function get_product_list(){
    // populate data
    $params = array();

    $plat = $this->plat->get_all_items(array("qty > 0"));
    $service = $this->service->get_all_items($params);

    $products = array();

    foreach($plat as $row){
      $item = array(
        "type" => 1, //plat
        "id" => $row->id,
        "name_plat" => $row->name_plat,
        "size" => $row->size,
        "qty" => $row->qty,
        "machine" => "N/A",
        "price" => format_money($row->price),
        "discount" => $row->discount." %",
      );

      $products[] = (object) $item;
    }

    foreach($service as $row){
      $easy = explode(".", format_money($row->easy));
      $diff = explode(".", format_money($row->difficult));
      $item = array(
        "type" => 2, //service
        "id" => $row->id,
        "name_plat" => $row->name_plat,
        "size" => $row->size,
        "qty" => "~",
        "machine" => $row->name_machine,
        "price" => format_money($easy[0]) . 'rb - ' . format_money($diff[0]) . 'rb',
        "discount" => $row->discount." %",
      );

      $products[] = (object) $item;
    }

    echo json_encode(array("data"=>$products));
  }

  public function add($is_new_transaction = 1){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    if($is_new_transaction){
      $this->cart->destroy();
      $this->session->unset_userdata('order_id_customer');
      $this->session->unset_userdata('order_id_office');
    }

    $id_customer = ($this->session->userdata("order_id_customer")) ? $this->session->userdata("order_id_customer") : 0;

    $customer = $this->customer->get_detail_item($id_customer);
       
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $customer;

    $this->data['before_body'] = $this->load->view('script_transaction', '', true);

    $this->render('add_order', 'admin_master'); 
  }

  public function add_payment($id_customer = 0){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $customer = $this->customer->get_detail_item($id_customer);
    if(isset($customer))
      $invoices = $this->customer->get_invoice_due($customer->id_office, $id_customer);
    else
      $invoices = array();
       
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $customer;
    $this->data['invoices'] = $invoices;

    $this->data['before_body'] = $this->load->view('script_payment', '', true);

    $this->render('add_payment', 'admin_clear'); 
  }

  public function update_cart(){
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");
    $id_product = $this->input->post("id_product");
    $type_product = $this->input->post("type_product");

    $this->session->set_userdata("order_id_customer", $id_customer);
    $this->session->set_userdata("order_id_office", $id_office);

    $items = array();
    for ($i=0; $i < count($id_product) ; $i++) {

      $name_product = "";
      $price_product = "";
      $price_option = array();
      $qty_left = 0;

      // 1 : PLAT, 2 : SERVICE
      if($type_product[$i] == 1){
        $item = $this->plat->get_detail_item($id_product[$i]);
        
        $name_product = "Plat ".$item->name_plat." ".$item->size." mm";
        $price_product = $item->price - ($item->discount/100*$item->price);
        $qty_left = $item->qty;
      }else{
        $item = $this->service->get_detail_item($id_product[$i]);
        $name_product = " Jasa Potong ".$item->name_plat." ".$item->size." mm" . " - ".$item->name_machine;
        $price_product = $item->easy - ($item->discount/100*$item->easy);

        $price_option = array(
          "easy" => $item->easy - ($item->discount/100*$item->easy),
          "medium" => $item->medium - ($item->discount/100*$item->medium),
          "difficult" => $item->difficult - ($item->discount/100*$item->difficult),
          "per_minute" => $item->per_minute - ($item->discount/100*$item->per_minute),
        );
      }

      $item = array(
        'id' => $type_product[$i]."_".$id_product[$i],
        'qty' => 1,
        'qty_left' => $qty_left,
        'price' => $price_product,
        'price_option' => $price_option,
        'minute' => "",
        'discount'=> 0,
        'name' => $name_product,
        'options' => array(
          'type' => $type_product[$i]
        )
      );

      if(count($this->cart->contents()) > 0) {
        $is_available = true;

        //check available stock for PLAT
        foreach($this->cart->contents() as $key => $row){
          if($item["options"]["type"] == 1 && $item["id"] == $row["id"] && ($row["qty"]+1) > $row["qty_left"]){
            //if stock is overstock
            $is_available = false;
          }
        }

        if($is_available){
          $is_new_item = true;
          $rowid = "";
          $qty = 0;
          foreach($this->cart->contents() as $key => $row){
            if($row["id"] == $item["id"]){
              $is_new_item = false;
              $rowid = $row["rowid"];
              $qty = $row["qty"];
              break;
            }
          }
          if($is_new_item)
            $items[] = $item;
          else {
            $data = array(
              'rowid' => $rowid,
              'qty'   => ($qty+1)
            );

            $this->cart->update($data);
          }
        }
      }
      else{
        $items[] = $item;
      }
    }

    if(count($items) > 0){
      $this->cart->insert($items);
    }
  }

  public function add_items(){
    $this->update_cart();
    
    redirect("orders/add/0");
  }

  public function cart() {
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");

    $this->update_cart();

    $products = array();
    $products["id_office"] = $id_office;
    $products["id_customer"] = $id_customer;
    $products["summary"]["subtotal"] = 0;
    $products["summary"]["payment_discount"] = 0;
    $products["summary"]["payment_discount_amount"] = 0;
    $products["summary"]["total"] = 0;

    foreach($this->cart->contents() as $key => $row) {
      $temp = explode("_", $row["id"]);
      $type_product = $temp[0];
      $id_product = $temp[1];

      $price_option = $row["price_option"];
      $discount = $row["discount"];

      $products["cart"][] = array(
        'guid'    => $key,
        'id'      => $row["id"],
        'qty'     => $row["qty"],
        'qty_left'=> $row["qty_left"],
        'price'   => $row["price"],
        'price_option' => $price_option,
        'minute'  =>  $row["minute"],
        'discount' => $discount,
        'name'    => $row["name"],
        'options' => $row["options"],
      );

      $products["summary"]["subtotal"] += ($row["price"] * $row["qty"]) - ($discount/100 * ($row["price"] * $row["qty"]));
    }

    $products["summary"]["payment_discount_amount"] = $products["summary"]["payment_discount"]/100*$products["summary"]["subtotal"];
    $products["summary"]["total"] = $products["summary"]["subtotal"] - $products["summary"]["payment_discount_amount"];

    $this->session->set_userdata("products", $products);

    redirect("orders/show_cart");
  }

  public function show_cart() {

    $products = $this->session->userdata("products");

    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $this->customer->get_detail_item($products["id_customer"]);
    $this->data['products'] = $products;

    $rank = $this->customer->get_rank($products["id_office"], $products["id_customer"]);
    if(count($rank) > 0){
      $temp = explode(" ", $rank->period_month);
      $rank->period_month = ucwords(to_indonesian_month($temp[0])) . ' ' . $temp[1];
    }else{
      $rank = array(
        "id_office" => $products["id_office"],
        "id_customer" => $products["id_customer"],
        "period" => "",
        "period_month" => "",
        "amount" =>0,
        "rank" => 0,

      );

      $rank = (object)$rank;
    }
    $this->data['rank'] = $rank;

    $this->data['before_body'] = $this->load->view('script_transaction', '', true);

    $this->render('cart', 'admin_master'); 
  }

  public function clear_cart() {
    $this->cart->destroy();
  }

  public function save_change($gotopayment = 0) {
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");
    $guid = $this->input->post("guid");
    $id = $this->input->post("id");
    $price = $this->input->post("price");
    $minute = $this->input->post("minute");
    $qty = $this->input->post("qty");
    $discount = $this->input->post("discount");
    $amount = $this->input->post("amount");
    $guid_delete = $this->input->post("guid_delete");
    $payment_discount = $this->input->post("payment_discount");

    for($i=0;$i<count($guid);$i++) {

      $data = array(
        'rowid'     => $guid[$i],
        'qty'       => (isset($guid_delete) && in_array($guid[$i], $guid_delete)) ? 0 : $qty[$i],
        'price'     => $price[$i],
        'minute'    => (isset($minute[$guid[$i]])) ? (empty($minute[$guid[$i]]) ? 0 : $minute[$guid[$i]]) : 0,
        'discount'  => (empty($discount[$i])) ? 0 : $discount[$i],
      );

      //update cart
      $this->cart->update($data);
    }    

    $products = array();
    $products["id_office"] = $id_office;
    $products["id_customer"] = $id_customer;
    $products["summary"]["subtotal"] = 0;
    $products["summary"]["payment_discount"] = round($payment_discount,2);
    $products["summary"]["payment_discount_amount"] = 0;
    $products["summary"]["total"] = 0;

    foreach($this->cart->contents() as $key => $row) {
      $temp = explode("_", $row["id"]);
      $type_product = $temp[0];
      $id_product = $temp[1];

      $price_option = $row["price_option"];
      $discount = (!empty($row["discount"])) ? round($row["discount"],2) : 0;

      $products["cart"][] = array(
        'guid'    => $key,
        'id'      => $row["id"],
        'qty'     => $row["qty"],
        'qty_left' => $row["qty_left"],
        'price'   => $row["price"],
        'price_option' => $price_option,
        'minute' => $row["minute"],
        'discount' => $discount,
        'name'    => $row["name"],
        'options' => $row["options"],
      );

      if($type_product == 2 && $row["price"] == $price_option["per_minute"]){
        $products["summary"]["subtotal"] += round(($row["price"] * $row["minute"] * $row["qty"]) - (($discount/100) * ($row["price"] * $row["minute"] * $row["qty"])));

      }else{
        $products["summary"]["subtotal"] += round(($row["price"] * $row["qty"]) - (($discount/100) * ($row["price"] * $row["qty"])));
      }
    }

    $products["summary"]["payment_discount_amount"] = round(($products["summary"]["payment_discount"]/100)*$products["summary"]["subtotal"]);
    $products["summary"]["total"] = round($products["summary"]["subtotal"] - $products["summary"]["payment_discount_amount"]);

    $this->session->set_userdata("products", $products);

    if(!$gotopayment){
      redirect("orders/show_cart");
    }else{
      redirect("orders/payment");
    }
  }

  public function payment(){
    $products = $this->session->userdata("products");

    $id_office = (isset($products["id_office"])) ? $products["id_office"] : $this->input->post("id_office");
    $id_customer = (isset($products["id_customer"])) ? $products["id_customer"] : $this->input->post("id_customer");
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $this->customer->get_detail_item($id_customer);
    $this->data['products'] = $products;

    $rank = $this->customer->get_rank($id_office, $id_customer);
    if(count($rank) > 0){
      $temp = explode(" ", $rank->period_month);
      $rank->period_month = ucwords(to_indonesian_month($temp[0])) . ' ' . $temp[1];
    }else{
      $rank = array(
        "id_office" => $products["id_office"],
        "id_customer" => $products["id_customer"],
        "period" => "",
        "period_month" => "",
        "amount" =>0,
        "rank" => 0,

      );

      $rank = (object)$rank;
    }
    $this->data['rank'] = $rank;


    if($this->input->post()){
      $cart = $this->cart->contents();
      /*
      ** Payment Method:
      ** 0. Pay later --> Just created Invoice
      ** 1. Cash
      ** 2. Transfer
      */

      $payment_method = $this->input->post("payment_method");
      $total_paid = unformat_money($this->input->post("total_paid")); //paid from purchase
      $paid_invoices = $this->input->post("paid_invoices"); //paid from make payment
      $is_down_payment = ($this->input->post("is_dp")) ? $this->input->post("is_dp") : 0; //is_downpayment

      $invoices = array();
      $result = true;

      if(isset($cart) && count($cart) > 0) {
        //Generate Invoice
        $transaction = array(
          "id_office" => $products["id_office"],
          "id_customer" => $products["id_customer"],
          "transaction_type" => 1, //purchase
          "handle_by_id" => $this->session->userdata("user_id"),
        );

        $transaction["invoice"] = array(
           "id_transaction" => 0,
           "invoice_number" => "",
           "amount" => $products["summary"]["subtotal"],
           "discount" => $products["summary"]["payment_discount"],
           "discount_amount" => $products["summary"]["payment_discount_amount"],
        );

        foreach($products["cart"] as $row) {
          $temp = explode("_", $row["id"]);

          $amount= ($row["price"] * $row["qty"]);
          $discount = round(($row["discount"] / 100 * $amount));
          $id_plat = $temp[1];
          $id_service = $temp[1];

          if($row["options"]["type"] == 2){
            if(!empty($row["minute"]) && $row["minute"] > 0){
              $amount= ($row["price"] * $row["minute"] * $row["qty"]);
            }
            
            $id_plat = 0;
          }else{
            $id_service = 0;
          }

          $transaction["invoice"]["items"][] = array(
             "id_invoice" => 0,
             "id_plat" => $id_plat,
             "id_service" => $id_service,
             "price" => $row["price"],
             "qty" => $row["qty"],
             "minutes" => $row["minute"],
             "amount" => $amount,           
             "discount" => $row["discount"],
             "discount_amount" => $discount,
          );
        }//end foreach

        $invoices = $this->transaction->generate_invoice($transaction);

        $result = (count($invoices) > 0) ? true : false;

        $this->cart->destroy();
      }

      if($result && $payment_method > 0) {

        //Generate Payment
        $transaction = array(
          "id_office" => $id_office,
          "id_customer" => $id_customer,
          "transaction_type" => 2, //purchase
          "handle_by_id" => $this->session->userdata("user_id"),
        );

        $transaction["payment"] = array(
           "payment_method_id" => $payment_method, 
        );
        /*
        payment data format
        array(
            "id_invoice" => $id_invoice,
            "paid" => 0
          );
        */

        //paid from purchasing
        if(count($products)>0 && isset($total_paid) && $total_paid > 0){
          $payments[] = array(
              "id_invoice" => $invoices["id_invoice"],
              "paid" => $total_paid,
              "is_down_payment" => $is_down_payment,
            );
        }
        //paid from make payment
        else {
          if(isset($paid_invoices) && count($paid_invoices) > 0) {
            foreach($paid_invoices as $id_invoice => $amount){
              $payments[] = array(
                "id_invoice" => $id_invoice,
                "paid" => $amount,
                "is_down_payment" => $is_down_payment,
              );
            }
          }
        }

        $transaction["payment"]["invoices"] = $payments;
        $id_payment = $this->transaction->generate_payment($transaction);

        if($id_payment > 0){
          //if success creating payment then save attachment if any
          if(isset($_FILES['files']['name'])) {
            $this->load->helper("url");

            $office = $this->office->get_detail_item($id_office);
            $office_name = url_title($office->name, $separator = '-', TRUE);

            $uploadPath = "uploads/offices/".$office_name;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts";
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts/".$id_payment;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }
            
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = "*";
            $config['encrypt_name'] = true;

            $this->load->library("upload", $config);

            for($i=0; $i<count($_FILES['files']['name']); $i++){
              if(!empty($_FILES['files']['name'][$i]) && $_FILES['files']['error'][$i] == 0){

                $filename = $_FILES['files']['name'][$i];
                $_FILES['file']['name'] = $filename;
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                if($this->upload->do_upload('file')){
                  $uploadData = $this->upload->data();
                  $data['id_payment'] = $id_payment;
                  $data['original_name'] = $filename;
                  $data['encrypted_name'] = $uploadData['file_name'];
                  $data['ext'] = $uploadData['file_ext'];
                  $data['location'] = $uploadPath;
                  $data['size'] = $uploadData['file_size'];
                  $this->db->insert('accounting_payment_attachments',$data);
                }
              }
            }    
          }
        }
      }      

      $this->session->unset_userdata('products');

      $this->session->set_userdata('notification', array("status" => true, "title" => "Informasi", "msg" => "Input penjualan berhasil disimpan."));
      
      echo json_encode(array(
          "success"=>1, 
          "msg"=>""
      ));
    }
    else{      
      $valid = 1;
      $msg = array();
      foreach($products["cart"] as $row){
        $temp = explode("_", $row["id"]);
        $type_product = $temp[0];
        $id_product = $temp[1];

        if($type_product == 1){
          $plat = $this->transaction->get_plat($id_product);
          if(count($plat) > 0 && $row["qty"] > $plat->qty){
            $valid = 0;
            $msg[] = "Stok produk <strong>".$row["name"]." ($plat->qty)</strong> tidak mencukupi.";
          }
        }
      }

      if($valid < 1){
        $this->session->set_flashdata('msg_alert', implode("<br>", $msg));
        redirect("orders/show_cart/");
      }else{
        $this->data['before_body'] = $this->load->view('script_transaction', '', true);
        $this->render('payment', 'admin_master');   
      }
    }    
  }

  public function get_offices_list(){
    $params = array();
    $list = array();

    
    $id_office = $this->session->userdata("id_office");
    if(isset($id_office) && !empty($id_office)){
      $params[] = "id = $id_office"; 
    }else if($this->session->userdata("order_id_customer")){
      $id_customer = $this->session->userdata("order_id_customer");
      $customer = $this->customer->get_detail_item($id_customer);

      $id_office = $customer->id_office;

      $params[] = "id = " .$id_office;
    }else{
      $id_office = "";      
    }

    $offices = $this->office->get_all_items($params);

    if(!empty($id_office)){}
    else{
      $list[] = array(
        "id" => 0,
        "text" => "--Pilih Kantor Cabang--",
      );
    }
    foreach($offices as $row){
      if($row->id == $id_office){
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
          "selected" => true
        );
      }
      else{
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
        );  
      }
      
    }

    echo json_encode(array("data" => $list));
  }

  public function get_customers_list(){
    $id_office = $this->input->post("id_office");
    $id_customer = ($this->session->userdata("order_id_customer")) ? $this->session->userdata("order_id_customer") : "";

    $params[] = "id_office = $id_office";
    if(!empty($id_customer)){
      $params[] = "id = $id_customer";
    }
    
    $list = array();
    $customers = $this->customer->get_all_items($params);

    if(!empty($id_customer)){}
    else{
      $list[] = array(
        "id" => 0,
        "text" => "--Pilih Customer--",
      );
    }

    foreach($customers as $row){
      if(!empty($id_customer) && $row->id == $id_customer){
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
          "selected" => true,
        );
      }
      else{
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
        );
      }
    }

    echo json_encode(array("data" => $list));
  }


}