<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales_model extends CI_Model
{
	private $_table = 'sales';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND s.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				s.`id` as id,
				s.`nik` as `nik`,
				s.`name`, 
				s.`address`, 
				s.`phone`, 
				s.`email`,
				s.`id_office`,
				o.`name` as name_office,
				IFNULL(cust.`jml_cus`, 0) as jml_cus
			FROM `sales` s 
			LEFT JOIN `offices` o ON o.`id` = s.`id_office` 
			LEFT JOIN (
				SELECT c.`id_sales`, IFNULL(COUNT(c.`id_sales`),0) jml_cus 
				FROM customers c 
				WHERE c.`deleted` = 0
				GROUP BY c.`id_sales`
			)cust ON cust.`id_sales` = s.`id`
			WHERE s.`deleted` = 0 $filter_office 
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND s.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				s.`id` as id,
				s.`nik` as `nik`,
				s.`name`, 
				s.`address`, 
				s.`phone`, 
				s.`email`,
				s.`id_office`,
				o.`name` as name_office,
				IFNULL(cust.`jml_cus`, 0) as jml_cus
			FROM `sales` s 
			LEFT JOIN `offices` o ON o.`id` = s.`id_office` 
			LEFT JOIN (
				SELECT c.`id_sales`, IFNULL(COUNT(c.`id_sales`),0) jml_cus 
				FROM customers c 
				WHERE c.`deleted` = 0
				GROUP BY c.`id_sales`
			)cust ON cust.`id_sales` = s.`id`
			WHERE s.`deleted` = 0 $filter_office 
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				s.`id` as id,
				s.`nik` as `nik`,
				s.`name`, 
				s.`address`, 
				s.`phone`, 
				s.`email`,
				s.`id_office`,
				o.`name` as name_office
			FROM `sales` s 
			LEFT JOIN `offices` o ON o.`id` = s.`id_office`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();
		$data["created_by"] = $this->session->userdata("user_id");

		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, 
			array(
				"deleted" => 1, 
				"deleted_on" => get_current_datetime(),
				"deleted_on" => $this->session->userdata("user_id")
			)
		);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function get_all_customers($id_sales)
	{
		$sql = "
		SELECT * 
		FROM customers c 
		WHERE c.`id_sales` = " . $id_sales ;

		return $this->db->query($sql)->result();
	}

	public function insert_customers($data)
	{
		$table = "customers";

		$inserted = 0;
		$this->db->trans_begin();

		foreach ($data as $d) {
			$this->db->insert($table, $d);
			if($this->db->affected_rows() > 0) {
				$inserted++;
			}
		}		

		if($inserted > 0 && $inserted == count($data)) {
			$this->db->trans_commit();

			return true;
		}
		else {
			$this->db->trans_rollback();

			return false;
		}
	}

	public function update_customer($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update("customers", $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_customer($id_sales, $id_customer)
	{
		$this->db->trans_start();

		$this->db->where("id", $id_customer);
		$this->db->where("id_sales", $id_sales);
		$this->db->delete("customers");

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}
}
