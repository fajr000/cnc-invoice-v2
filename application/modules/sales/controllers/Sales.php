<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends Admin_Controller
{ 
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Sales_model', 'sales');
    $this->load->model('offices/Offices_model', 'office');
    $this->load->model('customers/Customers_model', 'customer');

    $this->data['page_title'] = 'Sales | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Sales';
    $this->data['page_subheader'] = 'Bagian Penjualan';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_sales_view', 'admin_master');
  }

  public function get_sales_list()
  {
    // populate data
    $params = array();

    $all_items = $this->sales->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" . $row->id . ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->sales->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_sales_view', 'admin_master'); 
  }

  public function edit($id_item)
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->sales->get_detail_item($id_item);

    if(!has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_sales_view', 'admin_master');
  }

  public function delete($id_item)
  {
    $result = $this->sales->delete_item($id_item);   

    if($result > 0){
      $customers = $this->sales->get_all_customers($id_item);

      foreach($customers as $row) {
        $this->customer->delete_item($row->id);
      }

      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('sales');
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id       = $this->input->post('id');
    $nik      = $this->input->post('nik');
    $name     = $this->input->post('name');
    $address  = $this->input->post('address');
    $phone    = $this->input->post('phone');
    $email    = $this->input->post('email');
    $id_office= $this->input->post('id_office');

    $temp = array(
      'id' => $id,
      'nik' => $nik,
      'name' => $name,
      'address' => $address,
      'phone' => $phone,
      'email' => $email,
      'id_office' => $id_office,
    );

    // validate form input
    $this->form_validation->set_rules('nik', 'NIK', 'trim|required');
    $this->form_validation->set_rules('name', 'Nama', 'trim|required');

    $error = "";
    if ($this->form_validation->run($this) === TRUE){

      $data = array(
        'nik' => $nik,
        'name' => $name,
        'address' => $address,
        'phone' => $phone,
        'email' => $email,
        'id_office' => $id_office,
      );

      //register
      if(empty($id)) {
          $result = $this->sales->insert_item($data);

          if($result > 0){
            $id = $result;
          }
      }
      //edit
      else {
          $result = $this->sales->update_item($id, $data);
          if($result > 0) {
            
          }
      }
    }
    else{
      $result = 0;

      $error =  str_replace("<p>", "", validation_errors());
      $error =  str_replace("</p>", "<br/>", $error);
    }

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil disimpan.';

      $this->session->set_flashdata('notif', $notif);
      redirect('sales');
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = '<strong>Data gagal disimpan!</strong>' . '<br/>' . $error;

      $this->session->set_flashdata('notif', $notif);

      // populate data
      $item = $temp;

      $this->data['new'] = true;
      $this->data['item'] = (object)$item;
      $this->data['offices'] = $this->office->get_all_items();
      $this->data['office'] = get_office_id();
    
      $this->data['before_body'] = $this->load->view('script', '', true);

      $this->render('edit_sales_view', 'admin_master'); 
    }
  }
}