<style type="text/css">
.center-text{
  text-align: center;
}

.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
}
.middle-text{
  vertical-align: middle !important;
}
.left-text{
  text-align: left !important;
}
.right-text{
  text-align: right !important;
}
.no{
  width: 2%;
}
.description{
  width: 50%;
}
.price{
  width: 15%;
}
.price_total{
  width: 15%;
}
.disc{
  width: 10%;
}
.qty{
  width: 8%;
}
.view-invoice{
  cursor: pointer;
}
@media print{
   body{
       font-size:20px;
   }
}
</style> 
<section class="invoce">
  <div class="row">
    <div class="col-md-12">
      <div class="invoice">
        <!-- title row -->
        <div class="row" style="border-bottom: 2px solid #d16900;">
          <div class="col-6">
            <h4 style="font-weight: 800;">PIONEER CNC INDONESIA</h4>
            <!-- info row -->
            <div class="row invoice-info">
              <div class="col-sm-12 invoice-col" style="font-size: 1rem; line-height: 1.3rem;">
                <address>
                  <strong><?php echo $office->name;?></strong><br>
                  <?php echo $office->address;?><br>
                  HP.<?php echo $office->phone;?><br>
                  <?php echo $office->website;?><br>
                  <?php echo $office->email;?>
                </address>
              </div>
              <!-- /.col -->
            </div>
            <!-- /.row -->
          </div>
          <div class="col-6 text-right" >
            <div class="widget-user-image">
              <img height="100" class="" src="assets/images/logo-long.png" alt="User Avatar">
            </div>
          </div>
          <!-- /.col -->
        </div>

        <div class="row">
          <div class="col-12 center-text" style="line-height: 1.2rem; margin-top: 20px;">
            <span style="font-weight: 800; font-size: 2rem; text-decoration: underline; letter-spacing: 8px;text-align:center;">INVOICE</span><br>
            <label>No : <?php echo $invoice_number;?></label>
          </div>
        </div>

        <div class="row">
          <div class="col-6" style="line-height: 1.2rem;">
            <span><strong>Kepada Yth.</strong></span><br>
            <span><?php echo $customer->name;?></span><br>
            <?php if(!empty($customer->name_contact)){
              echo "<span>Attn. $customer->name_contact</span><br>";
            }
            ?>
            <span><?php echo $customer->address;?></span><br>
            <span>Telp/HP. <?php echo $customer->phone;?></span><br>
          </div>
          <div class="col-6" style="line-height: 1.2rem; text-align: center; color: #d16900;"><br>
            <div style="font-size:  1.5rem;">PELAYANAN DILUAR JAM KERJA<br>TIDAK DILAYANI</div>
          </div>
          <div class="col-12" style="line-height: 1.2rem; margin-top: 10px;">
            <div class="" style="font-style: italic;">Dengan ini kami memohon kepada Saudara untuk melakukan pembayaran dengan rincian sebagai berikut:</div>
          </div>
        </div>

        <!-- Table row -->
        <div class="row">
          <div class="col-12 table-responsive">
            <table class="table table-bordered table-condensed table-sm" style="margin-top: 10px;">
              <thead>
              <tr>
                <th class="center-text no">No</th>
                <th class="center-text description">Deskripsi</th>
                <th class="center-text price">Harga (&#13217; )</th>
                <th class="center-text disc">Disc(%)</th>
                <th class="center-text qty">Qty (&#13217; )</th>
                <th class="center-text price_total">Subtotal</th>
              </tr>
              </thead>
              <tbody>
              <?php 
              $i = 1;
              foreach($invoice as $key=>$row){
                echo '
                <tr>
                <td class="center-text">'.$i.'</td>
                <td class="">'.$row->description.'</td>
                <td class="right-text">'.format_money($row->price).'</td>
                <td class="center-text">'.$row->discount.'</td>
                <td class="center-text">'.$row->qty.'</td>
                <td class="right-text" style="padding-right:10px;">'.format_money($row->due).'</td>
                </tr>
                ';

                $i++;
              }

              echo '
              <tr>
              <td colspan="5" class="right-text">Total</td>
              <td class="right-text" style="padding-right:10px;">'.format_money($total_due).'</td>
              </tr>';
              echo '
              <tr>
              <td colspan="5" class="right-text">Disc.('.$invoice_discount.'%)</td>
              <td class="right-text" style="padding-right:10px;">'.format_money($invoice_discount_amount).'</td>
              </tr>';

              if(isset($payments) && count($payments) > 0){
                foreach($payments as $row){
                  echo '
                  <tr>
                  <td colspan="5" class="right-text">'.$row["description"].'</td>
                  <td class="right-text">('.format_money($row["amount"]).')</td>
                  </tr>
                  ';
                }                      
              }

              $due = ($invoice_amount-$invoice_discount_amount-$invoice_payment_amount);
              echo '
              <tr>
              <td colspan="5" class="right-text"><strong>Sisa Tagihan</strong></td>
              <td class="right-text" style="padding-right:10px;"><strong>'.format_money($due).'</strong></td>
              </tr>';


              ?>
              </tbody>
            </table>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        <div class="row">
          <div class="col-12" style="line-height: 1.2rem; margin-top: 10px;">
            <?php 
            $due_txt = "<span style='font-style:italic;'><strong>LUNAS</strong></span>";
            if($due>0){
              $due_txt = ucwords(number_to_text($due)) . " Rupiah";
            }
            echo "Terbilang : <span style='font-style:italic;'>".$due_txt."</span>";?>
          </div>
        </div>

        <div class="row">
          <div class="col-6" style="line-height: 1.2rem; margin-top: 10px; font-size: 1rem;">
            <table>
              <tr>
                <td colspan="2">Catatan:</td>
              </tr>
              <tr>
                <td>1.</td>
                <td>Mohon pembayaran ditransfer ke rekening berikut ini :</td>
              </tr>
              <tr>
                <td></td>
                <td><?php 
                $account_name = isset($office_account->name) ? $office_account->name : "";
                $number_account = isset($office_account->number_account) ? $office_account->number_account : "";
                $owner_name = isset($office_account->owner_name) ? $office_account->owner_name : "";

                echo $account_name;?></td>
              </tr>
              <tr>
                <td></td>
                <td><?php 
                echo "No Rek. ".$number_account;?></td>
              </tr>
              <tr>
                <td></td>
                <td><?php echo "a.n. ".$owner_name;?></td>
              </tr>
              <tr>
                <td>2.</td>
                <td>Pembayaran baru dianggap sah setelah cek/giro dicairkan</td>
              </tr>
            </table>
          </div>
          <div class="col-6" style="line-height: 1.2rem;">
            <table style="width: 100%;">
              <tr><td class="center-text"><?php echo $office->city.", ".get_short_current_date();?></td></tr>
              <tr>
                <td class="center-text" style="padding-bottom: 5px;">
                  <br><br><br><br>
                  <span style="text-decoration: none;"><?php echo $settings["INVOICE_PERSON_NAME"];?></span>
                </td>
              </tr>
              <tr><td class="center-text"><span style="border-top: 1px solid #000;"><?php echo $settings["INVOICE_PERSON_POSITION"];?></span></td></tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
     
<script type="text/javascript">
  
    window.print();
    
    window.onafterprint = function() {
        history.go(-1);
    };
</script>