<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    vertical-align: middle;
  }
  
  .center-text{
    vertical-align: middle;
    text-align: center; 
  } 
  .right-text{
    vertical-align: middle;
    text-align: right;
  } 
  .left-text{
    vertical-align: middle;
    text-align: left; 
  } 
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">

              <div class="row" style="display: <?php echo ($this->session->userdata("id_office") && !empty($this->session->userdata("id_office"))) ? "none" : "block";?>;">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12 pull-left">
                      <input type="hidden" id="default_id_office" value="<?php echo $this->session->userdata("id_office");?>">
                      <div class="row">
                        <div class="col-md-8">
                          <div style="margin-bottom:5px;">
                            <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                            </select>
                          </div>    
                        </div>
                        <div class="col-md-4"></div>
                      </div>
                      <div class="row">
                        <div class="col-md-8">
                          <div style="margin-bottom:5px;">  
                            <select placeholder="Nama Customer" class="form-control" name="head_id_customer" id="head_id_customer" style="width: 50%;">
                            </select>
                          </div>
                        </div>
                        <div class="col-md-4"></div>
                      </div>
                    </div><!-- /.row -->
                  </div>
                </div> 

                <br/>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <input type="hidden" name="id_office" id="id_office" value="">
                    <input type="hidden" name="id_customer" id="id_customer" value="">
                    <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                      <thead>
                        <tr>
                          <th class="">&nbsp;</th>
                          <th class="">Tanggal</th>
                          <th class="">No. Invoice</th>
                          <th class="">Nama Customer</th>
                          <th class="">Jumlah</th>
                          <th class="">Status</th>
                          <th class="">&nbsp;</th> 
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div id="confirmation" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <input type="hidden" name="cancelledIdOffice" id="cancelledIdOffice" value="">
      <input type="hidden" name="cancelledIdCustomer" id="cancelledIdCustomer" value="">
      <input type="hidden" name="cancelledIdInvoice" id="cancelledIdInvoice" value="">
      <div id="confirmation-text" class="modal-body">
        <p>Anda yakin akan membatalkan transaksi ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitCancel" class="btn btn-primary" style="display: none;">Ya</button>
        <button type="button" id="submitDelete" class="btn btn-primary" style="display: none;">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->