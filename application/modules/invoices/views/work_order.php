<style type="text/css">
.center-text{
  text-align: center;
}

.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
}
.middle-text{
  vertical-align: middle !important;
}
.left-text{
  text-align: left !important;
}
.right-text{
  text-align: right !important;
}
.no{
  width: 2%;
}
.description{
  width: 90%;
}
.price{
  width: 15%;
}
.price_total{
  width: 20%;
}
.disc{
  width: 10%;
}
.qty{
  width: 8%;
}
.view-invoice{
  cursor: pointer;
}

</style> 
<div class="content-header">
  <div class="container-fluid">  
    <div class="content">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-default card-outline">
              <div class="card-body">
                <div class="invoice p-3 mb-3">
                  <!-- title row -->
                  <div class="row" style="border-bottom: 2px solid #d16900;">
                    <div class="col-6">
                      <h4 style="font-weight: 800;">PIONEER CNC INDONESIA</h4>
                      <!-- info row -->
                      <div class="row invoice-info">
                        <div class="col-sm-12 invoice-col" style="font-size: 1rem; line-height: 1.3rem;">
                          <address>
                            <strong><?php echo $office->name;?></strong><br>
                            <?php echo $office->address;?><br>
                            HP.<?php echo $office->phone;?><br>
                            <?php echo $office->website;?><br>
                            <?php echo $office->email;?>
                          </address>
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <div class="col-6 text-right" >
                      <div class="widget-user-image">
                        <img height="100" class="" src="assets/images/logo-long.png" alt="User Avatar">
                      </div>
                    </div>
                    <!-- /.col -->
                  </div>

                  <div class="row">
                    <div class="col-12 center-text" style="line-height: 1.2rem; margin-top: 20px;">
                      <span style="font-weight: 800; font-size: 2rem; text-decoration: underline; letter-spacing: 8px;text-align:center;">SURAT PERINTAH KERJA</span><br>
                      <label>No. Invoice : <?php echo $invoice_number;?></label>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-6" style="line-height: 1.2rem;">
                      <span><strong>Kepada Yth.</strong></span><br>
                      <span>Operator</span><br/><br/>
                    </div>
                    <div class="col-6" style="line-height: 1.2rem; text-align: center; color: #d16900;"></div>
                    <div class="col-12" style="line-height: 1.2rem; margin-top: 10px;">
                      <div class="" style="font-style: italic;">Dengan ini kami memohon kepada Saudara untuk melakukan pekerjaan dengan rincian sebagai berikut:</div>
                    </div>
                  </div>

                  <!-- Table row -->
                  <div class="row">
                    <div class="col-12 table-responsive">
                      <table class="table table-bordered table-condensed table-sm" style="margin-top: 10px;">
                        <thead>
                        <tr>
                          <th class="center-text no">No</th>
                          <th class="center-text description">Deskripsi</th>
                          <th class="center-text qty">Qty (&#13217; )</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        $no = 1;
                        foreach($invoice as $key=>$row){
                          
                          echo '
                          <tr>
                          <td class="center-text" style="vertical-align:top;">'.$no.'</td>
                          <td class="" style="vertical-align:top;">'.$row->description.'</td>
                          <td class="center-text" style="vertical-align:top;">'.$row->qty.'</td>
                          </tr>
                          ';

                          $file_str = "";
                          if(count($row->files) > 0){
                            $filename = "";
                            echo '
                            <tr>
                            <td class="center-text" style="vertical-align:top;"></td>
                            <td class="" style="vertical-align:top;"><strong>Nama File:</strong></td>
                            <td class="center-text" style="vertical-align:top;"></td>
                            </tr>
                            ';
                            for($i=0;$i<count($row->files);$i++){
                              $fl = $row->files;
                              $filename = $fl[$i]->original_name."<br>";

                              echo '
                              <tr>
                              <td class="center-text" style="vertical-align:top;"></td>
                              <td class="" style="vertical-align:top;">'.$filename.'</td>
                              <td class="center-text" style="vertical-align:top;"></td>
                              </tr>
                              ';
                            }
                          }

                          $no++;
                        }

                        echo '
                        <tr>
                            <td colspan="3" class="center-text" style="vertical-align:top;">&nbsp;</td>
                        </tr>
                        <tr>
                            <td class="center-text" style="vertical-align:top;"></td>
                            <td class="" style="vertical-align:top;">';
                            foreach($token as $tok){
                            echo '
                              <div>Token: '.$tok->token_order.'#'.$tok->minutes.' menit : '.$tok->token .'</div>
                            ';
                            }  
                            echo '
                            </td>
                            <td class="center-text" style="vertical-align:top;"></td>
                        </tr>';
                        
                        echo '
                        <tr>
                            <td class="center-text" style="vertical-align:top;"></td>
                            <td class="" style="vertical-align:top;">';
                            foreach($token as $tok){
                            echo '
                            <div class="widget-user-image" style="text-align:center; float:left; width: 120px;">
                                <img height="100" class="" src="assets/images/qrcode/'.$tok->token.'.png" alt="User Avatar">
                                <br>'.$tok->token_order.'#'.$tok->minutes.'
                            </div>
                            
                            ';
                            }  
                            echo '
                            <div style="clear:both;"></div>
                            </td>
                            <td class="center-text" style="vertical-align:top;"></td>
                        </tr>';
                        
                        ?>
                        </tbody>
                      </table>
                    </div>
                    <!-- /.col -->
                  </div>
                  <!-- /.row -->

                  <div class="row">
                    <div class="col-6" style="line-height: 1.2rem; margin-top: 10px; font-size: 1rem;">
                      
                    </div>
                    <div class="col-6" style="line-height: 1.2rem;">
                      <table style="width: 100%;">
                        <tr><td class="center-text"><?php echo $office->city.", ".get_short_current_date();?></td></tr>
                        <tr>
                          <td class="center-text" style="padding-bottom: 5px;">
                            <br><br><br><br>
                            <span style="text-decoration: none;"><?php echo $this->session->userdata("name");?></span>
                          </td>
                        </tr>
                        <tr><td class="center-text"><span style="border-top: 1px solid #000;"><?php echo ucwords($this->session->userdata("group_name"));?></span></td></tr>
                      </table>
                    </div>
                  </div>

                </div>

                <!-- this row will not appear when printing -->
                <div class="row no-print">
                  <div class="col-12">                    
                    <a href="invoices/go_work?id_office=<?php echo encrypt_url($office->id); ?>&id_customer=<?php echo encrypt_url($customer->id); ?>&id_invoice=<?php echo encrypt_url($id_invoice); ?>&print=1'" class="btn btn-danger float-right btn-sm"  style="margin-right: 5px;" rel="noopener" target="_self" ><i class="fas fa-print"></i> Print</a> 
                    <button onclick="history.go(-1);" class="btn btn-default float-right btn-sm"  style="margin-right: 5px;"><i class="fas fa-step-backward"></i> Close</button> 
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
// window.addEventListener("load", window.print());
</script>