<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
select option:disabled {
    color: #000;
    background-color: #ccc;
}
</style>
<div class="content-header">
  
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user"> 
                <div class="row">
                  <div class="col-md-6"> 
                    <table>
                      <tr><td><strong>No. Invoice</strong></td><td>:</td><td><?php echo $invoices["invoice_number"];?></td></tr>
                      <tr><td><strong>Tgl. Transaksi</strong></td><td>:</td><td><?php echo $invoices["transacted_on"];?></td></tr>
                      <tr><td><strong>Nama Customer</strong></td><td>:</td><td><?php echo $customer->name;?></td></tr>
                      <tr><td><strong>Nama Petugas</strong></td><td>:</td><td><?php echo $invoices["name_operator"] ;?></td></tr>
                    </table>
                  </div>
                  <div class="col-md-6"> 
                    <table>
                    <tr>
                      <td style="width: 30%;"><strong>No. Mesin</strong></td>
                      <td>:</td>
                      <td style="width: 70%;" id="no_machine">
                        <?php
                        if(isset($invoices["token"]) && !empty($invoices["token"]->id)){

                        }else{

                        }
                        ?>
                        <input type="hidden" id="id_token" value="<?php echo (isset($invoices["token"]) && isset($invoices["token"]->id)) ? encrypt_url($invoices["token"]->id) : 0;?>">
                        <?php echo $invoices["machines"];?>
                          
                      </td>
                    </tr>
                    <tr>
                      <td style="vertical-align: top !important; padding-top: 10px;"><strong>Lama Pengerjaan</strong></td>
                      <td style="vertical-align: top !important; padding-top: 10px;">:</td>
                      <td id="workhour" style="vertical-align: middle;">
                        <?php 
                        //while invoice already confirmed and not yet processed, let user to generate token
                        if($invoices["status"] > 0 && $invoices["status"] <= 3){
                          $minutes = (isset($invoices["token"]) && isset($invoices["token"]->minutes)) ? $invoices["token"]->minutes : 0;

                          if(count($token_logs) > 0){
                            $temp1 = explode(",", $token_logs->no_order);
                            $temp2 = explode(",", $token_logs->minutes);
                            $temp3 = explode(",", $token_logs->token);
                            for($i=0;$i<count($temp1);$i++){
                              echo '
                              <div class="form-horizontal">
                                <div class="form-group row" style="margin-bottom:0px; margin-top:5px;">
                                    <div class="col-sm-2" style="padding-top:3px;"><input type="text" class="form-control form-control-sm" value ="#'.$temp1[$i].'" disabled></div>
                                    <div class="col-sm-3" style="padding-top:3px;"><input type="text" class="form-control form-control-sm" value ="'.$temp2[$i].'" disabled></div>
                                    <div class="col-sm-7"><strong>Token:</strong>'.$temp3[$i].'</div>
                                </div>
                              </div>
                              ';
                            }
                          }

                          $options = "";
                          for($i=1;$i<=9;$i++){
                            if(count($token_logs) > 0){
                              $logs = explode(",", $token_logs->no_order);
                              if(!in_array($i, $logs)){
                                $options[] = '<option value="'.$i.'">'.$i.'</option>';
                              }
                            }else{
                              $options[] = '<option value="'.$i.'">'.$i.'</option>';
                            }
                          }

                          echo '
                          <div class="form-horizontal">
                          <div class="form-group row">
                            <div class="col-sm-2">
                              <select name="order" id="order" class="form-control form-control-sm">
                                '.implode($options).'
                              </select>
                            </div>
                            <div class="col-sm-3">
                              <input type="text" class="form-control form-control-sm minutes" id="minutes" id="minutes" value="" placeholder="menit">
                            </div>
                          </div>
                          </div>
                          ';
                        }else{
                          $minutes = (isset($invoices["token"]) && isset($invoices["token"]->minutes)) ? $invoices["token"]->minutes : 0;

                          if(count($token_logs) > 0){
                            $temp1 = explode(",", $token_logs->no_order);
                            $temp2 = explode(",", $token_logs->minutes);
                            for($i=0;$i<count($temp1);$i++){
                              echo '
                              <div class="form-horizontal">
                              <div class="form-group row">
                              <div class="col-sm-2"><input type="text" class="form-control form-control-sm" value ="#'.$temp1[$i].'" disabled></div>
                              <div class="col-sm-3"><input type="text" class="form-control form-control-sm" value ="'.$temp2[$i].'" disabled></div>
                              </div>
                              </div>
                              ';
                            }
                          }
                        }
                        echo (isset($invoices["workhour"]) && !empty($invoices["workhour"])) ? $invoices["workhour"]."" : "";
                        ?>
                        
                      </td>
                    </tr>
                    <?php
                    // if(isset($invoices["token"]) && !empty($invoices["token"]->token)){
                    ?>
                    <!-- <tr>
                      <td style="vertical-align: top !important;"><strong>Token</strong></td>
                      <td style="vertical-align: top !important;">:</td>
                      <td style="vertical-align: top !important;"><?php //echo $invoices["token"]->token;?></td>
                    </tr> -->

                    <?php
                    // }
                    ?>
                    </table>
                  </div>
                </div>
                
                <form autocomplete="off" name="frmFile" id="frmFile" method="post" target="_self" action="invoice/upload_invoice_item_attachment" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo encrypt_url($invoices["id_office"]); ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo encrypt_url($invoices["id_customer"]); ?>">
                  <input type="hidden" name="id_invoice" id="id_invoice" value="<?php echo encrypt_url($invoices["id_invoice"]); ?>">
                  <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="center-text" style="width: 2%;"></th>
                            <th class="center-text" style="width: 50%;">Nama Produk</th>
                            <th class="center-text" style="width: 10%;">Harga (Rp.)</th>
                            <th class="center-text" style="width: 10%;">Jml</th>
                            <th class="center-text" style="width: 10%;">Diskon(%)</th>
                            <th class="center-text" style="width: 18%;">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      foreach ($invoices["items"] as $key => $row) {
                        echo '
                        <tr>
                        <td class="center-text">'.$i.'</td>
                        <td>'.$row["description"].'</td>
                        <td class="right-text">'.format_money($row["price"]).'</td>
                        <td class="center-text">'.$row["qty"].'</td>
                        <td class="center-text">'.$row["discount"].'</td>
                        <td class="right-text">'.format_money($row["due"]).'</td>
                        </tr>
                        ';

                        $files = array();
                        foreach ($row["files"] as $file) {
                          
                          $view = !exif_imagetype($file->location.'/'.$file->encrypted_name) ? "" : '
                          <div class="col-sm-1">
                            <a href="'.$file->location.'/'.$file->encrypted_name.'" data-toggle="lightbox" data-title="'.$file->original_name.'" data-gallery="gallery">
                              <i class="fas fa-eye"></i>
                            </a>
                          </div>';
                          $download = '<a target="_self" href="invoices/get_file/'.$file->id.'" alt="Download" title="Download" class="btn-sm btn-success"><i class="fas fa-download"></i></a>';
                          $delete = '<a  target="_self" alt="Hapus" title="Hapus" onclick="remove(this,\''.encrypt_url($file->id).'\')" class="btn-sm btn-danger btn-flat"><i class="fa fa-trash"></i></a>';

                          $files[] = '
                          <tr>
                          <td style="width:85%;">'.$file->original_name.'</td>
                          <td style="width:5%; text-align:center;">'.$view.'</td>
                          <td style="width:5%; text-align:center;">'.$download.'</td>
                          <td style="width:5%; text-align:center;">'.$delete.'</td>
                          </tr>
                          ';
                        }
                        $files_html = count($files) > 0 ? "<table style='margin-top:5px; width: 100%;' class='table table-condensed table-sm'>".implode("", $files)."</table>" : "";

                        $display = (empty($category) || $category == "confirm") ? "block" : "none";
                        echo '
                        <tr>
                        <td></td>
                        <td colspan="5">
                        <div class="row" style="display:'.$display.';">
                          <div class="col-md-2">
                            <button type="button" class="btn btn-block btn-success btn-sm" onclick="addFile('.$key.')"><i class="fas fa-plus"></i> Add File </button>
                            </div> 
                          <div class="col-md-3">
                            <small>(Max file size: '.$upload_max.')</small>
                          </div>
                        </div>
                          '.$files_html.'
                          <div class="file_container_'.$key.'">
                          </div>
                        </td>
                        </tr>
                        ';

                        $i++;
                      }

                      echo '
                      <tr>
                      <td colspan="5" class="right-text">Subtotal</td>
                      <td class="right-text">'.format_money($invoices["invoice_amount"]).'</td>
                      </tr>
                      <tr>
                      <td colspan="5" class="right-text">Disc.('.$invoices["invoice_discount"].'%)</td>
                      <td class="right-text">'.format_money($invoices["invoice_discount_amount"]).'</td>
                      </tr>
                      <tr>
                      <td colspan="5" class="right-text"><strong>Total</strong></td>
                      <td class="right-text"><strong>'.format_money($invoices["invoice_due"]).'</strong></td>
                      </tr>
                      ';

                      ?>
                    </tbody>
                  </table>
                
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="progress" style="display:none">
                  <div id="progressBar" class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only">0%</span>
                  </div>
                </div>
                <div class="msg alert alert-info text-left" style="display:none"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <br>
                <center>
                  <div class="row">
                    <?php 
                      $disabled_upload_file = "disabled";
                      $disabled_request_token = "disabled";
                      $disabled_generate_token = "disabled";

                      if(!is_operator()){
                        $disabled_upload_file = "";
                      }

                      if($invoices["status"] < 3 && !empty($invoices["machines"]) && !is_customer_services() && !is_operator()){
                        $disabled_request_token = "";
                      }

                      if(($invoices["status"] >= 1 && $invoices["status"] <= 3) && !is_branch_admin() && !is_customer_services() && !is_operator()){
                        $disabled_generate_token = "";
                      }

                      if($category == "confirm"){
                        echo '
                        <div class="col-md-2"></div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-step-backward"></i> Close</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-warning btn-sm" id="uploadfile" '.$disabled_upload_file.'><i class="fas fa-upload"></i> Upload File</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-primary btn-sm" id="requesttoken" '.$disabled_request_token.'><i class="fas fa-key"></i> Request Token</button>
                        </div>   
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-danger btn-sm" id="print" onclick="window.location.href=\'invoices/go_print?id_office='.encrypt_url($invoices["id_office"]).'&id_customer='.encrypt_url($invoices["id_customer"]).'&id_invoice='.encrypt_url($invoices["id_invoice"]).'&print=0\'"><i class="fas fa-print"></i> Print</button>
                        </div>    
                        <div class="col-md-2"></div>
                        ';
                      }
                      else if($category == "requesttoken"){
                        echo '
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-step-backward"></i> Close</button>
                        </div>   
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-info btn-sm" id="generatetoken" '.$disabled_generate_token.' ><i class="fas fa-history"></i> Generate Token</button>
                        </div>         
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-danger btn-sm" id="print" onclick="window.location.href=\'invoices/go_print?id_office='.encrypt_url($invoices["id_office"]).'&id_customer='.encrypt_url($invoices["id_customer"]).'&id_invoice='.encrypt_url($invoices["id_invoice"]).'&print=0\'"><i class="fas fa-print"></i> Print</button>
                        </div>    
                        <div class="col-md-3"></div>
                        ';
                      }
                      else if($category == "done" || $category == "cancel"){
                        echo '
                        <div class="col-md-4"></div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-step-backward"></i> Close</button>
                        </div>';
                        echo '
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-danger btn-sm" id="print" onclick="window.location.href=\'invoices/go_print?id_office='.encrypt_url($invoices["id_office"]).'&id_customer='.encrypt_url($invoices["id_customer"]).'&id_invoice='.encrypt_url($invoices["id_invoice"]).'&print=0\'"><i class="fas fa-print"></i> Print</button>
                        </div>    
                        <div class="col-md-4"></div>
                        ';
                      }
                      else if($category == "waitinglist"){
                        echo '
                        <div class="col-md-3"></div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-step-backward"></i> Close</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-primary btn-sm" id="run_process" data-id_office="'.encrypt_url($invoices["id_office"]).'" data-id_customer="'.encrypt_url($invoices["id_customer"]).'" data-id_invoice="'.encrypt_url($invoices["id_invoice"]).'"><i class="fas fa-cogs"></i> Process</button>
                        </div>  
                        ';
                        echo '
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-danger btn-sm" id="print" onclick="window.location.href=\'invoices/go_work?id_office='.encrypt_url($invoices["id_office"]).'&id_customer='.encrypt_url($invoices["id_customer"]).'&id_invoice='.encrypt_url($invoices["id_invoice"]).'&print=0\'"><i class="fas fa-print"></i> Cetak Perintah Kerja</button>
                        </div>    
                        <div class="col-md-3"></div>
                        ';
                      }
                      else if($category == "processing"){
                        echo '
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-default btn-sm" onclick="window.close();"><i class="fas fa-step-backward"></i> Close</button>
                        </div>
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-primary btn-sm" id="run_process" data-id_office="'.encrypt_url($invoices["id_office"]).'" data-id_customer="'.encrypt_url($invoices["id_customer"]).'" data-id_invoice="'.encrypt_url($invoices["id_invoice"]).'"><i class="fas fa-download"></i> Download Files</button>
                        </div>  
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-info btn-sm" id="generatetoken" '.$disabled_generate_token.' ><i class="fas fa-history"></i> Generate Token</button>
                        </div>  
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-success btn-sm" id="endprocess"><i class="fas fa-check"></i> Selesai</button>
                        </div>     
                        <div class="col-md-2">
                          <button type="button" class="btn btn-block btn-danger btn-sm" id="print" onclick="window.location.href=\'invoices/go_print?id_office='.encrypt_url($invoices["id_office"]).'&id_customer='.encrypt_url($invoices["id_customer"]).'&id_invoice='.encrypt_url($invoices["id_invoice"]).'&print=0\'"><i class="fas fa-print"></i> Print</button>
                        </div>    
                        <div class="col-md-1"></div>
                        ';
                      }

                    ?>

                  </div>
                </center>
              </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="notification" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p id="notif_msg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
  window.onunload = refreshParent;
  function refreshParent() {
      window.opener.location.reload();
  }
</script>