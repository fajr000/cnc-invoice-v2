<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();


			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		var table = $('#list_data').DataTable( {
	        "ajax": "invoices/get_invoice_list",
	        "columns": [
	            { data: "id_office", className: "center-text" },
	            { data: "transacted_on", className: "center-text" },
	            { data: "invoice_number", className: "center-text" },
	            { data: "name_customer", className: "" },
	            { data: "total", className: "right-text" },
	            { data: "status_label", className: "center-text" },
	        	{ data: "id_invoice", className: "",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(oData.tools);
			        } 
	        	}
	        ],
	        'order': [[0, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

		if ($('#head_id_office').length > 0) {
			$.ajax({
				url: "invoices/get_offices_list", 
				type: "post",
				dataType: "json",
				data: {
				},
				success: function(result){

		  			$("#head_id_office").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Kantor Cabang--",
		  			});

		  			var id_office = $("#default_id_office").val();

		  			if(id_office.length > 0){
		  				$('#head_id_office').val(id_office).trigger('change');
		  			}
				}
			});
		}

		$('#head_id_office').on('change', function (e) {
		  	var id_office = $("#head_id_office").find("option:selected").val();

		  	if(id_office == "0"){
		  		$("#id_office").val("");
		  	}else{
		  		$("#id_office").val(id_office);
		  	}

		  	if(id_office == "0"){
		  		$('#list_data').DataTable().column(0).search('').draw();
		  		if ($('#head_id_customer').hasClass("select2-hidden-accessible")) {
				    // Select2 has been initialized
				    $("#head_id_customer").select2('destroy');
				}
		  		
		  	}else{		  		
		  		if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}

				$.ajax({
					url: "invoices/get_customers_list", 
					type: "post",
					dataType: "json",
					data: {
						id_office : id_office
					},
					success: function(result){
						if ($('#head_id_customer').hasClass("select2-hidden-accessible")) {
						    // Select2 has been initialized
						    $("#head_id_customer").select2('destroy');
						}
						$("#head_id_customer").html("");
			  			$("#head_id_customer").select2({
			  				data : result.data,
			  				theme: 'bootstrap4',
			  				placeholder: "--Pilih Customer--",
			  			});
					}
				});	
		  	}	  	
		});

		$('#head_id_customer').select2({
			theme: 'bootstrap4',
		  	placeholder: "--Pilih Customer--",
		});

		$('#head_id_customer').on('select2:select', function (e) {
		  	var data = e.params.data;

		  	$("#id_customer").val(data.id);
		  	
		  	if(data.id == "0"){
		  		$('#list_data').DataTable().column(3).search('').draw();
		  	}else{		  		
		  		if ( table.column(3).search() !== data.text ) {
					table
					.column(3)
					.search( data.text )
					.draw();
				}
		  	}
		});

		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
			event.preventDefault();
			$(this).ekkoLightbox({
				alwaysShowClose: true
			});
		});

		$('.money').mask('000.000.000.000.000', {reverse: true});
		$('.minutes').mask('0000', {reverse: true});
		

		$(".custom-file-input").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});

		$("#uploadfile").on("click", function(){
			var bar = $('#bar');
  			var percent = $('#percent');

  			var form = $('#frmFile')[0];
			var data = new FormData(form);	

			$('.msg').hide();
			$('.progress').show();

			$.ajax({
				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(e){
						if(e.lengthComputable){
							console.log('Bytes Loaded : ' + e.loaded);
							console.log('Total Size : ' + e.total);
							console.log('Persen : ' + (e.loaded / e.total));
							
							var percent = Math.round((e.loaded / e.total) * 100);
							
							$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
						}
					});
					return xhr;
				},
				
				type : 'POST',
				url : "invoices/upload_invoice_item_attachment",
				data : data,
				processData : false,
				contentType : false,
				success : function(response){
					$('form')[0].reset();
					$('.progress').hide();
					$('.msg').show();
					if(response == ""){
						$('.msg').html('File gagal di upload!');
					}else{
						var msg = response;
						$('.msg').html(msg+". <span style='color:#fff;'>Halaman akan direfresh dalam <span id='timeleft'>5</span> detik.</span>");

						var countDownDate = new Date().getTime()+6000;
						var x = setInterval(function() {
							var now = new Date().getTime();
							var distance = countDownDate - now;
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);

							document.getElementById("timeleft").innerHTML = seconds;

							// If the count down is finished, write some text
							if (seconds < 1) {
								clearInterval(x);
								history.go(0);
							}
						}, 1000);

					}
				}
			});
		});

		$("#requesttoken").on("click", function(){
			$("#requesttoken").prop("disabled", true);
			var option = $("#id_machine").find("option:selected");
			var id_office = $(option).data("id_office");
			var id_customer = $(option).data("id_customer");
			var id_invoice = $(option).data("id_invoice");
			var no_invoice = $(option).data("no_invoice");
			var id_machine = $(option).val();
			var no_machine = $(option).text();
			
			$.ajax({
				url: "invoices/request_token", 
				type: "post",
				dataType: "json",
				data: {
					id_office : id_office,
					id_invoice : id_invoice,
					no_invoice : no_invoice,
					id_machine : id_machine,
					no_machine : no_machine,
				},
				success: function(result){

					if(result.success == 1){
						$("#notif_msg").html("Request Token berhasil.");
						$("#notification").modal("show");

						var countDownDate = new Date().getTime()+6000;
						var x = setInterval(function() {
							var now = new Date().getTime();
							var distance = countDownDate - now;
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);

							// If the count down is finished, write some text
							if (seconds < 1) {
								clearInterval(x);
								history.go(0);
							}
						}, 1000);
					}else{
						$("#notif_msg").html("Request Token gagal!");
						$("#notification").modal("show");
					}
				}
			});	
			// $("#requesttoken").prop("disabled", true);
		});

		
		$("#generatetoken").on("click", function(){
			var id_token = $("#id_token").val();
			var no_order = $("#order").find("option:selected").val();
			var minutes = $("#minutes").val();

			if(minutes.length <= 0){
				$("#notif_msg").html("Waktu (menit) tidak boleh kosong!");
				$("#notification").modal("show");

				return false;
			}

			$("#generatetoken").prop("disabled", true);
						
			$.ajax({
				url: "invoices/generate_token", 
				type: "post",
				dataType: "json",
				data: {
					id_token : id_token,
					no_order : no_order,
					minutes : minutes,
				},
				success: function(result){

					if(result.success == 1){
						$("#notif_msg").html("Generate Token berhasil.");
						$("#notification").modal("show");

						var countDownDate = new Date().getTime()+6000;
						var x = setInterval(function() {
							var now = new Date().getTime();
							var distance = countDownDate - now;
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);

							// If the count down is finished, write some text
							if (seconds < 1) {
								clearInterval(x);
								history.go(0);
							}
						}, 1000);
					}else{
						$("#notif_msg").html("Generate Token gagal!");
						$("#notification").modal("show");
					}
				}
			});	
		});

		$("#run_process").on("click", function(){
			var id_office = $(this).data("id_office");
			var id_customer = $(this).data("id_customer");
			var id_invoice = $(this).data("id_invoice");
			var id_token = $("#id_token").val();

			$.ajax({
				url: "invoices/start_process", 
				type: "post",
				dataType: "json",
				data: {
					id_office : id_office,
					id_customer : id_customer,
					id_invoice : id_invoice,
					id_token : id_token,
				},
				success: function(result){

					if(result.success == 1){
						$("#notif_msg").html(result.msg);
						$("#notification").modal("show");

						var countDownDate = new Date().getTime()+6000;
						var x = setInterval(function() {
							var now = new Date().getTime();
							var distance = countDownDate - now;
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);

							// If the count down is finished, write some text
							if (seconds < 1) {
								clearInterval(x);
								if(result.link != "")
									location.href = result.link;
								else
									history.go(0);

							}
						}, 1000);
					}else{
						$("#notif_msg").html(result.msg);
						$("#notification").modal("show");
					}
				}
			});	

		});

		$("#endprocess").on("click", function(){
			var id_token = $("#id_token").val();
			var id_invoice = $("#id_invoice").val();

			$("#endprocess").prop("disabled", true);
						
			$.ajax({
				url: "invoices/end_process", 
				type: "post",
				dataType: "json",
				data: {
					id_invoice : id_invoice,
					id_token : id_token
				},
				success: function(result){

					if(result.success == 1){
						$("#notif_msg").html(result.msg);
						$("#notification").modal("show");

						var countDownDate = new Date().getTime()+6000;
						var x = setInterval(function() {
							var now = new Date().getTime();
							var distance = countDownDate - now;
							var seconds = Math.floor((distance % (1000 * 60)) / 1000);

							// If the count down is finished, write some text
							if (seconds < 1) {
								clearInterval(x);
								history.go(0);
							}
						}, 1000);
					}else{
						$("#notif_msg").html(result.msg);
						$("#notification").modal("show");
					}
				}
			});	
		});

		$("#submitCancel").on("click", function(){
        	var id_office = $("#cancelledIdOffice").val();
        	var id_customer = $("#cancelledIdCustomer").val();
        	var id_invoice = $("#cancelledIdInvoice").val();

        	window.location.href = "invoices/cancel?action=cancel&id_office="+id_office+"&id_customer="+id_customer+"&id_invoice="+id_invoice;
        });

        $("#submitDelete").on("click", function(){
        	var id_office = $("#cancelledIdOffice").val();
        	var id_customer = $("#cancelledIdCustomer").val();
        	var id_invoice = $("#cancelledIdInvoice").val();

        	window.location.href = "invoices/cancel?action=delete&id_office="+id_office+"&id_customer="+id_customer+"&id_invoice="+id_invoice;
        });
		
	});

	function invoiceTodo(elm, id_invoice, category = ""){
    	var option = $(elm).find("option:selected").val()
    	var id_office = $(elm).data("id_office");
    	var id_customer = $(elm).data("id_customer");
    	
    	if(option == "detail"){
    		openWindow('invoices/detail?cat='+category+'&id_office='+id_office+'&id_customer='+id_customer+'&id_invoice='+id_invoice, 'Detil Invoice', 'max', 'max');
    	}
    	else if(option == "cancel"){
    		$("#cancelledIdOffice").val(id_office);
    		$("#cancelledIdCustomer").val(id_customer);
    		$("#cancelledIdInvoice").val(id_invoice);
    		$('.modal-body').html("Anda yakin akan membatalkan transaksi ini?");
    		$("#submitCancel").show();
    		$("#submitDelete").hide(); 
			$('#confirmation').modal('show');
    	}
    	else if(option == "delete"){
    		$("#cancelledIdOffice").val(id_office);
    		$("#cancelledIdCustomer").val(id_customer);
    		$("#cancelledIdInvoice").val(id_invoice);
    		$('.modal-body').html("Anda yakin akan menghapus transaksi ini?");
    		$("#submitCancel").hide();
    		$("#submitDelete").show();
			$('#confirmation').modal('show');
    	}
    	else{
    		$("#cancelledIdOffice").val("");
    		$("#cancelledIdCustomer").val("");
    		$("#cancelledIdInvoice").val("");
    	}
    }   

    function addFile(id_invoice_item){
		var html = ''+
		'<div class="input-group row_file" style="margin: 5px 0;">'+
        '	<div class="custom-file">'+
        '   	<input type="file" class="custom-file-input form-control-sm" name="files[]['+id_invoice_item+']" onchange="showFileName(this)">'+
        '       <label class="custom-file-label col-form-label-sm" for="custmFile">Choose file</label>'+
        '   </div>'+
        '   <div class="input-group-append">'+
        '   	<button class="btn btn-warning btn-sm" type="button" onclick="removeFile(this)"><i class="fas fa-times-circle"></i> <span>Cancel</span></button>'+
        '  </div>'+
        '</div>';

        $(".file_container_"+id_invoice_item).append(html);
	}

	function removeFile(elm){
		var row = $(elm).closest("div.row_file");
		$(row).remove();
	}

	function showFileName(elm){
		var fileName = $(elm).val().split("\\").pop();
		$(elm).siblings(".custom-file-label").addClass("selected").html(fileName);
	}

	function remove(elm, id_file) {
		$.ajax({
			url: "invoices/remove_file", 
			type: "post",
			dataType: "json",
			data: {
				id_file : id_file
			},
			success: function(data){
				if(data.result == 1)	{
					$(elm).closest("tr").remove();
				}
			}
		});
	}
	
</script> 