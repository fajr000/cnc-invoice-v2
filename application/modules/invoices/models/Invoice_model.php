<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model
{

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		$office_id = $this->session->userdata("id_office");
		if(!is_admin() && !is_center_admin() && isset($office_id) && !empty($office_id)) {
			$params[] = "id_office = " . $office_id ;
		} 

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT ai.`id` id_invoice,
			       tr.`id` AS id_transaction,
			       tr.`id_office`,
			       tr.`id_customer`,
			       tr.`transacted_on`,
			       tr.`cancelled_on`,
			       tr.`posted`,
			       of.`name` name_office,
			       cr.`name` name_customer,
			       ai.`invoice_number`,
			       ai.`amount` due,
			       ai.`discount_amount` discount,
			       ( ai.`amount` - ai.`discount_amount` ) total,
			       ai.`status` status
			FROM   accounting_invoices ai
			       LEFT JOIN transactions tr ON tr.`id` = ai.`id_transaction`
			       LEFT JOIN offices of ON of.`id` = tr.`id_office`
			       LEFT JOIN customers cr ON cr.`id` = tr.`id_customer`
			#WHERE  tr.`cancelled_on` = '0000-00-00 00:00:00'
			#ORDER  BY IF(tr.`cancelled_on` = '0000-00-00 00:00:00', ai.`status`, 0) ASC, tr.`transacted_on` ASC 
	        ORDER  BY ai.`status` ASC, tr.`transacted_on` ASC 
			
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				ai.`id` as id,
				ai.`id_transaction`,
				ai.`invoice_number`, 
				ai.`amount`, 
				ai.`discount`,
				ai.`discount_amount`,
				ai.`status`,
				t.`id_office`,
				t.`id_customer`
			FROM `accounting_invoices` ai 
			LEFT JOIN `transactions` t ON t.`id` = ai.`id_transaction`
			LEFT JOIN `offices` o ON o.`id` = t.`id_office`
			LEFT JOIN `customers` c ON c.`id` = t.`id_customer`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	
}
