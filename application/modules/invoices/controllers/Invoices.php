<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends My_Controller {
	
	function __construct()
	{ 
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
	    $this->load->helper(array('url', 'language'));

	    $this->load->model('Invoice_model', 'invoice');
	    $this->load->model('offices/Offices_model', 'office');
	    $this->load->model('customers/Customers_model', 'customer');
	    $this->load->model('sales/Sales_model', 'sales');    
	    $this->load->model('transactions/Transaction_model', 'transaction');    

	    $this->data['page_title'] = 'Invoice | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Invoices';
	    $this->data['page_subheader'] = 'Daftar Invoice';

	    // Set pemission/ access
	    $class_name = $this->router->fetch_class();
	    set_parent_url($class_name);
	    $this->data['allowed'] = build_access($class_name);
	}

	public function index(){
		if (!logged_in())
		{
			//redirect them to the login page
			redirect('user/login', 'refresh');
		}

		$this->get_lists();        
	}

	public function get_lists(){
		// page script js
		$this->data['before_body'] = $this->load->view('script', '', true);
		// echo "<pre>";print_r($items);die();
		$this->render('list_invoice_view', 'admin_master');
	}

	public function get_invoice_list($id_office = null) {
		$params = array();

		if(isset($id_office) && !empty($id_office)){
			$id_office = decrypt_url($id_office);
			$params[] = "id_office = $id_office";
		}

	    $inv = $this->invoice->get_all_items($params);

	    foreach ($inv as $key => $row) {
	    	$inv[$key]->id_transaction = encrypt_url($row->id_transaction);
	    	$inv[$key]->id_office = encrypt_url($row->id_office);
	    	$inv[$key]->id_customer = encrypt_url($row->id_customer);
	    	$inv[$key]->id_invoice = encrypt_url($row->id_invoice);
	    	$inv[$key]->invoice_number = format_invoice($row->invoice_number);
	    	$inv[$key]->total = format_money($row->total);
	    	$inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

	    	$category = "";
	    	switch ($row->status) {
	    		case 1:
	    			$category = "requesttoken";
	    			$inv[$key]->status_label = '<small class="badge badge-info" style="font-weight: normal;"> Request Token </small>';
	    			break;
	    		case 2:
	    			$category = "waitinglist";
	    			$inv[$key]->status_label = '<small class="badge badge-warning" style="font-weight: normal;"> Waiting List </small>';
	    			break;
	    		case 3:
	    			$category = "processing";
	    			$inv[$key]->status_label = '<small class="badge badge-primary" style="font-weight: normal;"> Diproses </small>';
	    			break;
	    		case 4:
	    			$category = "done";
	    			$inv[$key]->status_label = '<small class="badge badge-success" style="font-weight: normal;"> Selesai </small>';
	    			break;
	    		case 5:
	    			$category = "cancel";
	    			$inv[$key]->status_label = '<small class="badge badge-danger" style="font-weight: normal;"> Dibatalkan </small>';
	    			break;
	    		
	    		default:
	    		$category = "confirm";
	    			$inv[$key]->status_label = '<small class="badge badge-secondary" style="font-weight: normal;"> Konfirmasi </small>';
	    			break;
	    	}

			$tools = "";
			
				$cancel_option = ($row->status < 3) ? '<option value="delete">Batal</option>' : '';
				$tools = '
				<select name="tools[]" class="form-control tool-act" data-id_office="'.$row->id_office.'" data-id_customer="'.$row->id_customer.'" onchange="invoiceTodo(this,\''.$row->id_invoice.'\', \''.$category.'\')">
				<option>--Pilih--</option>
				<option value="detail">Detail</option>
				'.$cancel_option.'
				</select>
				';
			

			$inv[$key]->tools = $tools;
	    }

	    echo json_encode(array("data"=>object_to_array($inv)));
	}

	public function get_offices_list(){
	    $params = array();
	    $list = array();
	    
	    $id_office = $this->session->userdata("id_office");
	    if(isset($id_office) && !empty($id_office)){
	      $params[] = "id = $id_office"; 
	    }else{
	      $id_office = "";      
	    }

	    $offices = $this->office->get_all_items($params);

	    if(!empty($id_office)){}
	    else{
	      $list[] = array(
	        "id" => 0,
	        "text" => "--Pilih Kantor Cabang--",
	      );
	    }
	    foreach($offices as $row){
	      if($row->id == $id_office){
	        $list[] = array(
	          "id" => encrypt_url($row->id),
	          "text" => $row->name,
	          "selected" => true
	        );
	      }
	      else{
	        $list[] = array(
	          "id" => encrypt_url($row->id),
	          "text" => $row->name,
	        );  
	      }
	      
	    }

	    echo json_encode(array("data" => $list));
	  }

	  public function get_customers_list(){
	    $id_office = $this->input->post("id_office");
	    $id_office = decrypt_url($id_office);

	    $params[] = "id_office = $id_office";
	    
	    $list = array();
	    $customers = $this->customer->get_all_items($params);

	    if(!empty($id_customer)){}
	    else{
	      $list[] = array(
	        "id" => 0,
	        "text" => "--Pilih Customer--",
	      );
	    }

	    foreach($customers as $row){
	      if(!empty($id_customer) && $row->id == $id_customer){
	        $list[] = array(
	          "id" => encrypt_url($row->id),
	          "text" => $row->name,
	          "selected" => true,
	        );
	      }
	      else{
	        $list[] = array(
	          "id" => encrypt_url($row->id),
	          "text" => $row->name,
	        );
	      }
	    }

	    echo json_encode(array("data" => $list));
	}

	public function detail(){
		$id_office = decrypt_url($this->input->get("id_office")); 
		$id_customer = decrypt_url($this->input->get("id_customer")); 
		$id_invoice = decrypt_url($this->input->get("id_invoice"));
		$category = ($this->input->get("cat")) ? $this->input->get("cat") : "";

		$post_max_size = parse_size(ini_get('post_max_size'));
		$upload_max = parse_size(ini_get('upload_max_filesize'));

		$cus = $this->customer->get_detail_item($id_customer);
		$inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);
		$det = $this->invoice->get_detail_item($id_invoice);
 
		$token = $this->db->query("SELECT * FROM tokens WHERE id_office = $id_office AND id_invoice = $id_invoice")->row();
		if(count($token) > 0){
			$token_logs = $this->db->query("SELECT GROUP_CONCAT(token_order) no_order, GROUP_CONCAT(minutes) minutes, GROUP_CONCAT(token) token FROM token_logs WHERE id_token = ".$token->id." GROUP BY id_token")->row();
			$this->data["token_logs"] = $token_logs;
		}else{
			$this->data["token_logs"] = array();
		}


		$machines = $this->db->query("SELECT of.`id`, of.`no_machine`, ct.`name` name_cutting_type FROM office_machines of LEFT JOIN cutting_types ct ON ct.`id` = of.`id_cutting_type` WHERE of.`id_office` = $id_office  AND of.`deleted` = 0  AND ct.`deleted` = 0")->result();
		$machines_arr = array();
		$machines_str = "";
		foreach($machines as $row){
			$selected = (count($token) > 0 && $token->id_machine == $row->id) ? "selected" : "";

			$machines_arr[] = '
			<option '.$selected.' data-id_office="'.$id_office.'" 
					data-id_customer="'.$id_customer.'" 
					data-id_invoice="'.$id_invoice.'" 
					data-no_invoice="'.$det->invoice_number.'" 
					value="'.$row->id.'">'.
					$row->name_cutting_type.'-'.$row->no_machine.
			'</option>';
		}
		$machines_str = '<select style="width:100%;" id="id_machine" name="id_machine" class="form-control form-control-sm">'.implode("", $machines_arr).'</select>';

		$inv_list = array();
		foreach ($inv as $row) {
			$inv_list["id_invoice"] = $row->id_invoice;
			$inv_list["id_office"] = $row->id_office;
			$inv_list["id_customer"] = $row->id_customer;
			$inv_list["invoice_number"] = format_invoice($row->invoice_number);
			$inv_list["invoice_amount"] = $row->invoice_amount;
			$inv_list["invoice_discount"] = $row->invoice_discount;
			$inv_list["invoice_discount_amount"] = $row->invoice_discount_amount;
			$inv_list["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
			$inv_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
			$inv_list["cancelled_on"] = $row->cancelled_on;
			$inv_list["posted"] = $row->posted;
			$inv_list["name_operator"] = $row->name_operator;
			$inv_list["status"] = $det->status;

			$inv_list["token"] = $token;
			$inv_list["machines"] = $machines_str;

			$minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";

			$machine = !(empty($row->machine)) ? $row->machine." " : "";
			$inv_list["items"][$row->id_invoice_item]["description"] = $row->product." ".$machine.$row->name_plat." ".$row->size."$minutes";
			$inv_list["items"][$row->id_invoice_item]["product"] = $row->product;
			$inv_list["items"][$row->id_invoice_item]["name_plat"] = $row->name_plat;
			$inv_list["items"][$row->id_invoice_item]["machine"] = $row->machine;
			$inv_list["items"][$row->id_invoice_item]["size"] = $row->size;
			$inv_list["items"][$row->id_invoice_item]["price"] = $row->price;
			$inv_list["items"][$row->id_invoice_item]["qty"] = $row->qty;
			$inv_list["items"][$row->id_invoice_item]["minute"] = $row->minute;
			$inv_list["items"][$row->id_invoice_item]["amount"] = $row->amount;
			$inv_list["items"][$row->id_invoice_item]["discount"] = $row->discount;
			$inv_list["items"][$row->id_invoice_item]["discount_amount"] = $row->discount_amount;
			$inv_list["items"][$row->id_invoice_item]["due"] = $row->due;

			$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id_invoice_item = $row->id_invoice_item";
			$files = $this->db->query($sql)->result();
			$inv_list["items"][$row->id_invoice_item]["files"] = $files;
		}

		$this->data["invoices"] = $inv_list;
		$this->data["customer"] = $cus;
		$this->data["category"] = $category;
		$this->data["post_max_size"] = formatSizeUnits($post_max_size);
		$this->data["upload_max"] = formatSizeUnits($upload_max);
		$this->data['before_body'] = $this->load->view('script', '', true);


		$this->render('detail', 'admin_clear'); 
	}

	public function cancel(){
		$action = $this->input->get("action");
		$id_office = $this->input->get("id_office");
		$id_customer = $this->input->get("id_customer");
		$id_invoice = $this->input->get("id_invoice");

		$id_office = decrypt_url($id_office);
		$id_customer = decrypt_url($id_customer);
		$id_invoice = decrypt_url($id_invoice);

		$transaction = array(
			"id_office" => $id_office,
			"id_customer" => $id_customer,
			"transaction_type" => 3, //cancel
			"handle_by_id" => $this->session->userdata("user_id"),
		);

		$transaction["invoices"][] = $id_invoice;

		$this->transaction->cancel_invoices($transaction);

		if($ction == "delete"){
			$invoice = $this->invoice->get_detail_item($id_invoice); 
			$trx_invoice = $this->transaction->get_detail_item($invoice->id_transaction);

			$this->transaction->delete_item($trx->id);
		}

		redirect("invoices");
	}

	public function go_print(){
		$id_office = decrypt_url($this->input->get("id_office"));
		$id_customer = decrypt_url($this->input->get("id_customer"));
		$id_invoice = decrypt_url($this->input->get("id_invoice"));
		$settings = $this->office->get_office_settings($id_office);
		$print = $this->input->get("print");

		$off = $this->office->get_detail_item($id_office);
	    $acc = $this->office->get_all_accounts($id_office);
	    $cus = $this->customer->get_detail_item($id_customer);
	    $inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);
	    $pay = $this->transaction->get_invoice_payments($id_office, $id_customer, $id_invoice);

	    $inv_list = array();
	    $invoice_number = "";
	    $total_due = 0;
	    $invoice_amount = 0;
	    $invoice_discount = 0;
	    $invoice_discount_amount = 0;
	    foreach ($inv as $key => $row) {
	      $inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

	      $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
	      $cancelled = ($row->cancelled_on != "0000-00-00 00:00:00") ? "cancelled" : "";
	      $cancelled_status = ($row->cancelled_on != "0000-00-00 00:00:00") ? '<small class="badge badge-danger"></i> Dibatalkan</small>' : '';

	      $machine = !(empty($row->machine)) ? $row->machine." " : "";
	      if(isset($inv[$key]->description)){
	        $inv[$key]->description .= "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
	      }
	      else{
	        $inv[$key]->description = "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
	      }

	      $total_due += $row->due;
	      $invoice_number = $row->invoice_number;
	      $invoice_amount = $row->invoice_amount;
	      $invoice_discount = $row->invoice_discount;
	      $invoice_discount_amount = $row->invoice_discount_amount;
	    }

	    $this->data['today'] = get_long_current_date();
	    $this->data['office'] = $off;
	    $this->data['office_account'] = (count($acc) > 0) ? $acc[0] : array();
	    $this->data['customer'] = $cus;
	    $this->data['invoice'] = $inv;
	    $this->data['id_invoice'] = $id_invoice;
	    $this->data['settings'] = $settings;
	    
	    $payments = array();
	    $payment_amount = 0;
	    foreach($pay as $row){
	      $payments[] = array(
	        "amount" => $row->amount,
	        "is_down_payment" => $row->is_down_payment,
	        "transacted_on" => $row->transacted_on,
	        "description" => ($row->is_down_payment) ? "DP" : "Pembayaran tgl. " . date_format(date_create($row->transacted_on),"d/m/Y")
	      );

	      $payment_amount += $row->amount;
	    }

	    $temp = explode("/", $invoice_number);
	    $this->data['total_due'] = $total_due;
	    $this->data['invoice_number'] = $temp[0]."/".$temp[1]."/".$temp[2]."/".to_romawi($temp[3])."/".$temp[4];
	    $this->data['invoice_amount'] = $invoice_amount;
	    $this->data['invoice_discount'] = $invoice_discount;
	    $this->data['invoice_discount_amount'] = $invoice_discount_amount;
	    $this->data['payments'] = $payments;
	    $this->data['invoice_payment_amount'] = $payment_amount;

	    $this->data['before_body'] = $this->load->view('script', '', true);

	    if(!$print){
	      $this->render('invoice', 'admin_clear'); 
	    }else{
	      $this->render('invoice_print', 'admin_clear'); 
	    }
	}

	public function go_work(){
		$id_office = decrypt_url($this->input->get("id_office"));
		$id_customer = decrypt_url($this->input->get("id_customer"));
		$id_invoice = decrypt_url($this->input->get("id_invoice"));
		$settings = $this->office->get_office_settings($id_office);
		$print = $this->input->get("print");

		$off = $this->office->get_detail_item($id_office);
	    $acc = $this->office->get_all_accounts($id_office);
	    $cus = $this->customer->get_detail_item($id_customer);
	    $inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);
	    $pay = $this->transaction->get_invoice_payments($id_office, $id_customer, $id_invoice);

	    $inv_list = array();
	    $invoice_number = "";
	    $total_due = 0;
	    $invoice_amount = 0;
	    $invoice_discount = 0;
	    $invoice_discount_amount = 0;
	    foreach ($inv as $key => $row) {
	      $inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

	      $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
	      $cancelled = ($row->cancelled_on != "0000-00-00 00:00:00") ? "cancelled" : "";
	      $cancelled_status = ($row->cancelled_on != "0000-00-00 00:00:00") ? '<small class="badge badge-danger"></i> Dibatalkan</small>' : '';

	      $machine = !(empty($row->machine)) ? $row->machine." " : "";
	      if(isset($inv[$key]->description)){
	        $inv[$key]->description .= "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
	      }
	      else{
	        $inv[$key]->description = "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
	      }

	      $total_due += $row->due;
	      $invoice_number = $row->invoice_number;
	      $invoice_amount = $row->invoice_amount;
	      $invoice_discount = $row->invoice_discount;
	      $invoice_discount_amount = $row->invoice_discount_amount;

	      $sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id_invoice_item = $row->id_invoice_item";
		  $files = $this->db->query($sql)->result();
		  $inv[$key]->files = $files;
	    }

	    $this->data['today'] = get_long_current_date();
	    $this->data['office'] = $off;
	    $this->data['office_account'] = (count($acc) > 0) ? $acc[0] : array();
	    $this->data['customer'] = $cus;
	    $this->data['invoice'] = $inv;
	    $this->data['id_invoice'] = $id_invoice;
	    $this->data['settings'] = $settings;
	    $this->data['token'] = $this->db->query("
	    	SELECT tl.`id_token`, tk.`id_office`, tk.`id_invoice`, tl.`id_machine`, tk.`invoice_number`, tk.`no_machine`, tl.`token_order`, tl.`minutes`, tl.`token`   
			FROM token_logs tl 
			LEFT JOIN tokens tk ON tk.`id` = tl.`id_token` 
			WHERE tk.`id_office` = $id_office AND tk.`id_invoice` = $id_invoice 
			ORDER BY tk.`id`")->result();
	    
	    $payments = array();
	    $payment_amount = 0;
	    foreach($pay as $row){
	      $payments[] = array(
	        "amount" => $row->amount,
	        "is_down_payment" => $row->is_down_payment,
	        "transacted_on" => $row->transacted_on,
	        "description" => ($row->is_down_payment) ? "DP" : "Pembayaran tgl. " . date_format(date_create($row->transacted_on),"d/m/Y")
	      );

	      $payment_amount += $row->amount;
	    }

	    $temp = explode("/", $invoice_number);
	    $this->data['total_due'] = $total_due;
	    $this->data['invoice_number'] = $temp[0]."/".$temp[1]."/".$temp[2]."/".to_romawi($temp[3])."/".$temp[4];
	    $this->data['invoice_amount'] = $invoice_amount;
	    $this->data['invoice_discount'] = $invoice_discount;
	    $this->data['invoice_discount_amount'] = $invoice_discount_amount;
	    $this->data['payments'] = $payments;
	    $this->data['invoice_payment_amount'] = $payment_amount;

	    $this->data['before_body'] = $this->load->view('script', '', true);

	    if(!$print){
	      $this->render('work_order', 'admin_clear'); 
	    }else{
	      $this->render('work_order_print', 'admin_clear'); 
	    }
	}


	/*functional func*/
	public function upload_invoice_item_attachment(){
	    $id_office = $this->input->post("id_office");
	    $id_customer = $this->input->post("id_customer");
	    $id_invoice = $this->input->post("id_invoice");

	    $id_office = decrypt_url($id_office);
	    $id_customer = decrypt_url($id_customer);
	    $id_invoice = decrypt_url($id_invoice);

	    $office = $this->office->get_detail_item($id_office);
	    $success = array();
	    $failed = array();

	    if(isset($_FILES['files']['name'])) {
	      $this->load->helper("url");
	      $office_name = url_title($office->name, $separator = '-', TRUE);


	      $uploadPath = "uploads/offices/".$office_name;
	      if (!is_dir($uploadPath)) {
	        mkdir($uploadPath, 0777, TRUE);
	      }

	      $uploadPath = "uploads/offices/".$office_name."/designs";
	      if (!is_dir($uploadPath)) {
	        mkdir($uploadPath, 0777, TRUE);
	      }

	      $uploadPath = "uploads/offices/".$office_name."/designs/".$id_invoice;
	      if (!is_dir($uploadPath)) {
	        mkdir($uploadPath, 0777, TRUE);
	      }


	      $config['upload_path'] = $uploadPath;
	      $config['allowed_types'] = "*";
	      $config['encrypt_name'] = true;

	      $this->load->library("upload", $config);
	      
	      for($i=0; $i<count($_FILES['files']['name']); $i++){
	        $name = $_FILES['files']['name'][$i];
	        $type = $_FILES['files']['type'][$i];
	        $tmp_name = $_FILES['files']['tmp_name'][$i];
	        $error = $_FILES['files']['error'][$i];
	        $size = $_FILES['files']['size'][$i];

	        $id_invoice_item = key($name);

	        if(!empty($name[$id_invoice_item]) && $name[$id_invoice_item] == 0){
	          $_FILES['file']['name'] = $name[$id_invoice_item];
	          $_FILES['file']['type'] = $type[$id_invoice_item];
	          $_FILES['file']['tmp_name'] = $tmp_name[$id_invoice_item];
	          $_FILES['file']['error'] = $error[$id_invoice_item];
	          $_FILES['file']['size'] = $size[$id_invoice_item];

	          if($this->upload->do_upload('file')){
	            $success[] = $name[$id_invoice_item];
	            $uploadData = $this->upload->data();
	            $data['id_invoice_item'] = $id_invoice_item;
	            $data['original_name'] = $name[$id_invoice_item];
	            $data['encrypted_name'] = $uploadData['file_name'];
	            $data['ext'] = $uploadData['file_ext'];
	            $data['location'] = $uploadPath;
	            $data['size'] = $uploadData['file_size'];
	            $this->db->insert('accounting_invoice_item_attachments',$data);
	          }
	          else{
	            $failed[] = $name[$id_invoice_item];
	          }
	        }
	      }

	      if(count($success) == count($_FILES['files']['name']) && count($success) > 0){
	        echo "File(s) berhasil di upload";
	      }else{
	        echo "File gagal di upload!";
	      }
	    }
	    else{
	      echo "";
	    }
	  }

	public function get_file($id){ 
		$this->load->helper('download');

		if(!empty($id)){
			$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id = $id";
			$result = $this->db->query($sql)->row();

			if(count($result) > 0){
				$file_url = $result->location.'/'.$result->encrypted_name;

				if(file_exists($file_url)){
					force_download($result->original_name, file_get_contents($file_url) , NULL);
				}else{
					echo '
					<style type="text/css">
					h3 {
					    font-size: 25px;
					    font-weight: 300;
					    margin-bottom: .5rem;
						font-family: inherit;
						font-weight: 500;
						line-height: 1.2;
						color: inherit;
					}
					p {font-size: 14px;
					    margin-top: 0; margin-bottom: 1rem; font-weight: 300;
					}
					</style>
					<section class="content">
				      <div class="error-page">
				        <div class="error-content">
				          <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Something went wrong.</h3>

				          <p>
				            File tidak ditemukan. Kemungkinan file telah dihapus. Hubungi Administrator untuk lebih lanjut.
				          </p>

				          <button onclick="window.history.back()">Kembali</button>

				        </div>
				      </div>
				      <!-- /.error-page -->

				    </section>';
				}
			}
		}
	}

	public function remove_file(){
		$id = $this->input->post("id_file");
		$id = decrypt_url($id);

		if(!empty($id)){
			$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id = $id";
			$del = "DELETE FROM accounting_invoice_item_attachments WHERE id = $id";
			$result = $this->db->query($sql)->row();

			if(count($result) > 0){
				$file_url = $result->location.'/'.$result->encrypted_name;

				if(file_exists($file_url)){
					unlink($file_url); 
					$this->db->query($del);

					echo json_encode(array("result" => 1));
				}else{
					echo json_encode(array("result" => 0));
				}
			}else{
				echo json_encode(array("result" => 2));
			}
		}else{
			echo json_encode(array("result" => 3));
		}
	}

	public function request_token(){
		$id_office = $this->input->post("id_office");
		$id_invoice = $this->input->post("id_invoice");
		$no_invoice = $this->input->post("no_invoice");
		$id_machine = $this->input->post("id_machine");
		$no_machine = $this->input->post("no_machine");

		$token = $this->db->query("SELECT * FROM tokens WHERE id_office = $id_office AND id_invoice = $id_invoice")->row();

		$this->db->trans_begin();

		$data['id_office'] = $id_office;
		$data['id_invoice'] = $id_invoice;
		$data['invoice_number'] = $no_invoice;
		$data['id_machine'] = $id_machine;
		$data['no_machine'] = substr($no_machine, -3);
		$data['created_by'] = $this->session->userdata("user_id");
		if(count($token) > 0 && isset($token->id)){
			//update existed token 
			$this->db->where("id", $token->id);
			$this->db->update("tokens", array(
				"id_machine" => $id_machine,
				"no_machine" => substr($no_machine, -3),
				"modified_on" => get_current_datetime(),
				"modified_by" => $this->session->userdata("user_id"),
			));
		}else{
			//insert new token
			$this->db->insert("tokens", $data);
		}

		if($this->db->affected_rows() > 0) {
			$this->db->query("UPDATE accounting_invoices SET status = 1 WHERE id = $id_invoice");
			$this->db->trans_commit();
			echo json_encode(array("success" => 1, "msg" => ""));
		}
		else {
			$this->db->trans_rollback();
			echo json_encode(array("success" => 0, "msg" => ""));
		}
	}

	public function generate_token(){
		$id_token = decrypt_url($this->input->post("id_token"));
		$no_order = $this->input->post("no_order");
		$minutes = $this->input->post("minutes");
		$id_invoice = 0;

		$token = $this->db->query("SELECT * FROM tokens WHERE id = $id_token")->row();

		if(count($token) > 0 && !empty($token)){
			$id_invoice = $token->id_invoice;
			$old_id_machine = $token->id_machine;
			$old_no_machine = $token->no_machine;
			$token = generate_token(format_invoice($token->invoice_number), $token->no_machine, $no_order, $minutes);

			$this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
	        $config['cacheable']    = true; //boolean, the default is true
	        $config['cachedir']     = './assets/'; //string, the default is application/cache/
	        $config['errorlog']     = './assets/'; //string, the default is application/logs/
	        $config['imagedir']     = './assets/images/qrcode/'; //direktori penyimpanan qr code
	        $config['quality']      = true; //boolean, the default is true
	        $config['size']         = '1024'; //interger, the default is 1024
	        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
	        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
	        $this->ciqrcode->initialize($config);
	 
	        $image_name=$token["token"].'.png'; //buat name dari qr code sesuai dengan nim
	 
	        $params['data'] = $token["token"]; //data yang akan di jadikan QR CODE
	        $params['level'] = 'H'; //H=High
	        $params['size'] = 10;
	        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
	        $this->ciqrcode->generate($params); 

			$data = array(
				"token_order" => $no_order,
				"minutes" => $minutes,
				"plaintext" => $token["plaintext"],
				"key_token" => $token["key_token"],
				"token" => $token["token"],
				"generated_on" => get_current_datetime(),
				"generated_by" => $this->session->userdata("user_id"),
			);
			$this->db->trans_start();
			//update token
			$this->db->where("id", $id_token);
			$this->db->update("tokens", $data);

			//insert into token_logs
			$this->db->insert("token_logs", array(
			  'id_token' => $id_token,
			  'id_machine'  => $old_id_machine,
			  'no_machine'  => $old_no_machine,
			  'token_order' => $no_order,
			  'minutes'  => $minutes,
			  'token'  => $token["token"],
			  'created_on' => get_current_datetime(),
			  'created_by' => $this->session->userdata("user_id")
			));

			//update invoice
			$this->db->flush_cache();
			$this->db->where("id", $id_invoice);
			$this->db->update("accounting_invoices", array("status" => 2));

			$this->db->trans_complete();
			if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
				$this->db->trans_commit();

				echo json_encode(array("success" => 1, "msg" => "Berhasil generate token."));
			}
			else {
				$this->db->trans_rollback();
				echo json_encode(array("success" => 0, "msg" => "Gagal generate token!"));
			}
		}else{
			echo json_encode(array("success" => 0, "msg" => "Token tidak ditemukan!"));
		}
	}

	public function zip(){
		$this->load->library('zip');
				
		$name = 'mydata1.txt';
		$data = 'A Data String!';

		$this->zip->read_file("uploads/offices/cabang-malang/designs/1/3a4c1d7503991fe793e8387f10db7740.png", "fileA.png");
		$this->zip->read_file("uploads/offices/cabang-malang/designs/1/06609335b5eef09f84c1ef3fab87ee34.xls", "fileB.xls");
		$this->zip->read_file("uploads/offices/cabang-malang/designs/1/22ab4f2c295b3129f50129c066d4b1d8.png", "fileB.png");

		// // Write the zip file to a folder on your server. Name it "my_backup.zip"
		// $this->zip->archive('uploads/temp/my_backup.zip');

		// Download the file to your desktop. Name it "my_backup.zip"
		$this->zip->download('my_backup.zip');
	}

	public function start_process(){
		$id_office = decrypt_url($this->input->post("id_office"));
		$id_customer = decrypt_url($this->input->post("id_customer"));
		$id_invoice = decrypt_url($this->input->post("id_invoice"));
		$id_token = decrypt_url($this->input->post("id_token"));

		$token = $this->db->query("
			SELECT * 
			FROM tokens 
			WHERE id = $id_token
		")->row();

		if(count($token) > 0){
			$this->db->trans_start();

			$this->db->where("id", $id_invoice);
			$this->db->update("accounting_invoices", array(
				"status" => 3,
				"done_time" => get_current_datetime(),
			));

			$invoice_files = $this->db->query("
				SELECT aiia.*
				FROM accounting_invoice_item_attachments aiia 
				LEFT JOIN accounting_invoice_items aii ON aii.`id` = aiia.`id_invoice_item` 
				WHERE aii.`id_invoice` = $id_invoice
				")->result();

			$link_zip = "";
			if(count($invoice_files) > 0){
				$this->load->library('zip');

				foreach ($invoice_files as $row) {				
					$file_url = $row->location.'/'.$row->encrypted_name;
					if(file_exists($file_url)){
						$this->zip->read_file($file_url, $row->original_name);
					}
				}
				$link_zip = "uploads/temp/invoice.zip";
				$this->zip->archive($link_zip);
				
			}

			$this->db->trans_complete();
			if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
				$this->db->trans_commit();
				echo json_encode(array("success" => 1, "msg" => "Proses berhasil dimulai.", "link" => $link_zip));
			}
			else {
				$this->db->trans_rollback();
				echo json_encode(array("success" => 0, "msg" => "Proses gagal dimulai!"));
			}
		}
		else{
			echo json_encode(array("success" => 1, "msg" => "Data tidak ditemukan!"));
		}

	}

	public function end_process(){
		$id_token = ($this->input->post("id_token") && !empty($this->input->post("id_token"))) ? $this->input->post("id_token") : 0;
		$id_token = ($id_token != 0) ? decrypt_url($this->input->post("id_token")) : 0;
		$id_invoice = decrypt_url($this->input->post("id_invoice"));

		$token = $this->db->query("
			SELECT * 
			FROM tokens 
			WHERE id = $id_token
		")->row();

		if(count($token) > 0){
			$id_office  = $token->id_office;
			$id_invoice = $token->id_invoice;

			$this->db->trans_start();

			$this->db->where("id", $id_invoice);
			$this->db->update("accounting_invoices", array(
				"status" => 4,
				"done_time" => get_current_datetime(),
			));

			$invoice_files = $this->db->query("
				SELECT aiia.*
				FROM accounting_invoice_item_attachments aiia 
				LEFT JOIN accounting_invoice_items aii ON aii.`id` = aiia.`id_invoice_item` 
				WHERE aii.`id_invoice` = $id_invoice
				")->result();

			foreach ($invoice_files as $row) {
				$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id = $row->id";
				$del = "DELETE FROM accounting_invoice_item_attachments WHERE id = $row->id";
				$result = $this->db->query($sql)->row();

				if(count($result) > 0){
					$file_url = $result->location.'/'.$result->encrypted_name;

					if(file_exists($file_url)){
						unlink($file_url); 
						$this->db->query($del);
					}
				}
			}

			$this->db->trans_complete();
			if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
				$this->db->trans_commit();
				echo json_encode(array("success" => 1, "msg" => "Proses selesai berhasil."));
			}
			else {
				$this->db->trans_rollback();
				echo json_encode(array("success" => 0, "msg" => "Proses selesai gagal!"));
			}
		}
		else{

			$this->db->trans_start();

			$this->db->where("id", $id_invoice);
			$this->db->update("accounting_invoices", array(
				"status" => 4,
				"done_time" => get_current_datetime(),
			));

			$invoice_files = $this->db->query("
				SELECT aiia.*
				FROM accounting_invoice_item_attachments aiia 
				LEFT JOIN accounting_invoice_items aii ON aii.`id` = aiia.`id_invoice_item` 
				WHERE aii.`id_invoice` = $id_invoice
				")->result();

			foreach ($invoice_files as $row) {
				$sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id = $row->id";
				$del = "DELETE FROM accounting_invoice_item_attachments WHERE id = $row->id";
				$result = $this->db->query($sql)->row();

				if(count($result) > 0){
					$file_url = $result->location.'/'.$result->encrypted_name;

					if(file_exists($file_url)){
						unlink($file_url); 
						$this->db->query($del);
					}
				}
			}

			$this->db->trans_complete();
			if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
				$this->db->trans_commit();
				echo json_encode(array("success" => 1, "msg" => "Proses selesai berhasil."));
			}
			else {
				$this->db->trans_rollback();
				echo json_encode(array("success" => 0, "msg" => "Proses selesai gagal!"));
			}
		}
	}

}


