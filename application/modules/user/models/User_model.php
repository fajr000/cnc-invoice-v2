<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
	private $_table = 'users'; 

	public function __construct(){
		parent::__construct();
	}

	public function login($identity, $password){ 
		$sql = "
		SELECT 
			u.`id` AS `id`,
			u.`first_name` AS `first_name`,
			CONCAT(IFNULL(u.`first_name`,'')) nama,
			u.`username`, 
			ug.`group_id`, 
			g.`name` group_name, 
			g.`description`group_description 
		FROM `users` u 
		LEFT JOIN `users_groups` ug ON ug.`user_id` = u.`id` 
		LEFT JOIN `groups` g ON g.`id` = ug.`group_id` 
		WHERE g.`id` = 1 AND (u.`username` = '".$identity."') AND u.`password` = '".$password."' 
		";

		$result = $this->db->query($sql);

		if($result->num_rows() > 0){
			$user = $result->row();

			$session_data = array(
			    'user_id'       => $user->id, //everyone likes to overwrite id so we'll use user_id
			    'name'			=> ucwords(strtolower($user->nama)),
			    'group_id'		=> $user->group_id,
			    'group_name'	=> $user->group_name,
			    'group'    		=> $user->group_description,

			);

			$this->session->set_userdata($session_data);

			return true;
		}

		return false;
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;		

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id)) ? "AND u.id_office = " . $office_id : "";

		$sql = "
		SELECT *
		FROM (
			SELECT 
				u.`id` as id,
				u.`first_name` as `first_name`,
				'' as `last_name`,
				CONCAT(IFNULL(u.`first_name`,'')) nama,
				u.`username`, 
				ug.`group_id`, 
				g.`name` group_name, 
				g.`description`group_description,
				u.`id_office`,
				o.`name` as name_office
			FROM `users` u 
			LEFT JOIN `users_groups` ug ON ug.`user_id` = u.`id` 
			LEFT JOIN `groups` g ON g.`id` = ug.`group_id` 
			LEFT JOIN `offices` o ON o.`id` = u.`id_office` 
			WHERE u.`active` = 1 
			$filter_office
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id)) ? "AND u.id_office = " . $office_id : "";

		$sql = "
		SELECT *
		FROM (
			SELECT 
				u.`id` as id,
				u.`first_name` as `first_name`,
				'' as `last_name`,
				CONCAT(IFNULL(u.`first_name`,'')) nama,
				u.`username`, 
				ug.`group_id`, 
				g.`name` group_name, 
				g.`description`group_description,
				u.`id_office`,
				o.`name` as name_office     
			FROM `users` u 
			LEFT JOIN `users_groups` ug ON ug.`user_id` = u.`id` 
			LEFT JOIN `groups` g ON g.`id` = ug.`group_id` 
			LEFT JOIN `offices` o ON o.`id` = u.`id_office` 
			WHERE u.`active` = 1 
			$filter_office
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				u.`id` as id,
				u.`first_name` as `first_name`,
				'' as `last_name`,
				CONCAT(IFNULL(u.`first_name`,'')) nama,
				u.`username`, 
				ug.`group_id`, 
				g.`name` group_name, 
				g.`description`group_description,
				u.`id_office`,
				o.`name` as name_office     
			FROM `users` u 
			LEFT JOIN `users_groups` ug ON ug.`user_id` = u.`id` 
			LEFT JOIN `groups` g ON g.`id` = ug.`group_id` 
			LEFT JOIN `offices` o ON o.`id` = u.`id_office`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function disabled_user($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, array("active" => 0));

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->delete($this->_table);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function insert_item_into_group($id, $id_group)
	{
		$this->db->trans_begin();

		$this->db->insert("users_groups", array("user_id" => $id, "group_id" => $id_group));

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item_into_group($id, $id_group)
	{
		$this->db->trans_begin();
		$this->db->where("user_id", $id);
		$this->db->update("users_groups", array("group_id" => $id_group));

		if($this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}
}
