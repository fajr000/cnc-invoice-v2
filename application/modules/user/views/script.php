<script>
	$(function () {
		// Custom autocomplete instance.
		$.widget( "app.autocomplete", $.ui.autocomplete, { 

			// Which class get's applied to matched text in the menu items.
			options: {
			    highlightClass: "ui-state-highlight"
			},

			_renderItem: function( ul, item ) {

				// Replace the matched text with a custom span. This
			    // span uses the class found in the "highlightClass" option.
			    var re = new RegExp( "(" + this.term + ")", "gi" ),
			        cls = this.options.highlightClass,
			        template = "<span class='" + cls + "'>$1</span>",
			        label = item.label.replace( re, template );
			        $li = $( "<li/>" ).appendTo( ul );
			    

			    // Create and return the custom menu item content.
			    $( "<div class='suggestion'/>" ).html( label )
			               .appendTo( $li );
			    console.log($li.html());
			    return $li;
			    
			}

		});

		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "user/";
		});

		$('.datepicker').datepicker({
			format:'dd/mm/yyyy',
		})
		.on("changeDate", function(e) {
		   $(this).datepicker('hide');
	    });

	    $('#password').val('');

	    $('.onlyNumeric').keypress(function (event) {
            return isNumeric(event, this);
        });

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "user/delete/?id="+deletedId;
        });

        $(':file').on('change', function(e){
		    var input = $(this),
	        numFiles = input.get(0).files ? input.get(0).files.length : 1,
	        label = input.val().replace(/\\/g, '/').replace(/.*\//, ''),
	        file_type = $(this).data('filetype'),
	        name = $(this).attr('name'),
	        label_elm = $(this).closest('div').children('input'),
	        wrapp_elm = $(this).closest('td').find('div.image_temp_wrapper'),
	        image_elm = $(this).closest('td').find('div.image_temp_wrapper').children('img'),
	        image_elm2= $(this).closest('td').find('div.image_temp_wrapper').children('a').children('img');

	        // show thumbnail selected image
	        if (this.files && this.files[0]) {
	            var reader = new FileReader();	            

	            reader.onload = function (e) {
	                if($(image_elm).length == 0){
	                	$(image_elm2).attr('src', e.target.result);
	                }else{
	                	$(image_elm).attr('src', e.target.result);
	                }
	            }
	            reader.readAsDataURL(this.files[0]);

	            $(wrapp_elm).show();

	            $(".original_image").hide();
	        }
	        else {
	        	$(wrapp_elm).hide();
	        }
		});

		var show_office = $("#show_office").val();
		if(show_office == 1) {
			var table = $('#list_data').DataTable( {
		        "ajax": "user/get_users_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "nama", className: "nama highlight",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='user/edit/?id="+oData.id+"'>"+oData.nama+"</a>");
				        }
		            },
		            { data: "name_office", className: "kantor" },
		            { data: "group_description", className: "jenis" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[1, 'asc']]
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}
		else {
			var table = $('#list_data').DataTable( {
		        "ajax": "user/get_users_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "nama", className: "nama highlight",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='user/edit/?id="+oData.id+"'>"+oData.nama+"</a>");
				        }
		            },
		            { data: "group_description", className: "jenis" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[1, 'asc']]
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}

	});

	function isNumeric(evt, element) {

        var charCode = (evt.which) ? evt.which : event.keyCode

        if (
            (charCode < 48 || charCode > 57) && 
            (charCode != 8)) //BACKSPACE
            return false;

        return true;
    }

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    }
	
</script> 