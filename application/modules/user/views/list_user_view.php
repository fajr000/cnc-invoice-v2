<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
  }
  
  .no{
    text-align: center;
    width: 2%;
  }
  .nama{
    vertical-align: middle;
    text-align: left;
    width: 40%; 
  }
  td.highlight a{
    color:  blue;
  }
  td.highlight a:hover{
    color:  blue;
    text-decoration: underline;
  }
  td.highlight a:visited{
    color:  blue;
    text-decoration: underline;
  }
  .namafull{
    vertical-align: middle;
    text-align: left;
    width: 65%; 
  }
  .kantor{
    vertical-align: middle;
    text-align: left;
    width: 25%; 
  }  
  .jenis{
    vertical-align: middle;
    text-align: center;
    width: 25%; 
  } 
  .tools{
    text-align: center;
    width: 8%;
  }   
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12 pull-left">
                  <?php
                  if($allowed['create']){
                  ?>
                  <a href="user/add/" class="btn-primary btn-sm" target="_self"><span>TAMBAH BARU</span></a>
                  <?php
                  }
                  ?>
                </div>
              </div>
              <br/>
              <input type="hidden" id="show_office" value="<?php echo (is_center_admin() || is_admin()) ? 1 : 0; ?>">

              <?php
              if(is_center_admin() || is_admin()){
                echo '
                <div class="table-responsive">
                    <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline">
                      <thead>
                        <tr>
                          <th class="no">&nbsp;</th>
                          <th class="nama">Nama</th>
                          <th class="kantor">Office</th>
                          <th class="jenis">Grup</th>
                          <th class="tools">&nbsp;</th>
                        </tr>
                      </thead>
                    </table>
                </div>
                ';
              }
              else {
                echo '
                <div class="table-responsive">
                    <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline">
                      <thead>
                        <tr>
                          <th class="no">&nbsp;</th>
                          <th class="namafull">Nama</th>
                          <th class="jenis">Grup</th>
                          <th class="tools">&nbsp;</th>
                        </tr>
                      </thead>
                    </table>
                </div>
                ';
              }
              ?>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->