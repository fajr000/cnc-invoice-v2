<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    
  }

  .btn-file {
      position: relative;
      overflow: hidden;
  }
  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      font-size: 100px;
      text-align: right;
      filter: alpha(opacity=0);
      opacity: 0;
      outline: none;
      background: white;
      cursor: inherit;
      display: block;
  }
  .item {
    float: left;
    padding: 5px;
    border: 1px solid #ccc;
    margin: 0px 3px;
    text-align: center;
  }
  .bold-text {
    font-weight: bold;
  }
  .suggestion {
    font-size: 12px;
  }
  .radio-label {
    display: inline-block;
    vertical-align: bottom;
    margin-right: 3%;
    font-weight: normal;
    line-height: 15px;
  }

  .radio-input {
      display: inline-block;
      vertical-align: bottom;
      margin-left: 0px;
  }
</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title">Detail User</h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
              <div class="col-md-12" id="form_user">
                <p>
                  <form autocomplete="off" name="form" id="form" method="post" action="user/save" target="_self" enctype="multipart/form-data">
                  <table class="table table-bordered">
                    <tbody>                      
                      <tr>
                        <td class="col-sm-2 label-form"><span>Kantor Cabang</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <?php
                          if(isset($office->id)){
                            echo '
                            <input type="hidden" class="form-control" id="id_office" name="id_office" value="'.$office->id.'">';
                            echo $office->name;
                          } 
                          else {
                            $selected = "";
                            echo '<select class="form-control" id="id_office" name="id_office">';
                            foreach ($offices as $key => $row) {
                              $selected = ((isset($office->id) && $office->id == $row->id) || (isset($item) && $item->id_office == $row->id)) ? "selected" : "";
                              echo "<option value='" . $row->id . "' " . $selected . ">" . $row->name . "</option>";
                          }
                          echo '</select>';
                          }
                          ?>                            
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2"><span>Posisi</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <?php 
                          if(!isset($item) || (isset($item) && $item->id != $this->session->userdata("user_id"))){
                            
                          ?>
                          <select class="form-control" name="id_group" id="id_group"> 
                            <?php
                            foreach ($groups as $group) {
                              if($group->name == 'administrator' && !is_admin())
                                continue;

                              if($group->name == 'admin cabang' && is_center_admin())
                                continue;

                              if($group->name == 'admin pusat' && is_branch_admin())
                                continue;

                              $selected = (isset($item) && $item->group_id == $group->id) ? "selected" : "";
                              echo "<option $selected value='".$group->id."'>".$group->description."</option>";
                            }
                            ?>
                          </select>
                          <?php 
                          }
                          else {
                            $name_group = "";
                            foreach ($groups as $group) {
                              $name_group = ($group->id == $item->group_id) ? $group->description : "";
                            }
                            echo '<input type="hidden" name="id_group" id="id_group" value="'.$item->group_id.'">';
                            echo $name_group;
                          }
                          ?>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2"><span>Nama</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo isset($item) ? $item->id : '';?>">
                          <input type="text" class="form-control" name="first_name" id="first_name" value="<?php echo isset($item) ? $item->first_name : '';?>">
                          </div>
                        </td>
                      </tr>                      
                      <tr style="<?php echo (isset($item)) ? "" : "";?>">
                        <td class="col-md-2"><span>Username</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="username" id="username" value=" <?php echo isset($item) && !empty($item->username) ? $item->username : '';?>" <?php echo (isset($item)) ? "readonly" : "";?>>
                          </div>
                        </td>
                      </tr>
                      <?php
                      if(isset($item)){
                      ?>
                      <tr style="display: none;">
                        <td class="col-md-2"><span>Old Password</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input  type="password" class="form-control" name="password_old" id="password_old" value="">
                          </div>
                        </td>
                      </tr>
                      <?php  
                      }
                      ?>
                      <tr>
                        <td class="col-md-2"><span><?php echo (isset($item)) ? "New Password" : "Password";?></span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="password" class="form-control" name="password" id="password" value=" <?php echo isset($item) && !empty($item->password) ? $item->password : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>
                    <button id="btn_back" class="btn btn-default btn btn-sm" name="btn_back"><span>Batal</span></button>&nbsp;
                    <button id="btn_submit" class="btn btn-primary btn btn-sm" name="btn_submit"><span>Simpan</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </form>
                </p>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->


<div class="modal fade" id="modal-default"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->