<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller
{  
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('User_model', 'user');
    $this->load->model('offices/Offices_model', 'office');

    $this->data['page_title'] = 'Users | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Users';
    $this->data['page_subheader'] = 'Pengguna';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    if(has_right_access($this->data['allowed'], "c") || has_right_access($this->data['allowed'], "u") || has_right_access($this->data['allowed'], "d")) {
      $this->get_lists();
    }
    else {
      if(!empty($this->session->userdata("user_id"))){
        redirect("user/edit/" . $this->session->userdata("user_id"));      
      }
    } 
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_user_view', 'admin_master');
  }

  public function get_users_list()
  {
    // populate data
    $params = array();

    $all_items = $this->user->get_all_items($params);

    $items = array();
    foreach($all_items as $key => $row) {
      $delete_access = "";
      if($row->group_name == "administrator" && !is_admin()) {

      }else{
        if($this->data['allowed']['delete'] > 0){
          $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(\"" . encrypt_url($row->id) . "\")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
        }

        $all_items[$key]->id = encrypt_url($row->id);
        $all_items[$key]->delete_access = $delete_access;
        $items[] = $row;
      }
    }

    echo json_encode(array("data"=>$items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    if(!has_right_access($this->data['allowed'], "c")){
      redirect_not_allowed();
    }

    $item = $this->user->get_detail_item(0);
    $groups = $this->db->query("SELECT * FROM groups")->result();

    $key_administrator = 0;
    foreach ($groups as $key => $value) {
      if($value->name == "administrator"){
        $key_administrator = $key;
      }
    }

    unset($groups[$key_administrator]);
    array_values($groups);

    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['groups'] = $groups;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_user_view', 'admin_master'); 
  }

  public function edit()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $id_item = $this->input->get("id");
    $id_item = decrypt_url($id_item);
    $item = $this->user->get_detail_item($id_item);
    $item->id = encrypt_url($item->id);
    $groups = $this->db->query("SELECT * FROM groups")->result();
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['groups'] = $groups;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_user_view', 'admin_master');
  }

  public function delete()
  {
    $id_item = $this->input->get("id");
    $id_item = decrypt_url($id_item);
    $result = $this->user->disabled_user($id_item);
   

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('user');
  }

  public function username_check($str, $id) 
  {
    // cek email exist
    $sql = "select * from users where active = 1 AND LOWER(username) = '".strtolower($str)."'";
    $result = $this->db->query($sql);

    if($result->num_rows() > 0){
      $user  = $result->row();
      if($user->id == $id)
        return true;
      else
        return false;
    }else
      return true;
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id           = $this->input->post('id');
    $first_name   = $this->input->post('first_name');
    $username     = $this->input->post('username');
    $password     = $this->input->post('password');
    $id_group     = $this->input->post('id_group');
    $id_office    = $this->input->post('id_office');
    $email        = "";

    $temp = array(
      'id' => $id,
      'first_name' => $first_name,
      'username' => $username,
      'password' => $password,
      'group_id' => $id_group,
      'id_office' => $id_office,
    );
 
    // validate form input
    $this->form_validation->set_rules('first_name', 'Nama', 'trim|required');
    $this->form_validation->set_message('username_check', 'Username telah digunakan. Masukkan username yang lain.');
    
    // Regstrasti 
    if((empty($id) || !isset($id))){
      $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_username_check['.$id.']');
      $this->form_validation->set_rules('password', 'Password', 'trim|required');
    }else{
      $id = decrypt_url($id);
      // Update data oleh guru 
      $this->form_validation->set_rules('username', 'Username', 'trim|required|callback_username_check['.$id.']');
    }

    $error = "";
    if ($this->form_validation->run($this) === TRUE){

      $data = array(
        'first_name' => trim($first_name),
        'id_office' => $id_office
      );

      //register
      if(empty($id)) {        
        $additional_data = array(
          'first_name' => trim($first_name),
          'id_office' => $id_office
        );
        $group = array($id_group);

        $id_user = $this->ion_auth->register(trim($username), trim($password), trim($email), $additional_data, $group);
        if($id_user > 0){
          $result = $id_user;
        }
      }
      //edit
      else{
        if(!empty($password)){
          $password_old = $this->input->post('password_old');
          $password_new = $this->input->post('password');

          $id_user = $this->ion_auth->reset_password(trim($username), trim($password_new));

          if(!$id_user){
            $result = 0;
          }
        }

        if($result){
          $result = $this->ion_auth->update($id, $data);  
          $this->user->update_item_into_group($id, $id_group);
        }        
      }
    }
    else{
      $result = 0;

      $error =  str_replace("<p>", "", validation_errors());
      $error =  str_replace("</p>", "<br/>", $error);
    }

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil disimpan.';

      $this->session->set_flashdata('notif', $notif);
      redirect('user');
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = '<strong>Data gagal disimpan!</strong>' . '<br/>' . $error;

      $this->session->set_flashdata('notif', $notif);

      // populate data
      $item = $temp;
      $groups = $this->db->query("SELECT * FROM groups")->result();

      $this->data['new'] = true;
      $this->data['item'] = (object)$item;
      $this->data['groups'] = $groups;
      $this->data['offices'] = $this->office->get_all_items();
      $this->data['office'] = get_office_id();
    
      $this->data['before_body'] = $this->load->view('script', '', true);

      $this->render('edit_user_view', 'admin_master'); 
    }
  }

  public function login()
  {
    $this->data['page_title'] = 'Login';
    if($this->input->post())
    {
      // validate form input
      $this->form_validation->set_rules('identity', 'Identity', 'required');
      $this->form_validation->set_rules('password', 'Password', 'required');
      if($this->form_validation->run()===TRUE)
      {
        if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password')))
        {
          redirect('dashboard', 'refresh');
        }
        else
        {
          $this->session->set_flashdata('message',$this->ion_auth->errors());
          redirect('user/login', 'refresh');
        }
      }
    }
    
    $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
    $this->render('user/login', 'admin_master');
    
  }

  public function resizeImage($filename, $width = 0, $height = 0)
    {

      $source_path = $_SERVER['DOCUMENT_ROOT'] . '/tbs/uploads/images/' . $filename;
      $target_path = $_SERVER['DOCUMENT_ROOT'] . '/tbs/uploads/images/thumbnails/';
      $config_manip = array(

          'image_library' => 'gd2',
          'source_image' => $source_path,
          'new_image' => $target_path,
          'maintain_ratio' => TRUE,
          'create_thumb' => TRUE,
          'quality' => '100%',
      );

      if($width > 0){
        $config_manip['width'] = $width;
      }

      if($height > 0){
        $config_manip['height'] = $height;
      }


      $this->load->library('image_lib', $config_manip);
      $this->image_lib->initialize($config_manip);

      if (!$this->image_lib->resize()) {

          echo $this->image_lib->display_errors();
          die();

      }

      $this->image_lib->clear();
      chmod('uploads/images/thumbnails/' . $this->get_thumb_filename('.', '_thumb.', $filename), 0777); // CHMOD file
    }

  public function get_thumb_filename($search, $replace, $subject)
  {
    $pos = strrpos($subject, $search);

    if($pos !== false)
    {
      $subject = substr_replace($subject, $replace, $pos, strlen($search));
    }

    return $subject;
  }

  public function view_photo($file_name) 
  {
    if(file_exists('uploads/images/' . $file_name.'')) {
      echo "<body style='background-color:#000;'><center><img src='".base_url()."uploads/images/".$file_name."'></center></body>";
    }
    else{
      if(file_exists('uploads/images/thumbnails/' . $this->get_thumb_filename('.', '_thumb.', $file_name))) {
        echo "<body style='background-color:#000;'><center><img src='".base_url()."uploads/images/thumbnails/".$this->get_thumb_filename('.', '_thumb.', $file_name)."'></center></body>";
      }
      else {
        echo "<h4>Opps! Foto tidak ditemukan.</h4>";
      }
    }
  }

  /**
   * Log the user out
   */
  public function logout()
  {
    $this->data['page_title'] = "Logout";

    // log the user out
    $logout = $this->ion_auth->logout();

    // redirect them to the login page
    $this->session->set_flashdata('message', $this->ion_auth->messages());
    redirect('user/login', 'refresh');
  }
}