<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transactions extends Admin_Controller
{ 
  function __construct(){
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination', 'cart'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Transaction_model', 'transaction');
    $this->load->model('Invoice_model', 'invoice');
    $this->load->model('offices/Offices_model', 'office');
    $this->load->model('customers/Customers_model', 'customer');
    $this->load->model('price_plat/Plat_model', 'plat');
    $this->load->model('price_service/Service_model', 'service');
 
    $this->data['page_title'] = 'Transactions | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Transactions';
    $this->data['page_subheader'] = 'Transaksi';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index(){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists(){
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    $this->render('list_sales_view', 'admin_master');
  }

  public function get_product_list(){
    // populate data
    $params = array();

    $plat = $this->plat->get_all_items(array("qty > 0"));
    $service = $this->service->get_all_items($params);

    $products = array();

    foreach($plat as $row){
      $item = array(
        "type" => 1, //plat
        "id" => $row->id,
        "name_plat" => $row->name_plat,
        "size" => $row->size,
        "qty" => $row->qty,
        "machine" => "N/A",
        "price" => format_money($row->price),
        "discount" => $row->discount." %",
      );

      $products[] = (object) $item;
    }

    foreach($service as $row){
      $easy = explode(".", format_money($row->easy));
      $diff = explode(".", format_money($row->difficult));
      $item = array(
        "type" => 2, //service
        "id" => $row->id,
        "name_plat" => $row->name_plat,
        "size" => $row->size,
        "qty" => "~",
        "machine" => $row->name_machine,
        "price" => format_money($easy[0]) . 'rb - ' . format_money($diff[0]) . 'rb',
        "discount" => $row->discount." %",
      );

      $products[] = (object) $item;
    }

    echo json_encode(array("data"=>$products));
  }

  public function add($id_customer = 0, $is_new_transaction = 1){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $customer = $this->customer->get_detail_item($id_customer);
       
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $customer;

    $this->data['before_body'] = $this->load->view('script_transaction', '', true);

    if($is_new_transaction){
      $this->cart->destroy();
    }

    $this->render('add', 'admin_clear'); 
  }

  public function add_payment($id_customer = 0){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $customer = $this->customer->get_detail_item($id_customer);
    if(isset($customer))
      $invoices = $this->customer->get_invoice_due($customer->id_office, $id_customer);
    else
      $invoices = array();
       
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $customer;
    $this->data['invoices'] = $invoices;

    $this->data['before_body'] = $this->load->view('script_payment', '', true);

    $this->render('add_payment', 'admin_clear'); 
  }

  public function update_cart(){
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");
    $id_product = $this->input->post("id_product");
    $type_product = $this->input->post("type_product");

    $items = array();
    for ($i=0; $i < count($id_product) ; $i++) {

      $name_product = "";
      $price_product = "";
      $price_option = array();
      $qty_left = 0;

      // 1 : PLAT, 2 : SERVICE
      if($type_product[$i] == 1){
        $item = $this->plat->get_detail_item($id_product[$i]);
        
        $name_product = "Plat ".$item->name_plat." ".$item->size." mm";
        $price_product = $item->price - ($item->discount/100*$item->price);
        $qty_left = $item->qty;
      }else{
        $item = $this->service->get_detail_item($id_product[$i]);
        $name_product = " Jasa Potong ".$item->name_plat." ".$item->size." mm" . " - ".$item->name_machine;
        $price_product = $item->easy - ($item->discount/100*$item->easy);

        $price_option = array(
          "easy" => $item->easy - ($item->discount/100*$item->easy),
          "medium" => $item->medium - ($item->discount/100*$item->medium),
          "difficult" => $item->difficult - ($item->discount/100*$item->difficult),
          "per_minute" => $item->per_minute - ($item->discount/100*$item->per_minute),
        );
      }

      $item = array(
        'id' => $type_product[$i]."_".$id_product[$i],
        'qty' => 1,
        'qty_left' => $qty_left,
        'price' => $price_product,
        'price_option' => $price_option,
        'minute' => "",
        'discount'=> 0,
        'name' => $name_product,
        'options' => array(
          'type' => $type_product[$i]
        )
      );

      if(count($this->cart->contents()) > 0) {
        $is_available = true;

        //check available stock for PLAT
        foreach($this->cart->contents() as $key => $row){
          if($item["options"]["type"] == 1 && $item["id"] == $row["id"] && ($row["qty"]+1) > $row["qty_left"]){
            //if stock is overstock
            $is_available = false;
          }
        }

        if($is_available){
          $is_new_item = true;
          $rowid = "";
          $qty = 0;
          foreach($this->cart->contents() as $key => $row){
            if($row["id"] == $item["id"]){
              $is_new_item = false;
              $rowid = $row["rowid"];
              $qty = $row["qty"];
              break;
            }
          }
          if($is_new_item)
            $items[] = $item;
          else {
            $data = array(
              'rowid' => $rowid,
              'qty'   => ($qty+1)
            );

            $this->cart->update($data);
          }
        }
      }
      else{
        $items[] = $item;
      }
    }

    if(count($items) > 0){
      $this->cart->insert($items);
    }
  }

  public function add_items(){
    $this->update_cart();
    
    redirect("transactions/add/".$this->input->post("id_customer")."/0");
  }

  public function cart() {
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");

    $this->update_cart();

    $products = array();
    $products["id_office"] = $id_office;
    $products["id_customer"] = $id_customer;
    $products["summary"]["subtotal"] = 0;
    $products["summary"]["payment_discount"] = 0;
    $products["summary"]["payment_discount_amount"] = 0;
    $products["summary"]["total"] = 0;

    foreach($this->cart->contents() as $key => $row) {
      $temp = explode("_", $row["id"]);
      $type_product = $temp[0];
      $id_product = $temp[1];

      $price_option = $row["price_option"];
      $discount = $row["discount"];

      $products["cart"][] = array(
        'guid'    => $key,
        'id'      => $row["id"],
        'qty'     => $row["qty"],
        'qty_left'=> $row["qty_left"],
        'price'   => $row["price"],
        'price_option' => $price_option,
        'minute'  =>  $row["minute"],
        'discount' => $discount,
        'name'    => $row["name"],
        'options' => $row["options"],
      );

      $products["summary"]["subtotal"] += ($row["price"] * $row["qty"]) - ($discount/100 * ($row["price"] * $row["qty"]));
    }

    $products["summary"]["payment_discount_amount"] = $products["summary"]["payment_discount"]/100*$products["summary"]["subtotal"];
    $products["summary"]["total"] = $products["summary"]["subtotal"] - $products["summary"]["payment_discount_amount"];

    $this->session->set_userdata("products", $products);

    redirect("transactions/show_cart");
  }

  public function show_cart() {

    $products = $this->session->userdata("products");

    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $this->customer->get_detail_item($products["id_customer"]);
    $this->data['products'] = $products;

    $rank = $this->customer->get_rank($products["id_office"], $products["id_customer"]);
    if(count($rank) > 0){
      $temp = explode(" ", $rank->period_month);
      $rank->period_month = ucwords(to_indonesian_month($temp[0])) . ' ' . $temp[1];
    }else{
      $rank = array(
        "id_office" => $products["id_office"],
        "id_customer" => $products["id_customer"],
        "period" => "",
        "period_month" => "",
        "amount" =>0,
        "rank" => 0,

      );

      $rank = (object)$rank;
    }
    $this->data['rank'] = $rank;

    $this->data['before_body'] = $this->load->view('script_transaction', '', true);

    $this->render('cart', 'admin_clear'); 
  }

  public function clear_cart() {
    $this->cart->destroy();
  }

  public function save_change($gotopayment = 0) {
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");
    $guid = $this->input->post("guid");
    $id = $this->input->post("id");
    $price = $this->input->post("price");
    $minute = $this->input->post("minute");
    $qty = $this->input->post("qty");
    $discount = $this->input->post("discount");
    $amount = $this->input->post("amount");
    $guid_delete = $this->input->post("guid_delete");
    $payment_discount = $this->input->post("payment_discount");

    for($i=0;$i<count($guid);$i++) {

      $data = array(
        'rowid'     => $guid[$i],
        'qty'       => (isset($guid_delete) && in_array($guid[$i], $guid_delete)) ? 0 : $qty[$i],
        'price'     => $price[$i],
        'minute'    => (isset($minute[$guid[$i]])) ? (empty($minute[$guid[$i]]) ? 0 : $minute[$guid[$i]]) : 0,
        'discount'  => (empty($discount[$i])) ? 0 : $discount[$i],
      );

      //update cart
      $this->cart->update($data);
    }    

    $products = array();
    $products["id_office"] = $id_office;
    $products["id_customer"] = $id_customer;
    $products["summary"]["subtotal"] = 0;
    $products["summary"]["payment_discount"] = $payment_discount;
    $products["summary"]["payment_discount_amount"] = 0;
    $products["summary"]["total"] = 0;

    foreach($this->cart->contents() as $key => $row) {
      $temp = explode("_", $row["id"]);
      $type_product = $temp[0];
      $id_product = $temp[1];

      $price_option = $row["price_option"];
      $discount = (!empty($row["discount"])) ? $row["discount"] : 0;

      $products["cart"][] = array(
        'guid'    => $key,
        'id'      => $row["id"],
        'qty'     => $row["qty"],
        'qty_left' => $row["qty_left"],
        'price'   => $row["price"],
        'price_option' => $price_option,
        'minute' => $row["minute"],
        'discount' => $discount,
        'name'    => $row["name"],
        'options' => $row["options"],
      );

      if($type_product == 2 && $row["price"] == $price_option["per_minute"]){
        $products["summary"]["subtotal"] += ($row["price"] * $row["minute"] * $row["qty"]) - (($discount/100) * ($row["price"] * $row["minute"] * $row["qty"]));

      }else{
        $products["summary"]["subtotal"] += ($row["price"] * $row["qty"]) - (($discount/100) * ($row["price"] * $row["qty"]));
      }
    }

    $products["summary"]["payment_discount_amount"] = ($products["summary"]["payment_discount"]/100)*$products["summary"]["subtotal"];
    $products["summary"]["total"] = $products["summary"]["subtotal"] - $products["summary"]["payment_discount_amount"];

    $this->session->set_userdata("products", $products);

    if(!$gotopayment){
      redirect("transactions/show_cart");
    }else{
      redirect("transactions/payment");
    }
  }

  public function payment(){
    $products = $this->session->userdata("products");

    $id_office = (isset($products["id_office"])) ? $products["id_office"] : $this->input->post("id_office");
    $id_customer = (isset($products["id_customer"])) ? $products["id_customer"] : $this->input->post("id_customer");
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $this->customer->get_detail_item($id_customer);
    $this->data['products'] = $products;

    $rank = $this->customer->get_rank($id_office, $id_customer);
    if(count($rank) > 0){
      $temp = explode(" ", $rank->period_month);
      $rank->period_month = ucwords(to_indonesian_month($temp[0])) . ' ' . $temp[1];
    }else{
      $rank = array(
        "id_office" => $products["id_office"],
        "id_customer" => $products["id_customer"],
        "period" => "",
        "period_month" => "",
        "amount" =>0,
        "rank" => 0,

      );

      $rank = (object)$rank;
    }
    $this->data['rank'] = $rank;


    if($this->input->post()){
      $cart = $this->cart->contents();
      /*
      ** Payment Method:
      ** 0. Pay later --> Just created Invoice
      ** 1. Cash
      ** 2. Transfer
      */

      $payment_method = $this->input->post("payment_method");
      $total_paid = unformat_money($this->input->post("total_paid")); //paid from purchase
      $paid_invoices = $this->input->post("paid_invoices"); //paid from make payment
      $is_down_payment = ($this->input->post("is_dp")) ? $this->input->post("is_dp") : 0; //is_downpayment

      $invoices = array();
      $result = true;

      if(isset($cart) && count($cart) > 0) {
        //Generate Invoice
        $transaction = array(
          "id_office" => $products["id_office"],
          "id_customer" => $products["id_customer"],
          "transaction_type" => 1, //purchase
          "handle_by_id" => $this->session->userdata("user_id"),
        );

        $transaction["invoice"] = array(
           "id_transaction" => 0,
           "invoice_number" => "",
           "amount" => $products["summary"]["subtotal"],
           "discount" => $products["summary"]["payment_discount"],
           "discount_amount" => $products["summary"]["payment_discount_amount"],
        );

        foreach($products["cart"] as $row) {
          $temp = explode("_", $row["id"]);

          $amount= ($row["price"] * $row["qty"]);
          $discount = ($row["discount"] / 100 * $amount);
          $id_plat = $temp[1];
          $id_service = $temp[1];

          if($row["options"]["type"] == 2){
            if(!empty($row["minute"]) && $row["minute"] > 0){
              $amount= ($row["price"] * $row["minute"] * $row["qty"]);
            }
            
            $id_plat = 0;
          }else{
            $id_service = 0;
          }

          $transaction["invoice"]["items"][] = array(
             "id_invoice" => 0,
             "id_plat" => $id_plat,
             "id_service" => $id_service,
             "price" => $row["price"],
             "qty" => $row["qty"],
             "minutes" => $row["minute"],
             "amount" => $amount,           
             "discount" => $row["discount"],
             "discount_amount" => $discount,
          );
        }//end foreach

        $invoices = $this->transaction->generate_invoice($transaction);

        $result = (count($invoices) > 0) ? true : false;

        $this->cart->destroy();
      }

      if($result && $payment_method > 0) {

        //Generate Payment
        $transaction = array(
          "id_office" => $id_office,
          "id_customer" => $id_customer,
          "transaction_type" => 2, //purchase
          "handle_by_id" => $this->session->userdata("user_id"),
        );

        $transaction["payment"] = array(
           "payment_method_id" => $payment_method, 
        );
        /*
        payment data format
        array(
            "id_invoice" => $id_invoice,
            "paid" => 0
          );
        */

        //paid from purchasing
        if(count($products)>0 && isset($total_paid) && $total_paid > 0){
          $payments[] = array(
              "id_invoice" => $invoices["id_invoice"],
              "paid" => $total_paid,
              "is_down_payment" => $is_down_payment,
            );
        }
        //paid from make payment
        else {
          if(isset($paid_invoices) && count($paid_invoices) > 0) {
            foreach($paid_invoices as $id_invoice => $amount){
              $payments[] = array(
                "id_invoice" => $id_invoice,
                "paid" => $amount,
                "is_down_payment" => $is_down_payment,
              );
            }
          }
        }

        $transaction["payment"]["invoices"] = $payments;
        $id_payment = $this->transaction->generate_payment($transaction);

        if($id_payment > 0){
          //if success creating payment then save attachment if any
          if(isset($_FILES['files']['name'])) {
            $this->load->helper("url");

            $office = $this->office->get_detail_item($id_office);
            $office_name = url_title($office->name, $separator = '-', TRUE);

            $uploadPath = "uploads/offices/".$office_name;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts";
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts/".$id_payment;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }
            
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = "*";
            $config['encrypt_name'] = true;

            $this->load->library("upload", $config);

            for($i=0; $i<count($_FILES['files']['name']); $i++){
              if(!empty($_FILES['files']['name'][$i]) && $_FILES['files']['error'][$i] == 0){

                $filename = $_FILES['files']['name'][$i];
                $_FILES['file']['name'] = $filename;
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                if($this->upload->do_upload('file')){
                  $uploadData = $this->upload->data();
                  $data['id_payment'] = $id_payment;
                  $data['original_name'] = $filename;
                  $data['encrypted_name'] = $uploadData['file_name'];
                  $data['ext'] = $uploadData['file_ext'];
                  $data['location'] = $uploadPath;
                  $data['size'] = $uploadData['file_size'];
                  $this->db->insert('accounting_payment_attachments',$data);
                }
              }
            }    
          }
        }
      }      

      $this->session->unset_userdata('products');
      
      echo json_encode(array(
          "success"=>1, 
          "msg"=>""
      ));
    }
    else{      
      $valid = 1;
      $msg = array();
      foreach($products["cart"] as $row){
        $temp = explode("_", $row["id"]);
        $type_product = $temp[0];
        $id_product = $temp[1];

        if($type_product == 1){
          $plat = $this->transaction->get_plat($id_product);
          if(count($plat) > 0 && $row["qty"] > $plat->qty){
            $valid = 0;
            $msg[] = "Stok produk <strong>".$row["name"]." ($plat->qty)</strong> tidak mencukupi.";
          }
        }
      }

      if($valid < 1){
        $this->session->set_flashdata('msg_alert', implode("<br>", $msg));
        redirect("transactions/show_cart/");
      }else{
        $this->data['before_body'] = $this->load->view('script_transaction', '', true);
        $this->render('payment', 'admin_clear');   
      }
    }    
  }

  public function invoice($id_office, $id_customer, $id_invoice){
    $post_max_size = parse_size(ini_get('post_max_size'));
    $upload_max = parse_size(ini_get('upload_max_filesize'));

    $cus = $this->customer->get_detail_item($id_customer);
    $inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);

    $inv_list = array();
    foreach ($inv as $row) {
      $inv_list["id_invoice"] = $row->id_invoice;
      $inv_list["id_office"] = $row->id_office;
      $inv_list["id_customer"] = $row->id_customer;
      $inv_list["invoice_number"] = format_invoice($row->invoice_number);
      $inv_list["invoice_amount"] = $row->invoice_amount;
      $inv_list["invoice_discount"] = $row->invoice_discount;
      $inv_list["invoice_discount_amount"] = $row->invoice_discount_amount;
      $inv_list["invoice_due"] = $row->invoice_amount-$row->invoice_discount_amount;
      $inv_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
      $inv_list["cancelled_on"] = $row->cancelled_on;
      $inv_list["posted"] = $row->posted;
      $inv_list["name_operator"] = $row->name_operator;

      $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
      
      $machine = !(empty($row->machine)) ? $row->machine." " : "";
      $inv_list["items"][$row->id_invoice_item]["description"] = $row->product." ".$machine.$row->name_plat." ".$row->size."$minutes";
      $inv_list["items"][$row->id_invoice_item]["product"] = $row->product;
      $inv_list["items"][$row->id_invoice_item]["name_plat"] = $row->name_plat;
      $inv_list["items"][$row->id_invoice_item]["machine"] = $row->machine;
      $inv_list["items"][$row->id_invoice_item]["size"] = $row->size;
      $inv_list["items"][$row->id_invoice_item]["price"] = $row->price;
      $inv_list["items"][$row->id_invoice_item]["qty"] = $row->qty;
      $inv_list["items"][$row->id_invoice_item]["minute"] = $row->minute;
      $inv_list["items"][$row->id_invoice_item]["amount"] = $row->amount;
      $inv_list["items"][$row->id_invoice_item]["discount"] = $row->discount;
      $inv_list["items"][$row->id_invoice_item]["discount_amount"] = $row->discount_amount;
      $inv_list["items"][$row->id_invoice_item]["due"] = $row->due;

      $sql = "SELECT * FROM accounting_invoice_item_attachments WHERE id_invoice_item = $row->id_invoice_item";
      $files = $this->db->query($sql)->result();
      $inv_list["items"][$row->id_invoice_item]["files"] = $files;
    }

    $this->data["invoices"] = $inv_list;
    $this->data["customer"] = $cus;
    $this->data["post_max_size"] = formatSizeUnits($post_max_size);
    $this->data["upload_max"] = formatSizeUnits($upload_max);
    $this->data['before_body'] = $this->load->view('script_invoice_detail', '', true);


    $this->render('invoice_detail', 'admin_clear'); 

  }

  public function upload_invoice_item_attachment(){
    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");
    $id_invoice = $this->input->post("id_invoice");

    $office = $this->office->get_detail_item($id_office);
    $success = array();
    $failed = array();

    if(isset($_FILES['files']['name'])) {
      $this->load->helper("url");
      $office_name = url_title($office->name, $separator = '-', TRUE);


      $uploadPath = "uploads/offices/".$office_name;
      if (!is_dir($uploadPath)) {
        mkdir($uploadPath, 0777, TRUE);
      }

      $uploadPath = "uploads/offices/".$office_name."/designs";
      if (!is_dir($uploadPath)) {
        mkdir($uploadPath, 0777, TRUE);
      }

      $uploadPath = "uploads/offices/".$office_name."/designs/".$id_invoice;
      if (!is_dir($uploadPath)) {
        mkdir($uploadPath, 0777, TRUE);
      }


      $config['upload_path'] = $uploadPath;
      $config['allowed_types'] = "*";
      $config['encrypt_name'] = true;

      $this->load->library("upload", $config);
      
      for($i=0; $i<count($_FILES['files']['name']); $i++){
        $name = $_FILES['files']['name'][$i];
        $type = $_FILES['files']['type'][$i];
        $tmp_name = $_FILES['files']['tmp_name'][$i];
        $error = $_FILES['files']['error'][$i];
        $size = $_FILES['files']['size'][$i];

        $id_invoice_item = key($name);

        if(!empty($name[$id_invoice_item]) && $name[$id_invoice_item] == 0){
          $_FILES['file']['name'] = $name[$id_invoice_item];
          $_FILES['file']['type'] = $type[$id_invoice_item];
          $_FILES['file']['tmp_name'] = $tmp_name[$id_invoice_item];
          $_FILES['file']['error'] = $error[$id_invoice_item];
          $_FILES['file']['size'] = $size[$id_invoice_item];

          if($this->upload->do_upload('file')){
            $success[] = $name[$id_invoice_item];
            $uploadData = $this->upload->data();
            $data['id_invoice_item'] = $id_invoice_item;
            $data['original_name'] = $name[$id_invoice_item];
            $data['encrypted_name'] = $uploadData['file_name'];
            $data['ext'] = $uploadData['file_ext'];
            $data['location'] = $uploadPath;
            $data['size'] = $uploadData['file_size'];
            $this->db->insert('accounting_invoice_item_attachments',$data);
          }
          else{
            $failed[] = $name[$id_invoice_item];
          }
        }
      }

      if(count($success) == count($_FILES['files']['name']) && count($success) > 0){
        echo "File(s) berhasil di upload";
      }else{
        echo "File gagal di upload!";
      }
    }
    else{
      echo "";
    }
  }

  public function cancel($id_office, $id_customer, $id_invoice){
    $transaction = array(
      "id_office" => $id_office,
      "id_customer" => $id_customer,
      "transaction_type" => 3, //cancel
      "handle_by_id" => $this->session->userdata("user_id"),
    );

    $transaction["invoices"][] = $id_invoice;

    $this->transaction->cancel_invoices($transaction);

    redirect("customers/edit/$id_customer/1");
  }

  public function delete($id_office, $id_customer, $id_invoice){
    $inv = $this->invoice->get_detail_item($id_invoice); 
    $trx = $this->transaction->get_detail_item($inv->id_transaction);

    $status = true;
    if($trx->cancelled_on == "0000-00-00 00:00:00"){
      $transaction = array(
        "id_office" => $id_office,
        "id_customer" => $id_customer,
        "transaction_type" => 3, //cancel
        "handle_by_id" => $this->session->userdata("user_id"),
      );

      $transaction["invoices"][] = $id_invoice;
      $status = $this->transaction->cancel_invoices($transaction);
    }
    
    if($status){
      $this->transaction->delete_item($trx->id);
    }

    redirect("customers/edit/$id_customer/1");
  }

  public function get_offices_list(){
    $params = array();
    $list = array();
    
    $id_office = $this->session->userdata("id_office");
    if(isset($id_office) && !empty($id_office)){
      $params[] = "id = $id_office"; 
    }

    $offices = $this->office->get_all_items($params);

    $list[] = array(
      "id" => 0,
      "text" => "--Pilih Kantor Cabang--",
    );
    foreach($offices as $row){
      $list[] = array(
        "id" => $row->id,
        "text" => $row->name,
      );
    }

    echo json_encode(array("data" => $list));
  }

  public function get_customers_list(){
    $id_office = $this->input->post("id_office");

    $params[] = "id_office = $id_office";
    
    $list = array();
    $customers = $this->customer->get_all_items($params);

    $list[] = array(
      "id" => 0,
      "text" => "--Pilih Customer--",
    );
    foreach($customers as $row){
      $list[] = array(
        "id" => $row->id,
        "text" => $row->name,
      );
    }

    echo json_encode(array("data" => $list));
  }

  public function get_invoice_due() {
    $id_office = $this->input->post("id_office"); 
    $id_customer = $this->input->post("id_customer");

    $invoices = $this->customer->get_invoice_due($id_office, $id_customer);

    $html = array();
    $i = 1;
    foreach($invoices as $row){
      $html[] = '
      <tr class="row_invoice">
        <td class="middle-text center-text">'.$i.'</td>
        <td class="middle-text">'.format_invoice($row->invoice_number).'</td>
        <td class="middle-text price">'.format_money($row->current_due).'</td>
        <td class="middle-text"><input type="form-text" class="form-control money price amount_paid" data-max_amount="'.intval($row->current_due).'" value="" ></td>
        <td class="center-text middle-text" style="">
            <input type="checkbox" class="paid_invoices" onclick="chooseInvoice(this)" name="paid_invoices['.$row->id_invoice.']" value="">
        </td>
      </tr>
      ';

      $i++;
    }

    echo implode("", $html);
  }

  public function get_invoice($id_office, $id_customer, $id_invoice = 0, $print = 0) {
    $off = $this->office->get_detail_item($id_office);
    $acc = $this->office->get_all_accounts($id_office);
    $cus = $this->customer->get_detail_item($id_customer);
    $inv = $this->customer->get_invoices($id_office, $id_customer, $id_invoice, 0);
    $pay = $this->transaction->get_invoice_payments($id_office, $id_customer, $id_invoice);

    $inv_list = array();
    $invoice_number = "";
    $total_due = 0;
    $invoice_amount = 0;
    $invoice_discount = 0;
    $invoice_discount_amount = 0;
    foreach ($inv as $key => $row) {
      $inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

      $minutes = (!empty($row->minute)) ? "($row->minute menit)" : "";
      $cancelled = ($row->cancelled_on != "0000-00-00 00:00:00") ? "cancelled" : "";
      $cancelled_status = ($row->cancelled_on != "0000-00-00 00:00:00") ? '<small class="badge badge-danger"></i> Dibatalkan</small>' : '';

      $machine = !(empty($row->machine)) ? $row->machine." " : "";
      if(isset($inv[$key]->description)){
        $inv[$key]->description .= "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
      }
      else{
        $inv[$key]->description = "<span class='".$cancelled."'>".$row->product." ".$machine.$row->name_plat." ".$row->size."mm $minutes</span>".$cancelled_status."<br>";
      }

      $total_due += $row->due;
      $invoice_number = $row->invoice_number;
      $invoice_amount = $row->invoice_amount;
      $invoice_discount = $row->invoice_discount;
      $invoice_discount_amount = $row->invoice_discount_amount;
    }

    $this->data['today'] = get_long_current_date();
    $this->data['office'] = $off;
    $this->data['office_account'] = $acc[0];
    $this->data['customer'] = $cus;
    $this->data['invoice'] = $inv;
    $this->data['id_invoice'] = $id_invoice;
    
    $payments = array();
    $payment_amount = 0;
    foreach($pay as $row){
      $payments[] = array(
        "amount" => $row->amount,
        "is_down_payment" => $row->is_down_payment,
        "transacted_on" => $row->transacted_on,
        "description" => ($row->is_down_payment) ? "DP" : "Pembayaran tgl. " . date_format(date_create($row->transacted_on),"d/m/Y")
      );

      $payment_amount += $row->amount;
    }

    $temp = explode("/", $invoice_number);
    $this->data['total_due'] = $total_due;
    $this->data['invoice_number'] = $temp[0]."/".$temp[1]."/".$temp[2]."/".to_romawi($temp[3])."/".$temp[4];
    $this->data['invoice_amount'] = $invoice_amount;
    $this->data['invoice_discount'] = $invoice_discount;
    $this->data['invoice_discount_amount'] = $invoice_discount_amount;
    $this->data['payments'] = $payments;
    $this->data['invoice_payment_amount'] = $payment_amount;

    $this->data['before_body'] = $this->load->view('script_invoice', '', true);

    if(!$print){
      $this->render('invoice', 'admin_clear'); 
    }else{
      $this->render('invoice_print', 'admin_clear'); 
    }
  }

  public function get_payment($id_office, $id_customer, $id_payment = 0) {
    $post_max_size = parse_size(ini_get('post_max_size'));
    $upload_max = parse_size(ini_get('upload_max_filesize'));

    $off = $this->office->get_detail_item($id_office);
    $cus = $this->customer->get_detail_item($id_customer);

    $payments = $this->transaction->get_detail_payment($id_office, $id_customer, $id_payment);

    $pay_list = array();
    $total_payment = 0;
    foreach ($payments as $row) {
      $pay_list["id_transaction"] = $row->id_invoice;
      $pay_list["id_office"] = $row->id_office;
      $pay_list["id_customer"] = $row->id_customer;
      $pay_list["id_payment"] = $row->id_payment;
      $pay_list["payment_method_id"] = $row->payment_method_id;
      $pay_list["code"] = $row->code;
      $pay_list["is_down_payment"] = $row->is_down_payment;
      $pay_list["transacted_on"] = date_format(date_create($row->transacted_on),"d/m/Y");
      $pay_list["posted"] = $row->posted;
      $pay_list["name_operator"] = $row->name_operator;
      $total_payment += $row->paid_now;

      $pay_list["items"][$row->id_invoice]["id_invoice"] = $row->id_invoice;
      $pay_list["items"][$row->id_invoice]["invoice_number"] = format_invoice($row->invoice_number);
      $pay_list["items"][$row->id_invoice]["due"] = $row->due_now;
      $pay_list["items"][$row->id_invoice]["pay_amount"] = $row->paid_now;
      
      $sql = "SELECT * FROM accounting_payment_attachments WHERE id_payment = $id_payment";
      $files = $this->db->query($sql)->result();
      $pay_list["items"][$row->id_invoice]["files"] = $files;
    }
    $pay_list["total_pay_amount"] = $total_payment;

    $this->data["payments"] = $pay_list;
    $this->data["customer"] = $cus;
    $this->data["post_max_size"] = formatSizeUnits($post_max_size);
    $this->data["upload_max"] = formatSizeUnits($upload_max);
    $this->data['before_body'] = $this->load->view('script_payment_detail', '', true);


    $this->render('payment_detail', 'admin_clear'); 
    

  }

  public function get_file_payment($id){
    $this->load->helper('download');

    if(!empty($id)){
      $sql = "SELECT * FROM accounting_payment_attachments WHERE id = $id";
      $result = $this->db->query($sql)->row();

      if(count($result) > 0){
        $file_url = $result->location.'/'.$result->encrypted_name;

        if(file_exists($file_url)){
          force_download($result->original_name, file_get_contents($file_url) , NULL);
        }else{
          echo '
          <style type="text/css">
          h3 {
              font-size: 25px;
              font-weight: 300;
              margin-bottom: .5rem;
            font-family: inherit;
            font-weight: 500;
            line-height: 1.2;
            color: inherit;
          }
          p {font-size: 14px;
              margin-top: 0; margin-bottom: 1rem; font-weight: 300;
          }
          </style>
          <section class="content">
              <div class="error-page">
                <div class="error-content">
                  <h3><i class="fas fa-exclamation-triangle text-danger"></i> Oops! Something went wrong.</h3>

                  <p>
                    File tidak ditemukan. Kemungkinan file telah dihapus. Hubungi Administrator untuk lebih lanjut.
                  </p>

                  <button onclick="window.history.back()">Kembali</button>

                </div>
              </div>
              <!-- /.error-page -->

            </section>';
        }
      }
    }
  }

  public function remove_file_payment(){
    $id = $this->input->post("id_file");

    if(!empty($id)){
      $sql = "SELECT * FROM accounting_payment_attachments WHERE id = $id";
      $del = "DELETE FROM accounting_payment_attachments WHERE id = $id";
      $result = $this->db->query($sql)->row();

      if(count($result) > 0){
        $file_url = $result->location.'/'.$result->encrypted_name;

        if(file_exists($file_url)){
          unlink($file_url); 
          $this->db->query($del);

          echo json_encode(array("result" => 1));
        }else{
          echo json_encode(array("result" => 0));
        }
      }else{
        echo json_encode(array("result" => 2));
      }
    }else{
      echo json_encode(array("result" => 3));
    }
  }

}