<script>
	$(function () {
		$('#btn_submit').on('click', function(e){ 
			e.preventDefault();

			var msg = new Array();


			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		var table = $('#list_product').DataTable( {
	        "ajax": "transactions/get_product_list",
	        "columns": [
	            { data: "type", className: "" },
	            { data: "name_plat", className: "" },
	            { data: "size", className: "size" },
	            { data: "qty", className: "qty_col"},
	            { data: "machine", className: "machine"},
	            { data: "price", className: "price"},
	            { data: "discount", className: "discount_col"},
	            { data: "id", className: "",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(""+
			            	"<input type='checkbox' class='checkbox id_product' name='id_product[]' value='"+oData.id+"' >"+
			            	"<input type='checkbox' class='checkbox type_product' name='type_product[]' value='"+oData.type+"' style='display:none;' >"+
			            	"<input type='checkbox' class='checkbox name_product' name='name_product[]' value='"+oData.name_plat + '_' + oData.size+ '_' + oData.qty +"' style='display:none;' >"
			            );
			        }, orderable: false
	            },
	        ],
	        "paging": false,
	        'order': [[1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();


	    $('#product_type').on('change', function () {
	    	clear_selection();

	    	var value = $(this).find("option:selected").val();
	    	if(value == 1) {
	    		table
				.column(4)
				.visible( false )
				.draw();

				$("#machine").hide();
	    	}
	    	else{
	    		table
				.column(4)
				.visible( true )
				.draw();

				$("#machine").show();
	    	}

			if ( table.column(0).search() !== value ) {
				table
				.column(0)
				.search( value )
				.draw();
			}
		});
	    $('#name_plat').on('keyup change', function () {
	    	clear_selection();
			if ( table.column(1).search() !== this.value ) {
				table
				.column(1)
				.search( this.value )
				.draw();
			}
		});

	    $('#size').on('keyup change', function () {
	    	clear_selection();
			if ( table.column(2).search() !== this.value ) {
				table
				.column(2)
				.search( this.value )
				.draw();
			}
		});

		$('#machine').on('keyup change', function () {
			clear_selection();
			if ( table.column(4).search() !== this.value ) {
				table
				.column(4)
				.search( this.value )
				.draw();
			}
		});

		$('.money').mask('000.000.000.000.000', {reverse: true});
		$('.disc').mask('000', {reverse: true});
		$('.discount').mask('000', {reverse: true});
		$('.payment_discount').mask('000', {reverse: true});
		$('.qty_input').mask('000', {reverse: true});
		$('.minute').mask('00000', {reverse: true});

		$('.price').on('change', function () {
			var index = $(this).find("option:selected").index();
			if(index == 3){
				$(this).closest("td").find("input.minute").prop("readonly", false);
			}else{
				$(this).closest("td").find("input.minute").val("");
				$(this).closest("td").find("input.minute").prop("readonly", true);
			}
			recalculate();			
		});

		$('.minute').on('keyup change', function () {
			recalculate();			
		});

		$('.qty').on('keyup change', function () {
			recalculate();			
		});

		$('.discount').on('keyup change', function () {
			recalculate();			
		});

		$('.payment_discount').on('keyup change', function () {
			recalculate();			
		});

		$('#gotocatalog').on('click', function () {
			var id_customer = $("#id_customer").val();
			location.href = "transactions/add/" + id_customer + "/0";
		});
		$('#resetchange').on('click', function () {
			location.href = "transactions/show_cart";
		});
		$('#savechange').on('click', function () {
			$("#cart").attr("action", "transactions/save_change");
			$("#cart").submit();
		});
		$('#gotopayment').on('click', function () {
			$("#cart").attr("action", "transactions/save_change/1");
			$("#cart").submit();
		});
		$('#canceltransaction').on('click', function () {
			if (window.opener && window.opener.open && !window.opener.closed){
				window.onunload = window.opener.goto_registrationhistory();
				window.close();
			}
			else {
				window.location.href = "transactions/";
			}
		});
		$('#submitpayment').on('click', function () {
			var form = $('#payment')[0];
			var data = new FormData(form);

			// var is_dp = ($("#is_dp").is(":checked")) ? 1 : 0;

			// data.append('payment_method', $("#payment_method").find("option:selected").val()); 
			// data.append('total_paid', $("#paid").val()); 
			// data.append('is_dp', is_dp); 			

			$.ajax({
				url: "transactions/payment", 
				type: "post",
				dataType: "json",
				processData: false,
				contentType: false,
            	cache: false,
				data: data,
				success: function(result){
					
					if(result.success == 1) {
						if (window.opener && window.opener.open && !window.opener.closed){
							window.onunload = window.opener.goto_registrationhistory();
							window.close();
						}
						else {
							window.location.href = "transactions/";
						}
					}
  				}
  			});
		});

		if ($('#head_id_office').length > 0) {
			$.ajax({
				url: "transactions/get_offices_list", 
				type: "post",
				dataType: "json",
				data: {
				},
				success: function(result){

		  			$("#head_id_office").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Kantor Cabang--",
		  			});

		  			var id_office = $("#default_id_office").val();

		  			if(id_office.length > 0){
		  				$('#head_id_office').val(id_office).trigger('change');
		  			}
				}
			});
		}

		$('#head_id_office').on('change', function (e) {
		  	var id_office = $("#head_id_office").find("option:selected").val();

		  	$("#id_office").val(id_office);
		  
			$.ajax({
				url: "transactions/get_customers_list", 
				type: "post",
				dataType: "json",
				data: {
					id_office : id_office
				},
				success: function(result){
					$("#head_id_customer").select2("destroy");
					$("#head_id_customer").html("");
		  			$("#head_id_customer").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Customer--",
		  			});
				}
			});		  	
		});

		$('#head_id_customer').select2({
			theme: 'bootstrap4',
		  	placeholder: "--Pilih Customer--",
		});

		$('#head_id_customer').on('select2:select', function (e) {
		  	var data = e.params.data;

		  	$("#id_customer").val(data.id);
		});
		
	});

	function clear_selection() {
		$("input[type=checkbox]").each(function() {
		   $(this).prop("checked", false);
		});
	}

	function addToCart() {
		$(".id_product").each(function(i, obj) {
		   if($(this).is(":checked")){		   	
		   	var currentCell = $(this).closest("td");
		   	var type = $(currentCell).find("input.type_product");
		   	$(type).prop("checked", true);
		   }
		});

		$("#form").submit();
	}

	function checkout() {
		$(".id_product").each(function(i, obj) {
		   if($(this).is(":checked")){		   	
		   	var currentCell = $(this).closest("td");
		   	var type = $(currentCell).find("input.type_product");
		   	$(type).prop("checked", true);
		   }
		});

		$("#form").attr("action", "transactions/cart");
		$("#form").submit();
	}

	function recalculate() {
		var subtotal = 0;
		$('tr.item').each(function(index, elem) { 
			var td_price = $(this).find("td").eq(2);
			var td_qty = $(this).find("td").eq(3);
			var td_disc = $(this).find("td").eq(4);
			var td_amount = $(this).find("td").eq(5);

			var price = $(td_price).find("input.price").val();
			var qty = $(td_qty).find("input.qty").val();
			if(qty == undefined){
				qty = $(td_qty).find("option:selected").val();
			}
			var disc = $(td_disc).find("input.discount").val();
			var disc_end = $("input.payment_discount").val();
			var min = 0;
			var amount = 0;
			var total_amount = 0;
			var total_discount = 0;

			if(qty.length == 0){qty = 0;}
			if(disc.length == 0){disc = 0;}
			if(disc_end.length == 0){disc_end = 0;}

			if(price == undefined) {
				price = $(td_price).find("select.price").find("option:selected").val();

				var index_price = $(td_price).find("select.price").find("option:selected").index();
				if(index_price == 3){	
					var min = $(td_price).find("input.minute").val();
					price = price * min;
				}
			}

			amount = price*qty;
			amount -= (disc/100*amount);
			
			subtotal = subtotal + amount;
			total_discount = disc_end / 100 * subtotal;

			total_amount = subtotal - total_discount;

			$(td_amount).find("input.amount").val(amount);
			$(td_amount).find("span").html(numberToCurrency(amount));

			$("#subtotal").html(numberToCurrency(subtotal));
			$("#total_discount").html(numberToCurrency(total_discount));
			$("#total_due").html(numberToCurrency(total_amount));
		});
	}

	function addFile(){
		var html = ''+
		'<div class="input-group row_file" style="margin: 5px 0;">'+
        '	<div class="custom-file">'+
        '   	<input type="file" class="custom-file-input form-control-sm" name="files[]" onchange="showFileName(this)">'+
        '       <label class="custom-file-label col-form-label-sm" for="custmFile">Choose file</label>'+
        '   </div>'+
        '   <div class="input-group-append">'+
        '   	<button class="btn btn-warning btn-sm" type="button" onclick="removeFile(this)"><i class="fas fa-times-circle"></i> <span>Cancel</span></button>'+
        '  </div>'+
        '</div>';

        $(".file_container").append(html);
	}

	function removeFile(elm){
		var row = $(elm).closest("div.row_file");
		$(row).remove();
	}

	function showFileName(elm){
		var fileName = $(elm).val().split("\\").pop();
		$(elm).siblings(".custom-file-label").addClass("selected").html(fileName);
	}

	function showDetailRank(){
		$("#modal-totalpurchase").modal("show");
	}
	
</script> 