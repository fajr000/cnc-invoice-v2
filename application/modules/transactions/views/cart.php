<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
</style>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-12">
        <table style="width: 100%;">
          <tr>
            <td style="width: 50%;">
              <?php echo $today; ?><br>
              <strong>
              <?php 
                echo $customer->name; 

                $ranking = ($rank->rank > 0) ? '<a style="cursor:pointer; color:blue;" onclick="showDetailRank()" target="_self" title="Total pembelian bulan '.$rank->period_month.' : Rp. '.format_money($rank->amount).'" alt="Total pembelian bulan '.$rank->period_month.' : Rp. '.format_money($rank->amount).'">'.$rank->rank.'</a>' : ''; 
              ?></strong><br>
              <span>Ranking : <?php echo $ranking;?></span>
            </td>
            <td style="width: 50%; text-align: right; vertical-align: top;">
              <div class="row" >
                <div class="col-md-6"></div>
                <div class="col-md-6">             
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <?php
        if($this->session->flashdata('msg_alert') != ""){
        ?>
          <div class="alert alert-warning alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h5><i class="icon fas fa-exclamation-triangle"></i> Peringatan!</h5>
            <?php echo $this->session->flashdata('msg_alert');?>
          </div>
        <?php 
        }
        ?>
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user">
                <form autocomplete="off" name="cart" id="cart" method="post" action="transactions/payments" target="_self" enctype="multipart/form-data">
                  <input type="hidden" id="default_id_office" value="<?php echo $products["id_office"]; ?>">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo $products["id_office"]; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo $products["id_customer"]; ?>">
                  <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="center-text" style="width: 2%;"></th>
                            <th class="center-text" style="width: 35%;">Nama Produk</th>
                            <th class="center-text" style="width: 25%;">Harga (Rp.)</th>
                            <th class="center-text" style="width: 10%;">Jml</th>
                            <th class="center-text" style="width: 10%;">Diskon(%)</th>
                            <th class="center-text" style="width: 13%;">Total</th>
                            <th class="center-text" style="width: 5%;"><i class='fa fa-trash'></i></th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i = 1;
                      foreach($products['cart'] as $row) {

                        $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;

                        $price = "";
                        if($row["options"]["type"] == 2){
                          $selected_easy = ($row["price"] == $row["price_option"]["easy"]) ? "selected" : "";
                          $selected_med = ($row["price"] == $row["price_option"]["medium"]) ? "selected" : "";
                          $selected_diff = ($row["price"] == $row["price_option"]["difficult"]) ? "selected" : "";
                          $selected_min = ($row["price"] == $row["price_option"]["per_minute"]) ? "selected" : "";
                          $readonly = ($selected_min == "selected") ? "" : "readonly";
                          $price = '
                          <div class="form-group row" style="margin-bottom: 0px;">
                            <select name="price[]" class="form-control input-sm price col-sm-8">
                              <option value="'.$row["price_option"]["easy"].'" '.$selected_easy.'>'.number_format($row["price_option"]["easy"], 0, '', '.').' (Easy)</option>
                              <option value="'.$row["price_option"]["medium"].'" '.$selected_med.'>'.number_format($row["price_option"]["medium"], 0, '', '.').' (Medium)</option>
                              <option value="'.$row["price_option"]["difficult"].'" '.$selected_diff.'>'.number_format($row["price_option"]["difficult"], 0, '', '.').' (Difficult)</option>
                              <option value="'.$row["price_option"]["per_minute"].'" '.$selected_min.'>'.number_format($row["price_option"]["per_minute"], 0, '', '.').' (Per menit)</option>
                            </select>
                            <div class="col-sm-4">
                              <input type="text" class="form-control minute" name="minute['.$row["guid"].']" placeholder="Menit" '.$readonly.' value="'.$row["minute"].'">
                            </div>
                          </div>
                          ';

                          if($selected_min == "selected"){
                            $amount= ($row["price"] * $row["minute"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["minute"] * $row["qty"])) ;
                          }
                        }else{
                          $price = '<input type="hidden" name="price[]" class="form-control input-sm price" value="'.$row["price"].'">
                              '.number_format($row["price"], 0, '', '.');

                          $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;
                        }

                        if(isset($row["qty_left"]) && $row["qty_left"] > 0){
                          $html_qty = '
                          <select class="custom-select qty" name="qty[]" style="text-align:right;">
                          ';
                          for($i=1;$i<=$row["qty_left"];$i++){
                            $html_qty .= '<option value="'.$i.'">'.$i.'</option>';
                          }
                          $html_qty .= '</select>';
                        }else{
                          $html_qty = '
                          <input type="text" name="qty[]" class="form-control input-sm qty qty_input" value="'.$row["qty"].'" style="text-align: left;">';
                        }

                        echo '
                        <tr class="item">
                            <td class="middle-text">'.$i.'
                            <input type="hidden" name="guid[]" class="form-control input-sm guid" value="'.$row["guid"].'">
                            <input type="hidden" name="id[]" class="form-control input-sm id" value="'.$row["id"].'">                            
                            </td>
                            <td class="middle-text">'.$row["name"].'</td>
                            <td class="middle-text right-text">'.$price.'
                            </td>
                            <td class="middle-text">
                              '.$html_qty.'
                            </td>
                            <td class="middle-text center-text">
                              <input type="hidden" name="discount[]" class="form-control input-sm discount" value="'.$row["discount"].'" style="text-align: center;">
                              '.$row["discount"].'
                            </td>
                            <td class="middle-text right-text">
                              <input type="hidden" name="amount[]" class="form-control input-sm amount" value="'.$amount.'">
                              <span class="amount_text">'.number_format($amount, 0, '', '.').'</span>
                            </td>
                            <td class="middle-text center-text">
                              <input type="checkbox" class="checkbox guid_delete" name="guid_delete[]" value="'.$row["guid"].'">
                            </td>
                        </tr>
                        ';

                        $i++;
                      }
                      ?>
                      <tr>
                          <td colspan="5" class="right-text">Sub Total</td>
                          <td class="right-text">
                            <div id="subtotal"><?php echo number_format($products["summary"]["subtotal"], 0, '','.');?></div>
                          </td>
                          <td></td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text">
                            <div class="form-group row">
                              <span class="col-sm-11 col-form-label right-text">Diskon(%)</span>
                              <div  class="col-sm-1">
                                <input type="text" class="form-control payment_discount center-text" name="payment_discount" value="<?php echo $products["summary"]["payment_discount"];?>" placeholder="%">
                              </div>
                            </div>
                          </td>
                          <td class="right-text">
                            <div id="total_discount"><?php echo number_format($products["summary"]["payment_discount_amount"], 0, '','.');?></div>
                          </td>
                          <td></td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text"><strong>Total Tagihan</strong></td>
                          <td class="right-text">
                            <strong><div id="total_due"><?php echo number_format($products["summary"]["total"], 0, '','.');?></div></strong>
                          </td>
                          <td></td>
                      </tr>
                    </tbody>
                  </table>
                </form>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <br>
                <center>
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-default btn-sm" id="gotocatalog"><i class="fas fa-step-backward"></i> Kembali ke Katalog</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-danger btn-sm" id="resetchange"><i class="fas fa-history"></i> Batalkan Perubahan</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-success btn-sm" id="savechange"><i class="fas fa-upload"></i> Simpan Perubahan</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-primary btn-sm" id="gotopayment">Pembayaran <i class="fa fa-step-forward"></i></button>
                    </div>                  
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-totalpurchase" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <p>
          <?php 
              echo "<strong>".$customer->name."</strong><br>"; 
              echo "Ranking <strong>".$rank->rank."</strong> pembelian bulan ".$rank->period_month." dengan total invoice Rp.".format_money($rank->amount);
          ?>                  
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

</script>