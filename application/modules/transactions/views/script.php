<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			if($("#name").val().length == 0){
				msg.push("Kolom <strong>Nama</strong> tidak boleh kosong!");
			}

			if($("#nik").val().length == 0){
				msg.push("Kolom <strong>NIK</strong> tidak boleh kosong!");
			}

			if(($("#email").val()).length > 0 && !ValidateEmail($("#email"))){
				msg.push("Kolom <strong>Email</strong> tidak valid!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "sales/";
		});

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "sales/delete/"+deletedId;
        });

        var table = $('#list_data').DataTable( {
	        "ajax": "sales/get_sales_list",
	        "columns": [
	            { data: "id", className: "no" },
	            { data: "name", className: "nama highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='sales/edit/"+oData.id+"'>"+oData.name+"</a>");
			        }
	            },
	            { data: "nik", className: "nik" },
	            { data: "address", className: "alamat" },
	            { data: "phone", className: "telp" },
	            { data: "jml_cus", className: "jml_customer"},
	        	{ data: "id", className: "tools",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" + oData.id + ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>");
			        } 
	        	}
	        ],
	        'order': [[1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

	});

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    } 
	
</script> 