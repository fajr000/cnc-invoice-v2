<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
</style>
<div class="content-header">
  
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user"> 
                <table>
                  <tr><td><strong>No. Invoice</strong></td><td>:</td><td><?php echo $invoices["invoice_number"];?></td></tr>
                  <tr><td><strong>Tgl. Transaksi</strong></td><td>:</td><td><?php echo $invoices["transacted_on"];?></td></tr>
                  <tr><td><strong>Nama Customer</strong></td><td>:</td><td><?php echo $customer->name;?></td></tr>
                  <tr><td><strong>Nama Petugas</strong></td><td>:</td><td><?php echo $invoices["name_operator"] ;?></td></tr>
                </table>
                <form autocomplete="off" name="frmFile" id="frmFile" method="post" target="_self" action="transactions/upload_invoice_item_attachment" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo $invoices["id_office"]; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo $invoices["id_customer"]; ?>">
                  <input type="hidden" name="id_invoice" id="id_invoice" value="<?php echo $invoices["id_invoice"]; ?>">
                  <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="center-text" style="width: 2%;"></th>
                            <th class="center-text" style="width: 50%;">Nama Produk</th>
                            <th class="center-text" style="width: 10%;">Harga (Rp.)</th>
                            <th class="center-text" style="width: 10%;">Jml</th>
                            <th class="center-text" style="width: 10%;">Diskon(%)</th>
                            <th class="center-text" style="width: 18%;">Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      foreach ($invoices["items"] as $key => $row) {
                        echo '
                        <tr>
                        <td class="center-text">'.$i.'</td>
                        <td>'.$row["description"].'</td>
                        <td class="right-text">'.format_money($row["price"]).'</td>
                        <td class="center-text">'.$row["qty"].'</td>
                        <td class="center-text">'.$row["discount"].'</td>
                        <td class="right-text">'.format_money($row["due"]).'</td>
                        </tr>
                        ';

                        $files = array();
                        foreach ($row["files"] as $file) {
                          
                          $view = !exif_imagetype($file->location.'/'.$file->encrypted_name) ? "" : '
                          <div class="col-sm-1">
                            <a href="'.$file->location.'/'.$file->encrypted_name.'" data-toggle="lightbox" data-title="'.$file->original_name.'" data-gallery="gallery">
                              <i class="fas fa-eye"></i>
                            </a>
                          </div>';
                          $download = '<a target="_self" href="invoices/get_file/'.$file->id.'" alt="Download" title="Download" class="btn-sm btn-success"><i class="fas fa-download"></i></a>';
                          $delete = '<a  target="_self" alt="Hapus" title="Hapus" onclick="remove(this,'.$file->id.')" class="btn-sm btn-danger btn-flat"><i class="fa fa-trash"></i></a>';

                          $files[] = '
                          <tr>
                          <td style="width:85%;">'.$file->original_name.'</td>
                          <td style="width:5%; text-align:center;">'.$view.'</td>
                          <td style="width:5%; text-align:center;">'.$download.'</td>
                          <td style="width:5%; text-align:center;">'.$delete.'</td>
                          </tr>
                          ';
                        }
                        $files_html = count($files) > 0 ? "<table style='margin-top:5px; width: 100%;' class='table table-condensed table-sm'>".implode("", $files)."</table>" : "";

                        echo '
                        <tr>
                        <td></td>
                        <td colspan="5">
                        <div class="row">
                          <div class="col-md-2">
                            <button type="button" class="btn btn-block btn-success btn-sm" onclick="addFile('.$key.')"><i class="fas fa-plus"></i> Add File </button>
                            </div> 
                          <div class="col-md-3">
                            <small>(Max file size: '.$upload_max.')</small>
                          </div>
                        </div>
                          '.$files_html.'
                          <div class="file_container_'.$key.'">
                          </div>
                        </td>
                        </tr>
                        ';

                        $i++;
                      }

                      echo '
                      <tr>
                      <td colspan="5" class="right-text">Subtotal</td>
                      <td class="right-text">'.format_money($invoices["invoice_amount"]).'</td>
                      </tr>
                      <tr>
                      <td colspan="5" class="right-text">Disc.('.$invoices["invoice_discount"].'%)</td>
                      <td class="right-text">'.format_money($invoices["invoice_discount_amount"]).'</td>
                      </tr>
                      <tr>
                      <td colspan="5" class="right-text"><strong>Total</strong></td>
                      <td class="right-text"><strong>'.format_money($invoices["invoice_due"]).'</strong></td>
                      </tr>
                      ';

                      ?>
                    </tbody>
                  </table>
                
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <div class="progress" style="display:none">
                  <div id="progressBar" class="progress-bar active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                    <span class="sr-only">0%</span>
                  </div>
                </div>
                <div class="msg alert alert-info text-left" style="display:none"></div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <br>
                <center>
                  <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-default btn-sm" id="gotocatalog" onclick="window.close();"><i class="fas fa-step-backward"></i> Kembali</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-success btn-sm" id="savechange"><i class="fas fa-upload"></i> Simpan Perubahan</button>
                    </div>                
                  </div>
                </center>
              </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

</script>