<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
</style>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-12">
        <table style="width: 100%;">
          <tr>
            <td style="width: 50%;">
              <?php echo $today; ?><br>
              <strong>
              <?php 
                echo $customer->name; 

                $ranking = ($rank->rank > 0) ? '<a style="cursor:pointer; color:blue;" onclick="showDetailRank()" target="_self" title="Total pembelian bulan '.$rank->period_month.' : Rp. '.format_money($rank->amount).'" alt="Total pembelian bulan '.$rank->period_month.' : Rp. '.format_money($rank->amount).'">'.$rank->rank.'</a>' : ''; 
              ?></strong><br>
              <span>Ranking : <?php echo $ranking;?></span>
            </td>
            <td style="width: 50%; text-align: right; vertical-align: top;">
              <div class="row" >
                <div class="col-md-6"></div>
                <div class="col-md-6">             
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user">
                <form autocomplete="off" name="payment" id="payment" method="post" action="transactions/payment" target="_self" enctype="multipart/form-data">
                  <input type="hidden" id="default_id_office" value="<?php echo $products["id_office"]; ?>">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo $products["id_office"]; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo $products["id_customer"]; ?>">

                  <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th class="center-text" style="width: 2%;"></th>
                            <th class="center-text" style="width: 40%;">Nama Produk</th>
                            <th class="center-text" style="width: 20%;">Harga (Rp.)</th>
                            <th class="center-text" style="width: 10%;">Jml</th>
                            <th class="center-text" style="width: 10%;">Diskon(%)</th>
                            <th class="center-text" style="width: 18%;">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $i = 1;
                      foreach($products['cart'] as $row) {

                        $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;

                        $price = number_format($row["price"], 0, '', '.');

                        if($row["options"]["type"] == 2){
                          $selected_easy = ($row["price"] == $row["price_option"]["easy"]) ? "selected" : "";
                          $selected_med = ($row["price"] == $row["price_option"]["medium"]) ? "selected" : "";
                          $selected_diff = ($row["price"] == $row["price_option"]["difficult"]) ? "selected" : "";
                          $selected_min = ($row["price"] == $row["price_option"]["per_minute"]) ? "selected" : "";
                          
                          if($selected_easy == "selected") {
                            $price = number_format($row["price_option"]["easy"], 0, '', '.');
                          }elseif($selected_med == "selected") {
                            $price = number_format($row["price_option"]["medium"], 0, '', '.');
                          }elseif($selected_diff == "selected") {
                            $price = number_format($row["price_option"]["difficult"], 0, '', '.');
                          }else{
                            $price = number_format($row["price_option"]["per_minute"], 0, '', '.');
                            $price.= " (".$row["minute"]." menit)";
                          }                        

                          if($selected_min == "selected"){
                              $amount= ($row["price"] * $row["minute"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["minute"] * $row["qty"])) ;
                          }else{
                            $price = number_format($row["price"], 0, '', '.');

                            $amount= ($row["price"] * $row["qty"]) - (($row["discount"]/100) * ($row["price"] * $row["qty"])) ;
                          }
                        }

                        echo '
                        <tr class="item">
                            <td class="middle-text">'.$i.'</td>
                            <td class="middle-text">'.$row["name"].'</td>
                            <td class="middle-text right-text">'.$price.'</td>
                            <td class="middle-text center-text">'.$row["qty"].'</td>
                            <td class="middle-text center-text">'.$row["discount"].'</td>
                            <td class="middle-text right-text" style="padding-right:25px;">'.number_format($amount, 0, '', '.').'
                            </td>
                        </tr>
                        ';

                        $i++;
                      }
                      ?>
                      <tr>
                          <td colspan="5" class="right-text">Sub Total</td>
                          <td class="right-text" style="padding-right:25px;">
                            <div id="subtotal"><?php echo number_format($products["summary"]["subtotal"], 0, '','.');?></div>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text">
                            <div class="form-group row">
                              <span class="col-sm-12 col-form-label right-text">Diskon (<?php echo $products["summary"]["payment_discount"];?>%)</span>
                            </div>
                          </td>
                          <td class="right-text" style="padding-right:25px;">
                            <div id="total_discount"><?php echo number_format($products["summary"]["payment_discount_amount"], 0, '','.');?></div>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text middle-text"><strong>Total Tagihan :</strong></td>
                          <td class="right-text" style="padding-right:25px;">
                            <strong><div id="total_due"><?php echo number_format($products["summary"]["total"], 0, '','.');?></div></strong>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text middle-text">
                            <div class="form-group clearfix" style="margin-bottom: 0px;">
                              <div class="icheck-primary d-inline">
                                <label>
                                  Bayar
                                </label>
                              </div>
                              <div class="icheck-primary d-inline">(
                                <input type="checkbox" id="is_dp" name="is_dp" value="1">
                                <label for="is_dp" style="font-weight: normal !important;">
                                  DP
                                </label>
                                )
                              </div>
                            </div>
                          </td>
                          <td>
                            <input type="text" class="form-control right-text money" name="total_paid" id="paid" value="<?php echo number_format($products["summary"]["total"], 0, '','.');?>">
                          </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text middle-text"><strong>Metode :</strong></td></td>
                          <td>
                            <select name="payment_method" class="form-control"  id="payment_method">
                                <option value="2">TRANSFER</option>
                                <option value="1">CASH</option>
                                <option value="0">PAY LATER</option>
                              </select>
                          </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="price middle-text">File Lampiran Pembayaran:</td></td>
                          <td class="middle-text">
                              <button type="button" class="btn btn-block btn-success btn-sm" onclick="addFile()"><i class="fas fa-plus"></i> Add </button>
                          </td>
                      </tr>
                      <tr>
                        <td colspan="6">
                          <div class='file_container'>
                          </div>
                        </td>
                      </tr>
                      <tr>
                          <td colspan="5" class="right-text">
                          </td>
                          <td class="right-text">
                            
                          </td>
                      </tr>
                    </tbody>
                  </table>
                
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <br>
                <center>
                  <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-default btn-sm" id="canceltransaction"><i class="fas fa-ban"></i> Batalkan Transaksi</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-primary btn-sm" id="submitpayment">Proses <i class="fa fa-step-forward"></i></button>
                    </div>                  
                  </div>
                </center>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-totalpurchase" style="display: none;" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <p>
          <?php 
              echo "<strong>".$customer->name."</strong><br>"; 
              echo "Ranking <strong>".$rank->rank."</strong> pembelian bulan ".$rank->period_month." dengan total invoice Rp.".format_money($rank->amount);
          ?>                  
        </p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<script type="text/javascript">

</script>