<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			if($("#no_machine").val().length == 0){
				msg.push("Kolom <strong>No. Mesin</strong> tidak boleh kosong!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "machines/";
		});

        $("#submitDelete").on("click", function(){
        	$.ajax({
				url: "machines/delete", 
				type: "post",
				dataType: "json",
				data: {
					id : $("#deletedId").val(),
				},
				success: function(data){
					if(data.success == 1){
						$("#notif_msg").html(data.msg);
						$("#notification").modal("show");
					}else{
						$("#notif_msg").html(data.msg);
						$("#notification").modal("show");
					}

					$('#list_data').DataTable().ajax.reload();
					$("#confirmation").modal('hide');
				}
			});
        });

        var show_office = $("#show_office").val();
        if(show_office == 1) {
        	var table = $('#list_data').DataTable( {
		        "ajax": "machines/get_machine_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "no_machine", className: "no_machine",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a onclick ='editMachine("+oData.id+")' alt='Edit' title='Edit' style='cursor:pointer;' class='text-primary'>"+oData.no_machine+"</a>");
				        }
		            },
		            { data: "name_cutting_type", className: "type" },
		            { data: "name_office", className: "office" },
		            { data: "is_used", className: "status" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[3, 'asc'],[2, 'asc'],[1, 'asc']]
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
        }else{
        	var table = $('#list_data').DataTable( {
		        "ajax": "machines/get_machine_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "no_machine", className: "typefull",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a onclick ='editMachine("+oData.id+")' alt='Edit' title='Edit' style='cursor:pointer;' class='text-primary'>"+oData.no_machine+"</a>");
				        }
		            },
		            { data: "name_cutting_type", className: "type" },
		            { data: "is_used", className: "status" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[2, 'asc'],[1, 'asc']]
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
        }

        $("#no_machine").mask('00');

	});

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$("#confirmation").modal('show');
    } 

    function addMachine(){
		var id_office = $("#header_id_office").val();
		$("#id_office > option").each(function() {
		    if(id_office != "" && this.value != id_office){
		    	$(this).remove();
		    }
		});
		$("select#id_cutting_type").prop('selectedIndex', 0);
		$("#no_machine").val("");
		
		$("#new_data").modal('show');
	}

	function editMachine(id){
		var id_office = $("#header_id_office").val();
		$("#id_office > option").each(function() {
		    if(id_office != "" && this.value != id_office){
		    	$(this).remove();
		    }
		});
		$("select#id_cutting_type").prop('selectedIndex', 0);
		$("#no_machine").val("");

		$.ajax({
			url: "machines/get_machine", 
			type: "post",
			dataType: "json",
			data: {
				id : id
			},
			success: function(data){
				$("#id").val(data.id);
				$("#header_id_office").val(data.id_office);
				$("#id_office").val(data.id_office);
				$("#id_cutting_type").val(data.id_cutting_type).trigger('change');

				var no_machine = data.no_machine;
				$("#no_machine").val(no_machine.substr(1,2));

				$("#new_data").modal('show');
			}
		});

		
	}

	function saveMachine(elm){
		var id = $("#id").val();
		var id_office = $("#id_office").find("option:selected").val();
		var id_cutting_type = $("#id_cutting_type").find("option:selected").val();
		var no_machine = $("#no_machine").val();
		var prefix_machine = $("#prefix_machine").val();

		if(id_cutting_type.length == 0){
			alert("Pilih jenis alat!");
			return false;
		}

		if(no_machine.length == 0){
			alert("Masukkan nomor mesin!");
			return false;
		}

		$.ajax({
			url: "machines/save", 
			type: "post",
			dataType: "json",
			data: {
				id : id,
				id_office : id_office,				
				id_cutting_type : id_cutting_type,
				no_machine : no_machine,
				prefix_machine : prefix_machine,
			},
			success: function(data){
				if(data.success == 1){
					$('#list_data').DataTable().ajax.reload();

					$("#notif_msg").html("Data berhasil disimpan!");
					$("#notification").modal("show");
					$("#new_data").modal('hide');
				}else{
					$("#notif_msg").html(data.msg);
					$("#notification").modal("show");
				}
			}
		});
	}
	
</script> 