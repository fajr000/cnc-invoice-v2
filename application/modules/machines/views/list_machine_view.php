<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
  }
  
  .no{
    text-align: center;
    width: 2%;
  }
  .type{
    vertical-align: middle;
    text-align: left;
    width: 20%; 
  } 
  .typefull{
    vertical-align: middle;
    text-align: left;
    width: 40%; 
  } 
  .no_machine{
    vertical-align: middle;
    text-align: left;
    width: 20%; 
  } 
  .status{
    vertical-align: middle;
    text-align: center;
    width: 20%; 
  } 
  .office{
    vertical-align: middle;
    text-align: left;
    width: 30%; 
  } 
  .tools{
    text-align: center;
    width: 8%;
  }   
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <div class="row">
              <div class="col-md-12 pull-left">
                <?php
                if($allowed['create']){
                ?>
                <a onclick="addMachine()" class="btn-primary btn-sm" target="_self" style="cursor: pointer;"><span>TAMBAH BARU</span></a>
                <?php
                }
                ?>
              </div>
            </div> 
              <br/>
              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    <input type="hidden" id="show_office" value="<?php echo (is_center_admin() || is_admin()) ? 1 : 0; ?>">
                    <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                      <thead>
                        <?php
                        if(is_center_admin() || is_admin()){
                          echo '
                          <tr>
                            <th class="no">&nbsp;</th>
                            <th class="no_machine">No. Mesin</th>
                            <th class="type">Jenis Mesin</th>
                            <th class="office">Kantor Cabang</th>
                            <th class="status">Terpakai</th>
                            <th class="tools">&nbsp;</th>
                          </tr>
                          ';
                        }
                        else{
                          echo '
                          <tr>
                            <th class="no">&nbsp;</th>
                            <th class="no_machine">No. Mesin</th>
                            <th class="typefull">Jenis Mesin</th>                            
                            <th class="status">Terpakai</th>
                            <th class="tools">&nbsp;</th>
                          </tr>
                          ';
                        }
                        ?>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div id="new_data" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Input Mesin</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="fr_purchase" name="fr_purchase" target="_self" action="machines/save">
          <div class="form-group">
            <label for="id_office">Kantor Cabang:</label>
            <input type="hidden" id="id" name="id" value="">
            <input type="hidden" id="header_id_office" value="<?php echo $id_office;?>">
            <select id="id_office" name="id_office" class="custom-select">
              <?php 
              foreach($offices as $row){
                $selected = ($row->id == $id_office) ? "selected" : "";
                echo '<option '.$selected.' value="'.$row->id.'">'.$row->name.'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="id_cutting_type">Jenis Cutting:</label>
            <select id="id_cutting_type" name="id_cutting_type" class="custom-select">
              <?php 
              foreach($cutting_types as $row){
                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
              }
              ?>
            </select>
          </div>  
          <div class="form-group">
            <label for="no_machine">No. Mesin:</label>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <input type="hidden" id="prefix_machine" name="prefix_machine" value="B">
                <span class="input-group-text" id="prefix_label">B</span>
              </div>
              <input type="text" class="form-control" id="no_machine" name="no_machine" value="" placeholder="00" maxlength="2">
            </div>
          </div>          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" onclick="saveMachine(this)" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="confirmation" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="notification" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <p id="notif_msg"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->