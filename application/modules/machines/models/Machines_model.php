<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Machines_model extends CI_Model
{
	private $_table = 'office_machines';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND om.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT om.*, ct.`name` name_cutting_type, o.`name` name_office 
			FROM office_machines om 
			LEFT JOIN cutting_types ct ON ct.`id` = om.`id_cutting_type` 
			LEFT JOIN `offices` o ON o.`id` = om.`id_office`
			WHERE om.`deleted` = 0 $filter_office 
		) qry
		$filter_str 
		ORDER BY qry.`id_office`, qry.`no_machine`
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id) && !is_center_admin()) ? "AND om.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT om.*, ct.`name` name_cutting_type, o.`name` name_office 
			FROM office_machines om 
			LEFT JOIN cutting_types ct ON ct.`id` = om.`id_cutting_type` 
			LEFT JOIN `offices` o ON o.`id` = om.`id_office`
			WHERE om.`deleted` = 0 $filter_office 
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT om.*, ct.`name` name_cutting_type, o.`name` name_office
			FROM office_machines om 
			LEFT JOIN cutting_types ct ON ct.`id` = om.`id_cutting_type`
			LEFT JOIN `offices` o ON o.`id` = om.`id_office`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, array("deleted" => 1));

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}
	}
}
