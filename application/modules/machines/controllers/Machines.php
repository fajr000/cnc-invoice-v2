<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Machines extends Admin_Controller
{ 
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Machines_model', 'machine');
    $this->load->model('offices/Offices_model', 'office');

    $this->data['page_title'] = 'Mesin | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Mesin';
    $this->data['page_subheader'] = 'Data Mesin';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists()
  {
    $this->data['offices'] = $this->office->get_all_items(array());
    $this->data['id_office'] = $this->session->userdata("id_office");
    $this->data['cutting_types'] = get_cutting_types();

    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_machine_view', 'admin_master');
  }

  public function get_machine_list()
  {
    // populate data
    $params = array();

    $all_items = $this->machine->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a style='cursor:pointer;' target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" . $row->id . ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      if($row->is_used == 1){
        $all_items[$key]->is_used = "Ya";
      }else{
        $all_items[$key]->is_used = "Tidak";
      }

      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->machine->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_machine_view', 'admin_master'); 
  }

  public function edit($id_item)
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->machine->get_detail_item($id_item);

    if(!has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['offices'] = $this->office->get_all_items();
    $this->data['office'] = get_office_id();
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_machine_view', 'admin_master');
  }

  public function delete()
  {
    $id_item = $this->input->post("id");

    $item = $this->machine->get_detail_item($id_item);
    if($item->is_used == 0){
      $result = $this->machine->delete_item($id_item);   

      if($result > 0){
        echo json_encode(array(
          "success" => 0,
          "msg" => "Data berhasil dihapus."
        ));
      }
      else {
        echo json_encode(array(
          "success" => 0,
          "msg" => "Data gagal dihapus!"
        ));
      }

    }else{
      echo json_encode(array(
        "success" => 0,
        "msg" => "Data gagal dihapus. Mesin sedang digunakan!"
      ));
    }
    
  }

  public function validate_no_machine($id, $id_office, $no_machine){
    $item = $this->db->query("SELECT * FROM office_machines WHERE deleted = 0 AND id_office = $id_office AND no_machine = '".$no_machine."'")->row();

    if(count($item) > 0){
      if($item->id == $id){
        return true;
      }else{
        return false;
      }
    }else{
      return true;
    }
  }

  public function get_machine(){
    $id   = $this->input->post('id');
    $item = $this->machine->get_detail_item($id);

    $item = (array) $item;

    echo json_encode($item);

  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id       = $this->input->post('id');
    $id_office= $this->input->post('id_office');
    $id_cutting_type= $this->input->post('id_cutting_type');
    $no_machine      = $this->input->post('no_machine');
    $prefix_machine  = $this->input->post('prefix_machine');    

    $no_machine = $prefix_machine."".str_pad($no_machine, 2, "0", STR_PAD_LEFT);

    $temp = array(
      'id' => $id,
      'id_office' => $id_office,
      'id_cutting_type' => $id_cutting_type,
      'no_machine' => $no_machine,
    );

    if($this->validate_no_machine($id, $id_office, $no_machine)){
      $data = array(
        'id_office' => $id_office,
        'id_cutting_type' => $id_cutting_type,
        'no_machine' => $no_machine,
      );

      //register
      if(empty($id)) {
          $result = $this->machine->insert_item($data);

          if($result > 0){
            $id = $result;
          }
      }
      //edit
      else {
          $result = $this->machine->update_item($id, $data);
          if($result > 0) {
            $id = $result;
          }
      }

      if($result > 0){
        echo json_encode(array(
          "success" => 1,
          "msg" => ""
        ));
      }else{
        echo json_encode(array(
          "success" => 0,
          "msg" => "Gagal menyimpan data mesin!"
        ));
      }

    }else{
      echo json_encode(array(
        "success" => 0,
        "msg" => "Nomor mesin telah terdaftar. Gunakan nomor yang lain!"
      ));
    }
  }
}