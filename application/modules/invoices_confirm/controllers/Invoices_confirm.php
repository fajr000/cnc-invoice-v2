<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices_confirm extends My_Controller {
	
	function __construct()
	{ 
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
	    $this->load->helper(array('url', 'language'));

	    $this->load->model('invoices/Invoice_model', 'invoice');
	    $this->load->model('offices/Offices_model', 'office');
	    $this->load->model('customers/Customers_model', 'customer');
	    $this->load->model('sales/Sales_model', 'sales');    
	    $this->load->model('transactions/Transaction_model', 'transaction');    

	    $this->data['page_title'] = 'Invoice | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Need Confirm Invoices';
	    $this->data['page_subheader'] = 'Daftar Invoice Perlu Dikonfirmasi';

	    // Set pemission/ access
	    $class_name = $this->router->fetch_class();
	    set_parent_url($class_name);
	    $this->data['allowed'] = build_access($class_name);
	}

	public function index()
	{
		if (!logged_in())
		{
			//redirect them to the login page
			redirect('user/login', 'refresh');
		}

		$this->get_lists();        
	}

	public function get_lists()
	{
		// page script js
		$this->data['before_body'] = $this->load->view('script', '', true);
		// echo "<pre>";print_r($items);die();
		$this->render('list_invoice_view', 'admin_master');
	}

	public function get_invoice_list($id_office = null) {
		$params = array();

		$params[] = "status = 0";
		if(isset($id_office) && !empty($id_office)){
			$id_office = decrypt_url($id_office);
			$params[] = "id_office = $id_office";
		}

	    $inv = $this->invoice->get_all_items($params);

	    foreach ($inv as $key => $row) {
	    	$inv[$key]->id_transaction = encrypt_url($row->id_transaction);
	    	$inv[$key]->id_office = encrypt_url($row->id_office);
	    	$inv[$key]->id_customer = encrypt_url($row->id_customer);
	    	$inv[$key]->id_invoice = encrypt_url($row->id_invoice);
	    	$inv[$key]->invoice_number = format_invoice($row->invoice_number);
	    	$inv[$key]->total = format_money($row->total);
	    	$inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

	    	switch ($row->status) {
	    		case 1:
	    			$inv[$key]->status_label = '<small class="badge badge-info" style="font-weight: normal;"> Request Token </small>';
	    			break;
	    		case 2:
	    			$inv[$key]->status_label = '<small class="badge badge-warning" style="font-weight: normal;"> Waiting List </small>';
	    			break;
	    		case 3:
	    			$inv[$key]->status_label = '<small class="badge badge-primary" style="font-weight: normal;"> Diproses </small>';
	    			break;
	    		case 4:
	    			$inv[$key]->status_label = '<small class="badge badge-success" style="font-weight: normal;"> Selesai </small>';
	    			break;
	    		case 5:
	    			$inv[$key]->status_label = '<small class="badge badge-danger" style="font-weight: normal;"> Dibatalkan </small>';
	    			break;
	    		
	    		default:
	    			$inv[$key]->status_label = '<small class="badge badge-secondary" style="font-weight: normal;"> Konfirmasi </small>';
	    			break;
	    	}

			$tools = "";
			if($row->cancelled_on == "0000-00-00 00:00:00"){
				$tools = '
				<select name="tools[]" class="form-control tool-act" data-id_office="'.$row->id_office.'" data-id_customer="'.$row->id_customer.'" onchange="invoiceTodo(this,\''.$row->id_invoice.'\', \'confirm\')">
				<option>--Pilih--</option>
				<option value="detail">Detail</option>
				<option value="delete">Batal</option>
				</select>
				';
			}

			$inv[$key]->tools = $tools;
	    }

	    echo json_encode(array("data"=>object_to_array($inv)));
	}	

}
