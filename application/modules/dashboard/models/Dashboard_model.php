<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
	public function __construct(){
		parent::__construct();
	}

	public function get_messages(){
		$office_filter = "";
		$office = get_office_id();
		if(count($office) > 0 && isset($office->id) && !empty($office->id)){
			$office_filter = " OR id_office = " . $office->id;
		}
		$sql = "SELECT * FROM office_messages WHERE id_office IS NULL $office_filter";

		return $this->db->query($sql)->result();
	}

	public function update_main_message($title, $message){
		$sql = "UPDATE office_messages SET title = '".$title."', message = '".$message."' WHERE id_office IS NULL";

		$this->db->trans_start();
		$this->db->query($sql);
		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function update_office_message($id_office, $title, $message){
		$sql = "UPDATE office_messages SET title = '".$title."', message = '".$message."' WHERE id_office = $id_office";

		$this->db->trans_start();
		$this->db->query($sql);
		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}
	}

}
