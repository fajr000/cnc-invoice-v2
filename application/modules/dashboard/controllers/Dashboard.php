<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Admin_Controller {
	
	public function __construct()
	{
		parent::__construct();

		$this->data['page_title'] = 'Home | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Beranda';
	    $this->data['page_subheader'] = 'Beranda';

	    $this->load->model('Dashboard_model', 'dashboard');  

		$subclass_name = $this->router->fetch_class();
    	set_parent_url($subclass_name);
	}
	public function index()
	{
		$msg = $this->dashboard->get_messages();

		$this->data["messages"] = $msg;

		$this->data['before_body'] = $this->load->view('script', '', true);
		$this->render('dashboard_view');
	}

	public function save(){
		$title = $this->input->post("main_title");
		$message = $this->input->post("main_message");

		$result = $this->dashboard->update_main_message($title, $message);

		if($result){
			$this->session->set_flashdata("msg", "Berhasil menyimpan data.");
		}
		else{
			$this->session->set_flashdata("msg", "Gagal menyimpan data.");
		}

		redirect("dashboard");
	}

	public function save_subs(){
		$title = $this->input->post("subs_title");
		$message = $this->input->post("subs_message");

		$result = $this->dashboard->update_office_message($this->session->userdata("id_office"), $title, $message);

		if($result){
			$this->session->set_flashdata("msg", "Berhasil menyimpan data.");
		}
		else{
			$this->session->set_flashdata("msg", "Gagal menyimpan data.");
		}

		redirect("dashboard");
	}

	public function e_405()
	{
		$this->render('error_not_allowed');
	}

	public function encrypt(){
		$invoice_number = "INV/02/02/VIII/2021";
		$no_order = "1";
		$minutes = 999;
		$no_machine = "B01";

		$temp = explode("/", $invoice_number);
		$month = $temp[3];
		$month = str_pad($month, 4, "0", STR_PAD_RIGHT);
		$invoice_number = $temp[0].$temp[1].$temp[2].$month.$temp[4];
		$plaintext = str_replace("/", "", $invoice_number)."#".$no_order.str_pad($minutes, 3, "0", STR_PAD_LEFT);
		$key = "J".$no_machine.$temp[1]."23456ABCDEFGHI";

		$venkrip = strtoupper(bin2hex($this->venkrip($key, $plaintext)));
		$vcdecrypt = $this->vcdecrypt($key, $this->venkrip($key, $plaintext));

		echo $venkrip."<br>".$vcdecrypt;
	}

	function venkrip($key,$plaintext){
		$j = $i = $shift = $offset = 0;
		$hasil = "";

		$shift_arr = array();
		for($i=0;$i<strlen($plaintext);$i++){
			$shift = ord($key[$j]) - 96;

			$offset = (ord($plaintext[$i]) + $shift) % 256;
			$hasil .= chr($offset);

			$j++;
		}
		
		return $hasil;
	}

	function vcdecrypt($key,$encryptedtext){
		$j = $i = $shift = $offset = 0;
		$hasil = "";

		$shift_arr = array();
		for($i=0;$i<strlen($encryptedtext);$i++){
			$shift = ord($key[$j]) - 96;

			$offset = (ord($encryptedtext[$i]) - $shift) % 256;
			if($offset < 0){
				$offset+=256;
			}
			$hasil .= chr($offset);

			$j++;
		}
		
		return $hasil;
	}



}
