<!-- Content Header (Page header)  -->
<style type="text/css">
.hidden{
  display: none;
}
</style>
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <!--
    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary card-outline">
          <div class="card-header">
            <h3 class="card-title">Selamat Datang</h3>
          </div>
          <div class="card-body">
            <div class="row">
              
            </div>
          </div>
        </div>
      </div>
    </div>
  -->
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
      <?php 
          foreach($messages as $msg){
            if(empty($msg->id_office)){

              $btnEditMain = "";
              $btnSaveMain = "";
              $btnEditSubs = "";
              $btnSaveSubs = "";
              if(is_admin() || is_center_admin()){
                $btnEditMain = '<button type="button" id="btn-edit-main-message" class="btn btn-default float-right" onclick="updateMainMsg(this)">Edit</button></button>';
                $btnSaveMain = '<button type="button" id="btn-save-main-message" class="btn btn-default float-right hidden" onclick="saveMainMsg(this)">Save</button></button>';
              }

              if(is_admin() || is_center_admin() || is_branch_admin()){
                $btnEditSubs = '<button type="button" id="btn-edit-subs-message" class="btn btn-default float-right" onclick="updateSubsMsg(this)">Edit</button></button>';
                $btnSaveSubs = '<button type="button" id="btn-save-subs-message" class="btn btn-default float-right hidden" onclick="saveSubsMsg(this)">Save</button></button>';
              }
            
              echo '
              <div id="main_message_block">
                <div class="callout callout-info">
                  <div id="main_label_title"><h5><strong>'.$msg->title.'</strong></h5></div>
                  <div>
                    <div id="main_body_message">
                      <p>'.$msg->message.'</p>
                    </div>
                    <form method="post" action="dashboard/save" target="_self" id="fr-main-message" style="display:none;">
                    <label>Judul:</label>
                      <input type="text" id="main_title" class="form-control" name="main_title" value="'.$msg->title.'" placeholder="Judul Pesan" />
                      <br/>
                      <label>Deskripsi:</label>
                      <textarea id="main_message" name="main_message">'.$msg->message.'</textarea>
                    </form>
                  </div>
                  '.$btnEditMain.' '.$btnSaveMain.'
                  <div style="clear:both;"></div>
                </div>
              </div>
                ';
            }
            else{
              if(!empty($msg->message)){
                echo '
                <div class="callout callout-warning">
                  <div id="subs_label_title"><h5><strong>'.$msg->title.'</strong></h5></div>
                    <div>
                      <div id="subs_body_message">
                        <p>'.$msg->message.'</p>
                      </div>                    
                      <form method="post" action="dashboard/save_subs" target="_self" id="fr-subs-message" style="display:none;">
                        <label>Judul:</label>
                        <input type="text" id="subs_title" class="form-control" style="width:100%;" name="subs_title" value="'.$msg->title.'" placeholder="Judul Pesan" />
                        <br/>
                        <label>Deskripsi:</label>
                        <textarea id="subs_message" name="subs_message">'.$msg->message.'</textarea>
                      </form>
                    </div>
                    '.$btnEditSubs.' '.$btnSaveSubs.'
                    <div style="clear:both;"></div>
                  </div>
                </div>
                ';
              }
            }
          }
          ?>
        </div>
    </div>
    <!-- /.row -->
  </div>
</section>
<!-- /.content-->