<script>
$(function () {
	$('#main_message').summernote();
	$('#subs_message').summernote();
});

function updateMainMsg(elm){
	$("#btn-edit-main-message").hide();
	$("#btn-save-main-message").show();
	$("#main_label_title").hide();
	$("#main_body_message").hide();
	$('#main_message').summernote();
	$('#fr-main-message').show();
}

function updateSubsMsg(elm){
	$("#btn-edit-subs-message").hide();
	$("#btn-save-subs-message").show();
	$("#subs_label_title").hide();
	$("#subs_body_message").hide();
	$('#subs_message').summernote();
	$('#fr-subs-message').show();
}

function saveMainMsg(elm){
	$("#fr-main-message")[0].submit();
}

function saveSubsMsg(elm){
	$("#fr-subs-message")[0].submit();
}
</script>