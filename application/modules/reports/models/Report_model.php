<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report_model extends CI_Model
{

	public function __construct(){
		parent::__construct();
	}

	public function get_customer_transaction_rank($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	
		$filter_str_sub = "";
		if(count($params) > 0)	{
			$filter_str_sub = "WHERE vcp.".$params[0];
		}

		$sql = "
		SELECT *
		FROM (
			SELECT vcp.*
				, CONCAT(SUBSTRING(vcp.`period`, 5, 2), SUBSTRING(vcp.`period`, 1, 4)) period_val 
				, DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%M %Y') period_val_string 
				, CONCAT(toIndonesianMonth(DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%c')), ' ', DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%Y')) period_val_month 
				, off.`name` name_office
				, cr.`name` name_customer
			FROM v_customer_period_rank_all vcp
			LEFT JOIN offices off ON off.`id` = vcp.`id_office`
			LEFT JOIN customers cr ON cr.`id` = vcp.`id_customer` 
			$filter_str_sub
			ORDER BY vcp.`id_office`, vcp.`period` DESC, vcp.`rank` ASC
		) qry
		$filter_str 
		ORDER BY id_office, period, rank ASC
";

		return $this->db->query($sql)->result();
	}

	public function get_sales_invoice_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " AND " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT vcp.* 
				, CONCAT(SUBSTRING(vcp.`period`, 5, 2), SUBSTRING(vcp.`period`, 1, 4)) period_val 
				, DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%M %Y') period_val_string 
				, off.`name` name_office
				, cr.`name` name_customer
				, cr.`id_sales`
				, sl.`name` name_sales
				, DATE_FORMAT(tr.`transacted_on`, '%d/%m/%Y') transacted_on
			FROM v_customer_amount_per_invoice vcp
			LEFT JOIN offices off ON off.`id` = vcp.`id_office`
			LEFT JOIN customers cr ON cr.`id` = vcp.`id_customer`
			LEFT JOIN sales sl ON sl.`id` = cr.`id_sales`
			LEFT JOIN transactions tr ON tr.`id` = vcp.`id_transaction`
			ORDER BY vcp.`id_office`, sl.`name` ASC, vcp.`period` ASC
		) qry 
		WHERE name_sales IS NOT NULL 
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_monthly_sales_compensation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " AND " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT vcp.`id_office`
				, cr.`id_sales`
				, off.`name` name_office
				, sl.`name` name_sales
				, DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%M %Y') period_val_string 
				, SUM(vcp.`amount`) amount
			FROM v_customer_period_rank_all vcp
			LEFT JOIN offices off ON off.`id` = vcp.`id_office`
			LEFT JOIN customers cr ON cr.`id` = vcp.`id_customer` 
			LEFT JOIN sales sl ON sl.`id` = cr.`id_sales`
			GROUP BY vcp.`id_office`, cr.`id_sales`, vcp.`period`
			ORDER BY vcp.`id_office`, vcp.`period` DESC, sl.`name` ASC
		) qry 
		WHERE name_sales IS NOT NULL
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_material_purchase_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT DATE_FORMAT(opo.`transacted_on`, '%d/%m/%Y') transacted_on
				, DATE_FORMAT(opo.`transacted_on`, '%M %Y') transacted_month
				, opo.`id_office`
				, off.`name` name_office
				, opo.`id_plat`
				, pt.`name` name_plat
				, opo.`size`
				, opo.`price_purchase`
				, opo.`qty`
				, (opo.`price_purchase` * opo.`qty`) total
			FROM office_plat_order opo
			LEFT JOIN offices off ON off.`id` = opo.`id_office`
			LEFT JOIN plat_types pt ON pt.`id` = opo.`id_plat`
			ORDER BY opo.`id_office`, opo.`transacted_on`, opo.`id_plat` DESC
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_machine_usage_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
		SELECT tk.`id_office`, DATE_FORMAT(ai.`processed_time`, '%M %Y') processed_month, off.`name` name_office, tk.`id_machine`, ct.`name` name_cutting_type, tk.`no_machine`, SUM(tk.`minutes`) minutes
		FROM tokens tk 
		LEFT JOIN accounting_invoices ai ON ai.`id` = tk.`id_invoice`
		LEFT JOIN offices off ON off.`id` = tk.`id_office`
		LEFT JOIN office_machines om ON om.`id_office` = off.`id` AND om.`id` = tk.`id_machine`
		LEFT JOIN cutting_types ct ON ct.`id` = om.`id_cutting_type`
		GROUP BY tk.`id_office`, DATE_FORMAT(ai.`processed_time`, '%M %Y'), tk.`id_machine` 
		ORDER BY tk.`id_office`, ai.`processed_time` DESC, tk.`no_machine`
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_material_sales_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
		SELECT tr.`id_office`, off.`name` name_office
		, DATE_FORMAT(tr.`transacted_on`, '%M %Y') transacted_month
		, CONCAT(toIndonesianMonth(DATE_FORMAT(tr.`transacted_on`, '%c')),' ',DATE_FORMAT(tr.`transacted_on`, '%Y')) transacted_month_sub
		, pp.`id_plat`, pt.`name` name_plat, pp.`size`, SUM(aii.`qty`) qty, SUM((aii.`amount` - aii.`discount_amount`)) amount 
		FROM accounting_invoice_items aii 
		LEFT JOIN accounting_invoices ai ON ai.`id` = aii.`id_invoice`
		LEFT JOIN transactions tr ON tr.`id` = ai.`id_transaction` 
		LEFT JOIN plat_prices pp ON pp.`id` = aii.`id_plat` 
		LEFT JOIN plat_types pt ON pt.`id` = pp.`id_plat` 
		LEFT JOIN offices off ON off.`id` = tr.`id_office`
		WHERE aii.`id_plat` > 0 AND aii.`id_service` = 0 
		AND tr.`cancelled_on` = '0000-00-00 00:00:00' AND tr.`transaction_type` = 1
		GROUP BY DATE_FORMAT(tr.`transacted_on`, '%M %Y'), pp.`id_plat`, pt.`name`, pp.`size`
		ORDER BY tr.`id_office`, tr.`transacted_on` DESC, pt.`name` ASC, pp.`size` ASC
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}


	public function get_invoice_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT vcp.* 
				, CONCAT(SUBSTRING(vcp.`period`, 5, 2), SUBSTRING(vcp.`period`, 1, 4)) period_val 
				, DATE_FORMAT(SUBSTRING(vcp.`period`, 1, 8), '%M %Y') period_val_string 
				, off.`name` name_office
				, cr.`name` name_customer
				, cr.`id_sales`
				, sl.`name` name_sales
				, DATE_FORMAT(tr.`transacted_on`, '%d/%m/%Y') transacted_on 
				, DATE_FORMAT(tr.`transacted_on`, '%Y%m%d') transacted_on_short
			FROM v_customer_amount_per_invoice vcp
			LEFT JOIN offices off ON off.`id` = vcp.`id_office`
			LEFT JOIN customers cr ON cr.`id` = vcp.`id_customer`
			LEFT JOIN sales sl ON sl.`id` = cr.`id_sales`
			LEFT JOIN transactions tr ON tr.`id` = vcp.`id_transaction`
			ORDER BY vcp.`id_office`, vcp.`period` ASC, tr.`transacted_on` ASC
		) qry
		$filter_str 
		ORDER BY qry.`id_office`, qry.`id_invoice`
";

		return $this->db->query($sql)->result();
	}

	public function get_payment_recapitulation($params = null){
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT vcp.* 
				, off.`name` name_office
			FROM v_customer_payment vcp
			LEFT JOIN offices off ON off.`id` = vcp.`id_office`
		) qry
		$filter_str  
		ORDER BY qry.`code`, qry.`transacted_on`, qry.`id_invoice` 
";

		return $this->db->query($sql)->result();
	}
}
