<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reports extends My_Controller {
	
	function __construct() { 
		parent::__construct();
		$this->load->library(array('ion_auth', 'form_validation', 'pagination'));
	    $this->load->helper(array('url', 'language'));

	    $this->load->model('Report_model', 'report');
	    $this->load->model('offices/Offices_model', 'office');
	    $this->load->model('customers/Customers_model', 'customer');
	    $this->load->model('sales/Sales_model', 'sales');    

	    $this->data['page_title'] = 'Report | Pioner CNC Indonesia';
	    $this->data['page_header'] = 'Report';
	    $this->data['page_subheader'] = 'Laporan';

	    // Set pemission/ access
	    $class_name = $this->router->fetch_class();
	    set_parent_url($class_name);
	    $this->data['allowed'] = build_access($class_name);
	}

	public function index(){
		if (!logged_in())
		{
			//redirect them to the login page
			redirect('user/login', 'refresh');
		}

		$this->get_lists();        
	}

	public function get_lists(){
		$this->data['report_options'] = $this->db->query("SELECT * FROM reports ORDER BY id")->result();

		// page script js
		$this->data['before_body'] = $this->load->view('script', '', true);
		// echo "<pre>";print_r($items);die();
		$this->render('list_view', 'admin_master');
	}

	public function customer_transaction_rank(){
		$id_office = ($this->input->post("id_office")) ? $this->input->post("id_office") : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_customer_transaction_rank($params);
		$office  = $this->get_offices_options();

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">'.$office.'</th>
				<th style="width:15%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_month" id="filter_month" placeholder="Bulan" style="width:100%; text-align:center;" onkeyup="searchData()"></th>
				<th style="width:30%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_customer" id="filter_customer" placeholder="Nama Customer" style="width:100%; text-align:left;" onkeyup="searchData()"></th>
				<th style="width:10%;" class="center-text"></th>
				<th style="width:20%;" class="center-text"></th>
			</tr>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">Nama Kantor</th>
				<th style="width:15%;" class="center-text">Bulan</th>
				<th style="width:30%;" class="center-text">Nama Customer</th>
				<th style="width:20%;" class="center-text">Total Transaksi</th>
				<th style="width:10%;" class="center-text">Peringkat</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$temp = explode(" ", $row->period_val_string);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:20%;" class="">'.$row->name_office.'</td>
				<td style="width:15%;" class="">'.$month.'</td>
				<td style="width:30%;" class="">'.$row->name_customer.'</td>
				<td style="width:20%;" class="right-text">'.number_format($row->amount, 0, ',', '').'</td>
				<td style="width:10%;" class="center-text">'.$row->rank.'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function sales_invoice_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? decrypt_url($this->input->post("id_office")) : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_sales_invoice_recapitulation($params);

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">Nama Sales</th>
				<th style="width:15%;" class="center-text">Tanggal</th>
				<th style="width:20%;" class="center-text">No. Invoice</th>
				<th style="width:20%;" class="center-text">Nama Customer</th>
				<th style="width:20%;" class="center-text">Total Transaksi</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$temp = explode(" ", $row->period_val_string);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:20%;" class="">'.$row->name_sales.'</td>
				<td style="width:15%;" class="center-text">'.$row->transacted_on.'</td>
				<td style="width:20%;" class="center-text">'.$row->invoice_number.'</td>
				<td style="width:20%;" class="">'.$row->name_customer.'</td>
				<td style="width:20%;" class="right-text">'.number_format($row->due, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function monthly_sales_compensation(){
		$settings = $this->db->query("SELECT `id_office`, `value` FROM office_settings WHERE `code` = 'SALES_COMPENSATION_PERCENTAGE'")->result();
		$temp = array();
		foreach($settings as $row){
			$temp[$row->id_office] = $row->value;
		}
		$settings = $temp;

		$id_office = ($this->input->post("id_office")) ? decrypt_url($this->input->post("id_office")) : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_monthly_sales_compensation($params);

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:15%;" class="center-text">Bulan</th>
				<th style="width:30%;" class="center-text">Nama Sales</th>				
				<th style="width:20%;" class="center-text">Total Invoice</th>
				<th style="width:10%;" class="center-text">% Komisi</th>
				<th style="width:20%;" class="center-text">Jumlah Komisi</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$percentage = (isset($settings[$row->id_office]) && !empty($settings[$row->id_office])) ? $settings[$row->id_office] : 0;

			$temp = explode(" ", $row->period_val_string);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$compensation = $percentage / 100 * $row->amount;
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:15%;" class="center-text">'.$month.'</td>
				<td style="width:30%;" class="">'.$row->name_sales.'</td>				
				<td style="width:20%;" class="right-text">'.number_format($row->amount, 0, ',', '').'</td>
				<td style="width:10%;" class="center-text">'.$percentage.'%</td>
				<td style="width:20%;" class="right-text">'.number_format($compensation, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function material_purchase_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? decrypt_url($this->input->post("id_office")) : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_material_purchase_recapitulation($params);

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:15%;" class="center-text">Bulan</th>
				<th style="width:15%;" class="center-text">Tanggal</th>				
				<th style="width:20%;" class="center-text">Plat</th>
				<th style="width:15%;" class="center-text">Harga Beli</th>
				<th style="width:10%;" class="center-text">Qty</th>
				<th style="width:20%;" class="center-text">Total</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$temp = explode(" ", $row->transacted_month);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:15%;" class="center-text">'.$month.'</td>
				<td style="width:15%;" class="center-text">'.$row->transacted_on.'</td>
				<td style="width:20%;" class="">'.$row->name_plat." ".$row->size.' mm</td>				
				<td style="width:20%;" class="right-text">'.number_format($row->price_purchase, 0, ',', '').'</td>
				<td style="width:10%;" class="center-text">'.$row->qty.'</td>
				<td style="width:20%;" class="right-text">'.number_format($row->total, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function machine_usage_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? decrypt_url($this->input->post("id_office")) : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_machine_usage_recapitulation($params);

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:50%;" class="center-text">Nama Kantor</th>
				<th style="width:15%;" class="center-text">Bulan</th>
				<th style="width:15%;" class="center-text">Jenis Mesin</th>
				<th style="width:15%;" class="center-text">Waktu</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$month = "";
			if(!empty($row->processed_month)){
				$temp = explode(" ", $row->processed_month);
				$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			}
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:50%;" class="">'.$row->name_office.'</td>
				<td style="width:15%;" class="">'.$month.' </td>				
				<td style="width:15%;" class="center-text">'.$row->name_cutting_type.'</td>
				<td style="width:15%;" class="right-text">'.number_format($row->minutes, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function material_sales_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? decrypt_url($this->input->post("id_office")) : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_material_sales_recapitulation($params);
		$office  = $this->get_offices_options();

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">'.$office.'</th>
				<th style="width:15%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_month" id="filter_month" placeholder="Bulan" style="width:100%; text-align:center;" onkeyup="searchData()"></th>
				<th style="width:30%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_plat" id="filter_plat" placeholder="Nama Plat" style="width:100%; text-align:left;" onkeyup="searchData()"></th>
				<th style="width:10%;" class="center-text"></th>
				<th style="width:20%;" class="center-text"></th>
			</tr>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">Nama Kantor</th>
				<th style="width:15%;" class="center-text">Bulan</th>
				<th style="width:30%;" class="center-text">Plat</th>
				<th style="width:10%;" class="center-text">Qty</th>
				<th style="width:20%;" class="center-text">Total</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$temp = explode(" ", $row->transacted_month);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:20%;" class="center-text">'.$row->name_office.'</td>
				<td style="width:15%;" class="center-text">'.$month.'</td>
				<td style="width:30%;" class="">'.$row->name_plat." ".$row->size.' mm</td>				
				<td style="width:10%;" class="center-text">'.$row->qty.'</td>
				<td style="width:20%;" class="right-text">'.number_format($row->amount, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function invoice_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? $this->input->post("id_office") : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_invoice_recapitulation($params);
		$office  = $this->get_offices_options();

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">'.$office.'</th>
				<th style="width:15%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control date_range" name="filter_date_range" id="filter_date_range" placeholder="Tanggal" style="width:100%; text-align:center;"></th>
				<th style="width:20%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_invoice" id="filter_invoice" placeholder="No. Invoice" style="width:100%; text-align:center;" onkeyup="convertUpperCase(this);searchData()"></th>
				<th style="width:20%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_customer" id="filter_customer" placeholder="Nama Customer" style="width:100%; text-align:left;" onkeyup="searchData()"></th>
				<th style="width:20%;" class="center-text"></th>
			</tr>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">Nama Kantor</th>
				<th style="width:15%;" class="center-text">Tanggal</th>
				<th style="width:20%;" class="center-text">No. Invoice</th>
				<th style="width:20%;" class="center-text">Nama Customer</th>
				<th style="width:20%;" class="center-text">Total Transaksi</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$temp = explode(" ", $row->period_val_string);
			$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:20%;" class="">'.$row->name_office.'</td>
				<td style="width:15%;" class="center-text">'.$row->transacted_on.'</td>
				<td style="width:20%;" class="center-text">'.$row->invoice_number.'</td>
				<td style="width:20%;" class="">'.$row->name_customer.'</td>
				<td style="width:20%;" class="right-text">'.number_format($row->due, 0, ',', '').'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function payment_recapitulation(){
		$id_office = ($this->input->post("id_office")) ? $this->input->post("id_office") : (($this->session->userdata("id_office")) ? $this->session->userdata("id_office") : "");

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		$results = $this->report->get_payment_recapitulation($params);
		$office  = $this->get_offices_options();

		$header_table = '
		<thead>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">'.$office.'</th>
				<th style="width:15%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control date_range" name="filter_date_range" id="filter_date_range" placeholder="Tanggal" style="width:100%; text-align:center;"></th>
				<th style="width:20%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_invoice" id="filter_invoice" placeholder="No. Invoice" style="width:100%; text-align:center;" onkeyup="convertUpperCase(this);searchData()"></th>
				<th style="width:20%; padding-right:.3rem;" class="center-text"><input type="text" class="form-control" name="filter_customer" id="filter_customer" placeholder="Nama Customer" style="width:100%; text-align:left;" onkeyup="searchData()"></th>
				<th style="width:15%;" class="center-text"></th>
				<th style="width:5%; padding-right:.3rem;" class="center-text"><select class="form-control" id="filter_payment_method" name="filter_payment_method" onchange="searchData()"><option value="">ALL</option><option value="CASH">CASH</option><option value="TRANSFER">TRANSFER</option></select>
				</th>
			</tr>
			<tr>
				<th style="width:5%;" class="center-text"></th>
				<th style="width:20%;" class="center-text">Nama Kantor</th>
				<th style="width:15%;" class="center-text">Tanggal</th>
				<th style="width:20%;" class="center-text">No. Invoice</th>
				<th style="width:20%;" class="center-text">Nama Customer</th>
				<th style="width:15%;" class="center-text">Jumlah</th>
				<th style="width:5%;" class="center-text">Pembayaran</th>
			</tr>
		</thead>
		';

		$body_table = '<tbody>';
		foreach($results as $row){
			$body_table .= '
			<tr>
				<td style="width:5%;" class="center-text">'.encrypt_url($row->id_office).'</td>
				<td style="width:20%;" class="">'.$row->name_office.'</td>
				<td style="width:15%;" class="center-text">'.$row->transacted_date.'</td>
				<td style="width:20%;" class="center-text">'.format_invoice($row->invoice_number).'</td>
				<td style="width:20%;" class="">'.$row->name_customer.'</td>
				<td style="width:15%;" class="right-text">'.number_format($row->amount, 0, ',', '').'</td>
				<td style="width:5%;" class="center-text">'.$row->code.'</td>
			</tr>
			';
		}
		$body_table .= '</tbody>';

		$table = $header_table . $body_table;

		echo $table;
	}

	public function get_offices_options(){
	    $params = array();
	    $list = array();
	    
	    $id_office = $this->session->userdata("id_office");
	    if(isset($id_office) && !empty($id_office)){
	      $params[] = "id = $id_office"; 
	    }else{
	      $id_office = "";      
	    }

	    $offices = $this->office->get_all_items($params);

	    $select_arr = array();

	    $select_arr[] = '<select name="filter_office"  id="filter_office" class="form-control" onchange="searchData()">';
	    if(!empty($id_office)){}
	    else{
	    	$select_arr[] = '<option value="0"> &nbsp;</option>';
	    }
	    foreach($offices as $row){
	    	$select_arr[] = '<option value="'.encrypt_url($row->id).'">'.$row->name.'</option>';	      
	    }
	    $select_arr[] = '</select>';

	    return implode("", $select_arr);
	  }

	public function export(){
		$id_office = $this->input->post("id_office");

		$field_office = $this->input->post("field_office") ? $this->input->post("field_office") : "";
		$field_date_range = $this->input->post("field_date_range") ? $this->input->post("field_date_range") : "";
		$field_invoice = $this->input->post("field_invoice") ? $this->input->post("field_invoice") : "";
		$field_customer = $this->input->post("field_customer") ? $this->input->post("field_customer") : "";
		$field_payment_method = $this->input->post("field_payment_method") ? $this->input->post("field_payment_method") : "";

		$field_month = $this->input->post("field_month") ? $this->input->post("field_month") : "";
		$field_plat = $this->input->post("field_plat") ? $this->input->post("field_plat") : "";

		
		$office = "";
		$report_type = $this->input->post("selected_report");

		if(!empty($id_office)){
			$id_office = decrypt_url($id_office);
			$temp = $this->db->query("SELECT * FROM offices WHERE id = $id_office")->row();
			$office = $temp->name;

		}

		$params = array();
		if(!empty($id_office)){
			$params[] = "id_office = $id_office";
		}

		if($report_type == "invoice_recapitulation"){
			$params = array();
			
			//filter_office
			if(!empty($field_office)){
				$params[] = "id_office = ".decrypt_url($field_office);
			}
			//filter_date
			if(!empty($field_date_range)){
				$parts = explode(" - ", $field_date_range);
				$dt1 = explode("/", $parts[0]);
				$dt2 = explode("/", $parts[1]);
				$start = $dt1[2].$dt1[1].$dt1[0];
				$end   = $dt2[2].$dt2[1].$dt2[0]; 

				$params[] = "(transacted_on_short >= $start AND transacted_on_short <= $end)";
			}
			//filter_invoice
			if(!empty($field_invoice)){
				$params[] = "(formatInvoice(invoice_number) LIKE '%".$field_invoice."%' )";
			}
			//filter_customer
			if(!empty($field_customer)){
				$params[] = "LOWER(name_customer) LIKE '%".strtolower($field_customer)."%'";
			}

			$items = $this->report->get_invoice_recapitulation($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/invoice_recapitulation.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;

				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $item->transacted_on)
				                            ->setCellValue('D'.$row, format_invoice($item->invoice_number))
				                            ->setCellValue('E'.$row, $item->name_customer)
				                            ->setCellValue('F'.$row, number_format($item->due, 0, ',', ''));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleRight);
    		}

    		$filename = "Laporan Rekapitulasi Invoice";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
		else if($report_type == "sales_invoice_recapitulation"){
			$items = $this->report->get_sales_invoice_recapitulation($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/sales_invoice_recapitulation.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;

				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $item->name_sales)
				                            ->setCellValue('D'.$row, $item->transacted_on)
				                            ->setCellValue('E'.$row, $item->invoice_number)
				                            ->setCellValue('F'.$row, $item->name_customer)
				                            ->setCellValue('G'.$row, number_format($item->due, 0, ',', ''));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($styleRight);
    		}

    		$filename = "Rekap Sales Per Invoice";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
		else if($report_type == "monthly_sales_compensation"){
			$settings = $this->db->query("SELECT `id_office`, `value` FROM office_settings WHERE `code` = 'SALES_COMPENSATION_PERCENTAGE'")->result();
			$temp = array();
			foreach($settings as $row){
				$temp[$row->id_office] = $row->value;
			}
			$settings = $temp;

			$items = $this->report->get_monthly_sales_compensation($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/monthly_sales_compensation.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;

    			$temp = explode(" ", $item->period_val_string);
				$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];

				$percentage = (isset($settings[$row->id_office]) && !empty($settings[$row->id_office])) ? $settings[$row->id_office] : 0;
				$compensation = $percentage / 100 * $item->amount;

				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $item->name_sales)
				                            ->setCellValue('D'.$row, $month)
				                            ->setCellValue('E'.$row, $item->amount)
				                            ->setCellValue('F'.$row, $percentage)
				                            ->setCellValue('G'.$row, number_format($compensation, 0, ',', ''));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleRight);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($styleRight);
    		}

    		$filename = "Rekap Komisi Sales Bulanan";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
		else if($report_type == "payment_recapitulation"){
			$params = array();
			
			//filter_office
			if(!empty($field_office)){
				$params[] = "id_office = ".decrypt_url($field_office);
			}
			//filter_date
			if(!empty($field_date_range)){
				$parts = explode(" - ", $field_date_range);
				$dt1 = explode("/", $parts[0]);
				$dt2 = explode("/", $parts[1]);
				$start = $dt1[2].$dt1[1].$dt1[0];
				$end   = $dt2[2].$dt2[1].$dt2[0]; 

				$params[] = "(transacted_on >= $start AND transacted_on <= $end)";
			}
			//filter_invoice
			if(!empty($field_invoice)){
				$params[] = "(formatInvoice(invoice_number) LIKE '%".$field_invoice."%' )";
			}
			//filter_customer
			if(!empty($field_customer)){
				$params[] = "LOWER(name_customer) LIKE '%".strtolower($field_customer)."%'";
			}
			//filter_payment_method
			if(!empty($field_payment_method)){
				$params[] = "code = '".$field_payment_method."'";
			}

			$items = $this->report->get_payment_recapitulation($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/payment_recapitulation.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;
				
				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $item->transacted_date)
				                            ->setCellValue('D'.$row, format_invoice($item->invoice_number))
				                            ->setCellValue('E'.$row, $item->name_customer)
				                            ->setCellValue('F'.$row, number_format($item->amount, 0, ',', ''))
				                        	->setCellValue('G'.$row, $item->code);

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleRight);
				$objPHPExcel->getActiveSheet()->getStyle('G'.$row)->applyFromArray($styleCenter);
    		}

    		$filename = "Rekap Pembayaran Customer";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
		else if($report_type == "material_sales_recapitulation"){
			$params = array();
			
			//filter_office
			if(!empty($field_office)){
				$params[] = "id_office = ".decrypt_url($field_office);
			}
			//filter_month
			if(!empty($field_month)){
				$params[] = "LOWER(transacted_month_sub) LIKE '%".strtolower($field_month)."%'";
			}
			//filter_plat
			if(!empty($field_plat)){
				$params[] = "CONCAT(LOWER(name_plat),' ',size) LIKE '%".strtolower($field_plat)."%'";
			}

			$items = $this->report->get_material_sales_recapitulation($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/material_sales_recapitulation.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;

    			$temp = explode(" ", $item->transacted_month);
				$month = ucwords(to_indonesian_month($temp[0]))." ".$temp[1];
				
				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $month)
				                            ->setCellValue('D'.$row, $item->name_plat." ".$item->size.' mm')
				                            ->setCellValue('E'.$row, $item->qty)
				                            ->setCellValue('F'.$row, number_format($item->amount, 0, ',', ''));

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleRight);
    		}

    		$filename = "Rekap Penjualan Bahan";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
		else if($report_type == "customer_transaction_rank"){
			$params = array();
			
			//filter_office
			if(!empty($field_office)){
				$params[] = "id_office = ".decrypt_url($field_office);
			}
			//filter_month
			if(!empty($field_month)){
				$params[] = "LOWER(period_val_month) LIKE '%".strtolower($field_month)."%'";
			}
			//filter_plat
			if(!empty($field_customer)){
				$params[] = "LOWER(name_customer) LIKE '%".strtolower($field_customer)."%'";
			}

			$items = $this->report->get_customer_transaction_rank($params);

			$this->load->library('PHPExcel', 'excel');
			$objReader = PHPExcel_IOFactory::createReader('Excel5');
    		$objPHPExcel = $objReader->load("uploads/template/customer_transaction_rank.xls");

    		$styleLeft = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleRight = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );
		    $styleCenter = array(
		      'font' => array('bold' => false,),
		      'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,'vertical' => PHPExcel_Style_Alignment::VERTICAL_TOP,),
		      'borders' => array(
		        'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		        'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN),
		      )
		    );

		    $objPHPExcel->getActiveSheet()->setCellValue('B2', $office);
		    
		    $base_row = 5;
    		$row = $base_row;
    		foreach ($items as $key => $item) {
    			$row = $base_row++;
				
				$objPHPExcel->getActiveSheet()->insertNewRowBefore($row,1);
				$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, ($key+1))
				                            ->setCellValue('B'.$row, $item->name_office)
				                            ->setCellValue('C'.$row, $item->period_val_month)
				                            ->setCellValue('D'.$row, $item->name_customer)
				                            ->setCellValue('E'.$row, number_format($item->amount, 0, ',', ''))
				                            ->setCellValue('F'.$row, $item->rank);

				$objPHPExcel->getActiveSheet()->getStyle('A'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('B'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('C'.$row)->applyFromArray($styleCenter);
				$objPHPExcel->getActiveSheet()->getStyle('D'.$row)->applyFromArray($styleLeft);
				$objPHPExcel->getActiveSheet()->getStyle('E'.$row)->applyFromArray($styleRight);
				$objPHPExcel->getActiveSheet()->getStyle('F'.$row)->applyFromArray($styleCenter);
    		}

    		$filename = "Ranking Transaksi Customer";
		    header('Content-Type: application/vnd.ms-excel'); //mime type
		    header('Content-Disposition: attachment;filename="'.$filename.'.xls"'); 
		    header('Cache-Control: max-age=0'); //no cache
		    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		    $objWriter->save('php://output');
		}
	}


}


