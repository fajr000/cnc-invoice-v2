<script>
	var table;
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();


			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$("#report_type").select2({
			theme: 'bootstrap4',
			placeholder: "--Pilih Jenis Laporan--",
		});

		if ($('#head_id_office').length > 0) {
			$.ajax({
				url: "invoices/get_offices_list", 
				type: "post",
				dataType: "json",
				data: {
				},
				success: function(result){

		  			$("#head_id_office").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Kantor Cabang--",
		  			});

		  			var id_office = $("#default_id_office").val();

		  			if(id_office.length > 0){
		  				$('#head_id_office').val(id_office).trigger('change');
		  			}
				}
			});
		}

		$('#head_id_office').on('change', function (e) {
		  	var id_office = $("#head_id_office").find("option:selected").val();

		  	if(id_office == "0"){
		  		$("#id_office").val("");
		  	}else{
		  		$("#id_office").val(id_office);
		  	}  	

		  	if(id_office == "0"){
		  		if($.fn.dataTable.isDataTable("#list_data")){
		  			$('#list_data').DataTable().column(0).search('').draw();
		  		}		  		
		  	}else{		  		
		  		if($.fn.dataTable.isDataTable("#list_data")){
		  			if ( table.column(0).search() !== id_office ) {
						table
						.column(0)
						.search( id_office )
						.draw();
					}
		  		}		  		
			}
		});

		$('#report_type').on('change', function (e) {
			$("#btn-export").hide();

			var id_office = $("#default_id_office").val();
		  	var link = $(this).find("option:selected").val();

		  	if($.fn.dataTable.isDataTable("#list_data")){
		  		$('#list_data').DataTable().clear();
		  		$('#list_data').DataTable().destroy();
		  		$('#list_data').html("");
		  	}

		  	if(link != "0"){
			  	$.ajax({
					url: "reports/"+link, 
					type: "post",
					dataType: "html",
					data: {
						id_office : id_office
					},
					success: function(html){
						$('#list_data').html(html);
						$('.date_range').daterangepicker({
							timePicker: false,
							timePickerIncrement: 30,
							locale: {
								format: 'DD/MM/YYYY'
							},
							startDate: moment().startOf('month'),
        					endDate  : moment().endOf('month')
						});

						if(link == "customer_transaction_rank"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 3, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								],
							});

							$("#btn-export").show();
						}
						else if(link == "sales_invoice_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 5, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});

							$("#btn-export").show();
						}	
						else if(link == "monthly_sales_compensation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 3, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) },
									{ targets: 5, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});

							$("#btn-export").show();
						}	
						else if(link == "material_purchase_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 4, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) },
									{ targets: 6, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});
						}		
						else if(link == "material_sales_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 4, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});

							$("#btn-export").show();
						}			
						else if(link == "machine_usage_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 4, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});
						}			
						else if(link == "invoice_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 5, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});

							$("#btn-export").show();
						}	
						else if(link == "payment_recapitulation"){
							table = $('#list_data').DataTable({
								"pageLength": 50,
								columnDefs : [
									{ targets: 5, render: $.fn.dataTable.render.number( '.', ',', 0, '', '' ) }
								]
							});

							$("#btn-export").show();
						}						

					    table.on( 'order.dt search.dt', function () {
					        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
					            cell.innerHTML = (i+1);  
					        } );
					    } ).draw();

					    $('.date_range').daterangepicker({
							timePicker: false,
							timePickerIncrement: 30,
							locale: {
								format: 'DD/MM/YYYY'
							},
							startDate: moment().startOf('month'),
        					endDate  : moment().endOf('month')
						});
						$(".date_range").on("change.datetimepicker", function (e) {
				            searchData();
				        });

						$('#list_data').show();
					}
				});
			  }

			  $('#head_id_office').val(0).trigger('change');
  	
		});

		$("#btn-export").on("click", function(){
			var report_type = $("#report_type").find(":selected").val();
			$("#selected_report").val(report_type);

			var filter_office = $("#filter_office");
			var filter_date_range = $("#filter_date_range");
			var filter_invoice = $("#filter_invoice");
			var filter_customer = $("#filter_customer");
			var filter_payment_method = $("#filter_payment_method");

			var filter_month = $("#filter_month");
			var filter_plat = $("#filter_plat");

			if(filter_office.length > 0){
				$("#field_office").val($("#filter_office").val());
			}
			if(filter_date_range.length > 0){
				$("#field_date_range").val($("#filter_date_range").val());
			}
			if(filter_invoice.length > 0){
				$("#field_invoice").val($("#filter_invoice").val());
			}
			if(filter_customer.length > 0){
				$("#field_customer").val($("#filter_customer").val());
			}
			if(filter_payment_method.length > 0){
				$("#field_payment_method").val($("#filter_payment_method").find(":selected").val());
			}
			if(filter_month.length > 0){
				$("#field_month").val($("#filter_month").val());
			}
			if(filter_plat.length > 0){
				$("#field_plat").val($("#filter_plat").val());
			}


			$("#fr-export")[0].submit();
		});

		$('#filter_invoice').val (function () {
		    return this.value.toUpperCase();
		})

		$.fn.dataTable.ext.search.push(
		    function( settings, data, dataIndex ) {
		    	var link = $("#report_type").find("option:selected").val();
		    	var elm = $("#filter_date_range");
		    	var date = $("#filter_date_range").val();
		    	var row = "";

		    	if(link == "invoice_recapitulation" || link == "payment_recapitulation"){
		    		row = data[2];
		    	}

		    	if(elm.length == 0 || row.length == 0){
		    		return true;
		    	}
		    	

		    	var min = null;
		        var max = null;		        
		        var cur = null;	

		    	if(date.length > 0){
					var temp = date.split(" - ");
					var temp1= temp[0].split("/");
					var temp2= temp[1].split("/");

					min = new Date(temp1[2],((temp1[1]*1) - 1),temp1[0]);
					max   = new Date(temp2[2],((temp2[1]*1) - 1),temp2[0]);
				}		  

				if(row.length > 0){
					var temp= row.split("/");
					cur = new Date(temp[2],((temp[1]*1) - 1),temp[0]);
				}
	
		        if (
		            ( min === null && max === null ) ||
		            ( min === null && cur <= max ) ||
		            ( min <= cur   && max === null ) ||
		            ( min <= cur   && cur <= max )
		        ) {
		            return true;
		        }
		        return false;
		    }
		);

			
	});

function convertUpperCase(elm){
	var value = $(elm).val();
	$(elm).val(value.toUpperCase());
}

function searchData(){
	var link = $("#report_type").find("option:selected").val();

	console.log(link);
	if(link == "customer_transaction_rank"){
		var id_office = $("#filter_office").find(":selected").val();
		var month = $("#filter_month").val();
		var customer_name = $("#filter_customer").val();


		//filter office
	  	if(id_office == "0"){
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			$('#list_data').DataTable().column(0).search('').draw();
	  		}		  		
	  	}else{		  		
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}
	  		}		  		
		}

		//filter month
		if(month.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(2).search() !== month ) {
					table
					.column(2)
					.search( month )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(2).search('').draw();
	  		}	
  		}

  		//filter plat
		if(customer_name.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(3).search() !== customer_name ) {
					table
					.column(3)
					.search( customer_name )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(3).search('').draw();
	  		}	
  		}
	}
	else if(link == "material_sales_recapitulation"){
		var id_office = $("#filter_office").find(":selected").val();
		var month = $("#filter_month").val();
		var plat = $("#filter_plat").val();


		//filter office
	  	if(id_office == "0"){
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			$('#list_data').DataTable().column(0).search('').draw();
	  		}		  		
	  	}else{		  		
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}
	  		}		  		
		}

		//filter month
		if(month.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(2).search() !== month ) {
					table
					.column(2)
					.search( month )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(2).search('').draw();
	  		}	
  		}

  		//filter plat
		if(plat.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(3).search() !== plat ) {
					table
					.column(3)
					.search( plat )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(3).search('').draw();
	  		}	
  		}
	}
	else if(link == "invoice_recapitulation"){
		var id_office = $("#filter_office").find(":selected").val();
		var date = $("#filter_date_range").val();
		var invoice_number = $("#filter_invoice").val();
		var customer_name = $("#filter_customer").val();

		if(date.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.draw();
	  		}
		}

		//filter office
	  	if(id_office == "0"){
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			$('#list_data').DataTable().column(0).search('').draw();
	  		}		  		
	  	}else{		  		
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}
	  		}		  		
		}

		//filter invoice
		if(invoice_number.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(3).search() !== invoice_number ) {
					table
					.column(3)
					.search( invoice_number )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(3).search('').draw();
	  		}	
  		}

  		//filter customer
		if(customer_name.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(4).search() !== customer_name ) {
					table
					.column(4)
					.search( customer_name )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(4).search('').draw();
	  		}	
  		}
	}
	else if(link == "payment_recapitulation"){
		var id_office = $("#filter_office").find(":selected").val();
		var date = $("#filter_date_range").val();
		var invoice_number = $("#filter_invoice").val();
		var customer_name = $("#filter_customer").val();
		var payment_method = $("#filter_payment_method").find(":selected").val();

		if(date.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.draw();
	  		}
		}

		//filter office
	  	if(id_office == "0"){
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			$('#list_data').DataTable().column(0).search('').draw();
	  		}		  		
	  	}else{		  		
	  		if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(0).search() !== id_office ) {
					table
					.column(0)
					.search( id_office )
					.draw();
				}
	  		}		  		
		}

		//filter invoice
		if(invoice_number.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(3).search() !== invoice_number ) {
					table
					.column(3)
					.search( invoice_number )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(3).search('').draw();
	  		}	
  		}

  		//filter customer
		if(customer_name.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(4).search() !== customer_name ) {
					table
					.column(4)
					.search( customer_name )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(4).search('').draw();
	  		}	
  		}

  		//filter payment_method
		if(payment_method.length > 0){
			if($.fn.dataTable.isDataTable("#list_data")){
	  			if ( table.column(6).search() !== payment_method ) {
					table
					.column(6)
					.search( payment_method )
					.draw();
				}
	  		}	
		}
		else{
  			if($.fn.dataTable.isDataTable("#list_data")){
	  			table.column(6).search('').draw();
	  		}	
  		}
	}
}

	
</script> 