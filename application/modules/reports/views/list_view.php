<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    vertical-align: middle;
  }
  
  .center-text{
    vertical-align: middle;
    text-align: center; 
  } 
  .right-text{
    vertical-align: middle;
    text-align: right;
  } 
  .left-text{
    vertical-align: middle;
    text-align: left; 
  } 
  .dataTables_filter {
    display: none;
  }
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">

              <div class="row">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12 pull-left">
                      <div class="row">
                        <div class="col-md-8">
                          <div style="margin-bottom:5px;">
                            <select placeholder="Jenis Laporan" class="form-control" name="report_type" id="report_type" style="width: 50%;">
                              <?php
                              echo '<option value="0">--Pilih Jenis Laporan--</option>';
                              foreach($report_options as $opt){
                                echo '<option value="'.$opt->name_func.'">'.$opt->name.'</option>';
                              }
                              ?>
                            </select>
                          </div>    
                        </div>
                        <div class="col-md-4">
                          <button style="display: none;" id="btn-export" type="button" class="btn btn-success float-right"><i class="fa fa-file-excel"></i> Export</button>
                        </div>
                      </div>
                    </div><!-- /.row -->
                  </div>
                </div>
              </div>

              <div class="row" style="display: <?php echo ($this->session->userdata("id_office") && !empty($this->session->userdata("id_office"))) ? "none" : "block";?>;">
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-12 pull-left">
                      <input type="hidden" id="default_id_office" value="<?php echo $this->session->userdata("id_office");?>">
                      <div class="row">
                        <div class="col-md-8">
                          <div style="margin-bottom:5px;">
                            <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                            </select>
                          </div>    
                        </div>
                        <div class="col-md-4">
                          
                        </div>
                      </div>
                    </div><!-- /.row -->
                  </div>
                </div>
                
              </div>
              <div class="row">
                <form id="fr-export" target="_self" method="post" action="reports/export">
                  <input type="hidden" name="id_office" id="id_office" value="">
                  <input type="hidden" name="selected_report" id="selected_report">

                  <input type="hidden" name="field_office" id="field_office">
                  <input type="hidden" name="field_date_range" id="field_date_range">
                  <input type="hidden" name="field_invoice" id="field_invoice">
                  <input type="hidden" name="field_customer" id="field_customer">
                  <input type="hidden" name="field_payment_method" id="field_payment_method">

                  <input type="hidden" name="field_month" id="field_month">
                  <input type="hidden" name="field_plat" id="field_plat">
                </form>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="table-responsive">
                    
                    <table id="list_data" style="width: 100%; display: none;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                      
                    </table>
                  </div>
                </div>
              </div>
              
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->
