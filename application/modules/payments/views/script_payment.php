<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();


			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form_payment').submit();
		});

		$('.money').mask('000.000.000.000.000', {reverse: true});

		if ($('#head_id_office').length > 0) {
			$.ajax({
				url: "payments/get_offices_list", 
				type: "post",
				dataType: "json",
				data: {
				},
				success: function(result){

		  			$("#head_id_office").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Kantor Cabang--",
		  			});

		  			var id_office = $("#default_id_office").val();

		  			if(id_office.length > 0){
		  				$('#head_id_office').val(id_office).trigger('change');
		  			}
				}
			});
		}

		$('#head_id_office').on('change', function (e) {
		  	var id_office = $("#head_id_office").find("option:selected").val();

		  	$("#id_office").val(id_office);
		  
			$.ajax({
				url: "payments/get_customers_list", 
				type: "post",
				dataType: "json",
				data: {
					id_office : id_office
				},
				success: function(result){
					$("#head_id_customer").select2("destroy");
					$("#head_id_customer").html("");
		  			$("#head_id_customer").select2({
		  				data : result.data,
		  				theme: 'bootstrap4',
		  				placeholder: "--Pilih Customer--",
		  			});
				}
			});		  	
		});

		$('#head_id_customer').select2({
			theme: 'bootstrap4',
		  	placeholder: "--Pilih Customer--",
		});

		$('#head_id_customer').on('select2:select', function (e) {
		  	var data = e.params.data;

		  	$("#id_customer").val(data.id);

		  	//get invoices
		  	$.ajax({
				url: "payments/get_invoice_due", 
				type: "post",
				dataType: "html",
				data: {
					id_office : $("#id_office").val(),
					id_customer : $("#id_customer").val(),
				},
				success: function(html){
					$(".row_invoice").remove();
		  			$(html).prependTo("#list_invoice > tbody");

		  			$('.money').mask('000.000.000.000.000', {reverse: true});
				}
			});
		});

		$("#list_invoice").on("keyup", "input.amount_paid", function(e){
			var max_amount = $(this).data("max_amount");
			var val_str = $(this).val();
			var val = val_str.replace(/\./g,'');

			if(parseFloat(val) > parseFloat(max_amount)){
				e.preventDefault();
				
				$(this).val(numberToCurrency(val.substring(0, val.length-1)));
			}
			else{
				var chk = $(this).closest("tr").find("td").eq(4).find("input");
				if(val > 0){
					$(chk).prop("checked", true);
					$(chk).val(val);
				}else{
					$(chk).prop("checked", false);
					$(chk).val("");
				}
			}

			recalculate();
		});

		$("#payment_method").on("change", function(){
			recalculate();
		});

		$(".custom-file-input").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});

		$("#submitpayment").on("click", function(){
			$(this).prop("disabled", true);
			var form = $('#form_payment')[0].submit();
		});
		
	});

	function recalculate() {
		var subtotal = 0;
		$('input.paid_invoices').each(function(index, elem) { 
			var amount = (($(this).val()).length > 0) ? $(this).val() : 0;
			subtotal += parseFloat(amount);
		});

		if(subtotal > 0) {
			$("#total_paid").val(subtotal);
			$("#total_paid_str").val(numberToCurrency(subtotal));
			
			var payment_method = $("#payment_method").find("option:selected").val();
			if(payment_method > 0){
				$("#submitpayment").prop("disabled", false);
			}
			else{
				$("#submitpayment").prop("disabled", true);
			}
		}
		else{
			$("#total_paid").val("");
			$("#total_paid_str").val("");
			$("#submitpayment").prop("disabled", true);
		}
	}

	function addFile(){
		var html = ''+
		'<div class="input-group row_file" style="margin: 5px 0;">'+
        '	<div class="custom-file">'+
        '   	<input type="file" class="custom-file-input form-control-sm" name="files[]" onchange="showFileName(this)">'+
        '       <label class="custom-file-label col-form-label-sm" for="custmFile">Choose file</label>'+
        '   </div>'+
        '   <div class="input-group-append">'+
        '   	<button class="btn btn-warning btn-sm" type="button" onclick="removeFile(this)"><i class="fas fa-times-circle"></i> <span>Cancel</span></button>'+
        '  </div>'+
        '</div>';

        $(".file_container").append(html);
	}

	function removeFile(elm){
		var row = $(elm).closest("div.row_file");
		$(row).remove();
	}

	function showFileName(elm){
		var fileName = $(elm).val().split("\\").pop();
		$(elm).siblings(".custom-file-label").addClass("selected").html(fileName);
	}

	function chooseInvoice(elm){
		var amount_paid = $(elm).closest("tr").find("td").eq(3).find("input");
		var maxim_paid = $(elm).closest("tr").find("td").eq(3).find("input").data("max_amount");
		if($(elm).is(":checked")){
			$(amount_paid).val(numberToCurrency(maxim_paid));
			$(elm).val(maxim_paid);
		}else{
			$(amount_paid).val("");
			$(elm).val("");
		}

		recalculate();
	}

	function changePaidAmount(elm){
		var max_amount = $(elm).data("max_amount");
		var val_str = $(elm).val();
		var val = val_str.replace(/\./g,'');

		if(parseFloat(val) > parseFloat(max_amount)){
			$(elm).val(numberToCurrency(val.substring(0, val.length-1)));
		}
		else{
			var chk = $(elm).closest("tr").find("td").eq(4).find("input");
			if(val > 0){
				$(chk).prop("checked", true);
				$(chk).val(val);
			}else{
				$(chk).prop("checked", false);
				$(chk).val("");
			}
		}

		recalculate();
	}
	
</script> 