<style type="text/css">
.size, .qty_col, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.middle-text{
  vertical-align: middle !important;
}
.right-text{
  text-align: right !important;
}
.center-text{
  text-align: center !important;
}
</style>
<div class="content-header">
  
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user">
                <table>
                  <tr><td><strong>Tgl. Transaksi</strong></td><td>:</td><td><?php echo $payments["transacted_on"];?></td></tr>
                  <tr><td><strong>Nama Customer</strong></td><td>:</td><td><?php echo $customer->name;?></td></tr>
                  <tr><td><strong>Nama Petugas</strong></td><td>:</td><td><?php echo $payments["name_operator"] ;?></td></tr>
                </table>
                <form autocomplete="off" name="frmFile" id="frmFile" method="post" target="_self" action="transactions/upload_invoice_item_attachment" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo $payments["id_office"]; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo $payments["id_customer"]; ?>">
                  <input type="hidden" name="id_payment" id="id_payment" value="<?php echo $payments["id_payment"]; ?>">
                  <table id="list_buy" class="table table-condensed table-bordered" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 5%; text-align: center;"></th>
                            <th style="width: 65%; text-align: left;">No. Invoice</th>
                            <th style="width: 15%; text-align: right;">Tagihan (Rp)</th>
                            <th style="width: 15%; text-align: center;">Bayar (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php
                      $i = 1;
                      $dp = 0;
                      foreach ($payments["items"] as $key => $row) {
                        echo '
                        <tr>
                        <td class="center-text">'.$i.'</td>
                        <td>'.$row["invoice_number"].'</td>
                        <td class="right-text">'.format_money($row["due"]).'</td>
                        <td class="right-text">'.format_money($row["pay_amount"]).'</td>
                        </tr>
                        ';

                        $files = array();
                        foreach ($row["files"] as $file) {
                          
                          $view = ! exif_imagetype($file->location.'/'.$file->encrypted_name) ? "" : '
                          <div class="col-sm-1">
                            <a href="'.$file->location.'/'.$file->encrypted_name.'" data-toggle="lightbox" data-title="'.$file->original_name.'" data-gallery="gallery">
                              <i class="fas fa-eye"></i>
                            </a>
                          </div>';
                          $download = '<a target="_self" href="transactions/get_file_payment/'.$file->id.'" alt="Download" title="Download" class="btn-sm btn-success"><i class="fas fa-download"></i></a>';
                          $delete = '<a  target="_self" alt="Hapus" title="Hapus" onclick="remove(this,'.$file->id.')" class="btn-sm btn-danger btn-flat"><i class="fa fa-trash"></i></a>';

                          $files[] = '
                          <tr>
                          <td style="width:85%;">'.$file->original_name.'</td>
                          <td style="width:5%; text-align:center;">'.$view.'</td>
                          <td style="width:5%; text-align:center;">'.$download.'</td>
                          <td style="width:5%; text-align:center;">'.$delete.'</td>
                          </tr>
                          ';
                        }
                        $files_html = count($files) > 0 ? "<table style='margin-top:5px; width: 100%;' class='table table-condensed table-sm'>".implode("", $files)."</table>" : "";

                        if(!empty($files_html)){
                          echo '
                          <tr>
                          <td></td>
                          <td colspan="3">
                            '.$files_html.'
                            <div class="file_container_'.$key.'">
                            </div>
                          </td>
                          </tr>
                          ';
                        }

                        $i++;
                        $dp = $payments["is_down_payment"];
                      }

                      $dp = ($dp > 0) ? "(DP)" : "";

                      echo '
                      <td colspan="3" class="right-text"><strong>Total Pembayaran</strong> '.$dp.'</td>
                      <td class="right-text"><strong>'.format_money($payments["total_pay_amount"]).'</strong></td>
                      </tr>
                      <tr>
                      ';

                      ?>
                    </tbody>
                  </table>
                
              </div>
            </div>

            <div class="row">
              <div class="col-md-12">
                <br>
                <center>
                  <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-default btn-sm" id="gotocatalog" onclick="window.close();">Close</button>
                    </div>
                    <div class="col-md-5">
                    </div>                
                  </div>
                </center>
              </div>

              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

</script>