<style type="text/css">
.size, .qty, .discount_col, .machine{
  text-align: center;
}
.price{
  text-align: right;
}
.dataTables_filter{
  display: none;
}
.text-warning{
  color: #bf2300 !important;
  font-weight: bold;
}
.select2-container--bootstrap4.select2-container--focus .select2-selection {
    /* border-color: #80bdff; */
    /* -webkit-box-shadow: 0 0 0 0.2rem rgb(0 123 255 / 25%); */
    box-shadow: 0 0 0 0rem rgb(0 123 255 / 25%) !important;
}

</style>
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-12">
        <table style="width: 100%;">
          <tr>
            <td style="width: 50%;"><?php echo $today; ?>
            <input type="hidden" id="default_id_office" value="<?php echo $this->session->userdata("id_office");?>">
              <br>
              <?php
              if(isset($customer) && !empty($customer->id_office)){
                echo '<strong>'.$customer->name_office.'</strong><br>
                <strong>'.$customer->name.'</strong>
                ';
              }else{
              ?>
                <div style="margin-bottom:5px;">
                  <select placeholder="Kantor Cabang" class="form-control" name="head_id_office" id="head_id_office" style="width: 50%;">
                  </select>
                </div>
                <div style="margin-bottom:5px;">  
                  <select placeholder="Nama Customer" class="form-control" name="head_id_customer" id="head_id_customer" style="width: 50%;">
                  </select>
                </div>
              <?php 
              }
              ?>
            </td>
            <td style="width: 50%; text-align: right; vertical-align: top;">
              <div class="row" >
                <div class="col-md-6"></div>
                <div class="col-md-6">
                  <nav class="main-header navbar navbar-expand navbar-white navbar-light" style="background-color: transparent  !important; border-bottom: 0px;">
                    <!-- Right navbar links -->
                    <ul class="navbar-nav ml-auto">
                      <!-- Messages Dropdown Menu -->
                      <li class="nav-item dropdown">
                        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">Keranjang: 
                          <i class="fa fa-cart-arrow-down"></i>
                          <?php
                          if(!empty($this->cart->contents())){
                            echo '
                            <span class="badge badge-danger navbar-badge" style="font-size: .7rem;; font-weight: 400;">'.$this->cart->total_items().'
                            </span>
                            ';
                          }
                          ?>                          
                        </a>
                        <?php
                        if(!empty($this->cart->contents())){
                        ?>
                        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="left: inherit; right: 0px; max-width: 500px; min-width: 400px;">
                          <span class="dropdown-item dropdown-header"><?php echo $this->cart->total_items();?> Item(s)</span>
                          <div class="dropdown-divider"></div>
                          <?php
                          foreach($this->cart->contents() as $row){
                            $name = substr($row['name']."", 0, 35);
                            $qty = $row["qty"];
                            if(strlen($row['name']."") > 35)
                              $name .= "...";


                            echo '
                            <a class="dropdown-item">
                              <strong>'.$name.'</strong><br><small>'.$qty.' Barang</small>
                              <span class="float-right text-warning text-md">Rp'.number_format($row['price'],0,'','.').'</span>
                            </a>
                            <div class="dropdown-divider"></div>
                            ';
                          }
                          ?>
                          <div class="dropdown-divider"></div>
                          <a href="transactions/show_cart" target="_self" class="dropdown-item dropdown-footer"><strong>Lihat Sekarang</strong></a>
                        </div>
                        <?php
                        }
                        ?>
                      </li>
                    </ul>
                    <ul class="navbar-nav">
                      <li class="nav-item d-none d-sm-inline-block">
                        <a href="#" class="nav-link">Rp. 
                          <?php
                          if(!empty($this->cart->contents())) {
                            echo "<strong>".number_format($this->cart->total(), 0, ',', '.')."</strong>";
                          }else
                          {
                            echo "0";
                          }
                          ?>
                        </a>
                      </li>
                    </ul>
                  </nav>                
                </div>
              </div>
            </td>
          </tr>
        </table>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<div class="content">
  <div class="container">

    <div class="row">
      <div class="col-md-12">
        <div class="card card-default card-outline">
          <div class="card-body">
            <div class="row">
              <div class="col-md-12" id="form_user">

                <form class="form-horizontal">
                  <div class="form-group row">
                    <div class="col-sm-12">Search by :</div>
                    
                    <div class="col-sm-3">
                      <select name="product_type" id="product_type" class="form-control">
                        <option value="">Jenis Produk</option>
                        <option value="1">Plat</option>
                        <option value="2">Jasa Cutting</option>
                      </select>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" id="name_plat" placeholder="Jenis Plat">
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" id="size" placeholder="Ukuran">
                    </div>
                    <div class="col-sm-3">
                      <input type="text" class="form-control" id="machine" placeholder="Mesin">
                    </div>
                  </div>
              </form>
                <center>
                  <div class="row">
                    <div class="col-md-8"></div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-warning btn-xs" onclick="addToCart();"><i class="fas fa-shopping-cart"></i> Add to Cart</button>
                    </div>
                    <div class="col-md-2">
                      <button type="button" class="btn btn-block btn-primary btn-xs" onclick="checkout();">Add &amp; Checkout <i class="fa fa-step-forward"></i></button>
                    </div>                  
                  </div>
                </center>
                <form autocomplete="off" name="form" id="form" method="post" action="transactions/add_items" target="_self" enctype="multipart/form-data">
                  <input type="hidden" name="id_office" id="id_office" value="<?php echo (isset($customer)) ? $customer->id_office : ""; ?>">
                  <input type="hidden" name="id_customer" id="id_customer" value="<?php echo (isset($customer)) ? $customer->id : ""; ?>">
                  <table id="list_product" class="table" style="width:100%">
                    <thead>
                        <tr>
                            <th style="width: 5%;"></th>
                            <th style="width: 15%;">Plat</th>
                            <th style="width: 15%;">Ukuran</th>
                            <th style="width: 5%;">Qty</th>
                            <th style="width: 10%;">Mesin</th>
                            <th style="width: 20%;">Harga</th>
                            <th style="width: 20%;">Disc.</th>
                            <th style="width: 10%; text-align: center;"></th>
                        </tr>
                    </thead>
                  </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

</script>