<script>
	$(function () {
		$('.money').mask('000.000.000.000.000', {reverse: true});


		$(".custom-file-input").on("change", function() {
		  var fileName = $(this).val().split("\\").pop();
		  $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
		});

		$("#savechange").on("click", function(){
			var bar = $('#bar');
  			var percent = $('#percent');

  			var form = $('#frmFile')[0];
			var data = new FormData(form);	

			$('.msg').hide();
			$('.progress').show();

			$.ajax({
				xhr : function() {
					var xhr = new window.XMLHttpRequest();
					xhr.upload.addEventListener('progress', function(e){
						if(e.lengthComputable){
							console.log('Bytes Loaded : ' + e.loaded);
							console.log('Total Size : ' + e.total);
							console.log('Persen : ' + (e.loaded / e.total));
							
							var percent = Math.round((e.loaded / e.total) * 100);
							
							$('#progressBar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%');
						}
					});
					return xhr;
				},
				
				type : 'POST',
				url : "transactions/upload_invoice_item_attachment",
				data : data,
				processData : false,
				contentType : false,
				success : function(response){
					$('form')[0].reset();
					$('.progress').hide();
					$('.msg').show();
					if(response == ""){
						$('.msg').html('File gagal di upload!');
					}else{
						var msg = response;
						$('.msg').html(msg+". <span style='color:yellow;'>Halaman akan direfresh dalam 5 detik.</span>");

						setTimeout(function () {
							history.go(0);
						}, 5000);
					}
				}
			});
		});

		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
      event.preventDefault();
      $(this).ekkoLightbox({
        alwaysShowClose: true
      });
    });
		
	});

	function addFile(id_invoice_item){
		var html = ''+
		'<div class="input-group row_file" style="margin: 5px 0;">'+
        '	<div class="custom-file">'+
        '   	<input type="file" class="custom-file-input form-control-sm" name="files[]['+id_invoice_item+']" onchange="showFileName(this)">'+
        '       <label class="custom-file-label col-form-label-sm" for="custmFile">Choose file</label>'+
        '   </div>'+
        '   <div class="input-group-append">'+
        '   	<button class="btn btn-warning btn-sm" type="button" onclick="removeFile(this)"><i class="fas fa-times-circle"></i> <span>Cancel</span></button>'+
        '  </div>'+
        '</div>';

        $(".file_container_"+id_invoice_item).append(html);
	}

	function removeFile(elm){
		var row = $(elm).closest("div.row_file");
		$(row).remove();
	}

	function showFileName(elm){
		var fileName = $(elm).val().split("\\").pop();
		$(elm).siblings(".custom-file-label").addClass("selected").html(fileName);
	}

	function remove(elm, id_file) {
		$.ajax({
			url: "invoices/remove_file", 
			type: "post",
			dataType: "json",
			data: {
				id_file : id_file
			},
			success: function(data){
				if(data.result == 1)	{
					$(elm).closest("tr").remove();
				}
			}
		});
	}
	
</script> 