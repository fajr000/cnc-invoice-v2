<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_model extends CI_Model
{
	private $_table = 'accounting_invoices';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id)) ? "AND t.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				ai.`id` as id,
				ai.`id_transaction`,
				ai.`invoice_number`, 
				ai.`amount`, 
				ai.`discount`,
				ai.`discount_amount`,
				t.`id_office`,
				t.`id_customer`
			FROM `accounting_invoices` ai 
			LEFT JOIN `transactions` t ON t.`id` = ai.`id_transaction`
			LEFT JOIN `offices` o ON o.`id` = t.`id_office`
			LEFT JOIN `customers` c ON c.`id` = t.`id_customer`
			WHERE t.`cancelled` IS NULL $filter_office
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$office_id = $this->session->userdata("id_office");
		$filter_office = (isset($office_id) && !empty($office_id)) ? "AND t.id_office = " . $office_id : "";	

		$sql = "
		SELECT *
		FROM (
			SELECT 
				ai.`id` as id,
				ai.`id_transaction`,
				ai.`invoice_number`, 
				ai.`amount`, 
				ai.`discount`,
				ai.`discount_amount`,
				t.`id_office`,
				t.`id_customer`
			FROM `accounting_invoices` ai 
			LEFT JOIN `transactions` t ON t.`id` = ai.`id_transaction`
			LEFT JOIN `offices` o ON o.`id` = t.`id_office`
			LEFT JOIN `customers` c ON c.`id` = t.`id_customer`
			WHERE t.`cancelled` IS NULL $filter_office
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				ai.`id` as id,
				ai.`id_transaction`,
				ai.`invoice_number`, 
				ai.`amount`, 
				ai.`discount`,
				ai.`discount_amount`,
				t.`id_office`,
				t.`id_customer`
			FROM `accounting_invoices` ai 
			LEFT JOIN `transactions` t ON t.`id` = ai.`id_transaction`
			LEFT JOIN `offices` o ON o.`id` = t.`id_office`
			LEFT JOIN `customers` c ON c.`id` = t.`id_customer`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->delete($this->_table);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}
}
