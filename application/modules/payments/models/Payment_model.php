<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model
{
	private $_table = 'transactions';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		$office_id = $this->session->userdata("id_office");
		if(!is_admin() && !is_center_admin() && isset($office_id) && !empty($office_id)) {
			$params[] = "id_office = " . $office_id ;
		} 

		if(count($params) > 0) 
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;	

		$sql = "
		SELECT *
		FROM (
			SELECT 
			  ap.`id_transaction`, 
			  tr.`transacted_on`, 
			  tr.`id_office`, 
			  tr.`id_customer`, 
			  api.`id_payment`, 
			  GROUP_CONCAT(api.`id_invoice`) id_invoice, 
			  SUM(api.`amount`) amount, 
			  pm.`code`, 
			  off.`name` name_office, 
			  cr.`name` name_customer, 
			  GROUP_CONCAT(ai.`invoice_number`) invoice_number 
			FROM 
			  accounting_payments ap 
			  LEFT JOIN transactions tr ON tr.`id` = ap.`id_transaction` 
			  LEFT JOIN payment_methods pm ON pm.`id` = ap.`payment_method_id`
			  LEFT JOIN accounting_payment_invoices api ON api.`id_payment` = ap.`id` 
			  LEFT JOIN offices off ON off.`id` = tr.`id_office` 
			  LEFT JOIN customers cr ON cr.`id` = tr.`id_customer` 
			  LEFT JOIN accounting_invoices ai ON ai.`id` = api.`id_invoice` 
			WHERE 
			  tr.`cancelled_on` = '0000-00-00 00:00:00' 
			GROUP BY 
			  ap.`id_transaction`, 
			  tr.`transacted_on`, 
			  tr.`id_office`, 
			  tr.`id_customer`, 
			  api.`id_payment`
			ORDER BY tr.`id`			
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				t.`id`,
				t.`id_office`,
				t.`id_customer`,
				t.`transaction_type`,
				t.`transacted_on`,
				t.`handle_by_id`,
				t.`cancelled_on`,
				t.`posted`,
				t.`notes`
			FROM `transactions` t
			LEFT JOIN `offices` o ON o.`id` = t.`id_office`
			LEFT JOIN `customers` c ON c.`id` = t.`id_customer`
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, array("posted" => 0));

		if($this->db->affected_rows() > 0 || $this->db->trans_status() !== FALSE)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function update_stock($id_office, $id_plat, $size, $qty, $type = "IN")
	{
		$this->db->trans_start();

		$stock = ($type == "IN") ? $qty : ($qty*-1);

		$sql = "UDPATE ".$this->_table." SET qty += (".$stock.") , updated_on = '".get_current_datetime()."', updated_by = ".$this->session->userdata('user_id')." WHERE id_office = $id_office AND id_plat = $id_price_plat AND size = $size";

		$this->db->query($sql);
		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function generate_invoice($data) {
		$this->db->trans_begin();

		$this->db->insert("transactions", array(
			"id_office" => $data["id_office"],
			"id_customer" => $data["id_customer"],
			"transaction_type" => $data["transaction_type"],
			"handle_by_id" => $data["handle_by_id"],
		));

		if($this->db->affected_rows() > 0) {
			$id_transaction = $this->db->insert_id();

			$this->generate_invoice_number($data["id_office"], $id_transaction);

			$invoice = $this->db->query("SELECT * FROM accounting_invoices WHERE id_transaction = $id_transaction")->row();

			$this->db->where("id", $invoice->id);
			$this->db->update("accounting_invoices", array(
				"amount" => $data["invoice"]["amount"],
				"discount" => $data["invoice"]["discount"],
				"discount_amount" => $data["invoice"]["discount_amount"],
			));

			if($this->db->affected_rows() > 0 || $this->db->trans_status() !== FALSE) {
				$id_invoice = $invoice->id;

				foreach ($data["invoice"]["items"] as $row) {
					
					$this->db->insert("accounting_invoice_items", array(
						"id_invoice" => $id_invoice,
						"id_plat" => $row["id_plat"],
						"id_service" => $row["id_service"],
						"price" => $row["price"],
						"qty" => $row["qty"],
						"minutes" => $row["minutes"],
						"amount" => $row["amount"],           
						"discount" => $row["discount"],
						"discount_amount" => $row["discount_amount"],
					));

					if($this->db->affected_rows() > 0) {
						//subtract existed qty
						$sql = "UPDATE plat_prices SET qty = qty - ".$row["qty"].", updated_on = '".get_current_datetime()."', updated_by = ".$this->session->userdata('user_id')." WHERE id_office = ".$data["id_office"]." AND id = ".$row["id_plat"];

						$this->db->query($sql);
					}
					else{
						$this->db->trans_rollback();
						return array();
					}
				}
				
				$this->db->trans_commit();
				return array(
					"id_invoice" => $id_invoice,
					"paid" => 0
				);
			}
		}
		else {
			$this->db->trans_rollback();
			return array();
		}
	}

	public function generate_payment($data) {
		$this->db->trans_begin();

		$this->db->insert("transactions", array(
			"id_office" => $data["id_office"],
			"id_customer" => $data["id_customer"],
			"transaction_type" => $data["transaction_type"],
			"handle_by_id" => $data["handle_by_id"],
		));

		if($this->db->affected_rows() > 0) {
			$id_transaction = $this->db->insert_id();

			$this->db->insert("accounting_payments", array(
				"id_transaction" => $id_transaction,
				"payment_method_id" => $data["payment"]["payment_method_id"],
			));

			if($this->db->affected_rows() > 0) {
				$id_payment = $this->db->insert_id();

				$invoices = $data["payment"]["invoices"];
				foreach($invoices as $invoice){
					$this->db->insert("accounting_payment_invoices", array(
						"id_payment" => $id_payment,
						"id_invoice" => $invoice["id_invoice"],
						"amount" => $invoice["paid"],
						"is_down_payment" => $invoice["is_down_payment"],
					));

					if($this->db->affected_rows() > 0) {}
					else{
						$this->db->trans_rollback();
						return false;
					}					
				}

				$this->db->trans_commit();
				return $id_payment;
			}
			else{
				$this->db->trans_rollback();
				return false;
			}
		}
		else{
			$this->db->trans_rollback();
			return false;
		}
	}

	public function cancel_invoices($data){
		$this->db->trans_begin();

		$this->db->insert("transactions", array(
			"id_office" => $data["id_office"],
			"id_customer" => $data["id_customer"],
			"transaction_type" => $data["transaction_type"],
			"handle_by_id" => $data["handle_by_id"],
		));

		if($this->db->affected_rows() > 0) {
			$id_transaction = $this->db->insert_id();

			$invoices = $data["invoices"];
			$success = true;
			foreach ($invoices as $id_invoice) {
				$this->db->insert("accounting_cancels", array(
					"id_transaction" => $id_transaction,
					"id_invoice" => $id_invoice,
				));

				if($this->db->affected_rows() > 0) {
					$this->db->flush_cache();
					$this->db->where("id_invoice", $id_invoice);
					$this->db->update("accounting_invoice_items", array(
						"fully_refunded" => 1,
						"cancelled_on" => get_current_datetime(),
						"status" => 3, //cancelled
					));

					if($this->db->affected_rows() > 0 || $this->db->trans_status() !== FALSE) {
						$trans = $this->db->get_where('accounting_invoices', array('id' => $id_invoice))->row();

						$this->db->flush_cache();
						$this->db->where("id", $trans->id_transaction);
						$this->db->update("transactions", array(
							"cancelled_on" => get_current_datetime()
						));

						if($this->db->affected_rows() > 0 || $this->db->trans_status() !== FALSE) {
							//also cancel the payment
							$this->db->flush_cache();
							$this->db->where("id_invoice", $id_invoice);
							$this->db->update("accounting_payment_invoices", array(
								"fully_refunded" => 1,
								"cancelled_on" => get_current_datetime(),
							));

							if($this->db->affected_rows() > 0 || $this->db->trans_status() !== FALSE) {
						      //return the qty
						      $items = $this->db->query("
						        SELECT vi.`id_office`, vi.`id_customer`, vi.`id_invoice`, aii.`id_plat`, aii.`qty` 
						        FROM v_invoices vi
						        LEFT JOIN accounting_invoice_items aii ON aii.`id` = vi.`id_invoice_item`
						        WHERE vi.`id_office` = ".$data["id_office"]."
						        AND vi.`id_customer` = ".$data["id_customer"]." 
						        AND vi.`id_invoice` = ".$id_invoice."
						        AND vi.`product` = 'Plat' 
						        ")->result();

						      if(count($items) > 0){
						        foreach($items as $row){
									//subtract esisted qty
									$sql = "UPDATE plat_prices SET qty = qty + ".$row->qty.", updated_on = '".get_current_datetime()."', updated_by = ".$this->session->userdata('user_id')." WHERE id_office = ".$data["id_office"]." AND id = ".$row->id_plat;

									$this->db->query($sql);
						        }
						      }
							}
							else{
								$this->db->trans_rollback();
								$success = false;
							}
						}
						else{
							$this->db->trans_rollback();
							$success = false;
						}

					}
					else{
						$this->db->trans_rollback();
						$success = false;
					}
				}
				else{
					$this->db->trans_rollback();
					$success = false;
				}	
			}

			if($success){
				$this->db->trans_commit();
				return true;
			}
			else{
				$this->db->trans_rollback();
				return false;
			}
		}
		else{
			$this->db->trans_rollback();
			return false;
		}	
	}

	public function generate_invoice_number($id_office, $id_transaction) {
		$format_str = "INV";

		//get last invoice number per office
		$qry = $this->db->query("
			SELECT no_file, MONTH(CURDATE()) AS month,  YEAR(CURDATE()) as year
			FROM office_format_files 
			WHERE prefix = '{$format_str}' 
				AND id_office = $id_office
				AND month_file = MONTH(CURDATE()) 
				AND year_file = YEAR(CURDATE()) 
			LIMIT 1
			")->row();

		$last_no   = (isset($qry) && count($qry) > 0) ? $qry->no_file : 0;
		$newest_no =  $last_no + 1;

		//delete last invoice number on office
		$this->db->where("prefix", $format_str);
		$this->db->where("id_office", $id_office);
		$this->db->delete("office_format_files");

		date_default_timezone_set('Asia/Jakarta');
		$month = date('n', time());
		$year = date('Y', time());

		$this->db->insert("office_format_files", array(
			"id_office" => $id_office,
			"prefix" => $format_str,
			"no_file" => $newest_no,
			"month_file" => $month,
			"year_file" => $year,
		));

		$this->db->insert("accounting_invoices", array(
			"id_transaction" => $id_transaction,
			"invoice_number" => $format_str . '/' . str_pad($id_office, 2, "0", STR_PAD_LEFT) . '/' . str_pad($newest_no, 3, "0", STR_PAD_LEFT) . '/' . $month . '/' . $year
		));

	}

	public function get_invoice_downpayment($id_office, $id_customer, $id_invoice){
		$sql = "
		SELECT tr.`id_office`, tr.`id_customer`, api.`id_invoice`, api.`id_payment`, api.`amount`, api.`is_down_payment`, tr.`transacted_on` 
		FROM accounting_payment_invoices api
		LEFT JOIN accounting_payments ap ON ap.`id` = api.`id_payment` 
		LEFT JOIN transactions tr ON tr.`id` = ap.`id_transaction`
		WHERE tr.`id_office` = $id_office 
			AND tr.`id_customer` = $id_customer  
			AND api.`id_invoice` = $id_invoice
			AND api.`is_down_payment` = 1 
		LIMIT 1
		";

		return $this->db->query($sql)->row();
	}

	public function get_invoice_payments($id_office, $id_customer, $id_invoice){
		$sql = "
		SELECT * 
		FROM v_invoices vi
		WHERE vi.`id_office` = $id_office
		AND vi.`id_customer` = $id_customer
		AND vi.`id_invoice` = $id_invoice
		AND vi.`transaction_type` = 2
		ORDER BY vi.`id_transaction` ASC, vi.`id_invoice` ASC;
		";

		return $this->db->query($sql)->result();
	}

	public function get_detail_payment($id_office, $id_customer, $id_payment){
		$sql = "
		SELECT tr.`id` id_transaction, tr.`transacted_on`, tr.`posted`, tr.`id_office`, tr.`id_customer`, api.`id_payment`, api.`id_invoice`, ai.`invoice_number`, ap.`payment_method_id` , pm.`code`, api.`is_down_payment`
		, u.`first_name` name_operator
		, (ai.`amount` - ai.`discount_amount`) total_due, IFNULL(SUM(paid.`amount`),0) paid_before
		, ((ai.`amount` - ai.`discount_amount`) - IFNULL(SUM(paid.`amount`),0)) due_now
		, api.`amount` paid_now
		FROM accounting_payment_invoices api 
		LEFT JOIN accounting_payments ap ON ap.`id` = api.`id_payment` 
		LEFT JOIN payment_methods pm ON pm.`id` = ap.`payment_method_id` 
		LEFT JOIN transactions tr ON tr.`id` = ap.`id_transaction` 
		LEFT JOIN accounting_invoices ai ON ai.`id` = api.`id_invoice` 
		LEFT JOIN (
			SELECT ap.`id_transaction`, api.`id_invoice`, api.`amount`
			FROM accounting_payments ap 
			LEFT JOIN accounting_payment_invoices api ON api.`id_payment` = ap.`id`
		)paid ON paid.`id_invoice` = api.`id_invoice` AND paid.`id_transaction` < tr.`id` 
		LEFT JOIN users u ON u.`id` = tr.`handle_by_id`
		WHERE tr.`id_office` = $id_office
		AND tr.`id_customer` = $id_customer
		AND api.`id_payment` = $id_payment 
		GROUP BY tr.`id`, tr.`id_office`, tr.`id_customer`, api.`id_payment`, api.`id_invoice`; 
		";

		$result = $this->db->query($sql)->result();

		return $result;
	}

	public function get_plat($id) {
		$sql = "SELECT * 
		 FROM plat_prices 
		 WHERE id = $id";

		 return $this->db->query($sql)->row();
	}
	
}
