<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends Admin_Controller
{ 
  function __construct(){
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination', 'cart'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Payment_model', 'transaction');
    $this->load->model('offices/Offices_model', 'office');
    $this->load->model('customers/Customers_model', 'customer');
    $this->load->model('price_plat/Plat_model', 'plat');
    $this->load->model('price_service/Service_model', 'service');
 
    $this->data['page_title'] = 'Payment | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Add New';
    $this->data['page_subheader'] = 'Invoice';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index(){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();        
  }

  public function get_lists(){

    $this->add_payment(); 
  }

  public function add_payment($id_customer = 0){
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $customer = $this->customer->get_detail_item($id_customer);
    if(isset($customer))
      $invoices = $this->customer->get_invoice_due($customer->id_office, $id_customer);
    else
      $invoices = array();
       
    $this->data['today'] = get_long_current_date();
    $this->data['customer'] = $customer;
    $this->data['invoices'] = $invoices;

    $this->data['before_body'] = $this->load->view('script_payment', '', true);

    $this->render('add_payment', 'admin_master'); 
  }

  public function payment(){

    $id_office = $this->input->post("id_office");
    $id_customer = $this->input->post("id_customer");

    if($this->input->post()){
      /*
      ** Payment Method:
      ** 0. Pay later --> Just created Invoice
      ** 1. Cash
      ** 2. Transfer
      */

      $payment_method = $this->input->post("payment_method");
      $total_paid = unformat_money($this->input->post("total_paid")); //paid from purchase
      $paid_invoices = $this->input->post("paid_invoices"); //paid from make payment
      $is_down_payment = ($this->input->post("is_dp")) ? $this->input->post("is_dp") : 0; //is_downpayment

      $invoices = array();
      $result = true;

      if($result && $payment_method > 0) {

        //Generate Payment
        $transaction = array(
          "id_office" => $id_office,
          "id_customer" => $id_customer,
          "transaction_type" => 2, //purchase
          "handle_by_id" => $this->session->userdata("user_id"),
        );

        $transaction["payment"] = array(
           "payment_method_id" => $payment_method, 
        );
        
        //paid from make payment
        if(isset($paid_invoices) && count($paid_invoices) > 0) {
          foreach($paid_invoices as $id_invoice => $amount){
            $payments[] = array(
              "id_invoice" => $id_invoice,
              "paid" => $amount,
              "is_down_payment" => $is_down_payment,
            );
          }
        }

        $transaction["payment"]["invoices"] = $payments;
        $id_payment = $this->transaction->generate_payment($transaction);

        if($id_payment > 0){
          //if success creating payment then save attachment if any
          if(isset($_FILES['files']['name'])) {
            $this->load->helper("url");

            $office = $this->office->get_detail_item($id_office);
            $office_name = url_title($office->name, $separator = '-', TRUE);

            $uploadPath = "uploads/offices/".$office_name;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts";
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }

            $uploadPath = "uploads/offices/".$office_name."/payment_receipts/".$id_payment;
            if (!is_dir($uploadPath)) {
              mkdir($uploadPath, 0777, TRUE);
            }
            
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = "*";
            $config['encrypt_name'] = true;

            $this->load->library("upload", $config);

            for($i=0; $i<count($_FILES['files']['name']); $i++){
              if(!empty($_FILES['files']['name'][$i]) && $_FILES['files']['error'][$i] == 0){

                $filename = $_FILES['files']['name'][$i];
                $_FILES['file']['name'] = $filename;
                $_FILES['file']['type'] = $_FILES['files']['type'][$i];
                $_FILES['file']['tmp_name'] = $_FILES['files']['tmp_name'][$i];
                $_FILES['file']['error'] = $_FILES['files']['error'][$i];
                $_FILES['file']['size'] = $_FILES['files']['size'][$i];

                if($this->upload->do_upload('file')){
                  $uploadData = $this->upload->data();
                  $data['id_payment'] = $id_payment;
                  $data['original_name'] = $filename;
                  $data['encrypted_name'] = $uploadData['file_name'];
                  $data['ext'] = $uploadData['file_ext'];
                  $data['location'] = $uploadPath;
                  $data['size'] = $uploadData['file_size'];
                  $this->db->insert('accounting_payment_attachments',$data);
                }
              }
            }    
          }
        }

        $this->session->set_flashdata('notification', array("status" => true, "title" => "Informasi", "msg" => "Pembayaran invoice berhasil disimpan."));
      }      

      
    }

    
    redirect("payments");
  }

  public function get_payment_list() {
    $params = array();

    $id_office = ($this->input->get("id_office")) ? decrypt_url($this->input->get("id_office")) : "";
    $id_customer = ($this->input->get("id_customer")) ? decrypt_url($this->input->get("id_customer")) : "";

    if(isset($id_office) && !empty($id_office)){
      $id_office = decrypt_url($id_office);
      $params[] = "id_office = $id_office";
    }

    if(isset($id_customer) && !empty($id_customer)){
      $id_customer = decrypt_url($id_customer);
      $params[] = "id_customer = $id_customer";
    }

      $inv = $this->payment->get_all_items($params);

      $no = 0;
      foreach ($inv as $key => $row) {
        $inv[$key]->no = $no;
        $inv[$key]->id_transaction = encrypt_url($row->id_transaction);
        $inv[$key]->id_office = encrypt_url($row->id_office);
        $inv[$key]->id_customer = encrypt_url($row->id_customer);
        $inv[$key]->id_payment = encrypt_url($row->id_payment);
        $inv[$key]->amount = format_money($row->amount);
        $inv[$key]->transacted_on = date_format(date_create($row->transacted_on),"d/m/Y");

        $temp = explode(",", $row->invoice_number);
        $temp2 = explode(",", $row->id_invoice);
        $id_arr = array();
        $invoice_arr = array();
        for($i=0;$i<count($temp);$i++){
          $invoice_arr[] = format_invoice($temp[$i]);          
          $id_arr[] = encrypt_url($temp2[$i]);          
        }

        $inv[$key]->invoice_number = implode("&amp; ", $invoice_arr);
        $inv[$key]->id_invoice = implode(",", $id_arr);

        $tools = '
        <select name="tools[]" class="form-control tool-act" data-id_office="'.$row->id_office.'" data-id_customer="'.$row->id_customer.'" onchange="paymentTodo(this,\''.$row->id_payment.'\')">
        <option>--Pilih--</option>
        <option value="detail">Detail</option>
        </select>
        ';
      

        $inv[$key]->tools = $tools;
        $no++;
      }

      echo json_encode(array("data"=>object_to_array($inv)));
  }

  public function get_offices_list(){
    $params = array();
    $list = array();

    
    $id_office = $this->session->userdata("id_office");
    if(isset($id_office) && !empty($id_office)){
      $params[] = "id = $id_office"; 
    }else if($this->session->userdata("order_id_customer")){
      $id_customer = $this->session->userdata("order_id_customer");
      $customer = $this->customer->get_detail_item($id_customer);

      $id_office = $customer->id_office;

      $params[] = "id = " .$id_office;
    }else{
      $id_office = "";      
    }

    $offices = $this->office->get_all_items($params);

    if(!empty($id_office)){}
    else{
      $list[] = array(
        "id" => 0,
        "text" => "--Pilih Kantor Cabang--",
      );
    }
    foreach($offices as $row){
      if($row->id == $id_office){
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
          "selected" => true
        );
      }
      else{
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
        );  
      }
      
    }

    echo json_encode(array("data" => $list));
  }

  public function get_customers_list(){
    $id_office = $this->input->post("id_office");
    $id_customer = ($this->session->userdata("order_id_customer")) ? $this->session->userdata("order_id_customer") : "";

    $params[] = "id_office = $id_office";
    if(!empty($id_customer)){
      $params[] = "id = $id_customer";
    }
    
    $list = array();
    $customers = $this->customer->get_all_items($params);

    if(!empty($id_customer)){}
    else{
      $list[] = array(
        "id" => 0,
        "text" => "--Pilih Customer--",
      );
    }

    foreach($customers as $row){
      if(!empty($id_customer) && $row->id == $id_customer){
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
          "selected" => true,
        );
      }
      else{
        $list[] = array(
          "id" => $row->id,
          "text" => $row->name,
        );
      }
    }

    echo json_encode(array("data" => $list));
  }

  public function get_invoice_due() {
    $id_office = $this->input->post("id_office"); 
    $id_customer = $this->input->post("id_customer");

    $invoices = $this->customer->get_invoice_due($id_office, $id_customer);

    $html = array();
    $i = 1;
    foreach($invoices as $row){
      $html[] = '
      <tr class="row_invoice">
        <td class="middle-text center-text">'.$i.'</td>
        <td class="middle-text">'.format_invoice($row->invoice_number).'</td>
        <td class="middle-text price">'.format_money($row->current_due).'</td>
        <td class="middle-text"><input type="form-text" class="form-control money price amount_paid" data-max_amount="'.intval($row->current_due).'" value="" ></td>
        <td class="center-text middle-text" style="">
            <input type="checkbox" class="paid_invoices" onclick="chooseInvoice(this)" name="paid_invoices['.$row->id_invoice.']" value="">
        </td>
      </tr>
      ';

      $i++;
    }

    echo implode("", $html);
  }


}