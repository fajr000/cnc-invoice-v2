<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price_plat extends Admin_Controller
{  
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Plat_model', 'plat');
    $this->load->model('offices/Offices_model', 'office');

    $this->data['page_title'] = 'Plat Price | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Plat Price';
    $this->data['page_subheader'] = 'Harga Plat';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();
  }

  public function get_lists()
  {
    $this->data['offices'] = $this->office->get_all_items(array());
    $this->data['id_office'] = $this->session->userdata("id_office");
    $this->data['plat_types'] = get_plat_types();
    $this->data['plat_sizes'] = get_plat_sizes();
    
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_plat_view', 'admin_master');
  }

  public function get_prices_list()
  {
    // populate data
    $params   = array();
    if(!empty($this->session->userdata("id_office"))){
      $params[] = "id_office = " . $this->session->userdata("id_office"); 
    }

    $all_items = $this->plat->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" . $row->id . ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->plat->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['plat_types'] = get_plat_types();
    $this->data['offices'] = $this->office->get_all_items(array());
    $this->data['office'] = get_office_id();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_plat_view', 'admin_master'); 
  }

  public function edit($id_item)
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->plat->get_detail_item($id_item);
    $prices = $this->plat->get_all_items(array("id_plat = " . $item->id_plat, "id_office = " . $item->id_office));

    usort($prices,function($a, $b) {
      return $a->size - $b->size;
    });

    if(!has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['plat_types'] = get_plat_types();
    $this->data['offices'] = $this->office->get_all_items(array());
    $this->data['office'] = get_office_id();

    $this->data['price_html'] = $this->generate_prices_html($prices);
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_plat_view', 'admin_master');
  }

  public function delete($id_item)
  {
    $result = $this->plat->delete_item($id_item);   

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('price_plat');
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id         = $this->input->post('id');
    $id_plat    = $this->input->post('id_plat');
    $id_office  = $this->input->post('id_office');
    
    //prices
    $id_price   = $this->input->post('id_price');
    $nama_size  = $this->input->post('name_size');
    $price      = $this->input->post('price');
    $discount   = $this->input->post('discount');
    $stock      = $this->input->post('qty');

    $item_new          = array();
    $item_update       = array();
    $item_update_ids   = array();

    if(count($id_price) == 1 && empty($nama_size[0]) && empty($price[0]) && empty($discount[0])){

    }
    else{
      for($i=0; $i <count($id_price); $i++) {
        if(isset($id_price[$i]) && !empty($id_price[$i])) {
          $item_update[] = array(
            "id" => $id_price[$i],
            "id_office" => $id_office,
            "id_plat" => $id_plat,
            "size" => $nama_size[$i],
            "price" => str_replace(".", "", $price[$i]),
            "discount" => $discount[$i],
            "qty" => $stock[$i],
          );
          $item_update_ids[] = $id_price[$i];
        }
        else {
          $item_new[] = array(
            "id_office" => $id_office,
            "id_plat" => $id_plat,
            "size" => $nama_size[$i],
            "price" => str_replace(".", "", $price[$i]),
            "discount" => $discount[$i],
            "qty" => $stock[$i],
          );
        }
      }
    }

    $error = "";

    //register
    if(empty($id)) {
        $result = $this->plat->insert_item($item_new);

        if($result > 0){
          $id = $result;
        }
    }
    //edit
    else {
        //collect all size before editing
        $all_items = $this->plat->get_all_items(array("id_plat = " . $id_plat, "id_office = " . $id_office));

        if(count($item_new) > 0) {
          //input new account on edit mode
          $this->plat->insert_item($item_new);
        }

        if(count($item_update) > 0) {
          for ($i=0; $i < count($item_update) ; $i++) { 
            $this->plat->update_item($item_update[$i]["id"], $item_update[$i]);
          }
        }

        //delete account on edit order which not included 
        foreach ($all_items as $row) {
          if(!in_array($row->id, $item_update_ids)) {
            $this->plat->delete_item($row->id);
          }
        }        
    }


    $notif['status'] = true;
    $notif['title'] = 'Info';
    $notif['msg'] = 'Data berhasil disimpan.';

    $this->session->set_flashdata('notif', $notif);
    redirect('price_plat');
  }

  function add_price() {
    $html  = '
    <tr>
      <td align="center">
        <input type="hidden" class="form-control input-sm" name="id_price[]" value="">
        <input type="text" class="form-control input-sm name_size" name="name_size[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm money" name="price[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm disc" name="discount[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm disc" name="qty[]" value="">
      </td>
      <td align="left">
        <div>
          <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
        </div>
      </td>
    </tr>';

    echo $html;
  }

  function generate_prices_html($prices) {
    $html_arr = array();
    foreach ($prices as $key => $row) {
      $html  = '
      <tr>
        <td align="center" style="vertical-align:middle;">
          <input type="hidden" class="form-control input-sm" name="id_price[]" value="'.$row->id.'">
          <input type="hidden" class="form-control input-sm name_size" name="name_size[]" value="'.$row->size.'">
          '.$row->size.' mm
        </td>
        <td align="center">
          <div class="input-group">
            <div class="input-group-prepend">
              <span class="input-group-text">Rp.</span>
            </div>
            <input type="text" class="form-control input-sm money" name="price[]" value="'.$row->price.'">
          </div>
        </td>
        <td align="center">
          <div class="input-group">
            <input type="text" class="form-control input-sm disc" name="discount[]" value="'.$row->discount.'" style="text-align:center;">
            <div class="input-group-append">
              <span class="input-group-text">%</span>
            </div>
          </div>
        </td>
        <td align="center" style="vertical-align:middle;">
          <input type="hidden" class="form-control input-sm disc" name="qty[]" value="'.$row->qty.'">
          '.$row->qty.'
        </td>
        <td align="left">
          <div style="display:none;">
            <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
          </div>
        </td>
      </tr>';

      $html_arr[] = $html;
    }

    $html_str = implode("", $html_arr);

    return $html_str;
  }

  function get_registered_plat(){
    if($this->input->post()){
      $id_office = $this->input->post("id_office");
      $id_plat = $this->input->post("id_plat");
      $size = $this->input->post("size");

      $this->db->where("id_office", $id_office);
      $this->db->where("id_plat", $id_plat);
      $this->db->where("size", $size);
      $plat = $this->db->get("plat_prices")->row();

      if(count($plat) > 0)
        echo format_money($plat->price);
      else
        echo "";
    }
  }

  function purchase(){
    if($this->input->post()){
      $id_office = $this->input->post("id_office");
      $id_plat = $this->input->post("id_plat");
      $size = $this->input->post("size");
      $qty = $this->input->post("qty");
      $price_purchase = $this->input->post("price_purchase");
      $price_sale = $this->input->post("price_sale");

      $data = array(
        "id_office" => $id_office,
        "id_plat" => $id_plat,
        "size" => $size,
        "qty" => $qty,
        "price_purchase" => unformat_money($price_purchase),
        "price_sale" => unformat_money($price_sale),
        "handle_by_id" => $this->session->userdata("user_id")
      );

      $this->db->trans_start();
      $this->db->insert('office_plat_order', $data);
      $plat = $this->db->query("SELECT * FROM plat_prices WHERE id_office = $id_office AND id_plat = $id_plat AND size = $size AND deleted = 0")->row();
      if(count($plat) > 0){
        //update stock 
        $data_plat = array(
          "qty" => ($plat->qty + $qty),
          "price" => unformat_money($price_sale)
        );

        if(empty($price_sale)){
          $data_plat = array(
            "qty" => ($plat->qty + $qty),
          );
        }

        $this->db->where("id", $plat->id);
        $this->db->update("plat_prices", $data_plat);
      }else{
        $price = (empty($price_sale)) ? unformat_money($price_purchase) : unformat_money($price_sale);
        //insert new stock
        $data_plat = array(
          "id_office" => $id_office,
          "id_plat" => $id_plat,
          "size" => $size,
          "qty" => $qty,
          "price" => $price,
          "created_by" => $this->session->userdata("user_id")
        );

        $this->db->insert("plat_prices", $data_plat);
      }
      
      $this->db->trans_complete();

      if ($this->db->trans_status() === FALSE){
        echo json_encode(array("success"=> 0));
      }else{
        echo json_encode(array("success"=> 1));
      }
    }
  }

}