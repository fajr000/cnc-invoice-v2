<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			var invalid_namesize = false;
			$('.name_size').each(function(index,element){
				var name_size = $(element).val();
				if(name_size.trim() == ""){
					invalid_namesize = true;
					return false;
				}
			});

			if(invalid_namesize){
				msg.push("Kolom <strong>Ukuran</strong> tidak boleh kosong!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false; 
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "price_plat/";
		});

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "price_plat/delete/"+deletedId;
        });

        var show_office = $("#show_office").val();
		if(show_office == 1) {
			var table = $('#list_data').DataTable( {
		        "ajax": "price_plat/get_prices_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "name_plat", className: "type highlight",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='price_plat/edit/"+oData.id+"'>"+oData.name_plat+"</a>");
				        }
		            },
		            { data: "size", className: "size" },
		            { data: "qty", className: "size" },
		            { data: 'price',  className: "price", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
		            { data: "discount", className: "discount" },
		            { data: 'price_end',  className: "endprice", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
		            { data: "name_office", className: "office" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[1, 'asc']],
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}
		else{
			var table = $('#list_data').DataTable( {
		        "ajax": "price_plat/get_prices_list",
		        "columns": [
		            { data: "id", className: "no" },
		            { data: "name_plat", className: "typefull highlight",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='price_plat/edit/"+oData.id+"'>"+oData.name_plat+"</a>");
				        }
		            },
		            { data: "size", className: "size" },
		            { data: "qty", className: "size" },
		            { data: "price", className: "price" },
		            { data: "discount", className: "discount" },
		            { data: "price_end", className: "endprice" },
		        	{ data: "id", className: "tools",
		            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
				            $(nTd).html(oData.delete_access);
				        } 
		        	}
		        ],
		        'order': [[1, 'asc']]
		    } );

		    table.on( 'order.dt search.dt', function () {
		        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
		            cell.innerHTML = (i+1);  
		        } );
		    } ).draw();
		}

        $('.money').mask('000.000.000.000.000', {reverse: true});
	    $('.disc').mask('000', {reverse: true});

	    $('.disc').on('change', function(){
	    	if($(this).val() > 100){
	    		$('.modal-body').html("Maksimal nilai Diskon adalah 100");
				$('#modal-default').modal('show');
	    		$(this).val("0");

	    		return false;
	    	}
	    });

	    var element = document.getElementById("id_item");
	    if(typeof(element) != 'undefined' && element != null){
	        var id_item = $("#id_item").val();
		    if(id_item.length > 0){
				$('#list_prices > tbody  > tr').each(function(index, tr) { 
					var id = $(this).find("td").eq(0).find("input[type=hidden]").val();
					
					if(id == id_item) {
						$(this).find("td").eq(0).find("input[type=text]").focus();
						var td = $(this).find("td");
						$(td).each(function(index, value){
							if(index < td.length - 1) {
								// $(value).addClass('bg-lightblue disabled color-palette');
							}
						});
					}
				});
		    }
	    }

	});

	function searchPlat(){
		var id_office = $("#id_office").val();
		var id_plat = $("#id_plat").val();
		var size = $("#size").val();

		if(id_office.length > 0 && id_plat.length > 0 && size.length > 0){
			$.ajax({
				url: "price_plat/get_registered_plat", 
				type: "post",
				dataType: "text",
				data: {
					id_office : id_office,
					id_plat : id_plat,
					size : size,
				},
				success: function(price){
					$("#price_sale").val(price);
				}
			});
		}
	}

	function addPurchase(){
		var id_office = $("#header_id_office").val();
		$("#id_office > option").each(function() {
		    if(id_office != "" && this.value != id_office){
		    	$(this).remove();
		    }
		});
		$("select#id_plat").prop('selectedIndex', 0);
		$("select#size").prop('selectedIndex', 0);
		$("select#qty").prop('selectedIndex', 0);
		$("#price_purchase").val("");
		$("#price_sale").val("");
		$("#purchase").modal('show');

		searchPlat();
	}

	function savePurchase(elm){
		var id_office = $("#id_office").find("option:selected").val();
		var id_plat = $("#id_plat").find("option:selected").val();
		var size = $("#size").find("option:selected").val();
		var qty = $("#qty").find("option:selected").val();
		var price_purchase = $("#price_purchase").val();
		var price_sale = $("#price_sale").val();

		if(id_plat.length == 0){
			alert("Pilih jenis plat!");
			return false;
		}

		if(price_purchase.length == 0){
			alert("Masukkan harga beli plat!");
			return false;
		}

		if(price_sale.length == 0){
			alert("Masukkan harga jual plat!");
			return false;
		}

		$.ajax({
			url: "price_plat/purchase", 
			type: "post",
			dataType: "json",
			data: {
				id_office : id_office,
				id_plat : id_plat,
				size : size,
				qty : qty,
				price_purchase : price_purchase,
				price_sale : price_sale,
			},
			success: function(data){
				window.location.href = "price_plat";
			}
		});
	}

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$("#confirmation").modal('show'); 
    }

    function addItem(elm) {
		var tr      = $(elm).closest('tr');
		var name    = $(tr).find('td').eq(0).find('input[type=text]').val();
		var price 	= $(tr).find('td').eq(1).find('input[type=text]').val();

		if(name.length == 0){
			return false;
		}

		if(price.length == 0){
			return false;
		}

		var is_not_complete_exist = false;
		$('#list_items > table > tbody > tr').each(function(){
			var name    = $(tr).find('td').eq(0).find('input[type=text]').val();
			var price 	= $(tr).find('td').eq(1).find('input[type=text]').val();

			if(name.length == 0 || price.length == 0){
				is_not_complete_exist = true;
			}
		});

		if(!is_not_complete_exist) {
			$.ajax( {
				url: "price_plat/add_price",
				type : 'post',
				dataType: "html",
				success: function( html ) {
					$('#list_items table > tbody').append(html);
				},
				complete: function(){
					$('.money').mask('000.000.000.000.000', {reverse: true});
					$('.disc').mask('000', {reverse: true});
				}
			} );
		}
	}

	function subItem(elm) {
		var rows	= $('#list_items > table > tbody').find('tr');
		
		var tr      = $(elm).closest('tr');
		var index	= $(tr).index();

		if(index > 0 || rows.length > 1){
			$(tr).remove();
		}
		else {
			$(tr).find('td').eq(0).find('input[type=hidden]').val("");
			$(tr).find('td').eq(0).find('input[type=text]').val("");
			$(tr).find('td').eq(1).find('input[type=text]').val("");
			$(tr).find('td').eq(2).find('input[type=text]').val("");
		}
	}

</script> 