<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
  }
  
  .no{
    text-align: center;
    width: 2%;
  }
  .type{
    vertical-align: middle;
    text-align: left;
    width: 15%; 
  } 
  .typefull{
    vertical-align: middle;
    text-align: left;
    width: 35%; 
  } 
  .size{
    vertical-align: middle;
    text-align: center;
    width: 10%; 
  } 
  td.highlight a{
    color:  blue;
  }
  td.highlight a:hover{
    color:  blue;
    text-decoration: underline;
  }
  td.highlight a:visited{
    color:  blue;
    text-decoration: underline;
  }
  .price{
    vertical-align: middle;
    text-align: right;
    width: 13%; 
  } 
  .discount{
    vertical-align: middle;
    text-align: center;
    width: 10%; 
  } 
  .office{
    vertical-align: middle;
    text-align: left;
    width: 20%; 
  } 
  .endprice{
    vertical-align: middle;
    text-align: right;
    width: 12%; 
  }
  .tools{
    text-align: center;
    width: 8%;
  }   
  .right-text{
    text-align: right;
  }   
</style>

<!-- Content Header (Page header)  -->
<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12 pull-left">
              <?php
              if($allowed['create']){
              ?>
              <a onclick="addPurchase()" class="btn-primary btn-sm" target="_self" style="cursor: pointer;"><span>TAMBAH BARU</span></a>
              <?php
              }
              ?>
            </div>
          </div>
          <br/> 
          <div class="row">
            <div class="col-sm-12">
            <input type="hidden" id="show_office" value="<?php echo (is_center_admin() || is_admin()) ? 1 : 0; ?>">
              <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline  table-sm">
                <thead>
                  <?php
                  if(is_center_admin() || is_admin()){
                    echo '
                    <tr>
                      <th class="no">&nbsp;</th>
                      <th class="type">Jenis Plat</th>
                      <th class="size">Ukuran (mm)</th>
                      <th class="size">Qty</th>
                      <th class="price">Harga (Rp)</th>
                      <th class="discount">Diskon (%)</th>
                      <th class="endprice">Harga Akhir (Rp)</th>
                      <th class="office">Kantor Cabang</th>
                      <th class="tools">&nbsp;</th>
                    </tr>
                    ';
                  }
                  else{
                    echo '
                    <tr>
                      <th class="no">&nbsp;</th>
                      <th class="typefull">Jenis Plat</th>
                      <th class="size">Ukuran (mm)</th>
                      <th class="size">Qty</th>
                      <th class="price">Harga (Rp)</th>
                      <th class="discount">Diskon (%)</th>
                      <th class="endprice">Harga Akhir (Rp)</th>
                      <th class="tools">&nbsp;</th>
                    </tr>
                    ';
                  }
                  ?>
                </thead>
              </table>
            </div>
          </div>
        <!-- /.col -->
        </div>
          <!-- /.row -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div id="confirmation" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="purchase" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Input Pembelian Plat</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="fr_purchase" name="fr_purchase" target="_self" action="price_plat/purchase">
          <div class="form-group">
            <label for="exampleInputEmail1">Kantor Cabang:</label>
            <input type="hidden" id="header_id_office" value="<?php echo $id_office;?>">
            <select id="id_office" name="id_office" class="custom-select" onchange="searchPlat()">
              <?php 
              foreach($offices as $row){
                $selected = ($row->id == $id_office) ? "selected" : "";
                echo '<option '.$selected.' value="'.$row->id.'">'.$row->name.'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Jenis Plat:</label>
            <select id="id_plat" name="id_plat" class="custom-select" onchange="searchPlat()">
              <?php 
              foreach($plat_types as $row){
                echo '<option value="'.$row->id.'">'.$row->name.'</option>';
              }
              ?>
            </select>
          </div>
          <div class="form-group row">
            <div class="col-sm-2">
              <label for="size" class="col-form-label">Size:</label>
              <select id="size" name="size" class="custom-select" onchange="searchPlat()">
                <?php 
                foreach($plat_sizes as $row){
                  echo '<option value="'.$row->size.'">'.$row->size.'</option>';
                }
                ?>
              </select>
            </div>
            <div class="col-sm-2">
              <label for="qty" class="col-form-label">Qty:</label>
              <select id="qty" name="qty" class="custom-select">
                <?php 
                for($i=1; $i<100;$i++){
                  echo '<option value="'.$i.'">'.$i.'</option>';
                }
                ?>
              </select>
            </div>
            <div class="col-sm-4">
              <label for="size" class="col-form-label">Harga Beli:</label>
              <input type="text" class="form-control right-text money" id="price_purchase" name="price_purchase" placeholder="rupiah">
            </div>
            <div class="col-sm-4">
              <label for="qty" class="col-form-label">Harga Jual:</label>
              <input type="text" class="form-control right-text money" id="price_sale" name="price_sale" placeholder="rupiah">
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <small>
                <strong>Catatan:</strong>
                <br>Harga jual yang <strong>sudah terisi</strong> menunjukkan harga jual plat <strong>saat ini</strong>.
              </small>
            </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" onclick="savePurchase(this)" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->