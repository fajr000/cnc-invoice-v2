<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Plat_model extends CI_Model
{
	private $_table = 'plat_prices';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;		

		$sql = "
		SELECT *
		FROM (
			SELECT pp.`id`,
				pp.`id_office`,
				pp.`id_plat`,
				pp.`size`,
				pp.`price`,
				pp.`discount`,
				pp.`qty`,
				IF(pp.`discount` > 0, pp.`price` - (pp.`discount`/100*pp.`price`) , pp.`price`) price_end,
				pt.`name` AS name_plat,
				of.`name` AS name_office
			FROM plat_prices pp
			LEFT JOIN plat_types pt ON pt.`id` = pp.`id_plat`
			LEFT JOIN offices of ON of.`id` = pp.`id_office` 
			WHERE pp.`deleted` = 0
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$sql = "
		SELECT *
		FROM (
			SELECT pp.`id`,
				pp.`id_office`,
				pp.`id_plat`,
				pp.`size`,
				pp.`price`,
				pp.`discount`,
				pp.`qty`,
				IF(pp.`discount` > 0, pp.`price` - (pp.`discount`/100*pp.`price`) , pp.`price`) price_end,
				pt.`name` AS name_plat,
				of.`name` AS name_office
			FROM plat_prices pp
			LEFT JOIN plat_types pt ON pt.`id` = pp.`id_plat`
			LEFT JOIN offices of ON of.`id` = pp.`id_office` 
			WHERE pp.`deleted` = 0
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT pp.`id`,
				pp.`id_office`,
				pp.`id_plat`,
				pp.`size`,
				pp.`price`,
				pp.`discount`,
				pp.`qty`,
				IF(pp.`discount` > 0, pp.`price` - (pp.`discount`/100*pp.`price`) , pp.`price`) price_end,
				pt.`name` AS name_plat,
				of.`name` AS name_office
			FROM plat_prices pp
			LEFT JOIN plat_types pt ON pt.`id` = pp.`id_plat`
			LEFT JOIN offices of ON of.`id` = pp.`id_office` 
			WHERE pp.`deleted` = 0
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$inserted = 0;
		$this->db->trans_begin();

		foreach ($data as $d) {
			$d['created_by'] = $this->session->userdata("user_id");

			$this->db->insert($this->_table, $d);
			if($this->db->affected_rows() > 0) {
				$inserted++;
			}
		}		

		if($inserted > 0 && $inserted == count($data)) {
			$this->db->trans_commit();

			return true;
		}
		else {
			$this->db->trans_rollback();

			return false;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, 
			array(
				"deleted" => 1, 
				"deleted_on" => get_current_datetime(),
				"deleted_on" => $this->session->userdata("user_id")
			)
		);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function update_stock($id_office, $id_plat, $size, $qty, $type = "IN")
	{
		$this->db->trans_start();

		$stock = ($type == "IN") ? $qty : ($qty*-1);

		$sql = "UPDATE ".$this->_table." SET qty += ".$stock.", updated_on = '".get_current_datetime()."', updated_by = ".$this->session->userdata('user_id')." WHERE id_office = $id_office AND id_plat = $id_price_plat AND size = $size";

		$this->db->query($sql);
		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}
}
