<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Service_model extends CI_Model
{
	private $_table = 'service_prices';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;		

		$sql = "
		SELECT *
		FROM (
			SELECT  
				sp.`id`,
				sp.`id_plat`,
				sp.`id_cutting_type`,
				sp.`size`,
				sp.`easy`,
				sp.`medium`,
				sp.`difficult`,
				sp.`per_minute`,
				sp.`discount`,
				sp.`deleted`,
				sp.`created_on`, 
				sp.`created_by`,
				sp.`deleted_on`, 
				sp.`deleted_by`,
				pt.`name` name_plat,
				ct.`name` name_machine
			FROM service_prices sp
			LEFT JOIN plat_types pt ON pt.`id` = sp.`id_plat` 
			LEFT JOIN cutting_types ct ON ct.`id` = sp.`id_cutting_type` 
			WHERE sp.`deleted` = 0
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$sql = "
		SELECT *
		FROM (
			SELECT  
				sp.`id`,
				sp.`id_plat`,
				sp.`id_cutting_type`,
				sp.`size`,
				sp.`easy`,
				sp.`medium`,
				sp.`difficult`,
				sp.`per_minute`,
				sp.`discount`,
				sp.`deleted`,
				sp.`created_on`, 
				sp.`created_by`,
				sp.`deleted_on`, 
				sp.`deleted_by`,
				pt.`name` name_plat,
				ct.`name` name_machine
			FROM service_prices sp
			LEFT JOIN plat_types pt ON pt.`id` = sp.`id_plat` 
			LEFT JOIN cutting_types ct ON ct.`id` = sp.`id_cutting_type` 
			WHERE sp.`deleted` = 0
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT  
				sp.`id`,
				sp.`id_plat`,
				sp.`id_cutting_type`,
				sp.`size`,
				sp.`easy`,
				sp.`medium`,
				sp.`difficult`,
				sp.`per_minute`,
				sp.`discount`,
				sp.`deleted`,
				sp.`created_on`, 
				sp.`created_by`,
				sp.`deleted_on`, 
				sp.`deleted_by`,
				pt.`name` name_plat,
				ct.`name` name_machine
			FROM service_prices sp
			LEFT JOIN plat_types pt ON pt.`id` = sp.`id_plat` 
			LEFT JOIN cutting_types ct ON ct.`id` = sp.`id_cutting_type` 
			WHERE sp.`deleted` = 0
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$inserted = 0;
		$this->db->trans_begin();
		
		foreach ($data as $d) {
			$d['created_by'] = $this->session->userdata("user_id");

			$this->db->insert($this->_table, $d);
			if($this->db->affected_rows() > 0) {
				$inserted++;
			}
		}		

		if($inserted > 0 && $inserted == count($data)) {
			$this->db->trans_commit();

			return true;
		}
		else {
			$this->db->trans_rollback();

			return false;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, 
			array(
				"deleted" => 1, 
				"deleted_on" => get_current_datetime(),
				"deleted_on" => $this->session->userdata("user_id")
			)
		);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}
}
