<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			var invalid_namesize = false;
			$('.name_size').each(function(index,element){
				var name_size = $(element).val();
				if(name_size.trim() == ""){
					invalid_namesize = true;
					return false;
				}
			});

			if(invalid_namesize){
				msg.push("Kolom <strong>Ukuran</strong> tidak boleh kosong!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "price_service/";
		});

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "price_service/delete/"+deletedId;
        });

        
		var table = $('#list_data').DataTable( {
	        "ajax": "price_service/get_services_list",
	        "columns": [
	            { data: "id", className: "no" },
	            { data: "name_machine", className: "machine" },
	            { data: "name_plat", className: "type highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='price_service/edit/"+oData.id+"'>"+oData.name_plat+"</a>");
			        }
	            },
	            { data: "size", className: "size" },
	            { data: 'easy',  className: "price", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
	            { data: 'medium',  className: "price", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
	            { data: 'difficult',  className: "price", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
	            { data: 'per_minute',  className: "price", render: $.fn.dataTable.render.number( '.', ',', 2, '', '' )},
	            { data: "discount", className: "discount" },
	        	{ data: "id", className: "tools",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(oData.delete_access);
			        } 
	        	}
	        ],
	        'order': [[1, 'asc'],[2, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

	    $('.money').mask('000.000.000.000.000', {reverse: true});
	    $('.disc').mask('000', {reverse: true});

	    $('.disc').on('change', function(){
	    	if($(this).val() > 100){
	    		$('.modal-body').html("Maksimal nilai Diskon adalah 100");
				$('#modal-default').modal('show');
	    		$(this).val("0");

	    		return false;
	    	}
	    });

	    if ($('#id_item').length > 0) {
	    	var id_item = $("#id_item").val();
		    if(id_item.length > 0){
				$('#list_prices > tbody  > tr').each(function(index, tr) { 
					var id = $(this).find("td").eq(0).find("input[type=hidden]").val();
					
					if(id == id_item) {
						$(this).find("td").eq(0).find("input[type=text]").focus();
						var td = $(this).find("td");
						$(td).each(function(index, value){
							if(index < td.length - 1) {
								$(value).addClass('bg-lightblue disabled color-palette');
							}
						});
					}
				});
		    }
	    }	    
	});

    function confirmDelete(id){
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    }

    function addItem(elm) {
		var tr      = $(elm).closest('tr');
		var name    = $(tr).find('td').eq(0).find('select').find('option:selected').val();

		if(name.length == 0){
			return false;
		}

		var is_not_complete_exist = false;
		$('#list_items > table > tbody > tr').each(function(){
			var name    = $(tr).find('td').eq(0).find('select').find('option:selected').val();

			if(name.length == 0 ){
				is_not_complete_exist = true;
			}
		});

		if(!is_not_complete_exist) {
			$.ajax( {
				url: "price_service/add_price",
				type : 'post',
				dataType: "html",
				success: function( html ) {
					$('#list_items table > tbody').append(html);
				},
				complete: function(){
					$('.money').mask('000.000.000.000.000', {reverse: true});
					$('.disc').mask('000', {reverse: true});
				}
			} );
		}
	}

	function subItem(elm) {
		var rows	= $('#list_items > table > tbody').find('tr');
		
		var tr      = $(elm).closest('tr');
		var index	= $(tr).index();

		if(index > 0 || rows.length > 1){
			$(tr).remove();
		}
		else {
			$(tr).find('td').eq(0).find('input[type=hidden]').val("");
			$(tr).find('td').eq(0).find('select').prop('selectedIndex', 0);//ukuran col
			$(tr).find('td').eq(1).find('input[type=text]').val("");//easy col
			$(tr).find('td').eq(2).find('input[type=text]').val("");//medium col
			$(tr).find('td').eq(3).find('input[type=text]').val("");//difficult col
			$(tr).find('td').eq(4).find('input[type=text]').val("");//discount col
		}
	}

	function checkSize(elm){
		var value = $(elm).find(":selected").val();

		if(value == 0){
			$(elm).addClass("custom_size_process");
			$("#input_custom").val("");
			$("#modal-custom").modal("show");
		}else{
			$(elm).removeClass("custom_size_process");
		}
	}

	function saveCustomeSize(elm){
		var size = $("#input_custom").val();
		if((size.trim()).length > 0){
			$.ajax({
				url: "price_service/save_custom_size", 
				type: "post",
				dataType: "json",
				data: {
					size : size
				},
				success: function(result){
					if(result.success == 1){
						var select_length = $('select.custom_size_process option').length;
						$('select.name_size').each(function(i, obj) {
						   	$(obj).find("option").eq(result.index).before($('<option>', {value: size, text: size})); 
						});


						$("select.custom_size_process").val(size);
						$("select.custom_size_process").removeClass("custom_size_process");
						$("#modal-custom").modal("hide");
					}
					else{
						alert(result.msg);
					}
  				}
  			});
		}
		else{
			alert("Masukkan ukuran custom");
		}
	}

	function closeCustomeSize(){
		$("#input_custom").val("");
		$("select.custom_size_process").prop('selectedIndex', 0);
		$("select.custom_size_process").removeClass("custom_size_process");
		$("#modal-custom").modal("hide");
	}

</script> 