<!--Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    
  }

  .btn-file {
      position: relative;
      overflow: hidden;
  }
  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      font-size: 100px;
      text-align: right;
      filter: alpha(opacity=0);
      opacity: 0;
      outline: none;
      background: white;
      cursor: inherit;
      display: block;
  }
  .item {
    float: left;
    padding: 5px;
    border: 1px solid #ccc;
    margin: 0px 3px;
    text-align: center;
  }
  .bold-text {
    font-weight: bold;
  }
  .suggestion {
    font-size: 12px;
  }
  .radio-label {
    display: inline-block;
    vertical-align: bottom;
    margin-right: 3%;
    font-weight: normal;
    line-height: 15px;
  }
  .radio-input {
      display: inline-block;
      vertical-align: bottom;
      margin-left: 0px;
  }
  .label-form {
      vertical-align: middle !important; 
  }
  .money {
      text-align: right; 
  }
</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title">Harga Jasa Potong</h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
              <div class="col-md-12" id="form_user">
                <p>
                  <form autocomplete="off" name="form" id="form" method="post" action="price_service/save" target="_self" enctype="multipart/form-data">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Jenis Plat</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo isset($item) ? $item->id : '';?>">
                          <select class="form-control" id="id_plat" name="id_plat">
                            <?php
                            foreach ($plat_types as $key => $row) {
                              $selected = ((isset($item) && $item->id_plat == $row->id)) ? "selected" : "";
                              echo "<option value='" . $row->id . "' " . $selected . ">" . $row->name . "</option>";
                            }
                            ?>
                          </select>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Jenis Mesin</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <select class="form-control" id="id_cutting" name="id_cutting">
                            <?php
                            foreach ($cutting_types as $key => $row) {
                              $selected = ((isset($item) && $item->id_cutting_type == $row->id)) ? "selected" : "";
                              echo "<option value='" . $row->id . "' " . $selected . ">" . $row->name . "</option>";
                            }
                            ?>
                          </select>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Ukuran</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                            <div class="row">
                              <div id="list_items" class="col-md-12 col-sm-12 col-xs-12 table-responsive" style="border: 0px;">
                                <input type="hidden" id="id_item" value="<?php echo isset($item)?$item->id:""; ?>">
                                <table id="list_prices" class="table table-bordered table-condensed">
                                  <thead>
                                    <tr>
                                      <th rowspan="2" style="width: 15%;">Ukuran (mm)</th>
                                      <th colspan="4" style="width: 48%;">Harga (Rp)</th>
                                      <th rowspan="2" style="width: 10%;">Discount (%)</th>
                                      <th rowspan="2" style="width: 27%;"></th>
                                    </tr>
                                    <tr>
                                      <th style="width: 12%;">Easy</th>
                                      <th style="width: 12%;">Medium</th>
                                      <th style="width: 12%;">Difficult</th>
                                      <th style="width: 12%;">Per Minute</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    if(isset($price_html) && !empty($price_html)) {
                                      echo $price_html;
                                    } 
                                    else {
                                    ?>
                                    <tr> 
                                      <td align="center">
                                        <input type="hidden" class="form-control input-sm" name="id_price[]" value="">
                                        <!-- <input type="text" class="form-control input-sm name_size" name="name_size[]" value=""> -->
                                        <select class="custom-select name_size" name="name_size[]" onchange="checkSize(this)">
                                          <?php 
                                          foreach($plat_sizes as $row){
                                            echo '<option value="'.$row->size.'">'.$row->size.'</option>';
                                          }
                                          ?>
                                          <option value="0">Custom</option>
                                        </select>
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm money" name="easy[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm money" name="medium[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm money" name="difficult[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm money" name="perminute[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm disc" name="discount[]" value="">
                                      </td>
                                      <td align="left">
                                        <div>
                                          <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
                                          <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
                                        </div>
                                      </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td></td>
                        <td>
                    <button id="btn_back" class="btn btn-default btn btn-sm" name="btn_back"><span>Batal</span></button>&nbsp;
                    <button id="btn_submit" class="btn btn-primary btn btn-sm" name="btn_submit"><span>Simpan</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </form>
                </p>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->

<div class="modal fade" id="modal-default"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="modal-custom">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <form class="form-horizontal">
            <div class="form-group row">
              <label for="input_custom" class="col-sm-4 col-form-label">Size: </label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="input_custom" placeholder="Custom Size" onkeypress="return isDecimal(event, this)">
              </div>
            </div>

        </form>
        <div class="float-right">
          <button type="button" class="btn btn-default" onclick="closeCustomeSize()">Batal</button>
          <button type="button" class="btn btn-primary" onclick="saveCustomeSize(this)">Simpan</button>
        </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

