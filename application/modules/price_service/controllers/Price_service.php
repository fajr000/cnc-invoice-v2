<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Price_service extends Admin_Controller
{  
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Service_model', 'service');

    $this->data['page_title'] = 'Cutting Service Price | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Cutting Service Price';
    $this->data['page_subheader'] = 'Harga Jasa Potong';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $this->get_lists();
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_service_view', 'admin_master');
  }

  public function get_services_list()
  {
    // populate data
    $params   = array();

    $all_items = $this->service->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" . $row->id . ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->service->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['plat_types'] = get_plat_types();
    $this->data['cutting_types'] = get_cutting_types();
    $this->data['plat_sizes'] = get_plat_sizes();

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_service_view', 'admin_master'); 
  }

  public function edit($id_item)
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->service->get_detail_item($id_item);
    $prices = $this->service->get_all_items(array("id_plat = " . $item->id_plat, "id_cutting_type = " . $item->id_cutting_type));

    usort($prices,function($a, $b) {
      return $a->size - $b->size;
    }); 

    if(!has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['plat_types'] = get_plat_types();
    $this->data['cutting_types'] = get_cutting_types();
    $this->data['plat_sizes'] = get_plat_sizes();

    $this->data['price_html'] = $this->generate_prices_html($prices);
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_service_view', 'admin_master');
  }

  public function delete($id_item)
  {
    $result = $this->service->delete_item($id_item);   

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('price_service');
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id         = $this->input->post('id');
    $id_plat    = $this->input->post('id_plat');
    $id_cutting = $this->input->post('id_cutting');
    
    //prices
    $id_price   = $this->input->post('id_price');
    $nama_size  = $this->input->post('name_size');
    $easy      = $this->input->post('easy');
    $medium     = $this->input->post('medium');
    $difficult  = $this->input->post('difficult');
    $perminute  = $this->input->post('perminute');
    $discount   = $this->input->post('discount');

    $item_new          = array();
    $item_update       = array();
    $item_update_ids   = array();

    if(count($id_price) == 1 && empty($nama_size[0])){

    }
    else{
      for($i=0; $i <count($id_price); $i++) {
        if(isset($id_price[$i]) && !empty($id_price[$i])) {
          $item_update[] = array(
            "id" => $id_price[$i],
            "id_plat" => $id_plat,
            "id_cutting_type" => $id_cutting,
            "size" => $nama_size[$i],
            "easy" => str_replace(".", "", $easy[$i]),
            "medium" => str_replace(".", "", $medium[$i]),
            "difficult" => str_replace(".", "", $difficult[$i]),
            "per_minute" => str_replace(".", "", $perminute[$i]),
            "discount" => $discount[$i],
          );
          $item_update_ids[] = $id_price[$i];
        }
        else {
          $item_new[] = array(
            "id_plat" => $id_plat,
            "id_cutting_type" => $id_cutting,
            "size" => $nama_size[$i],
            "easy" => str_replace(".", "", $easy[$i]),
            "medium" => str_replace(".", "", $medium[$i]),
            "difficult" => str_replace(".", "", $difficult[$i]),
            "per_minute" => str_replace(".", "", $perminute[$i]),
            "discount" => $discount[$i],
          );
        }
      }
    }

    $error = "";

    //register
    if(empty($id)) {
        $result = $this->service->insert_item($item_new);

        if($result > 0){
          $id = $result;
        }
    }
    //edit
    else {
        //collect all size before editing
        $all_items = $this->service->get_all_items(array("id_plat = " . $id_plat, "id_cutting_type = " . $id_cutting));

        if(count($item_new) > 0) {
          //input new account on edit mode
          $this->service->insert_item($item_new);
        }

        if(count($item_update) > 0) {
          for ($i=0; $i < count($item_update) ; $i++) { 
            $this->service->update_item($item_update[$i]["id"], $item_update[$i]);
          }
        }

        //delete account on edit order which not included 
        foreach ($all_items as $row) {
          if(!in_array($row->id, $item_update_ids)) {
            $this->service->delete_item($row->id);
          }
        }        
    }


      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil disimpan.';

      $this->session->set_flashdata('notif', $notif);
      redirect('price_service');
  }

  function add_price() {
    $plat_sizes = get_plat_sizes();

    $select = '<select class="custom-select name_size" name="name_size[]" onchange="checkSize(this)">';
    foreach($plat_sizes as $plat){
      $select .= '<option value="'.$plat->size.'">'.$plat->size.'</option>';
    }
    $select .= '<option value="0">Custom</option>';
    $select .= '</select>';

    $html  = '
    <tr>
      <td align="center">
        <input type="hidden" class="form-control input-sm" name="id_price[]" value="">
        '.$select.'
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm money" name="easy[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm money" name="medium[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm money" name="difficult[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm money" name="perminute[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm" name="discount[]" value="">
      </td>
      <td align="left">
        <div>
          <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
        </div>
      </td>
    </tr>';

    echo $html;
  }

  function generate_prices_html($prices) {
    $plat_sizes = get_plat_sizes();

    $html_arr = array();
    foreach ($prices as $key => $row) {
      $select = '<select class="custom-select name_size" name="name_size[]" onchange="checkSize(this)">';
      foreach($plat_sizes as $plat){
        $selected = ($plat->size == $row->size) ? "selected" : "";
        $select .= '<option '.$selected.' value="'.$plat->size.'">'.$plat->size.'</option>';
      }
      $select .= '<option value="0">Custom</option>';
      $select .= '</select>';

      $html  = '
      <tr>
        <td align="center">
          <input type="hidden" class="form-control input-sm" name="id_price[]" value="'.$row->id.'">
          '.$select.'
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm money" name="easy[]" value="'.$row->easy.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm money" name="medium[]" value="'.$row->medium.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm money" name="difficult[]" value="'.$row->difficult.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm money" name="perminute[]" value="'.$row->per_minute.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm" name="discount[]" value="'.$row->discount.'">
        </td>
        <td align="left">
          <div>
            <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
          </div>
        </td>
      </tr>';

      $html_arr[] = $html;
    }

    $html_str = implode("", $html_arr);

    return $html_str;
  }

  function save_custom_size(){
    $size = $this->input->post("size");
    $size = trim($size);

    $start = ($size - 0.00001);
    $end = ($size + 0.00001);
    $exist = $this->db->query("SELECT * FROM plat_sizes WHERE size BETWEEN $start AND $end")->row();
    
    if(count($exist) > 0){
      echo json_encode(array("success" => 0, "msg" => "Size/ukuran telah terdaftar"));
    }
    else{
      $this->db->insert("plat_sizes", array("size" => $size));
      $sizes = $this->db->query("SELECT * FROM plat_sizes ORDER BY size ASC")->result();

      $index = count($sizes) - 1;
      for($i=0; $i<count($sizes); $i++){
        if($sizes[$i]->size > $size){
          $index = $i - 1;

          break;
        }
      }

      echo json_encode(array("success" => 1, "index" => $index));
    }
  }

}