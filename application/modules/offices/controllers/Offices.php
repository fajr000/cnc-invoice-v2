<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices extends Admin_Controller
{ 
  function __construct()
  {
    parent::__construct();
    $this->load->library(array('ion_auth', 'form_validation', 'pagination'));
    $this->load->helper(array('url', 'language'));

    $this->load->model('Offices_model', 'office');

    $this->data['page_title'] = 'Branch Offices | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Branch Offices';
    $this->data['page_subheader'] = 'Kantor Cabang';

    // Set pemission/ access
    $class_name = $this->router->fetch_class();
    set_parent_url($class_name);
    $this->data['allowed'] = build_access($class_name);
  }

  public function index()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    if(has_right_access($this->data['allowed'], "c") || has_right_access($this->data['allowed'], "u") || has_right_access($this->data['allowed'], "d")) {

      $this->get_lists();
    }
    else {
      if(!empty($this->session->userdata("id_office"))){
        redirect("offices/edit?id=" . encrypt_url($this->session->userdata("id_office")));      
      }
    }    
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    // echo "<pre>";print_r($items);die();
    $this->render('list_office_view', 'admin_master');
  }

  public function get_offices_list()
  {
    // populate data
    $params = array();

    $all_items = $this->office->get_all_items($params);

    $delete_access = "";
    foreach($all_items as $key => $row){
      if($this->data['allowed']['delete'] > 0){
        $delete_access = "<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(\"" . encrypt_url($row->id) . "\")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>";
      }

      $all_items[$key]->id = encrypt_url($row->id);
      $all_items[$key]->delete_access = $delete_access;
    }

    echo json_encode(array("data"=>$all_items));
  }

  public function add()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $item = $this->office->get_detail_item(0);
    
    $this->data['new'] = true;
    $this->data['item'] = $item;

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_office_view', 'admin_master'); 
  }

  public function edit()
  {
    if (!logged_in())
    {
      //redirect them to the login page
      redirect('user/login', 'refresh');
    }

    $id_item = $this->input->get("id");
    $id_item = decrypt_url($id_item);
    $item = $this->office->get_detail_item($id_item);
    

    $accounts = $this->office->get_all_accounts($id_item);
    $settings = $this->office->get_office_settings($id_item);

    if($item->id != $this->session->userdata("id_office") && !has_right_access($this->data['allowed'], "u")){
      redirect_not_allowed();
    }
    
    $item->id = encrypt_url($item->id);
    $this->data['new'] = false;
    $this->data['item'] = $item; 
    $this->data['account_html'] = $this->generate_accounts_html($accounts);
    $this->data['settings'] = $settings;
    
    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit_office_view', 'admin_master');
  }

  public function delete()
  {

    $id_item = $this->input->get("id");
    $id_item = decrypt_url($id_item);
    //prevent to remove it's own data 
    if($id_item == $this->session->userdata("id_office")){
      redirect_not_allowed();
    }

    $result = $this->office->delete_item($id_item);   

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil dihapus.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Data gagal dihapus!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('offices');
  }

  public function save() 
  {
    $notif = array();
    $result= 1;

    $id       = $this->input->post('id');
    $name     = $this->input->post('name');
    $address  = $this->input->post('address');
    $city     = $this->input->post('city');
    $phone    = $this->input->post('phone');
    $website  = $this->input->post('website');
    $email    = $this->input->post('email');
    $active   = $this->input->post('active');
    $alert    = ($this->input->post('stock_alert') && !empty($this->input->post('stock_alert'))) ? $this->input->post('stock_alert') : 0;
    $compensation    = ($this->input->post('sales_compensation_percentage') && !empty($this->input->post('sales_compensation_percentage'))) ? $this->input->post('sales_compensation_percentage') : 0;
    $invoice_name    = ($this->input->post('invoice_person_name') && !empty($this->input->post('invoice_person_name'))) ? $this->input->post('invoice_person_name') : 0;
    $invoice_position    = ($this->input->post('invoice_person_position') && !empty($this->input->post('invoice_person_position'))) ? $this->input->post('invoice_person_position') : 0;

    //accounts
    $id_bank      = $this->input->post('id_bank');
    $nama_bank    = $this->input->post('nama_bank');
    $norek        = $this->input->post('norek');
    $nama_pemilik = $this->input->post('nama_pemilik');
    
    $temp = array(
      'id' => $id,
      'name' => $name,
      'address' => $address,
      'city' => $city,
      'phone' => $phone,
      'website' => $website,
      'email' => $email,
      'active' => $active,
    );

    $item_new          = array();
    $item_update       = array();
    $item_update_ids   = array();

    if(count($id_bank) == 1 && empty($nama_bank[0])){

    }
    else{
      for($i=0; $i <count($id_bank); $i++) {
        if(isset($id_bank[$i]) && !empty($id_bank[$i])) {
          $item_update[] = array(
            "id" => $id_bank[$i],
            "id_office" => $id,
            "name" => $nama_bank[$i],
            "number_account" => $norek[$i],
            "owner_name" => $nama_pemilik[$i],
          );
          $item_update_ids[] = $id_bank[$i];
        }
        else {
          $item_new[] = array(
            "name" => $nama_bank[$i],
            "number_account" => $norek[$i],
            "owner_name" => $nama_pemilik[$i],
          );
        }
      }
    }

    // validate form input 
    $this->form_validation->set_rules('name', 'Nama', 'trim|required');

    $error = "";
    if ($this->form_validation->run($this) === TRUE){

      $data = array(
        'name' => $name,
        'address' => $address,
        'city' => $city,
        'phone' => $phone,
        'website' => $website,
        'email' => $email,
        'active' => $active,
      );

      if(!empty($id)){
         $id = decrypt_url($id);
      }

      //register
      if(empty($id)) {
          $result = $this->office->insert_item($data);

          if($result > 0){
            $id = $result;

            for ($i=0; $i < count($item_new) ; $i++) { 
              $item_new[$i]["id_office"] = $id;  
            }

            //input new account
            $this->office->insert_accounts($item_new);
            $this->office->save_office_setting($id,  
              array(
                array("code" => "MIN_STOCK", "value" => $alert),
                array("code" => "SALES_COMPENSATION_PERCENTAGE", "value" => $compensation),
                array("code" => "INVOICE_PERSON_NAME", "value" => $invoice_name),
                array("code" => "INVOICE_PERSON_POSITION", "value" => $invoice_position),
              )
            );
            $this->office->insert_message($id);
          }
      }
      //edit
      else {
          $result = $this->office->update_item($id, $data);
          if($result > 0) {
            //collect all account before editing
            $all_items = $this->office->get_all_accounts($id);

            if(count($item_new) > 0) {
              for ($i=0; $i < count($item_new) ; $i++) { 
                $item_new[$i]["id_office"] = $id;  
              }
              //input new account on edit mode
              $this->office->insert_accounts($item_new);
            }

            if(count($item_update) > 0) {
              for ($i=0; $i < count($item_update) ; $i++) { 
                $this->office->update_account($item_update[$i]["id"], $item_update[$i]);
              }
            }

            //delete account on edit order which not included 
            foreach ($all_items as $row) {
              if(!in_array($row->id, $item_update_ids)) {
                $this->office->delete_account($id, $row->id);
              }
            }

            $this->office->save_office_setting($id, 
              array(
                array("code" => "MIN_STOCK", "value" => $alert),
                array("code" => "SALES_COMPENSATION_PERCENTAGE", "value" => $compensation),
                array("code" => "INVOICE_PERSON_NAME", "value" => $invoice_name),
                array("code" => "INVOICE_PERSON_POSITION", "value" => $invoice_position),
              )
            );
          }
      }
    }
    else{
      $result = 0;

      $error =  str_replace("<p>", "", validation_errors());
      $error =  str_replace("</p>", "<br/>", $error);
    }

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Data berhasil disimpan.';

      $this->session->set_flashdata('notif', $notif);
      redirect('offices');
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = '<strong>Data gagal disimpan!</strong>' . '<br/>' . $error;

      $this->session->set_flashdata('notif', $notif);

      // populate data
      $item = $temp;

      $this->data['new'] = true;
      $this->data['item'] = (object)$item;
      if(!empty($id)) {
        $accounts = $this->office->get_all_accounts($id);
        $this->data['account_html'] = $this->generate_accounts_html($accounts);
      }
    
      $this->data['before_body'] = $this->load->view('script', '', true);

      $this->render('edit_office_view', 'admin_master'); 
    }
  }

  function add_account() {
    $html  = '
    <tr>
      <td align="center">
        <input type="hidden" class="form-control input-sm" name="id_bank[]" value="">
        <input type="text" class="form-control input-sm nama_bank" name="nama_bank[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm" name="norek[]" value="">
      </td>
      <td align="center">
        <input type="text" class="form-control input-sm" name="nama_pemilik[]" value="">
      </td>
      <td align="center">
        <div>
          <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
          <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
        </div>
      </td>
    </tr>';

    echo $html;
  }

  function generate_accounts_html($accounts) {
    $html_arr = array();
    foreach ($accounts as $key => $row) {
      $html  = '
      <tr>
        <td align="center">
          <input type="hidden" class="form-control input-sm" name="id_bank[]" value="'.$row->id.'">
          <input type="text" class="form-control input-sm nama_bank" name="nama_bank[]" value="'.$row->name.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm" name="norek[]" value="'.$row->number_account.'">
        </td>
        <td align="center">
          <input type="text" class="form-control input-sm" name="nama_pemilik[]" value="'.$row->owner_name.'">
        </td>
        <td align="center">
          <div>
            <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
            <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
          </div>
        </td>
      </tr>';

      $html_arr[] = $html;
    }

    $html_str = implode("", $html_arr);

    return $html_str;
  }
}