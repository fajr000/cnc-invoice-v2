<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Offices_model extends CI_Model
{
	private $_table = 'offices';

	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;		

		$sql = "
		SELECT *
		FROM (
			SELECT 
				o.`id` as id,
				o.`name` as `name`,
				o.`address`, 
				o.`city`, 
				o.`phone`, 
				o.`website`, 
				o.`email`,
				o.`active`
			FROM `offices` o 
			WHERE o.`deleted` = 0 
		) qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(count($params) > 0)
			$filter_str = implode(" AND ", $params);

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$sql = "
		SELECT *
		FROM (
			SELECT 
				o.`id` as id,
				o.`name` as `name`,
				o.`address`, 
				o.`city`, 
				o.`phone`, 
				o.`website`, 
				o.`email`,
				o.`active`
			FROM `offices` o 
			WHERE o.`deleted` = 0 
		) qry 
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT *
		FROM (
			SELECT 
				o.`id` as id,
				o.`name` as `name`,
				o.`address`, 
				o.`city`, 
				o.`phone`, 
				o.`website`, 
				o.`email`,
				o.`active`
			FROM `offices` o  
		) qry 
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$data['created_by'] = $this->session->userdata("user_id");
		$this->db->insert($this->_table, $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update($this->_table, 
			array(
				"deleted" => 1, 
				"deleted_on" => get_current_datetime(),
				"deleted_on" => $this->session->userdata("user_id")
			)
		);

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	public function get_all_accounts($id_office)
	{
		$sql = "
		SELECT * 
		FROM office_accounts oa 
		WHERE oa.`id_office` = " . $id_office ;

		return $this->db->query($sql)->result();
	}

	public function insert_accounts($data)
	{
		$table = "office_accounts";

		$inserted = 0;
		$this->db->trans_begin();

		foreach ($data as $d) {
			$this->db->insert($table, $d);
			if($this->db->affected_rows() > 0) {
				$inserted++;
			}
		}		

		if($inserted > 0 && $inserted == count($data)) {
			$this->db->trans_commit();

			return true;
		}
		else {
			$this->db->trans_rollback();

			return false;
		}
	}

	public function update_account($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update("office_accounts", $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_account($id_office, $id_account)
	{
		$this->db->trans_start();

		$this->db->where("id", $id_account);
		$this->db->where("id_office", $id_office);
		$this->db->delete("office_accounts");

		if($this->db->affected_rows() > 0)
		{
			$this->db->trans_commit();

			return 1;	
		}
		else 
		{
			$this->db->trans_rollback();

			return 0;
		}
	}

	// data array("code" => xxx, "value" => yyy)
	public function save_office_setting($id_office, $data = array()){

		$settings = $this->get_office_settings($id_office);
		for($i=0;$i<count($data);$i++){
			$is_exist = false;

			foreach($settings as $key => $value){
				if($data[$i]["code"] == $key){
					$is_exist = true;
				}
			}

			if($is_exist){
				$this->db->where("id_office", $id_office);
				$this->db->where("code", $data[$i]["code"]);
				$this->db->update("office_settings", $data[$i]);
			}
			else{
				$data[$i]["id_office"] = $id_office;
				$this->db->insert("office_settings", $data[$i]);
			}
		}
	}

	public function get_office_settings($id_office){
		$sql = "SELECT * FROM office_settings WHERE id_office = $id_office";
		$result = $this->db->query($sql)->result();

		$settings = array();
		foreach($result as $row){
			$settings[$row->code] = $row->value;
		}
 
		return $settings;
	}

	public function insert_message($id_office)
	{
		$data = array(
			"id_office" => $id_office,
			"title" => "",
			"message" => ""
		);
		$this->db->insert("office_messages", $data);
	}
}
