<script>
	$(function () {
		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			var invalid_namesize = false;
			$('.name').each(function(index,element){
				var name = $(element).val();
				if(name.trim() == ""){
					invalid_namesize = true;
					return false;
				}
			});

			var invalid_namebank = false;
			$('.nama_bank').each(function(index,element){
				var name = $(element).val();
				var number = $(element).closest("tr").find("td:eq(1)").find("input").val();
				var owner = $(element).closest("tr").find("td:eq(2)").find("input").val();
				
				if(name.trim() == "" && (number.length > 0 || owner.length > 0)){
					invalid_namebank = true;
					return false;
				}
			});

			if(invalid_namesize){
				msg.push("Kolom <strong>Nama Cabang</strong> tidak boleh kosong!");
			}

			if(invalid_namebank){
				msg.push("Kolom <strong>Nama Bank Rekening</strong> tidak boleh kosong!");
			}

			if(($("#email").val()).length > 0 && !ValidateEmail($("#email"))){
				msg.push("Kolom <strong>Email</strong> tidak valid!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "offices/";
		});

        $("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "offices/delete/?id="+deletedId;
        });

        var table = $('#list_data').DataTable( { 
	        "ajax": "offices/get_offices_list",
	        "columns": [
	            { data: "id", className: "no" },
	            { data: "name", className: "nama highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='offices/edit/?id="+oData.id+"'>"+oData.name+"</a>");
			        }
	            },
	            { data: "address", className: "alamat" },
	            { data: "phone", className: "telp" },
	        	{ data: "id", className: "tools",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html(oData.delete_access);
			        } 
	        	}
	        ],
	        'order': [[1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

	    $('.sales_compensation_percentage').mask('0', {reverse: true});

	});

    function confirmDelete(id){ 
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    }

    function addItem(elm) {
		var tr      = $(elm).closest('tr');
		var bank    = $(tr).find('td').eq(0).find('input[type=text]').val();
		var norek 	= $(tr).find('td').eq(1).find('input[type=text]').val();
		var pemilik	= $(tr).find('td').eq(2).find('input[type=text]').val();

		if(bank.length == 0){
			return false;
		}

		if(norek.length == 0){
			return false;
		}

		if(pemilik.length == 0){
			return false;
		}		

		var is_not_complete_exist = false;
		$('#list_items > table > tbody > tr').each(function(){
			var bank    = $(tr).find('td').eq(0).find('input[type=text]').val();
			var norek 	= $(tr).find('td').eq(1).find('input[type=text]').val();
			var pemilik	= $(tr).find('td').eq(2).find('input[type=text]').val();

			if(bank.length == 0 || norek.length == 0 || pemilik.length == 0){
				is_not_complete_exist = true;
			}
		});

		if(!is_not_complete_exist) {
			$.ajax( {
				url: "offices/add_account",
				type : 'post',
				dataType: "html",
				success: function( html ) {
					$('#list_items table > tbody').append(html);
				}
			} );
		}
	}

	function subItem(elm) {
		var rows	= $('#list_items > table > tbody').find('tr');
		
		var tr      = $(elm).closest('tr');
		var index	= $(tr).index();

		if(index > 0 || rows.length > 1){
			$(tr).remove();
		}
		else {
			$(tr).find('td').eq(0).find('input[type=hidden]').val("");
			$(tr).find('td').eq(0).find('input[type=text]').val("");
			$(tr).find('td').eq(1).find('input[type=text]').val("");
			$(tr).find('td').eq(2).find('input[type=text]').val("");
		}
	}
	
</script> 