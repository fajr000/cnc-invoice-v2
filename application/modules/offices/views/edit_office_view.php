<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .table > tbody > tr > td {
    
  }

  .btn-file {
      position: relative;
      overflow: hidden;
  }
  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      font-size: 100px;
      text-align: right;
      filter: alpha(opacity=0);
      opacity: 0;
      outline: none;
      background: white;
      cursor: inherit;
      display: block;
  }
  .item {
    float: left;
    padding: 5px;
    border: 1px solid #ccc;
    margin: 0px 3px;
    text-align: center;
  }
  .bold-text {
    font-weight: bold;
  }
  .suggestion {
    font-size: 12px;
  }
  .radio-label {
    display: inline-block;
    vertical-align: bottom;
    margin-right: 3%;
    font-weight: normal;
    line-height: 15px;
  }
  .radio-input {
      display: inline-block;
      vertical-align: bottom;
      margin-left: 0px;
  }
  .label-form {
      vertical-align: middle !important; 
  }
  .right-text {
      text-align: right !important; 
  }
  .center-text {
      text-align: center !important; 
  }
</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
    <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title">Data Office</h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
              <div class="col-md-12" id="form_user">
                <p>
                  <form autocomplete="off" name="form" id="form" method="post" action="offices/save" target="_self" enctype="multipart/form-data">
                  <table class="table table-bordered">
                    <tbody>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Nama</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="hidden" class="form-control" name="id" id="id" value="<?php echo isset($item) ? $item->id : '';?>">
                          <input type="text" class="form-control name" name="name" id="name" value="<?php echo isset($item) ? $item->name : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Alamat</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="address" id="address" value="<?php echo isset($item) ? $item->address : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Kota</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="city" id="city" value="<?php echo isset($item) ? $item->city : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Telp</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="phone" id="phone" value="<?php echo isset($item) ? $item->phone : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Website</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="website" id="website" value="<?php echo isset($item) ? $item->website : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Email</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                          <input type="text" class="form-control" name="email" id="email" value="<?php echo isset($item) ? $item->email : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Rekening</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-12 col-md-12 col-sm-12" style="padding-left: 0px;">
                            <div class="row">
                              <div id="list_items" class="col-md-12 col-sm-12 col-xs-12 table-responsive" style="border: 0px;">
                                <table class="table table-bordered table-condensed">
                                  <thead>
                                    <tr>
                                      <th style="width: 20%;">Nama Bank</th>
                                      <th style="width: 20%;">No. Rekening</th>
                                      <th style="width: 20%;">Nama Pemilik </th>
                                      <th style="width: 8%;"></th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <?php
                                    if(isset($account_html) && !empty($account_html)) {
                                      echo $account_html;
                                    } 
                                    else {
                                    ?>
                                    <tr>
                                      <td align="center">
                                        <input type="hidden" class="form-control input-sm" name="id_bank[]" value="">
                                        <input type="text" class="form-control input-sm nama_bank" name="nama_bank[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm" name="norek[]" value="">
                                      </td>
                                      <td align="center">
                                        <input type="text" class="form-control input-sm" name="nama_pemilik[]" value="">
                                      </td>
                                      <td align="center">
                                        <div>
                                          <button type="button" class="btn btn-sm btn-primary" onclick="addItem(this)"><i class="fa fa-plus"></i></button>
                                          <button type="button" class="btn btn-sm btn-danger" onclick="subItem(this)"><i class="fa fa-minus"></i></button>
                                        </div>
                                      </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                  </tbody>
                                </table>
                              </div>
                            </div>
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Stok Reminder</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px;">
                          <input type="text" class="form-control right-text " name="stock_alert" id="stock_alert" value="<?php echo isset($item) ? $settings["MIN_STOCK"] : '';?>">
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Komisi Sales (%)</span></td>
                        <td class="col-sm-10">
                          <div class="col-lg-1 col-md-1 col-sm-1" style="padding-left: 0px;">
                          <input type="text" class="form-control right-text " name="sales_compensation_percentage" id="sales_compensation_percentage" value="<?php echo isset($item) ? $settings["SALES_COMPENSATION_PERCENTAGE"] : '2';?>" onkeypress="return isDecimal(event, this)"> 
                          </div>
                        </td>
                      </tr>
                      <tr>
                        <td class="col-sm-2 label-form"><span>Pengesahan Invoice</span></td>
                        <td class="col-sm-10">
                          <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px;">
                            <input type="text" class="form-control left-text " name="invoice_person_name" id="invoice_person_name" value="<?php echo isset($item) ? $settings["INVOICE_PERSON_NAME"] : '';?>" placeholder="Nama Admin Keuangan">
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3" style="padding-left: 0px;">
                            <input type="text" class="form-control left-text " name="invoice_person_position" id="invoice_person_position" value="<?php echo isset($item) ? $settings["INVOICE_PERSON_POSITION"] : '';?>" placeholder="Jabatan Admin Keuangan">
                            </div>
                          </div>
                        </td>
                      </tr>
                      <?php

                      $checked = "";
                      $show_status = "";
                      if(is_admin()){
                        $checked = ((!isset($item)) ? "checked=checked" : ((isset($item) && $item->active == 1) ? "checked=checked" : ""));
                      } 
                      else{
                        $checked = (isset($item) && $item->active == 1) ? "checked=checked" : "";
                        $show_status = "style='display: none;'";
                      }


                      echo '  
                      <tr '.$show_status.'>
                        <td class="col-sm-2 label-form"><span>Status</span></td>
                        <td class="col-sm-10">
                          <div class="checkbox">
                            <label><input type="checkbox" name="active" id="active" value="1" '.$checked.'>Aktif</label>
                          </div>
                        </td>
                      </tr>';
                      
                      ?>
                      <tr>
                        <td></td>
                        <td>
                    <button id="btn_back" class="btn btn-default btn btn-sm" name="btn_back"><span>Batal</span></button>&nbsp;
                    <button id="btn_submit" class="btn btn-primary btn btn-sm" name="btn_submit"><span>Simpan</span></button>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                  </form>
                </p>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

<div class="modal fade" id="modal-default"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->