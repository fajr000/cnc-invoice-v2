<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privileges extends Admin_Controller
{
  function __construct()
  {
    parent::__construct();
    
    $this->load->library(array('form_validation', 'pagination'));
    $this->form_validation->CI =& $this;

    $this->load->model('privileges_model', 'privileges');
    
    $this->data['page_title'] = 'Privileges | Pioner CNC Indonesia';
    $this->data['page_header'] = 'Privileges';
    $this->data['page_subheader'] = 'Hak Akses';

    $subclass_name = $this->router->fetch_class();
    set_parent_url($subclass_name);
    $this->data['allowed'] = build_access($subclass_name);
  }

  public function index()
  {
    $this->get_lists();
  }

  public function get_lists()
  {
    // page script js
    $this->data['before_body'] = $this->load->view('script', '', true);
    
    $this->render('list');
  }

  public function get_privileges_list()
  {
    // populate data
    $params = array();

    $all_items = $this->privileges->get_all_items($params);

    $items = array();
    foreach($all_items as $key => $row) {
      if($row->name = "administrator" && !is_admin()) {

      }else{
        $items[] = $row;
      }
    }

    echo json_encode(array("data"=>$items));
  }

  public function add()
  {
    $date = new DateTime();

    $params = array();
    
    $item = $this->privileges->get_detail_item(0);

    $menus = $this->privileges->get_menus();
    $menus = ordered_menu($menus);

    $this->data['new'] = true;
    $this->data['item'] = $item;
    $this->data['menus'] = $menus;

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit'); 
  }

  public function edit($id_item)
  {
    $item = $this->privileges->get_detail_item($id_item);

    $prev_temp = $this->privileges->get_privileges($id_item);
    $prev      = array();

    foreach ($prev_temp as $key => $value) {
       $prev[$value['menu_id']] = array(
        "create" => $value['create'],
        "read" => $value['read'],
        "update" => $value['update'],
        "delete" => $value['delete'],
       );
     } 

    $menus = $this->privileges->get_menus();

    $menus = ordered_menu($menus);
    $order = array_column($menus, 'sort');

    array_multisort($order, SORT_ASC, $menus);

    foreach ($menus as $key => $parent) { 
      if(isset($prev[$parent['id']])){
        $menus[$key]['create'] = $prev[$parent['id']]['create'];
        $menus[$key]['read'] = $prev[$parent['id']]['read'];
        $menus[$key]['update'] = $prev[$parent['id']]['update'];
        $menus[$key]['delete'] = $prev[$parent['id']]['delete'];
      }

      if(isset($parent['subs']) && count($parent['subs']) > 0){
        foreach ($parent['subs'] as $key2 => $child) { 
          if(isset($prev[$child['id']])){
            $menus[$key]['subs'][$key2]['create'] = $prev[$child['id']]['create'];
            $menus[$key]['subs'][$key2]['read'] = $prev[$child['id']]['read'];
            $menus[$key]['subs'][$key2]['update'] = $prev[$child['id']]['update'];
            $menus[$key]['subs'][$key2]['delete'] = $prev[$child['id']]['delete'];
          }
          
        }
      }
    }

    // echo "<pre>";print_r($prev);
    // die();

    $this->data['new'] = false;
    $this->data['item'] = $item;
    $this->data['menus'] = $menus;

    $this->data['before_body'] = $this->load->view('script', '', true);

    $this->render('edit'); 
  }


  public function delete($id_item)
  {
    $item =  $this->privileges->get_detail_item($id_item);
    
    $result = $this->privileges->delete_item($id_item);

    if($result > 0){
      $notif['status'] = true;
      $notif['title'] = 'Info';
      $notif['msg'] = 'Berhasil menghapus data.';

      $this->session->set_flashdata('notif', $notif);
    }
    else {
      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = 'Gagal menghapus data!';

      $this->session->set_flashdata('notif', $notif);
    }

    redirect('privileges');
  }

  public function save() 
  {
    $result = 0;
    $notif  = array();
    $date   = new DateTime();
    $time   = $date->format('Y-m-d H:i:s');

    $this->form_validation->set_rules('description', 'Keterangan', 'trim|required');

    $id         = $this->input->post('id');
    $description= $this->input->post('description');
    $action     = $this->input->post('action');

    $menus = $this->privileges->get_menus();
    $menus = ordered_menu($menus);

    $menus_all = array();
    foreach ($menus as $parent) { 
      $menus_all[] = array(
        "group_id" => 0,
        "menu_id" => $parent['id'],
        "create" => 0,
        "read" => 0,
        "update" => 0,
        "delete" => 0,
      );

      if(isset($parent['subs']) && count($parent['subs']) > 0){
        foreach ($parent['subs'] as $child) { 
          $menus_all[] = array(
            "group_id" => 0,
            "menu_id" => $child['id'],
            "create" => 0,
            "read" => 0,
            "update" => 0,
            "delete" => 0,
          );
        }
      }
    }

    foreach ($menus_all as $key1 => $menu) {
      if(isset($action) && count($action)>0){
        foreach ($action as $key2 => $value) {
          if($menu['menu_id'] == $key2){
            foreach ($value as $key3 => $value2) {
              switch ($key3) {
                case 0:
                  $menus_all[$key1]['read'] = 1;
                  break;
                case 1:
                  $menus_all[$key1]['create'] = 1;
                  break;
                case 2:
                  $menus_all[$key1]['update'] = 1;
                  break;
                case 3:
                  $menus_all[$key1]['delete'] = 1;
                  break;
                default:
                  break;
              }
            }
          }
        }
      }
    }
    
    $data = array(
      'name'=> strtolower($description),
      'description'=> $description,
    );   

    if($this->form_validation->run() === TRUE)
    {    
      if(empty($id)) {
        $result = $this->privileges->insert_item($data);

        if($result > 0){
          $id = $result;

          foreach ($menus_all as $key => $value) {
            $menus_all[$key]['group_id'] = $id;
          }

          $this->privileges->insert_privileges($id, $menus_all);
        }
      }
      else {
        $item = $this->privileges->get_detail_item($id);
        $result = $this->privileges->update_item($id, $data);

        foreach ($menus_all as $key => $value) {
          $menus_all[$key]['group_id'] = $id;
        }

        $this->privileges->insert_privileges($id, $menus_all);
      }

      $notif['msg'] = "";
      if($result > 0){
        $notif['status'] = true;
        $notif['title'] = 'Info';
        $notif['msg'] .= 'Data berhasil disimpan.';
      }
      else {
        $notif['status'] = false;
        $notif['title'] = 'Warning';
        $notif['msg'] .= 'Data gagal disimpan!';
      }

      $this->session->set_flashdata('notif', $notif);
      redirect('privileges');
    }
    else 
    {
      $menus = $this->privileges->get_menus();
      $menus = ordered_menu($menus);

      $error =  str_replace("<p>", "", validation_errors());
      $error =  str_replace("</p>", "<br/>", $error);

      $notif['status'] = false;
      $notif['title'] = 'Warning';
      $notif['msg'] = '<strong>Data gagal disimpan!</strong>' . '<br/>' . $error;

      $this->data["notif"] = $notif;

      if(isset($id) && !empty($id)) {
        $data['id'] = $id;
        $this->data['new'] = false;
      }
      else {
        $data['id'] = '';
        $this->data['new'] = true;
      }

      $this->data["item"] = (object) $data;
      $this->data['menus'] = $menus;

      $this->data['before_body'] = $this->load->view('script', '', true);
      $this->render('edit'); 
    }
  }
}