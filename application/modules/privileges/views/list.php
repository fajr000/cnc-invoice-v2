<!-- Content Header (Page header) -->
<style type="text/css">  
  .no{
    text-align: center;
    width: 2%;
  }

  .dt-no{
    text-align: center;
  }
  .name{
    text-align: center;
    width: 83%;
  }
  .dt-name{
    text-align: left;
  }
  .description{
    text-align: center;
    width: 35%;
  }
  .dt-description{
    text-align: left;
  }
  .tools{
    text-align: center;
    width: 15%;
  }   
  .dt-tools{
    text-align: center;
  } 

</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <?php if($this->session->flashdata('notif')){ $notif = $this->session->flashdata('notif');?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'warning';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title"><?php echo $page_subheader;?></h3>
        </div>

        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12">
              <?php
              if($allowed['create']){
              ?>
              <div class="row">
              <div class="col-md-12 pull-left">
                <a href="privileges/add/" class="btn-primary btn-sm" target="_self"><span>TAMBAH BARU</span></a>
              </div>
              </div>
              <?php
              }
              ?>
              <br/> 
              <div class="row">
              <div class="col-sm-12">
                  <table id="list_data" style="width: 100%;" class="table table-condensed table-bordered table-hover dtr-inline table-sm">
                    <thead>
                      <tr>
                        <th class="no">&nbsp;</th>
                        <th class="name">Nama</th>
                        <th class="tools">&nbsp;</th>
                      </tr>
                    </thead>
                  </table>
              </div>
              </div>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->


<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Konfirmasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <input type="hidden" name="deletedId" id="deletedId" value="">
        <p>Anda yakin akan menghapus data ini?</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="button" id="submitDelete" class="btn btn-primary">Ya</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->