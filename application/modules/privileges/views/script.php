<script>
	$(function () {

		$('#btn_submit').on('click', function(e){
			e.preventDefault();

			var msg = new Array();

			if(($("#description").val()).length == 0){
				msg.push("Kolom <strong>Nama</strong> tidak boleh kosong!");
			}

			if(msg.length > 0) {

				$('.modal-body').html(msg.join('<br>'));
				$('#modal-default').modal('show');

				return false;
			}

			$('#form').submit();
		});

		$('#btn_back').on('click', function(e){
			e.preventDefault();

			window.location.href = "privileges/get_lists/";
		});

		$("#submitDelete").on("click", function(){
        	var deletedId = $("#deletedId").val();

        	window.location.href = "privileges/delete/"+deletedId;
        });

		$('.grant_all').on('click', function(e){
			var is_checked = $('input[name="grant_all"]:checked').length > 0;

			if(is_checked){
				$('.previledge').prop('checked', true);
			}
			else{
				$('.previledge').prop('checked', false);
			}
		});

		var previledge_checkbox 		 = $('.previledge:checkbox').length;
		var previledge_checkbox_checked  = $('.previledge:checkbox:checked').length;

		if(previledge_checkbox_checked == previledge_checkbox){
			$('.grant_all').prop('checked', true);
		}

		$('.previledge').on('click', function(e){
			var is_checked = $(this).is(':checked');

			if(is_checked){
				var previledge_checkbox 		 = $('.previledge:checkbox').length;
				var previledge_checkbox_checked  = $('.previledge:checkbox:checked').length;

				if(previledge_checkbox_checked == previledge_checkbox){
					$('.grant_all').prop('checked', true);
				}
			}
			else{
				$('.grant_all').prop('checked', false);
			}
		});

		var table = $('#list_data').DataTable( {
	        "ajax": "privileges/get_privileges_list",
	        "columns": [
	            { data: "id", className: "no" },
	            { data: "description", className: "nama highlight",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Edit' title='Edit' href='privileges/edit/"+oData.id+"'>"+oData.description+"</a>");
			        }
	            },
	        	{ data: "id", className: "tools",
	            	fnCreatedCell: function (nTd, sData, oData, iRow, iCol) {
			            $(nTd).html("<a target='_self' alt='Hapus' title='Hapus' onclick='confirmDelete(" + oData.id + ")' class='btn-sm btn-danger'><i class='fa fa-trash'></i></a>");
			        } 
	        	}
	        ],
	        'order': [[1, 'asc']]
	    } );

	    table.on( 'order.dt search.dt', function () {
	        table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	            cell.innerHTML = (i+1);  
	        } );
	    } ).draw();

	})

	function confirmDelete(id){
    	$("#deletedId").val(id);
    	$(".modal").modal('show');
    }
</script> 