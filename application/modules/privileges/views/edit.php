<!-- Content Header (Page header) -->
<style type="text/css">
  .table > thead > tr > th {
    text-align: center;
    vertical-align: middle;
  }

  .no{
    text-align: center;
    width: 2%;
  }
  .nama{
    vertical-align: middle;
    text-align: left;
    width: 86%; 
  } 
  .tools{
    text-align: center;
    width: 12%;
  }

  .table > tbody > tr > td {
    
  }

  .btn-file {
      position: relative;
      overflow: hidden;
  }
  .btn-file input[type=file] {
      position: absolute;
      top: 0;
      right: 0;
      min-width: 100%;
      min-height: 100%;
      font-size: 100px;
      text-align: right;
      filter: alpha(opacity=0);
      opacity: 0;
      outline: none;
      background: white;
      cursor: inherit;
      display: block;
  }
  .item {
    float: left;
    padding: 5px;
    border: 1px solid #ccc;
    margin: 0px 3px;
    text-align: center;
  }

  .radio-label {
    display: inline-block;
    vertical-align: bottom;
    margin-right: 3%;
    font-weight: normal;
    line-height: 18px;
  }

  .radio-input {
      display: inline-block;
      vertical-align: bottom;
      margin-left: 0px;
  }
</style>

<section class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-12">
        <h1><?php echo $page_header;?></h1>
      </div>
    </div>
  </div><!-- /.container-fluid -->
</section>


<!-- Main content -->
<section class="content">
  <?php if(isset($notif)){ ?>
  <div class="callout callout-<?php echo ($notif['status'] == true) ? 'info' : 'danger';?>">
    <h4><?php echo $notif["title"];?></h4>
    <?php echo $notif["msg"];?>
  </div>
  <?php }?>
  <div class="container-fluid">
  <div class="row">
    <div class="col-md-12 col-sm-12">
      <div class="card card-default card-outline">
        <!-- .box-header -->
        <div class="card-header">
          <h3 class="card-title">Detil Hak Akses</h3>
        </div>
        <!-- /.box-header -->
        <div class="card-body">
          <div class="row">
            <div class="col-md-12" id="form_master">
              <p>
                <form autocomplete="off" name="form" id="form" method="post" action="privileges/save" target="_self" enctype="multipart/form-data">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td class="col-md-2"><span>Nama</span></td>
                      <td class="col-md-10">
                        <div class="col-md-6 col-sm-11 col-xs-12" style="padding-left: 0px;">
                          <input type="hidden" class="form-control input-sm" name="id" id="id" value="<?php echo isset($item) ? $item->id : '';?>">
                          <input type="text" class="form-control input-sm" name="description" id="description" value="<?php echo isset($item) ? $item->description : '';?>">
                        </div>
                        </td>
                    </tr>
                    <tr>
                      <td class="col-md-2"><span>Privileges</span></td>
                      <td class="col-md-10">
                        <div class="col-md-6 col-sm-11 col-xs-12" style="padding-left: 0px;">
                          <div class="checkbox">
                            <label>
                            <input type="checkbox" class="grant_all" name="grant_all" value="1" onChange=""/>ALL PRIVILEGES                              
                            </label>
                          </div>
                         <hr/>
                         <table class="table table-bordered table-condensed">
                           <thead>
                             <tr>
                               <th rowspan="2">Nama Menu</th>
                               <th colspan="4">Hak Akses</th>
                             </tr>
                             <tr>
                               <th>Read</th>
                               <th>Add</th>
                               <th>Edit</th>
                               <th>Delete</th>
                             </tr>
                           </thead>
                           <tbody>
                            <?php
                              foreach ($menus as $parent) {
                                $checked_create = (isset($parent['create']) && $parent['create'] == 1) ? "checked" : "";
                                $checked_read   = (isset($parent['create']) && $parent['read'] == 1) ? "checked" : "";
                                $checked_update = (isset($parent['create']) && $parent['update'] == 1) ? "checked" : "";
                                $checked_delete = (isset($parent['create']) && $parent['delete'] == 1) ? "checked" : "";

                                $can_read = (!$parent['can_read']) ? 'style="display:none;"' : '';
                                $can_add = (!$parent['can_add']) ? 'style="display:none;"' : '';
                                $can_update = (!$parent['can_update']) ? 'style="display:none;"' : '';
                                $can_delete = (!$parent['can_delete']) ? 'style="display:none;"' : '';

                                $read = '<input '.$can_read.' type="checkbox" class="radio-input previledge" name="action['.$parent['id'].'][0]" value="1" onChange="" '.$checked_read.'/>';
                                $create = '<input '.$can_add.' type="checkbox" class="radio-input previledge" name="action['.$parent['id'].'][1]" value="1" onChange="" '.$checked_create.'/>';
                                $update = '<input '.$can_update.' type="checkbox" class="radio-input previledge" name="action['.$parent['id'].'][2]" value="1" onChange="" '.$checked_update.'/>';
                                $delete = '<input '.$can_delete.' type="checkbox" class="radio-input previledge" name="action['.$parent['id'].'][3]" value="1" onChange="" '.$checked_delete.'/>';
                                
                                echo '
                                <tr>
                                  <td>'.$parent['name'].'</td>
                                  <td align="center">'.$read.'</td>
                                  <td align="center">'.$create.'</td>
                                  <td align="center">'.$update.'</td>
                                  <td align="center">'.$delete.'</td>
                                <tr>
                                ';

                                if(isset($parent['subs']) && count($parent['subs']) > 0){
                                  foreach ($parent['subs'] as $child) {
                                    $checked_create = (isset($child['create']) && $child['create'] == 1) ? "checked" : "";
                                    $checked_read   = (isset($child['create']) && $child['read'] == 1) ? "checked" : "";
                                    $checked_update = (isset($child['create']) && $child['update'] == 1) ? "checked" : "";
                                    $checked_delete = (isset($child['create']) && $child['delete'] == 1) ? "checked" : "";

                                    $can_read = (!$child['can_read']) ? 'style="display:none;"' : '';
                                    $can_add = (!$child['can_add']) ? 'style="display:none;"' : '';
                                    $can_update = (!$child['can_update']) ? 'style="display:none;"' : '';
                                    $can_delete = (!$child['can_delete']) ? 'style="display:none;"' : '';
                                    echo '
                                    <tr>
                                      <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child['name'].'</td>
                                      <td align="center"><input '.$can_read.' type="checkbox" class="radio-input previledge" name="action['.$child['id'].'][0]" value="1" onChange="" '.$checked_read.'/></td>
                                      <td align="center"><input '.$can_add.' type="checkbox" class="radio-input previledge" name="action['.$child['id'].'][1]" value="1" onChange="" '.$checked_create.'/></td>
                                      <td align="center"><input '.$can_update.' type="checkbox" class="radio-input previledge" name="action['.$child['id'].'][2]" value="1" onChange="" '.$checked_update.'/></td>
                                      <td align="center"><input '.$can_delete.' type="checkbox" class="radio-input previledge" name="action['.$child['id'].'][3]" value="1" onChange="" '.$checked_delete.'/></td>
                                    <tr>
                                    ';
                                  }  
                                }
                              }
                            ?>
                          </tbody>
                         </table>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td></td>
                      <td>
                        <button id="btn_back" class="btn btn-default btn-sm" name="btn_back"><span>Batal</span></button>&nbsp;
                        <button id="btn_submit" class="btn btn-primary btn-sm" name="btn_submit"><span>Simpan</span></button>
                      </td>
                    </tr>
                  </tbody>
                </table>
                </form>
              </p>
            </div>
            <!-- /.col -->
          </div>
          <!-- /.row -->
        </div>
        <!-- /.box body -->
      </div>
      <!-- /.box -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
  </div>
</section>
<!-- /.content -->


<div class="modal fade" id="modal-default"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Peringatan!</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->