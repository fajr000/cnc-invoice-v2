<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Privileges_model extends CI_Model
{
	private $_tableName;


	public function __construct(){
		parent::__construct();
	}

	public function get_all_items($params = null)
	{
		if(isset($params))
			extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(isset($params) && count($params) > 0){
			$filter_str = implode(" AND ", $params);
		}

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;		

		$sql = "
		SELECT * FROM (
			SELECT * 
			FROM groups 
			ORDER BY `id` ASC 
		)qry
		$filter_str 
";

		return $this->db->query($sql)->result();
	}

	public function get_items($start = 0, $offset = 10, $params = null)
	{
		extract($params);

		$filter_arr = array();
		$filter_str = "";

		if(isset($params) && count($params) > 0){
			$filter_str = implode(" AND ", $params);
		}

		$filter_str = (!empty($filter_str)) ? " WHERE " . $filter_str : $filter_str;

		$sql = "
		SELECT * FROM (
			SELECT * 
			FROM groups 
			ORDER BY `id` ASC 
		)qry
		$filter_str 
		LIMIT $start, $offset
";

		return $this->db->query($sql)->result();
	}

	public function get_detail_item($id)
	{
		$sql = "
		SELECT * FROM (
			SELECT * 
			FROM groups 
			ORDER BY `id` ASC
		)qry
		WHERE id = $id  
";

		return $this->db->query($sql)->row();
	}

	public function insert_item($data)
	{
		$this->db->trans_begin();

		$this->db->insert("groups", $data);

		if($this->db->affected_rows() > 0) {
			$id = $this->db->insert_id();

			$this->db->trans_commit();

			return $id;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function get_privileges($group_id)
	{
		$sql = "SELECT * FROM groups_access WHERE group_id = $group_id ORDER BY menu_id";

		return $this->db->query($sql)->result_array();
	}

	public function insert_privileges($group_id, $data)
	{
		$inserted = 0;
		$this->db->trans_begin();

		$this->db->query("DELETE FROM groups_access WHERE group_id = $group_id");

		foreach ($data as $d) {
			$this->db->insert("groups_access", $d);
			if($this->db->affected_rows() > 0) {
				$inserted++;
			}
		}		

		if($inserted > 0 && $inserted == count($data)) {
			$this->db->trans_commit();

			return true;
		}
		else {
			$this->db->trans_rollback();

			return false;
		}
	}

	public function update_item($id, $data)
	{
		$this->db->trans_start();

		$this->db->where("id", $id);
		$this->db->update("groups", $data);

		$this->db->trans_complete();

		if($this->db->trans_status() !== FALSE || $this->db->affected_rows() > 0) {
			$this->db->trans_commit();

			return 1;
		}
		else {
			$this->db->trans_rollback();

			return 0;
		}

	}

	public function delete_item($id)
	{
		$this->db->where("id", $id);
		$this->db->delete("groups");

		$this->db->query("DELETE FROM groups_access WHERE group_id = $id");

		return 1;
	}

	public function get_menus(){
		$sql = "
		SELECT mm.* 
		FROM `menu` mm 
		WHERE mm.`active` = 1 AND publish = 1; 
		";

		return $this->db->query($sql)->result_array();
	}
}
